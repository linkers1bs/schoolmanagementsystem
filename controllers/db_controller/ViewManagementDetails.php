<?php 

class ViewManagementDetails extends DB
{
	
	public function subject_details(){
		$sql = "SELECT DISTINCT * FROM subjects ORDER BY subjectcode ASC";
        return $this->has_data_db($sql);   
	}

	public function rooms_details(){
		$sql = "SELECT DISTINCT * FROM room ORDER BY roomcode ASC";
		return $this->has_data_db($sql);   
	}
	public function instructor_details(){
		 $sql = "SELECT branch.name as branchname, grouptype.name as groupname, EXTRACT(YEAR from AGE(dateofbirth)) as age,* FROM userinfo
              INNER JOIN branch USING(branchid)
              INNER JOIN usergroup USING (username)
              INNER join grouptype USING (grouptypeid) WHERE grouptype.name = 'Instructor' ORDER BY lastname";
		 return $this->has_data_db($sql);   
	}

	public function course_details(){
		$sql = "SELECT DISTINCT * FROM course ORDER BY coursename ASC";
		return $this->has_data_db($sql);
	}

	public function curriculum_details($curcode = false, $semnum =false){
		if($curcode){
			
            if (!$semnum) {
            	$sql = "SELECT * FROM curriculum 
              	INNER JOIN course USING(coursecode) WHERE curriculum.curcode = '$curcode' ";
            } else {
            	$sql = "SELECT * FROM curriculum 
              	INNER JOIN course USING(coursecode)
              	INNER JOIN curriculumdetailed USING(curcode)
				INNER JOIN subjects USING (subjectcode)
				WHERE curriculumdetailed.semnum = $semnum AND curriculum.curcode = '$curcode'
              	  ";
            }

		} else {
			$sql = "SELECT * FROM curriculum 
              	INNER JOIN course USING(coursecode) ORDER BY curcode ASC";
		}
		
        return $this->has_data_db($sql);
	}

	public function get_block($sem,$sy,$studenttypeid,$branchid){
		$sql = "SELECT DISTINCT blockname FROM schedules
		 WHERE semid = $sem AND syid = '$sy' AND studenttypeid = $studenttypeid AND branchid = $branchid ORDER BY blockname ASC";
		return $this->has_data_db($sql);
	}

	public function get_block_info_table($block,$sem,$sy,$branchid,$studenttypeid){
		$sql = "SELECT DISTINCT schedules.username as name,* FROM schedules
			LEFT JOIN userinfo USING (username)
			LEFT JOIN classschedules USING(scheduleid)
			LEFT JOIN room USING (roomcode)
			WHERE schedules.blockname = '$block'
			AND schedules.semid = $sem 
			AND schedules.syid = $sy
			AND schedules.studenttypeid = $studenttypeid
			AND schedules.branchid = $branchid 
			ORDER BY roomcode ASC";
		return $this->has_data_db($sql);
	}

	public function sectioning_details(){
		$sql = "SELECT initcap(CONCAT(lastname, ' ', firstname, ' ', middlename )) as fullname, username, c.name, c.task , c.classsectionid FROM classsection c 
			INNER JOIN userinfo USING (username)
				ORDER BY c.name ASC";
        return $this->has_data_db($sql);   
	}

	public function adviser_details(){
		$sql = "SELECT initcap(CONCAT(lastname, ' ', firstname, ' ', middlename )) as fullname, username FROM userinfo 
				WHERE branchid = ".$_SESSION["branch_id"]." ";
        return $this->has_data_db($sql);   
	}



	
}


?>