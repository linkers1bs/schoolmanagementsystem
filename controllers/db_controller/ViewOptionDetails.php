<?php

/**
 * 
 */
class ViewOptionDetails extends DB
{
	// [type] = name of the table
	public function optionDetails($type){
		if ($type == "semester") {
			$sql = "SELECT DISTINCT * FROM semester ORDER BY sem ASC";
		} elseif ($type == "schoolyear" ) {
			$sql = "SELECT DISTINCT * FROM schoolyear ORDER BY sy DESC";
		} elseif ($type == "branch") {
			$sql = "SELECT DISTINCT * FROM branch ORDER BY name ASC";
		} elseif ($type == "studenttype") {
			$sql = "SELECT DISTINCT * FROM studenttype ORDER BY name ASC";
		} elseif($type == "scholarship"){
			$sql = "SELECT DISTINCT * FROM scholarship ORDER BY name ASC";
		} elseif ($type == "curriculum") {
			$sql = "SELECT  * FROM course 
                    INNER JOIN curriculum ON curriculum.coursecode = course.coursecode ORDER BY coursename ASC";
		} elseif ($type == "studentinfo") {
			$sql = "SELECT studentid, lastname, firstname,middlename FROM studentinfo ORDER BY lastname ASC";
		} elseif ($type == "course") {
			$sql = "SELECT * FROM course ORDER BY coursename ASC";
		} elseif ($type == "subject") {
			$sql = "SELECT subjectcode,* FROM subjects ORDER BY subjectcode ASC";
		} elseif ($type == "instructor") {
			$sql = "SELECT * FROM userinfo
		              LEFT JOIN usergroup USING(username)
		              LEFT JOIN grouptype USING(grouptypeid)
		              WHERE grouptype.name = 'Instructor'
		             ORDER BY username ASC";
		} elseif ($type == "room") {
			$sql = "SELECT * FROM room ORDER BY roomcode ASC";
		} elseif ($type == "section") {
			$sql = "SELECT * FROM classsection ORDER BY name ASC";
		}

		return $this->has_data_db($sql);
	}


}
?>