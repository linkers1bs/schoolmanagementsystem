<?php 

class ViewInstructorDetails extends DB {

    public function get_block_subject($sem,$sy,$studenttypeid,$branchid){
		$sql = "SELECT * FROM schedules
		 WHERE semid = $sem AND syid = '$sy' AND studenttypeid = $studenttypeid AND branchid = $branchid ORDER BY blockname ASC";
		return $this->has_data_db($sql);
    }
    
    public function get_details_classlist($scheduleid){
        $sql = "SELECT CONCAT(userinfo.lastname, ' ', userinfo.firstname,' ', userinfo.middlename ) AS instructorname, studentgrades.remarks AS gremarks, * FROM schedules 
            INNER JOIN userinfo USING (username)
            INNER JOIN subjects USING (subjectcode)
            INNER JOIN studentgrades USING (scheduleid)
            INNER JOIN classschedules USING (scheduleid)
            INNER JOIN studentinfo USING (studentid)
            WHERE schedules.scheduleid = $scheduleid ";
        return $this->has_data_db($sql);
    }

    public function attendace_details($blockname){
        $sql = "SELECT *, studentgrades.remarks AS studentremarks FROM studentgrades 
                INNER JOIN schedules USING (scheduleid)
                INNER JOIN studentinfo USING(studentid) 
                WHERE schedules.scheduleid = $blockname ";
        return $this->has_data_db($sql);
    }
}

?>