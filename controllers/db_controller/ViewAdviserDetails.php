<?php

class ViewAdviserDetails extends DB
{
	

	public function get_curriculum($curcode = false, $semnum =false){
		$sql = "SELECT * FROM curriculum 
      	INNER JOIN course USING(coursecode)
      	INNER JOIN curriculumdetailed USING(curcode)
		INNER JOIN subjects USING (subjectcode)";

		if (!$semnum) {
			$sql .= "WHERE curriculum.curcode = '$curcode' ";
		} else {
			$sql .= "WHERE curriculumdetailed.semnum = $semnum AND curriculum.curcode = '$curcode'";
		}
        return $this->has_data_db($sql);
	}

	public function get_applied($curcode){
		$sql = "SELECT COUNT(curriculumdetailed.subjecttype) as applied FROM curriculum 
		      	INNER JOIN course USING(coursecode)
		      	LEFT JOIN curriculumdetailed USING(curcode)
				INNER JOIN subjects USING (subjectcode)
				WHERE curriculum.curcode = '$curcode' AND  curriculumdetailed.subjecttype = 'APPLIED'";
		$query = $this->has_data_db($sql);
		if ($query) {
			return $this->getVal('applied', $sql);
		} else {
			return 0;
		}

	} 

	public function get_block_offer($coursecode,$studenttypeid,$semid,$syid,$branchid,$subjectcode = false){
		$sql ="SELECT CONCAT(lastname, ', ',firstname, ' ',middlename ) as fullname, * FROM subjects
				INNER JOIN schedules USING(subjectcode)
				INNER JOIN classschedules USING (scheduleid)
				INNER JOIN userinfo USING (username)
				where coursecode = $coursecode AND studenttypeid = $studenttypeid
				AND semid = $semid AND syid = $syid AND schedules.branchid = $branchid
				";
		if ($subjectcode) {
			$sql .= "AND subjects.subjectcode = '$subjectcode'";
		}
		return $this->has_data_db($sql);
	}

	public function get_my_controlle_subject($coursecode,$studenttypeid,$semid,$syid,$branchid,$subjectcode = false,$studentid = false){
		$sql ="SELECT CONCAT(lastname, ', ',firstname, ' ',middlename ) as fullname, * FROM subjects
				INNER JOIN curriculumdetailed using(subjectcode)
				INNER JOIN schedules USING(subjectcode)
				INNER JOIN classschedules USING (scheduleid)
				INNER JOIN userinfo USING (username)
				INNER JOIN  studentgrades USING(scheduleid)
				where studenttypeid = $studenttypeid
				AND semid = $semid AND syid = $syid AND schedules.branchid = $branchid 
				";
		if (($subjectcode != "false") && $studentid) {
			$sql .= "AND subjects.subjectcode = '$subjectcode' AND studentid = '$studentid' ";
			
		} else {
			$sql .= "AND studentid = '$studentid'";
		}  

		if ($coursecode != "false") {
			$sql .= "AND coursecode = $coursecode";
		}
		return $this->has_data_db($sql);

	}


	public function adviser_sf1_detials_block($block,$sem,$sy,$branchid,$studenttypeid){
		$sql = "SELECT DISTINCT * FROM schedules
			LEFT JOIN userinfo USING (username)
			LEFT JOIN classschedules USING(scheduleid)
			LEFT JOIN room USING (roomcode)
			WHERE schedules.blockname = '$block'
			AND schedules.semid = $sem 
			AND schedules.syid = $sy
			AND schedules.studenttypeid = $studenttypeid
			AND schedules.branchid = $branchid 
			ORDER BY roomcode ASC";
		return $this->has_data_db($sql);
	}

	public function advise_sf1r_get_block($sem,$sy,$studenttypeid,$branchid){
		$sql = "SELECT DISTINCT blockname FROM schedules
		 WHERE semid = $sem AND syid = '$sy' AND studenttypeid = $studenttypeid AND branchid = $branchid ORDER BY blockname ASC";
		return $this->has_data_db($sql);
	}

	public function get_subject_change_schedule($studenttypeid,$semid,$syid,$branchid,$subjectcode = false, $scheduleid = false){
		$sql = "SELECT CONCAT(lastname, ', ',firstname, ' ',middlename ) as fullname, * FROM subjects
				INNER JOIN schedules USING(subjectcode)
				INNER JOIN classschedules USING (scheduleid)
				INNER JOIN userinfo USING (username)
				";
		if ($subjectcode != "false") {
			$sql .= "where studenttypeid = $studenttypeid
				AND semid = $semid AND syid = $syid AND schedules.branchid = $branchid AND subjects.subjectcode = '$subjectcode'";
		} elseif ($scheduleid) {
			$sql .= "INNER JOIN  studentgrades USING(scheduleid)
				where studenttypeid = $studenttypeid
				AND semid = $semid AND syid = $syid AND schedules.branchid = $branchid AND studentgrades.gradeid = $scheduleid ";
		}
		return $this->has_data_db($sql);
	}

	public function section_view_details($semid,$syid,$branchid,$studenttypeid,$sectionid){
		$sql = "SELECT CONCAT(studentinfo.lastname, ', ',studentinfo.firstname, ' ',studentinfo.middlename ) as fullname, * FROM register 	
		INNER JOIN studentinfo USING (studentid)
		WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND studenttypeid = $studenttypeid AND classsectionid = $sectionid";
		return $this->has_data_db($sql);
	}

	public function view_form135_details($semid,$syid,$branchid,$studenttypeid,$gradelvl,$coursecode, $type, $studentid = false){
		if($type == "view_name"){
			$sql = "SELECT * FROM register r 
				INNER JOIN studentinfo USING (studentid)
				INNER JOIN studentcourse using (studentid)
				INNER JOIN curriculum USING (curcode)
				INNER JOIN course  USING (coursecode)
				WHERE r.semid = $semid
				AND r.syid = $syid 
				AND r.branchid = $branchid 
				AND r.studenttypeid = $studenttypeid AND r.gradelevel = $gradelvl AND course.coursecode = $coursecode
				";
		}
		return $this->has_data_db($sql);

	}

	public function view_sf1_details(){
		$sql = "SELECT sc.blockname, sc.semid, sc.syid, sc.branchid, cd.curcode, 
				stc.studentid, sinfo.firstname, sinfo.lastname, sinfo.middlename
				FROM schedules as sc
				INNER JOIN subjects as s USING (subjectcode)
				INNER JOIN curriculumdetailed cd USING (subjectcode)
				LEFT JOIN studentcourse stc USING (curcode)
				INNER JOIN studentinfo sinfo USING (studentid)
				WHERE sc.blockname IN
				(SELECT blockname FROM schedules WHERE semid = 2 AND syid = 10 AND branchid = 0 AND blockname = 'G12_IT1')
				AND studentid IN (SELECT studentid FROM register WHERE semid = 2 AND syid = 10 AND branchid = 0)
				GROUP BY sc.blockname, sc.semid, sc.syid, sc.branchid, cd.curcode ,
				stc.studentid, sinfo.firstname, sinfo.lastname, sinfo.middlename";
	}

	

	public function data(){
		$sql ="SELECT 
			sinfo.studentid, CONCAT(sinfo.firstname,' ',sinfo.lastname,' ',sinfo.middlename ) as fullname, 
			stc.curcode, cur.description
			FROM studentinfo as sinfo
			INNER JOIN studentcourse stc USING (studentid)
			INNER JOIN curriculum cur USING (curcode)
			INNER JOIN course USING (coursecode)
			INNER JOIN curriculumdetailed cd USING (curcode)
			WHERE studentid IN (
				SELECT studentid FROM register 
				WHERE semid = 2 AND syid = 10 AND branchid = 0 AND studentid = '2021-F0009'
				GROUP BY studentid 
			)
			GROUP BY sinfo.studentid, fullname, stc.curcode, cur.description";
	}

	public function schedules(){
		$sql = "SELECT 
				sinfo.studentid, CONCAT(sinfo.firstname,' ',sinfo.lastname,' ',sinfo.middlename ) as fullname, 
				stc.curcode, cur.description, subjects.subjectcode, subjects.description,
				subjects.lab, subjects.lec ,schedules.blockname,  schedules.branchid, schedules.semid,schedules.syid,
				CASE
					WHEN  schedules.branchid = 0 AND schedules.semid = 2 AND schedules.syid = 10 THEN 1
					ELSE 0
				END AS controller
				FROM studentinfo as sinfo
				INNER JOIN studentcourse stc USING (studentid)
				INNER JOIN curriculum cur USING (curcode)
				INNER JOIN course USING (coursecode)
				INNER JOIN curriculumdetailed cd USING (curcode)
				INNER JOIN subjects USING (subjectcode)
				LEFT JOIN schedules USING (subjectcode)
				INNER JOIN classschedules USING (scheduleid)
				WHERE studentid IN (
					SELECT studentid FROM register 
					WHERE semid = 2 AND syid = 10 AND branchid = 0 AND studentid = '2021-F0009'
					GROUP BY studentid 
				) AND schedules.blockname = 'G12_IT1'
				GROUP BY sinfo.studentid, fullname, stc.curcode, cur.description,
				subjects.subjectcode,subjects.description,subjects.lab, subjects.lec,schedules.blockname,
				schedules.branchid, schedules.semid,schedules.syid";
	}

	public function showing_curriculum()
	{
		$sql = "SELECT 
				sinfo.studentid, CONCAT(sinfo.firstname,' ',sinfo.lastname,' ',sinfo.middlename ) as fullname, 
				stc.curcode, cur.description, subjects.subjectcode, subjects.description,
				subjects.lab, subjects.lec, cd.semnum 
				FROM studentinfo as sinfo
				INNER JOIN studentcourse stc USING (studentid)
				INNER JOIN curriculum cur USING (curcode)
				INNER JOIN course USING (coursecode)
				INNER JOIN curriculumdetailed cd USING (curcode)
				LEFT JOIN subjects USING (subjectcode)
				WHERE studentid IN (
					SELECT studentid FROM register 
					WHERE semid = 2 AND syid = 10 AND branchid = 0 AND studentid = '2021-F0009'
					GROUP BY studentid 
				)
				GROUP BY sinfo.studentid, fullname, stc.curcode, cur.description,
				subjects.subjectcode,subjects.description,subjects.lab, subjects.lec, cd.semnum 
				ORDER BY cd.semnum ASC";
	}

	public function testing_data(){
		$sql = "SELECT sg.studentid, sg.first, sg.second,
				CASE 
					WHEN (COALESCE(first, 0) + COALESCE(second, 0)) > 120 THEN (COALESCE(first, 0) + COALESCE(second, 0)) / 2
					ELSE 0
				END
				as average, s.subjectcode,
				CASE
					WHEN s.subjectcode = 'SS1' THEN sg.first
					ELSE 0
				END as SS1
				FROM studentgrades sg
				INNER JOIN schedules s USING(scheduleid);";
				
	}

}
?>