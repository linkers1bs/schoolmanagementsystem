<?php 

class ViewCashierDetails extends DB
{
	
	public function get_name_register($semid,$syid,$branchid,$studenttypeid,$scholarshipid){
     $sql = "SELECT DISTINCT * FROM studentinfo
            LEFT JOIN register USING (studentid) 
            LEFT JOIN studentcourse USING (studentid)
            WHERE register.semid = $semid 
            AND register.syid = $syid 
            AND register.branchid = $branchid 
            AND register.studenttypeid = $studenttypeid
            AND register.scholarshipid = $scholarshipid
            ORDER BY lastname ASC";
    return $this->has_data_db($sql);
  }

  public function get_info_student_account($regnum){
     $sql = "SELECT scholarship.name AS scholarshipname, accounttype.name as accounttypename, * FROM studentinfo
        LEFT JOIN register USING (studentid) 
        LEFT JOIN scholarship USING (scholarshipid)
        LEFT JOIN studentcourse USING (studentid)
        LEFT JOIN curriculum USING (curcode)
        LEFT JOIN course USING (coursecode)
        LEFT JOIN studentaccounts USING (regnum)
        LEFT JOIN accounttype USING (actypeid)
        WHERE register.regnum = $regnum 
        ORDER BY lastname ASC";
    
    return $sql;
  }

  public function get_perbilling($semid,$syid,$branchid,$id,$type){
     $sql = "SELECT scholarship.name AS scholarshipname,  * FROM studentinfo
        LEFT JOIN register USING (studentid) 
        LEFT JOIN schoolyear USING (syid) 
        LEFT JOIN semester USING (semid) 
        LEFT JOIN scholarship USING (scholarshipid)
        LEFT JOIN studentcourse USING (studentid)
        LEFT JOIN curriculum USING (curcode)
        LEFT JOIN course USING (coursecode)
        ";

      if($type == "all"){
          $sql .= "
                  WHERE register.semid = $semid 
                  AND register.syid = $syid 
                  AND register.branchid = $branchid 
                  ORDER BY lastname ASC";
      } elseif ($type == "student") {
        if (is_int($id)) {
          $sql .= "WHERE register.regnum = $id ";
        }else {
          $sql .= "WHERE register.studentid = '$id' AND register.semid = $semid AND register.syid = $syid";
        }
      } elseif ($type == "lvl") {
          $sql .=" WHERE register.semid = $semid 
                    AND register.syid = $syid 
                    AND register.branchid = $branchid 
                    AND register.gradelevel = '$id'";
        # code...
      } elseif ($type == "scholarship") {
          $sql .= " WHERE register.semid = $semid 
                    AND register.syid = $syid 
                    AND register.branchid = $branchid 
                    AND register.scholarshipid = '$id'
                    ORDER BY lastname ASC";
      } elseif ($type =="blockname") {
        $sql = "SELECT scholarship.name AS scholarshipname,  * FROM register 
                INNER JOIN studentinfo USING (studentid)
                LEFT JOIN schoolyear USING (syid) 
                LEFT JOIN semester USING (semid) 
                LEFT JOIN scholarship USING (scholarshipid)
                LEFT JOIN studentcourse USING (studentid)
                LEFT JOIN curriculum USING (curcode)
                LEFT JOIN course USING (coursecode)
                WHERE studentid IN(SELECT studentid FROM studentgrades WHERE scheduleid IN
                (SELECT scheduleid FROM schedules WHERE semid = $semid AND syid = $syid AND branchid = $branchid 
                AND blockname = '$id') 
                GROUP BY studentid) AND register.semid = $semid AND register.syid = $syid AND register.branchid = $branchid ";
      }
      
      return $this->has_data_db($sql);
  }

  public function get_type_block_billing($sem,$sy){
    $sql = "SELECT DISTINCT blockname FROM schedules WHERE semid = $sem AND syid = '$sy' ORDER BY blockname ASC";
    return $this->has_data_db($sql);
  }

  public function get_name_register_new($semid,$syid,$branchid,$studenttypeid,$gradelevel){
     $sql = "SELECT register.regnum, studentinfo.lastname, studentinfo.firstname, studentinfo.middlename FROM studentinfo
                INNER JOIN register USING (studentid) 
                INNER JOIN studentcourse USING (studentid)
                WHERE register.semid = $semid 
                AND register.syid = $syid 
                AND register.branchid = $branchid 
                AND register.studenttypeid = $studenttypeid
                AND register.gradelevel = $gradelevel
                ORDER BY lastname ASC";
    return $this->has_data_db($sql);
  }
  ///NEW QUERY UPDATE PAYMENT HISTORY
  public function studentInfoHeader($data, $type){

      if ($type == "header") {
        $sql = "SELECT 
                  CONCAT(s.lastname, ', ', s.firstname, ' ', s.middlename) as fullname,
                  r.studentid as studentid,
                  course.coursename,
                  (SELECT sem FROM semester WHERE semid = r.semid) as semester,
                  (SELECT sy FROM schoolyear WHERE syid = r.syid) as schoolyear,
                  (SELECT name FROM scholarship WHERE scholarshipid = r.scholarshipid) as scholarship,
                  r.dateregistered as dateregistered
                  FROM studentinfo s
                INNER JOIN register r USING (studentid)
                INNER JOIN studentcourse USING (studentid)
                INNER JOIN curriculum USING (curcode)
                INNER JOIN course USING (coursecode)
                WHERE r.regnum = ".$data['regnum']." ";

      } elseif ($type == "subjectTaken") {
            $sql = "SELECT sc.subjectcode, subjects.description as subject_taken, cs.datestart as datestart, subjects.lec, subjects.lab, subjects.price
              FROM subjects
            INNER JOIN schedules sc USING(subjectcode)
            INNER JOIN classschedules cs USING (scheduleid)
            INNER JOIN userinfo USING (username)
            INNER JOIN  studentgrades USING(scheduleid)
            where studenttypeid = ".$data["studenttypeid"]."
            AND sc.semid = ".$data["semid"]." AND sc.syid = ".$data["syid"]." AND sc.branchid = ".$data["branchid"]." AND studentid = '".$data["studentid"]."'";
      }
      
      return $this->has_data_db($sql);
  }

  public function schollAssessmentPayment($regnum, $type){
   
        if ($type == "assessment") {
          $sql = "SELECT 
            assessment,
            case
              when assessment = 'Miscellaneous fees' THEN (amount / 2)
              else amount
            end as amount,
            discount
            FROM
              (SELECT 
                act.name as assessment,
                sum(CASE 
                  WHEN act.name = 'Miscellaneous fees' THEN (SELECT amount FROM miscellaneous WHERE sta.fees = miscid)
                  WHEN act.name = 'Tuition fees' THEN (SELECT amount FROM tuition WHERE sta.fees = tutionid)
                  WHEN act.name = 'Registration fees' THEN (SELECT amount FROM registrationfee WHERE sta.fees = regfee)
                  WHEN act.name = 'Other fees' THEN (SELECT amount FROM othersfees WHERE sta.fees = othersfeesid)
                  WHEN act.name = 'Previous Balance' THEN (SELECT pb.amount FROM previousbalance pb WHERE sta.fees = pb.pbid  AND pb.regnum = regnum)
                END) as amount,
                sum(discount) as discount
                FROM studentaccounts sta
                INNER JOIN accounttype act USING (actypeid)
                WHERE regnum = $regnum GROUP BY assessment
              ) table1
          ORDER BY assessment";
          } elseif($type == "total"){
            $sql = "SELECT sum(amount) as total
                      FROM studentaccounts
                    INNER JOIN pay using (accountid)
                    WHERE regnum = $regnum";
          } elseif ($type == "history") {
            $sql = "SELECT regnum, orno, amount, createdat
                      FROM studentaccounts
                    INNER JOIN pay using (accountid)
                    WHERE regnum = $regnum ORDER BY createdat asc";
          }
    return $this->has_data_db($sql);
  }



  // public function subjectTaken($){
  //   $sql = "SELECT sc.subjectcode, subjects.description as subject_taken, cs.datestart as datestart, subjects.lec, subjects.lab, subjects.price
  //             FROM subjects
  //           INNER JOIN schedules sc USING(subjectcode)
  //           INNER JOIN classschedules cs USING (scheduleid)
  //           INNER JOIN userinfo USING (username)
  //           INNER JOIN  studentgrades USING(scheduleid)
  //           where studenttypeid = 2
  //           AND sc.semid = 2 AND sc.syid = 10 AND sc.branchid = 0 AND studentid = '2021-F0009'";
  //   return $this->has_data_db($sql);
  // }

  public function collectionRreport(){
          $sql = "SELECT
                    syid,
                    sy,
                     (COALESCE( collectables, 0 ) - COALESCE(discount,0)) as collectables,
                    COALESCE( collected, 0 ) as collected,
                    COALESCE((COALESCE( collectables, 0 ) - COALESCE(collected,0) ), 0 ) as un_collected,
                    COALESCE( previous_balance_collectables, 0 ) as previous_balance_collectables,
                    COALESCE( collected_previous_balance, 0 ) as collected_previous_balance,
                     (COALESCE( previous_balance_collectables, 0 ) - COALESCE(collected_previous_balance,0) )as un_collected_previous_balance,
                     COALESCE( discount, 0 ) as discount
                  FROM
                    (SELECT 
                    sy,
                    syid,
                    sum(CASE 
                      WHEN act.name = 'Miscellaneous fees' THEN (SELECT amount FROM miscellaneous WHERE sta.fees = miscid)
                      WHEN act.name = 'Tuition fees' THEN (SELECT amount FROM tuition WHERE sta.fees = tutionid)
                      WHEN act.name = 'Registration fees' THEN (SELECT amount FROM registrationfee WHERE sta.fees = regfee)
                      WHEN act.name = 'Other fees' THEN (SELECT amount FROM othersfees WHERE sta.fees = othersfeesid)
                     END) as collectables,
                     sum((SELECT sum(amount) as amount FROM pay WHERE accountid = sta.accountid AND act.name != 'Previous Balance')) as collected,
                     sum((SELECT sum(amount) as amount FROM previousbalance WHERE sta.fees = pbid AND regnum = r.regnum )) as previous_balance_collectables,
                     sum((SELECT sum(amount) as amount FROM pay WHERE accountid = sta.accountid AND act.name = 'Previous Balance')) as collected_previous_balance,
                    sum(sta.discount) as discount
                    FROM register r
                    INNER JOIN  schoolyear USING(syid)
                    INNER JOIN studentaccounts sta USING (regnum)
                    INNER JOIN accounttype act USING (actypeid)
                    WHERE r.branchid = '".$_SESSION['branch_id']."' GROUP BY sy,syid) as tbl1
                  ORDER BY sy DESC";
          return $this->has_data_db($sql);
  }


  public function persem($syid){
       $sql = "SELECT
                  syid,
                  sy,
                  semid,
                  sem,
                 (COALESCE( collectables, 0 ) - COALESCE(discount,0)) as collectables,
                  COALESCE( collected, 0 ) as collected,
                  COALESCE((COALESCE( collectables, 0 ) - COALESCE(collected,0) ), 0 ) as un_collected,
                  COALESCE( previous_balance_collectables, 0 ) as previous_balance_collectables,
                  COALESCE( collected_previous_balance, 0 ) as collected_previous_balance,
                 (COALESCE( previous_balance_collectables, 0 ) - COALESCE(collected_previous_balance,0) )as un_collected_previous_balance
              FROM
                (SELECT 
                  sy,
                  syid,
                  sem,
                  semid,
                  sum(CASE 
                      WHEN act.name = 'Miscellaneous fees' THEN (SELECT amount FROM miscellaneous WHERE sta.fees = miscid)
                      WHEN act.name = 'Tuition fees' THEN (SELECT amount FROM tuition WHERE sta.fees = tutionid)
                      WHEN act.name = 'Registration fees' THEN (SELECT amount FROM registrationfee WHERE sta.fees = regfee)
                      WHEN act.name = 'Other fees' THEN (SELECT amount FROM othersfees WHERE sta.fees = othersfeesid)
                   END) as collectables,
                   sum((SELECT sum(amount) as amount FROM pay WHERE accountid = sta.accountid AND act.name != 'Previous Balance')) as collected,
                   sum((SELECT amount FROM previousbalance WHERE sta.fees = pbid AND regnum = r.regnum  )) as previous_balance_collectables,
                   sum((SELECT sum(amount) as amount FROM pay WHERE accountid = sta.accountid AND act.name = 'Previous Balance' AND regnum = r.regnum )) as collected_previous_balance,
                  sum(sta.discount) as discount
                FROM register r
                INNER JOIN  schoolyear USING(syid)
                INNER JOIN  semester USING (semid)
                INNER JOIN studentaccounts sta USING (regnum)
                INNER JOIN accounttype act USING (actypeid)
                WHERE r.branchid = '".$_SESSION['branch_id']."' AND syid = $syid GROUP BY sy,syid,sem,semid) as tbl1
              ORDER BY sem ASC";
       return $this->has_data_db($sql);
  }

  public function permonth($semid, $syid){
        $sql = "
              SELECT
                  ROUND( (COALESCE( collectables, 0 ) - COALESCE(discount,0)) / 5 ,0) as collectables,
                  COALESCE( collected, 0 ) as collected,
                  COALESCE((ROUND( (COALESCE( collectables, 0 ) - COALESCE(discount,0)) / 5 ,0) - COALESCE(collected,0) ), 0 ) as un_collected,
                  ROUND( COALESCE( previous_balance_collectables, 0 ) / 5 ,0) as previous_balance_collectables,
                  COALESCE( collected_previous_balance, 0 ) as collected_previous_balance,
                  (COALESCE( ROUND( COALESCE( previous_balance_collectables, 0 ) / 5 ,0) , 0 ) - COALESCE(collected_previous_balance,0) )as un_collected_previous_balance
                 ,months, discount,syid,semid
              FROM
                (SELECT 
                  (
                  SELECT
                      sum(CASE 
                        WHEN act.name = 'Miscellaneous fees' THEN (SELECT amount FROM miscellaneous WHERE sta.fees = miscid)
                        WHEN act.name = 'Tuition fees' THEN (SELECT amount FROM tuition WHERE sta.fees = tutionid)
                        WHEN act.name = 'Registration fees' THEN (SELECT amount FROM registrationfee WHERE sta.fees = regfee)
                        WHEN act.name = 'Other fees' THEN (SELECT amount FROM othersfees WHERE sta.fees = othersfeesid)
                        END) as collectables
                      FROM register r 
                      INNER JOIN studentaccounts sta USING(regnum)
                      INNER JOIN accounttype act USING(actypeid)
                      WHERE r.semid = $semid AND r.syid = $syid AND r.branchid = '".$_SESSION['branch_id']."'
                      ) as collectables,
                  sum(CASE 
                    WHEN act.name = 'Miscellaneous fees' THEN pay.amount
                    WHEN act.name = 'Tuition fees' THEN pay.amount
                    WHEN act.name = 'Registration fees' THEN pay.amount
                    WHEN act.name = 'Other fees' THEN pay.amount
                    ELSE 0
                  END) as collected,
                  (
                    SELECT
                        SUM(CASE 
                              WHEN act.name = 'Previous Balance' THEN (SELECT amount FROM previousbalance WHERE regnum = r.regnum AND pbid = sta.fees)
                            END) as amount
                      FROM register r 
                      INNER JOIN studentaccounts sta USING(regnum)
                      INNER JOIN accounttype act USING(actypeid)
                      WHERE r.semid = $semid AND r.syid = $syid AND r.branchid = '".$_SESSION['branch_id']."'
                      ) as previous_balance_collectables,
                  sum(CASE WHEN act.name = 'Previous Balance' THEN pay.amount END) as collected_previous_balance,
                  (
                    SELECT
                      SUM(sta.discount) as discount
                      FROM register r 
                      INNER JOIN studentaccounts sta USING(regnum)
                      INNER JOIN accounttype act USING(actypeid)
                      WHERE r.semid = $semid AND r.syid = $syid AND r.branchid = '".$_SESSION['branch_id']."'
                      ) as discount,
                  (SELECT TO_CHAR((createdat ),'MM') AS months FROM pay WHERE sta.accountid = accountid  GROUP BY months) as months,
                  r.syid,
                  r.semid
                FROM register r
                INNER JOIN studentaccounts sta USING (regnum)
                INNER JOIN accounttype act USING (actypeid)
                INNER JOIN pay USING (accountid)
                WHERE r.branchid = '".$_SESSION['branch_id']."' AND r.syid = $syid AND r.semid = $semid GROUP BY months,r.syid, r.semid ) as tbl1
                order by months ASC";
        return $this->has_data_db($sql);
    }

    public function perDay($semid, $syid,$month){
      $sql = "SELECT * 
              FROM
                (SELECT 
                  pay.createdat,
                  TO_CHAR((pay.createdat ),'MM') AS months,
                   sum(pay.amount) as collected,
                  r.semid, r.syid
                FROM studentaccounts sta
                  INNER JOIN register r USING (regnum)
                  INNER JOIN pay USING (accountid)
                  INNER JOIN accounttype act USING (actypeid)
                  WHERE 
                  r.semid = $semid AND r.syid = $syid AND r.branchid = '".$_SESSION['branch_id']."'
                  GROUP BY pay.createdat, r.semid, r.syid ORDER BY months ASC ) as tbl1
              WHERE months = '$month' ORDER BY createdat ASC";
      return $this->has_data_db($sql);
    }

    public function perPerson($semid, $syid,$month,$createdat){
      $sql = "SELECT * 
              FROM
                (SELECT 
                  pay.createdat,
                  TO_CHAR((pay.createdat ),'MM') AS months,
                  act.name as assessment,
                  pay.orno,
                  (SELECT CONCAT(lastname, ', ',firstname,' ', middlename) as full_name FROM studentinfo WHERE r.studentid = studentid) as full_name,
                  pay.amount as collected
                FROM studentaccounts sta
                  INNER JOIN register r USING (regnum)
                  INNER JOIN pay USING (accountid)
                  INNER JOIN accounttype act USING (actypeid)
                  WHERE 
                  r.semid = $semid AND r.syid = $syid AND r.branchid = '".$_SESSION['branch_id']."' AND pay.createdat = '$createdat'
                  ORDER BY months ASC ) as tbl1
                  WHERE months = '$month' ORDER BY createdat ASC";
      return $this->has_data_db($sql);
    }
}
?>