<?php
/**
 * 
 */
class ViewReportDetails extends DB
{
	
	public function enrollementReport(){
		$sql ="SELECT 
				categories,
				sum(grade11) as grade11,
				sum(grade12) as grade12,
				(sum(grade11)  + sum(grade12)) as total,
				num
				FROM(
				SELECT 
					'Registered Students' as categories,
					SUM(CASE WHEN gradelevel = 11 THEN 1 ELSE 0 END) as grade11,
					SUM(CASE WHEN gradelevel = 12 THEN 1 ELSE 0 END) as grade12,
					(SUM(CASE WHEN gradelevel = 11 THEN 1 ELSE 0 END) + SUM(CASE WHEN gradelevel = 12 THEN 1 ELSE 0 END)) total,
					'1' as num
					FROM register
						WHERE branchid = 0 AND semid = 2 AND syid = 10
					GROUP BY gradelevel
				UNION
				(
					SELECT 
					'Registered Students Today' as categories,
					SUM(CASE WHEN gradelevel = 11 THEN 1 ELSE 0 END) as grade11,
					SUM(CASE WHEN gradelevel = 12 THEN 1 ELSE 0 END) as grade12,
					(SUM(CASE WHEN gradelevel = 11 THEN 1 ELSE 0 END) + SUM(CASE WHEN gradelevel = 12 THEN 1 ELSE 0 END)) total,
					'2' as num
					FROM register
						WHERE branchid = 0 AND semid = 2 AND syid = 10 AND dateregistered = '2020-12-07'
					GROUP BY gradelevel
				)
				UNION
				(
					SELECT 
					'Registered Students Last Day' as categories,
					SUM(CASE WHEN gradelevel = 11 THEN 1 ELSE 0 END) as grade11,
					SUM(CASE WHEN gradelevel = 12 THEN 1 ELSE 0 END) as grade12,
					(SUM(CASE WHEN gradelevel = 11 THEN 1 ELSE 0 END) + SUM(CASE WHEN gradelevel = 12 THEN 1 ELSE 0 END)) total,
					'3' as num
					FROM register
						WHERE branchid = 0 AND semid = 2 AND syid = 10  AND dateregistered = '2020-12-03'
					GROUP BY gradelevel
				) ) as tbl1
				GROUP BY categories,num ORDER BY num ASC";
		return $this->has_data_db($sql);
	}

	public function enrollementReportScholarship(){
		$sql = "SELECT 
				scholarshipname,
				grade11,
				grade12,
				(grade11 + grade12) as total
				FROM (
				SELECT 
				scholarshipid,
				name as scholarshipname,
				(SELECT 	
					count(scholarshipid) as grade11
				 	FROM register
					WHERE branchid = 0 AND semid = 2 AND syid = 10 and gradelevel = 11 and scholarshipid = sp.scholarshipid) as grade11,
				(SELECT 	
				count(scholarshipid) as grade11
				FROM register
				WHERE branchid = 0 AND semid = 2 AND syid = 10 and gradelevel = 12 and scholarshipid = sp.scholarshipid) as grade12
				FROM scholarship sp ) as tble1";
		return $this->has_data_db($sql);
	}

	public function enrollementReportCourse(){
		$sql = "SELECT 
				coursecode,
				coursename,
				grade11,
				grade12,
				(grade11 + grade12) as total
				FROM(
					SELECT
						cour.coursecode,
						cour.coursename,
						(select 
						count(studentid) as total
						from register
							inner join studentcourse using (studentid)
							inner join curriculum using(curcode)
						WHERE branchid = 0 AND semid = 2 AND syid = 10 and gradelevel = 11 AND coursecode = cour.coursecode) as grade11,
						(select 
						count(studentid) as total
						from register
							inner join studentcourse using (studentid)
							inner join curriculum using(curcode)
						WHERE branchid = 0 AND semid = 2 AND syid = 10 and gradelevel = 12 AND coursecode = cour.coursecode) as grade12
						FROM course cour ) as tbl1";
		return $this->has_data_db($sql);
	}
}

?>