<?php

class DB 
{
	public $link = "",$host = "", $dbname = "", $user="", $port = "", $password="";

	//connecting to db
	public function __construct() {
		// TESTING DB = ici02
		// MAIN DB = icishs
		$db_info = [
			"host" => $this->host = "localhost",
			"dbname" => $this->dbname = "ici02",
			"user" => $this->user = "postgres",
			"port" => $this->port = "5432",
			"password" => $this->password = "root"
		];
		$this->link  = pg_connect("host=".$db_info["host"]." dbname=".$db_info["dbname"]." user=".$db_info["user"]." port=".$db_info["port"]." password=".$db_info["password"]);
	}	

	//close a connection
	public function close(){
		pg_close($this->link);
		$this->link = "";
		return $link = $this->link;

	}	

	//execute the query
	public function openqry($sql){
		$openqry = pg_query($this->link, $sql);
		if( $openqry === false) {
			$openqry = [
				'alertType' => "Danger",
				'alertmsg' => "Unable to run query!<b>"
			];
		} 
		return $openqry;
	}

	//Get Value from specific column name in query
	public function getVal($val,$sql){
		try {
			$qry = $this->openqry($sql);
            $count = pg_num_rows($qry);
            $retVal = "";
            if($count > 0){
                $colname = pg_fetch_assoc( $qry);
                $retVal = $colname[$val];
            }
			return $retVal;
		}
		catch (exception $e) {
			echo $e->getMessage();
		}
	}

	//Check if the query is not empty
    public function notEmpty($sql){
		$openqry = $this->openqry($sql);
		$retVal = false;
		if ( pg_num_rows($openqry) > 0 ) {
			$retVal = true;
		}
		return $retVal;
	}
	
	//use this for db insert, update, delete
    public function run($sql){
		if (!$this->openqry($sql)) {
			$msg = "Unable to save data." . pg_error();
		}
		return $msg; 
    }

    public  function destroySession() {
		session_unset();
		session_destroy();
		session_write_close();
		session_unset();
		session_destroy();
		session_write_close();
		setcookie(session_name(),'',0,'/');
		session_regenerate_id(true);
	}

	public function has_data_db($sql){
	  	if ($this->notEmpty($sql)) {
	      	if($this->openqry($sql)){
	      		$qry = $this->openqry($sql);
	      	}else {
	      		$qry = false;
	      	}
	        	
	      }else {
	       	$qry = false;
	      }

	      return $qry;
  	}

  	public function get_post($data){
  		echo "<pre>";
  			var_dump($data);
  		echo "</pre>";
  	}

	
}

?>