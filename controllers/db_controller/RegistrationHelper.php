<?php 
class RegistrationHelper extends DB
{

	public function createStudetID($sem,$sy){

		$sql  = "SELECT DISTINCT * FROM semester WHERE semid = $sem ";
		$sem = $this->getVal('sem',$sql);
		$sql = "SELECT DISTINCT * FROM schoolyear WHERE syid = $sy ";
		$sy = $this->getVal('sy',$sql);
		$s = "F";
		$year = substr($sy, 0, 4);
		if ($sem == '1' || $sem == 1) {
			$s = "F";
			$sql = "SELECT
			   max(studentid)
			FROM
			   studentinfo WHERE  studentid LIKE '%$year%' AND  studentid  LIKE '%F%';";
		} 	elseif  ($sem == '2' || $sem == 2) {
			$s = "S";
			$sql = "SELECT
			   max(studentid)
			FROM
			   studentinfo WHERE  studentid LIKE '%$year%' AND  studentid  LIKE '%S%';";
		} else {
			$s = "T";
			$sql = "SELECT
			   max(studentid)
			FROM
			   studentinfo WHERE  studentid LIKE '%$year%' AND  studentid  LIKE '%T%';";
		}
		 $lastid = $this->getVal('max',$sql);
		 $getstring = substr($lastid, 6); 
		
		 $id =  sprintf( '%04d', $getstring + 1 );
	
		 $studentid = $year."-".$s.$id;
		
		return $studentid;

	}

	public function getMisc($register) {

		$sql = "SELECT miscid FROM miscellaneous WHERE syid = $register[1] AND branchid = $register[0] AND s_type = '$register[6]' AND studenttypeid = $register[4] AND semid = $register[2]";			
		$data =  $this->has_data_db($sql);
		$array = [];
		if ($data) {
			while ($r = pg_fetch_assoc($data)){
			  array_push($array, $r["miscid"]);
			}
		}
		return $array;
		// if($this->notEmpty($sql)) {
		// 	return $getmis = ($this->getVal('sum',$sql) / 2);
		// } else {
		// 	return 0;
		// }

	}

	public function getTuition($register) {
	
		$sql = "SELECT tutionid FROM tuition WHERE syid = $register[1] AND branchid = $register[0] AND scholarshipid = $register[3] AND studenttypeid = $register[4] AND semid = $register[2]";			
		$data =  $this->has_data_db($sql);
		$array = [];
		if ($data) {
			while ($r = pg_fetch_assoc($data)){
			  array_push($array, $r["tutionid"]);
			}
		}
		return $array;
		
	}
	public function getRegistration($register) {

		$sql = "SELECT regfee FROM registrationfee WHERE syid = $register[1] AND branchid = $register[0] AND scholarshipid = $register[3] AND studenttypeid = $register[4] AND semid = $register[2]";			
		$data =  $this->has_data_db($sql);
		$array = [];
		if ($data) {
			while ($r = pg_fetch_assoc($data)){
			  array_push($array, $r["regfee"]);
			}
		}
		return $array;
	}

	public function insertStudentAccount($name,$feesid,$regnum) {
		// if($amount == '' || $amount == 0 ) {
		// 	$fees = 0;
		// } else {
		// 	$fees = $amount;
		// }
		$boole = false;
		$sql = "SELECT * FROM accounttype WHERE name = '$name' ";
		$getID = $this->getVal('actypeid',$sql);

		$sql = "INSERT INTO studentaccounts (regnum,fees,actypeid) VALUES ($regnum,$feesid,$getID)";
		if($this->openqry($sql)) {
			$boole = true;
		} 

		return $boole;
			
	}
		

	public function getRegisterInfo($semid,$syid,$branchid,$id,$type){
		$sql = "SELECT DISTINCT * FROM studentinfo
			  INNER JOIN register USING (studentid) 
			  INNER JOIN studentcourse USING (studentid)
			  WHERE register.semid = $semid AND register.syid = $syid AND register.branchid = $branchid";
		if($type == "studentid"){
			$sql .= "AND register.studentid = '".$id."' ";
		}elseif($type == "regnum") {
			$sql .= "AND register.regnum = '".$id."' ";
		}

		if($this->has_data_db($sql)){
			$sql = $this->has_data_db($sql);
		}else {
			$sql = false;
		}

		return $sql;
	}

	public function getPreviousBalance($register,$regnum,$studentid){
		$sql = "SELECT
				ROUND(COALESCE(collectables, 0) - (COALESCE(payamount,0) + COALESCE(discount,0)) )as previous_balance
				FROM
					(SELECT 
						studentid,
						regnum,
						sum(CASE 
							  WHEN act.name = 'Miscellaneous fees' THEN (SELECT amount FROM miscellaneous WHERE sta.fees = miscid)
							  WHEN act.name = 'Tuition fees' THEN (SELECT amount FROM tuition WHERE sta.fees = tutionid)
							  WHEN act.name = 'Registration fees' THEN (SELECT amount FROM registrationfee WHERE sta.fees = regfee)
							  WHEN act.name = 'Other fees' THEN (SELECT amount FROM othersfees WHERE sta.fees = othersfeesid)
							 END) as collectables,
						SUM((SELECT amount FROM pay WHERE accountid = sta.accountid)) as payamount,
						sum(sta.discount) as discount
						FROM register 
						INNER JOIN  studentaccounts sta USING (regnum)
						INNER JOIN accounttype act USING (actypeid)
						WHERE studentid = '$studentid' AND syid = $register[1]
						GROUP BY studentid,regnum) as tbl1";
		
		if($this->has_data_db($sql)){
			$amount = $this->getVal('previous_balance', $sql);
			    $sql = "INSERT INTO previousbalance(regnum,amount) 
                  		VALUES ($regnum, $amount)";
                if($this->openqry($sql)) {
					$sql = "SELECT pbid FROM previousbalance WHERE regnum = $regnum";
					$getPreviousBID = $this->getVal('pbid', $sql);
					$sql = "SELECT * FROM accounttype WHERE name = 'Previous Balance' ";
					$getID = $this->getVal('actypeid',$sql);
					$sql = "INSERT INTO studentaccounts (regnum,fees,actypeid) VALUES ($regnum,$getPreviousBID,$getID)";
					if($this->openqry($sql)) {
						$boole = true;
					} 
				} 
		}else {
			$sql = false;
		}			
	}
}

?>