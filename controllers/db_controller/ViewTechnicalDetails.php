<?php 
/**
 * DIPLAYING DATA
 */
class ViewDetails extends DB
{
	//VIEW USER INFORMATION DETAILS
	public function view_user_details($branchid){
		 $sql = "
          	SELECT branch.name as branchname, userinfo.username as user, *,
            (SELECT COALESCE(string_agg(description, ', '), 'No Role') FROM  usergroup ug INNER JOIN grouptype g USING (grouptypeid) WHERE ug.username = userinfo.username AND ug.type = '1' ) as member_of_group
            FROM userinfo
                    INNER JOIN branch USING(branchid)
      		";
		if ($branchid == "all") {
            $sql .= "ORDER BY branch.name, userinfo.lastname ASC";
         }else {
          	$sql .= "WHERE branch.branchid = ".$branchid." ORDER BY userinfo.lastname ASC";
         }

        return $this->has_data_db($sql);
    }

  public function view_member_details($username , $branchid){
    $sql = " SELECT branch.name as branchname, grouptype.name as groupname, * FROM userinfo
              LEFT JOIN branch USING(branchid)
              LEFT JOIN usergroup USING (username)
              LEFT join grouptype USING (grouptypeid)
              ";
    if($branchid == "all"){
      $sql .= "WHERE userinfo.username = '$username' "; 
    } else {
      $sql .= " WHERE userinfo.username = '$username' AND branch.branchid = '$branchid'";
    }
    return $this->has_data_db($sql);
  }

  public function view_branch_details(){
    $sql = "SELECT name,address,landline,mobile,branchid FROM branch ORDER BY name ASC";
    return $this->has_data_db($sql);
  }

  public function view_group_details(){
    $sql = "SELECT * FROM grouptype ORDER By name ASC";
    return $this->has_data_db($sql);
  }
  
  
}

?>