<?php  

class ViewRegistrarDetails extends DB
{

	public function sy_details(){
		$sql = "SELECT DISTINCT * FROM schoolyear ORDER BY sy ASC";
        return $this->has_data_db($sql);  
	}

	public function scholar_details(){
		$sql = "SELECT DISTINCT * FROM scholarship ORDER BY name ASC";
        return $this->has_data_db($sql);  
	}

	public function studenttype_details(){
		$sql = "SELECT DISTINCT * FROM studenttype ORDER BY name ASC";
		return $this->has_data_db($sql);
	}

	public function studentlist_details(){
		$sql = "SELECT * FROM studentinfo 
		          INNER JOIN studentcourse USING (studentid)
		          INNER JOIN curriculum USING (curcode)
		          INNER JOIN course USING (coursecode) ORDER BY studentinfo.lastname";
		return $this->has_data_db($sql);
	}

	public function cardcoverpage_details($sem,$sy,$branchid,$scholarshipid){
		$sql = "SELECT * FROM studentinfo
		          INNER JOIN register USING (studentid)
		          WHERE register.semid = $sem
		          AND register.syid = $sy
		          AND register.branchid = $branchid 
		          AND register.scholarshipid = $scholarshipid
		          ORDER BY lastname ASC";
		return $this->has_data_db($sql);
	}

	public function studentinfo_details(){
		$studentid = $_POST["studentid"];
		$sql = "SELECT scholarship.name AS scholarshipname,  * FROM studentinfo
              INNER JOIN register USING (studentid) 
              INNER JOIN schoolyear USING (syid) 
              INNER JOIN semester USING (semid) 
              INNER JOIN scholarship USING (scholarshipid)
              INNER JOIN studentcourse USING (studentid)
              INNER JOIN curriculum USING (curcode)
              INNER JOIN course USING (coursecode)
              WHERE studentinfo.studentid = '$studentid' ";
        return $this->has_data_db($sql);
	}

	public function updategrades_details($semid,$syid){
		$sql = "SELECT * FROM schedules 
	            INNER JOIN subjects USING (subjectcode)
	            WHERE schedules.semid = $semid AND schedules.syid = $syid ";
	    return $this->has_data_db($sql);
	}
}


?>