<?php
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');


class InstructorController extends DB
{	
	use errormsg;
	
	public function add_grade_management($data){
		$first = $data["first"];
		$second = $data["second"];
		$remarks = $data["remarks"];
		$gradeid = $data["gradeid"];
		$bool = false;
        for ($i=0; $i <  sizeof($gradeid); $i++) { 
            if($first[$i] == '') {
                $first[$i] = 0;
            } 
            if($second[$i] == ''){
                $second[$i] = 0;
            }
        
       		$sql = "UPDATE studentgrades SET first = $first[$i], second = $second[$i], remarks = '$remarks[$i]', createat = current_timestamp   WHERE gradeid = $gradeid[$i]";
            if($this->openqry($sql)) {
                $bool = true;
            }
        }      
		if ($bool) {
			echo errormsg::alertmsg('Success', "Successfully updated..");
		} else {
			echo errormsg::alertmsg('Warning', "Unable to advise data.. ");
		}
	}

	public function check_attendance($gradeid,$remarks,$attended,$currentdata){
  		$bol = false;
		for ($i=0; $i <  sizeof($gradeid); $i++) { 
    
	        $sql = "INSERT INTO attendance (atdate,remarks,attended,gradeid) VALUES ('$currentdata','".$remarks[$i]."','$attended[$i]',$gradeid[$i])";
	        if($this->openqry($sql)) {
	            $bol = true;
	        }
      
  		}      
	    if ($bol) {
			echo errormsg::alertmsg('Success', "Successfully updated..");
		} else {
			echo errormsg::alertmsg('Warning', "Unable to advise data.. ");
		}
	}
}


$controller = new InstructorController;

if (isset($_POST["type"])) {
	$type = $_POST["type"];

	if($type == "add_grade_management"){
		$controller->add_grade_management($_POST);
	} elseif ($type == "check_attendance") {
		$controller->check_attendance($_POST["gradeid"],$_POST["remarks"],$_POST["attended"],$_POST["currentdate"]);
	}
}

?>