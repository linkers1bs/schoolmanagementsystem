<?php 
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');

class ManagementController extends DB
{
	
	use errormsg;

	public function subjectInformation($post,$subjectcode,$description,$totalunit,$lecture,$labaratory,$price,$remarks,$code1){
		$subjectcode = trim($subjectcode);
		$code1 = trim($code1);
		switch ($post) {
			case 'add':
				
				 $sql = "SELECT * FROM subjects WHERE subjectcode = '$subjectcode'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "Subject code already registered!");
				}else{
				 $sql = "INSERT INTO subjects (subjectcode,description,numofunit,lec,lab,remarks,price) VALUES ('$subjectcode','$description',$totalunit,$lecture,$labaratory,'$remarks',$price)";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Subject code successfully added!");
						echo '
							<script>
								setTimeout(function(){ 
									$("#subject_form_add").trigger("reset");
		       						$(".wid-fix2").removeAttr("style"); 
		       					}, 2000);
							 </script>';
					} else{
						echo errormsg::alertmsg('Warning', "Unable to added branch.. ");
					}
				}
				
				break;
			case 'update':
				$sql = "UPDATE subjects
				SET subjectcode = '$subjectcode', description = '$description', numofunit = $totalunit, lec = $lecture, lab = $labaratory, remarks = '$remarks', price = $price 
				WHERE subjectcode = '$code1'";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Subject code successfully updated!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to update data..");
				}
				break;
			case 'delete':
				$check = false;
				$sql = "SELECT * FROM curriculumdetailed WHERE subjectcode = '$subjectcode'";
				if($this->notEmpty($sql)){
					$check = true;
				}
				if ($check) {
					echo errormsg::alertmsg('Warning', "Unable to delete subject.. ");
				} else { 
					$sql = "DELETE FROM subjects WHERE subjectcode = '$subjectcode'";
					if($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Subject code successfully Deleted!");
					} else {
						echo errormsg::alertmsg('Warning', "Unable to delete subject.. ");
					}
				}
				
				break;
			default:
				echo "&nbsp;";
				break;
		}

	}

	public function roomInformation($post,$roomcode,$roomnumber,$building,$address,$floornumber,$remarks,$rcode){
		$roomcode = trim($roomcode);
		$rcode = trim($rcode);
		switch ($post) {
			case 'add':
				$sql = "SELECT DISTINCT * FROM room WHERE roomcode = '$roomcode'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "Room code already registered!");
				}else{
					$sql = "INSERT INTO room (roomcode,roomnum,building,address,floornum,remarks) VALUES ('$roomcode','$roomnumber','$building','$address',$floornumber,'$remarks')";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","The Room successfully added!");
						echo '
							<script>
								setTimeout(function(){ 
									$("#room_form_add").trigger("reset");
		       						$(".wid-fix2").removeAttr("style"); 
		       					}, 2000);
							 </script>';
					} else {
						echo errormsg::alertmsg('Warning', "Unable to add room.. ");
					}
				}
				
				break;
			case 'modify':
				$sql = "UPDATE room
				SET roomcode = '$roomcode',
				 roomnum = $roomnumber, 
				 building = '$building', 
				 address = '$address', 
				 floornum = $floornumber,
				 remarks = '$remarks' 
				WHERE roomcode = '$rcode'";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","The Room successfully updated!");
				} else {
					echo errormsg::alertmsg('Warning', "Unable to update room.. ");
				}
				break;
			case 'delete':
				$sql = "SELECT * from classschedules WHERE roomcode = '$roomcode'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "Unable to delete room.. ");
				} else {
					$sql = "DELETE FROM room WHERE roomcode = '$roomcode'";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","The room has been deleted!");
					} else {
						echo errormsg::alertmsg('Warning', "Unable to delete room.. ");
					}
				}
				
				break;
			default:
				echo "&nbsp;";
				break;
		}

	}

	public function courseinformation($post,$cname,$numyear,$track,$strand,$courseid){
		switch ($post) {
			case 'add':

				 $sql = "INSERT INTO course (coursename,numofyear,track,strand,ctype) 
				 VALUES ('$cname',$numyear,'$track','$strand','K12')";

				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Course name successfully added!");
				}else {
					echo errormsg::alertmsg("Warning", "Unable to save data..");
				}

				break;
			case 'update':
				$sql = "UPDATE course
				SET coursename = '$cname',
				 numofyear = '$numyear', 
				 track = '$track', 
				 strand = '$strand'
				WHERE coursecode = $courseid";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Course name successfully updated!");
				}else {
					echo errormsg::alertmsg("Warning", "Unable to update data..");
				}

				break;
			case 'delete':
				$check = false;
				$sql = "SELECT * FROM curriculum WHERE coursecode = $courseid";
				if($this->notEmpty($sql)){
					$check = true;
				}
				
				if ($check) {
					echo errormsg::alertmsg("Warning","Unable to delete data..");
				} else { 
					 $sql = "DELETE FROM course WHERE coursecode = $courseid";
					if($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Course name successfully Deleted!");
					}
				}

				break;
			default:
				echo "&nbsp;";
				break;
		}

	}

	public function curriculumInformation($post,$curricode,$description,$remarks,$coursecode,$curcode){
		$curricode = trim($curricode);
		$curcode = trim($curcode);
		switch ($post) {
			case 'add':
				$sql = "INSERT INTO curriculum (curcode,description,datecreated,coursecode,remarks) 
				 VALUES ('$curricode','$description',current_timestamp,$coursecode,'$remarks')";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Curriculum successfully added!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to add data...");
				}
				break;
			case 'update':
				$sql = "UPDATE curriculum
				SET curcode = '$curricode',
				datecreated = current_timestamp,
				description = '$description', 
				 remarks = '$remarks', 
				 coursecode = $coursecode
				WHERE curcode = '$curcode' ";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Curriculum successfully updated!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to modify data...");
				}
				break;
			case 'delete':
				$check = false;
				$sql = "SELECT * FROM curriculumdetailed WHERE curcode = '$curcode'";
				if($this->notEmpty($sql)){
					$check = true;
				}
				$sql = "SELECT * FROM studentcourse WHERE curcode = '$curcode'";
				if($this->notEmpty($sql)){
					$check = true;
				}
				if ($check) {
					echo errormsg::alertmsg("Warning","Unable to delete data..");
				} else { 
					 $sql = "DELETE FROM curriculum WHERE curcode = '$curcode'";
					if($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","The curriculum has been deleted!");
					} else {
						echo errormsg::alertmsg("Warning","Unable to delete data..");
					}
				}
				
				break;
			default:
				echo "&nbsp;";
				break;
		}

	}

	public function curriculum_details($data){
		if ($data["type"] == "add") {

			if ($data["subjectcode"] == "none") {
				echo errormsg::alertmsg("Warning","THE SUBJECTCODE IS EMPTY"); 
			} else {

				$sem = $data["sem"];
				$subjectcode = $data["subjectcode"];
				$subjecttype = $data["subjecttype"];
				$curcode = $data["curcode"];
				$sql = "
					SELECT * FROM curriculumdetailed 
					WHERE semnum = '$sem'
					AND subjectcode  = '$subjectcode'
					AND curcode = '$curcode'
				";

				if (!empty($data["subjecttype"])) {
					$sql .= "AND subjecttype = '$subjecttype'";
				}


				if($this->notEmpty($sql)){
					echo errormsg::alertmsg("Warning","Unable to save data already registered.."); 
				}else {

					$sql = "INSERT INTO curriculumdetailed (semnum,subjecttype,curcode,subjectcode)
						VALUES($sem,'$subjecttype','$curcode','$subjectcode')
					";

					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Successfully added...");
					}else {
						echo errormsg::alertmsg("Warning","Unable to save data..."); 
					}
					 
				}
			}
			
		} elseif ($data["type"] == "delete") {
			$id = $data["cdetailedid"];
			$sql = "DELETE FROM curriculumdetailed WHERE cdetailedid = $id";
			if ($this->openqry($sql)) {
				echo errormsg::alertmsg("Success","Successfully deleted...");
			}else {
				echo errormsg::alertmsg("Warning","Unable to delete data..."); 
			}
		}

		 else {
			echo errormsg::alertmsg("Warning","NOT RECOGNIZE TYPE CONTROLLER..".$data["type"]); 
		}
	}


	public function blocking_schedule_add($data){
		$num = count($data["roomcode"]);
                      
        $subjectblocking = array($data['block'], $data['subjectcode'],$data['capacity'],$data['sem'],$data['sy'],$data['restric'],$data['coursecode'],$data['instructor'],$data["studenttypeid"],$data["branchid"]);

		$i = 0;
		$no_duplicate = array();
		$has_duplicate = array();
		do {
			$room = $data['roomcode'][$i];
			$day = $data['day'][$i];
			$datestart = $data['datestart'][$i];
			$dateend = $data['dateend'][$i];
			$timestart = $data["timestart"][$i];
			$timeend = $data["timeend"][$i];
			# code...
			$sql = "SELECT * FROM schedules 
           	INNER JOIN classschedules USING (scheduleid)
            WHERE
            blockname = '$subjectblocking[0]' 
            AND branchid = $subjectblocking[9]
            AND subjectcode = '$subjectblocking[1]' 
            AND semid = $subjectblocking[3]
            AND syid = $subjectblocking[4]
            AND coursecode = $subjectblocking[6]
            AND studenttypeid = $subjectblocking[8]
            And username = '$subjectblocking[7]'
            AND roomcode = '$room'
            AND days = '$day'
            AND datestart = '$datestart'
            AND dateend = ' $dateend'
            AND starttime = '$timestart' 
            AND endtime = '$timeend'";


            if(!$this->notEmpty($sql)){
           		array_push($no_duplicate, $i);
            } else {
            	array_push($has_duplicate, $i);
            }

			$i++;
		} while ($num > $i);
		
		if(count($has_duplicate) > 0){
			echo errormsg::alertmsg("Warning", "Schedule has already registered. please try other schedule.");
		} else {

			$sql = "INSERT INTO schedules (blockname,subjectcode,studenttypeid,semid,syid,username,coursecode,branchid) 
	             VALUES ('".$data['block']."','".$data['subjectcode']."',".$data['studenttypeid'].",".$data['sem'].",".$data['sy'].",'".$data['instructor']."',".$data['coursecode'].", ".$data["branchid"].")";
	        
	        if ($this->openqry($sql)) {
	    	    $count  =count($no_duplicate);
	    	    $sql = "SELECT * FROM schedules WHERE
					blockname = '$subjectblocking[0]' 
					AND subjectcode = '$subjectblocking[1]' 
					AND semid = $subjectblocking[3]
					AND syid = $subjectblocking[4]
					AND coursecode = $subjectblocking[6]
					AND branchid = $subjectblocking[9]
					And username = '$subjectblocking[7]'";
	            $scheduleid = $this->getVal('scheduleid',$sql);
	            $boolean = true;
	            $j = 0;
	            do {
	            	$sql = "INSERT INTO classschedules(roomcode,days,datestart,dateend,starttime,endtime,scheduleid,capacity) 
							VALUES ('".$data["roomcode"][$j]."','".$data["day"][$j]."','".$data["datestart"][$j]."','".$data["dateend"][$j]."','".$data["timestart"][$j]."','".$data["timeend"][$j]."',$scheduleid,$subjectblocking[2])"; 

					if (!$this->openqry($sql)) {
						$boolean = false;
						break;
					}

					$j++;
	            } while ($count > $j);

	            if($boolean){
	            	echo errormsg::alertmsg("Success", "Successfully added..");
	            } else {
	            	echo errormsg::alertmsg("Warning", "Unable to save data ROW ".$data["roomcode"][$j - 1]."");
	            }

	        } else {
	        	echo errormsg::alertmsg("Warning", "Unable to save data..");
	        }

		}

	}

	public function blocking_schedule_update($data){

		$studenttypeid = $data["studenttypeid"];
		$sem = $data["sem"];
		$sy = $data["sy"];
		$branchid = $data["branchid"];
		$coursecode = $data["coursecode"];
		$scheduleid = $data["scheduleid"];
		$capacity = $data["capacity"];
		if ($data["type"] == "delete") {
			$sql = "SELECT FROM studentgrades WHERE scheduleid = $scheduleid";
			if ($this->notEmpty($sql)) {
				echo errormsg::alertmsg("Warning", "Unable to delete data...");
			}else {
				$sql = "DELETE FROM classschedules WHERE scheduleid = $scheduleid";
				$this->openqry($sql);
				$sql = "DELETE FROM schedules WHERE scheduleid = $scheduleid";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Subject Blocking successfully deleted!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to delete data...");
				}
			}
			
		} else {
			$sql = "UPDATE schedules
			SET blockname = '".$data["block"]."',
			studenttypeid = $studenttypeid,
			semid = $sem, 
			syid = $sy, 
			username = '".$data["instructor"]."',
			subjectcode = '".$data["subjectcode"]."',
			coursecode = $coursecode,
			branchid = $branchid
			WHERE scheduleid = $scheduleid ";
			if ($this->openqry($sql)) {
				$sql = "UPDATE classschedules
					SET days = '".$data["day"]."',
					roomcode ='".$data["roomcode"]."',
					capacity = $capacity, 
					starttime = '".$data["timestart"]."',
					endtime = '".$data["timeend"]."',
					datestart = '".$data["datestart"]."',
					dateend = '".$data["dateend"]."'
					WHERE scheduleid = $scheduleid ";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Subject Blocking successfully updated!");
				}	
			} else {
				echo errormsg::alertmsg("Warning", "Unable to modify data...");
			}
		}
		
	}

	public function sectioningInformation($data){
		$type = $data["type"];
		switch ($type) {
			case 'add':
				
				 $sql = "SELECT * FROM classsection WHERE name = '".$data["section_name"]."'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "Class section Adviser already registered!");
				}else{
				 $sql = "INSERT INTO classsection (name,username,task) VALUES ('".$data["section_name"]."','".$data["adviser"]."','".$data["task"]."')";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Class section Adviser successfully added!");
						echo '
							<script>
								setTimeout(function(){ 
									$("#section_form_add").trigger("reset");
		       						$("#section_form_add").find(".wid-fix2").removeAttr("style"); 
		       					}, 2000);
							 </script>';
					} else{
						echo errormsg::alertmsg('Warning', "Unable to added Class section Adviser.. ");
					}
				}
				
				break;
			default:
				echo "&nbsp;";
				break;
		}

	}

}


$action = new ManagementController;

if (isset($_POST["file"])) {
	
	$file = $_POST["file"]; // this indicate the file

	if($file == "subject"){

		$action->subjectInformation($_POST["type"],$_POST["sub_code"],$_POST["description"],$_POST["total_unit"],$_POST["lecture"],$_POST["lab"],$_POST["price"],$_POST["remarks"],$_POST["subjectcode"]); 

	} elseif ($file == "room") {
		
		$roomcode = "";
		if (isset($_POST["roomcode_select_modify"])) {
			$roomcode =$_POST["roomcode_select_modify"];
		} elseif (isset($_POST["roomcode_select_delete"])) {
			$roomcode =$_POST["roomcode_select_delete"];
		}
		$action->roomInformation($_POST["type"],$_POST["roomcode"],$_POST["roomnum"],$_POST["building"],$_POST["address"],$_POST["floornum"],$_POST["remarks"],$roomcode);
	} elseif ($file == "course") {

		$coursecode = "";
		if (isset($_POST["coursecode_modify"])) {
			$coursecode =$_POST["coursecode_modify"];
		} elseif (isset($_POST["coursecode_delete"])) {
			$coursecode =$_POST["coursecode_delete"];
		}

		$action->courseinformation($_POST["type"],$_POST["coursename"],$_POST["numyears"],$_POST["track"],$_POST["strand"],$coursecode);

	} elseif ($file == "curriculum") {

		$curriculumcode = "";
		if (isset($_POST["curcode_modify"])) {
			$curriculumcode =$_POST["curcode_modify"];
		} elseif (isset($_POST["curcode_delte"])) {
			$curriculumcode =$_POST["curcode_delte"];
		}

		$action->curriculumInformation($_POST["type"],$_POST["curricode"],$_POST["description"],$_POST["remarks"],$_POST["coursecode"],$curriculumcode);
	
	} elseif ($file == "block") {

		$data = $_POST;
		// $schedule_id = "";
		// if (isset($_POST["coursecode_modify"])) {
		// 	$schedule_id =$_POST["coursecode_modify"];
		// } elseif (isset($_POST["coursecode_delete"])) {
		// 	$schedule_id =$_POST["coursecode_delete"];
		// }
		if ($_POST["type"] == "update") {
			$action->blocking_schedule_update($data);
		} else if ($_POST["type"] == "delete") {
			$action->blocking_schedule_update($data);
		} else {
			$action->blocking_schedule_add($data);
		}
	} elseif ($file == "curriculum_details") {
		
		$data = $_POST;
		$action->curriculum_details($data);
	} elseif ($file == "sectioning") {
		
		$data = $_POST;
		// $action->get_post($data);
		$action->sectioningInformation($data);
	}


}
	

?>