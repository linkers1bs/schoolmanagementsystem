<?php
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');


class BranchController extends DB
{
	use errormsg;

	public function add($branchname,$mobile,$address,$landline){
		$sql = "SELECT name,address, landline, mobile FROM branch WHERE name = '$branchname' AND address = '$address' AND landline = '$landline' AND mobile = $mobile";
		
		if ($this->notEmpty($sql)) {
			echo errormsg::alertmsg('Warning', "Unable to added branch.. ");
		}else {
			$sql = "INSERT INTO branch(name,address,landline,mobile) VALUES('$branchname','$address','$landline',$mobile)";
			if ($this->openqry($sql)) {

				$sql = "SELECT name,address, landline, mobile ,branchid FROM branch WHERE name = '$branchname' AND address = '$address' AND landline = '$landline' AND mobile = $mobile";

				$get_id = $this->getVal("branchid",$sql);

				echo errormsg::alertmsg('Success', "Successfully Added..");
				echo '
					<script>
						setTimeout(function(){ 
							$("#branchAdd").trigger("reset");
       						$(".wid-fix2").removeAttr("style"); 
       					}, 2000);
					 </script>';
			}
		}
	}

	public function modify($branchname,$mobile,$address,$landline,$branchid){
		 $sql = "UPDATE branch
			SET name = '$branchname', mobile = $mobile, address = '$address', landline = '$landline'
			WHERE branchid = $branchid ";
		if ($this->openqry($sql)) {
			echo errormsg::alertmsg('Success', "Successfully updated..");
		} else {
			echo errormsg::alertmsg('Warning', "Unable to update data.. ");
		}
	}

	public function delete($branchid){
		$sql = "SELECT * FROM branch WHERE branchid IN (SELECT branchid FROM userinfo WHERE branchid = $branchid) ";
		if($this->notEmpty($sql)){
			echo errormsg::alertmsg('Warning', "Unable to delete data.. ");
		} else {
			$sql = "DELETE FROM branch WHERE branchid = $branchid";
			if ($this->openqry($sql)) {
				echo errormsg::alertmsg('Success', "Successfully deleted..");
			}
		}
	}
}


if (isset($_GET["type"])) {
	$branch = new BranchController();

	$type = $_GET["type"];

	if ($type == "add") {
		$branch->add($_GET["branchname"],$_GET["mobile"],$_GET["address"],$_GET["landline"]);
	} elseif($type == "update"){
		$branch->modify($_GET["branchname"],$_GET["mobile"],$_GET["address"],$_GET["landline"],$_GET["branchid"]);
	} else {
		$branch->delete($_GET["branchid"]);
	}
}

?>