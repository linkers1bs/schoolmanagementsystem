<?php
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');
class UserController extends DB
{
	use errormsg;
	// ADD NEW USER
	public function add($username,$pwd,$pwd2,$branchid,$grouptypeid,$lastname,$firstname,$middlename,$email,$position){
		if ($pwd == $pwd2) {
			if(strlen($pwd) < 5){
				echo errormsg::alertmsg('Warning', "Your password is too short");
				echo '<script> $(".userAdd #pwd, .userAdd #pwd2").css({"border":"2px solid red","box-shadow":"0 0 3px red"});</script>';
			}else {
				$login = 'false';
				$pass = $this->passwd($pwd);
				$sql = "SELECT username FROM userinfo WHERE username = '$username'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "The Username is already registered..");
					echo '<script>$(".userAdd #username").css({"border":"2px solid red","box-shadow":"0 0 3px red"});</script>';
				}else {

					if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
						echo errormsg::alertmsg('Warning', "Invalid email format!...");
						echo '<script>$(".userAdd #email").css({"border":"2px solid red","box-shadow":"0 0 3px red"});</script>';
					}else {
						$sql = "SELECT username FROM userinfo WHERE email = '$email'";
						if ($this->notEmpty($sql)) {
							echo errormsg::alertmsg('Warning', "The Email already registered..");
							echo '<script>$(".userAdd #email").css({"border":"2px solid red","box-shadow":"0 0 3px red"});</script>';
						}else {
							$sql = "INSERT INTO userinfo(username,firstname,lastname,middlename,branchid,passwrd,login,email,position) VALUES ('$username','$firstname','$lastname','$middlename',$branchid,'$pass','$login','$email','$position')";
							if ($this->openqry($sql)) {
								$sql = "INSERT INTO usergroup(username,grouptypeid,type) VALUES ('$username',$grouptypeid,'true')";
								if ($this->openqry($sql)) {
									echo errormsg::alertmsg('Success', "The User Successfully registered..");
									echo '
										<script>
											setTimeout(function(){ 
												$("form").trigger("reset");
			               						$(".wid-fix2").removeAttr("style"); 
			               					}, 2000);
										 </script>';
								}
							}	
						}
					}
					
				}
			}
		}else {
			echo errormsg::alertmsg('Warning', "The password did not match try again..");
		}
	}
	// HASHING PASSWORD
	public function passwd($passwd){
		$passwd = hash('sha512',(hash('md5',hash('sha512',($_GET["pwd"])))));
		return $passwd;
	}

	//MOIDFY USER INFORMATION
	public function modify($branchid,$lastname,$firstname,$middlename,$email,$position,$userid){
		$sql = "UPDATE userinfo
				SET  
					branchid = $branchid, lastname = '$lastname', firstname = '$firstname', 
					middlename = '$middlename', position = '$position', email = '$email'
				WHERE username = '$userid' ";

			if($this->openqry($sql)){
				echo errormsg::alertmsg('Success', "Successfully updated..");
			} else {
				echo errormsg::alertmsg('Warning', "Unable to updated.. ");
			}
	}

	//DELETE USER
	public function delete($username){
		$sql = "DELETE FROM usergroup WHERE username = '$username' ";
		if($this->openqry($sql)){
			$sql = "DELETE FROM userinfo WHERE username = '$username'";
			if($this->openqry($sql)){
				echo errormsg::alertmsg('Success', "User has been deleted..");
			} else {
				echo errormsg::alertmsg('Warning', "Unable to delete user.. ");
			}
		} else {
				echo errormsg::alertmsg('Warning', "Unable to delete user.. ");
		}
	}

	//ADD GROUP MEMBER OF THE USER 
	public function add_member_group($username, $gid){
	    $sql = "SELECT * FROM usergroup WHERE username = '$username' AND grouptypeid = $gid";
	       if ($this->has_data_db($sql)) {
	          echo errormsg::alertmsg('Warning', "Unable to added member.. ");
	        } else {
	            $sql = "INSERT INTO usergroup(username,grouptypeid,type) VALUES('$username',$gid,'false')";
	            if ($this->openqry($sql)) {
	              echo errormsg::alertmsg('Success', "Successfully added..");
	            }
	       }
	}

	//DELETE GROUP FORM THE USER
	public function delete_member_group($username,$gid){
		$username = trim($username);
		$sql = "SELECT count(*) as count from usergroup WHERE username = '$username'";
		if ($this->getVal('count' , $sql) > 1) {
			$sql = "SELECT * FROM usergroup WHERE username = '$username' AND grouptypeid = $gid";
			$id = $this->getVal('id', $sql);

			$sql = "DELETE FROM usergroup WHERE id = $id";

			if ($this->openqry($sql)) {

				 echo errormsg::alertmsg('Success', "Successfully deleted..");

			}
		} else {
			 echo errormsg::alertmsg('Warning', "Unable to delete group please click the button Inactive the user.. ");
		}
		
	}

	//UPDATE THE STATUS USER GROUP MEMBER
	public function update_action_member($username,$gid,$action){
		$username = trim($username);
		$bollean = 'false';
		if($action == 'Active' || $action == "Active"){
		 $bollean = 'true';
		}
		$sql = "SELECT * FROM usergroup WHERE username = '$username' AND grouptypeid = $gid";
		$id = $this->getVal('id', $sql);
		$sql = "UPDATE usergroup
					SET type = '$bollean'
					WHERE id = $id ";
		if ($this->openqry($sql)) {
			echo errormsg::alertmsg('Success', "Successfully updated..");
		}
	}


}

/*
*THIS ARE CALLING PARAMATER TO USERCONTROLLER THEN PASS DATA TO JAVASCRIPT
*
*/
$user_controller = new UserController;
if (isset($_GET["type"])) {

	if($_GET["type"] == 'add'){
		$user_controller->add($_GET['username'],$_GET['pwd'],$_GET['pwd2'],$_GET['branchid'],$_GET['grouptypeid'],$_GET['lastname'],$_GET['firstname'],$_GET['middlename'],$_GET['email'],$_GET['position']);
	}elseif ($_GET["type"] == "update") {
		$user_controller->modify($_GET['branchid'],$_GET['lastname'],$_GET['firstname'],$_GET['middlename'],$_GET['email'],$_GET['position'],$_GET['username1']);
	} else {
		$user_controller->delete($_GET['userid']);
	}

 } elseif ( isset($_GET['memberof']) ) {
 		
 		if($_GET["type_member"] == "add"){
 			$user_controller->add_member_group($_GET["username"], $_GET["gid"]);
 		}elseif ($_GET["type_member"] == "delete") {
 			$user_controller->delete_member_group($_GET["username"], $_GET["gid"]);
 		} else {
 			$user_controller->update_action_member($_GET["username"],$_GET["gid"],$_GET["type_action"]);
 		}
 }
?>