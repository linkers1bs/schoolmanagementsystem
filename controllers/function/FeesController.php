<?php

require '../db_controller/theDBConn.php';
include('errorrmsg.php');
class FeesController extends DB
{

	use errormsg;

	public function add_misc_fee($data){
		$sql = "SELECT * FROM miscellaneous WHERE name = '$data[0]' AND amount = $data[1] AND syid = $data[3] AND branchid = $data[4] AND s_type = '$data[5]' AND studenttypeid = $data[6]  AND semid = $data[2]";
	    if($this->notEmpty($sql)){
	        echo errormsg::alertmsg('Warning', "Unable to save data.. ");
	    }else {
	     $sql = "INSERT INTO miscellaneous (name,amount,semid,syid,branchid,s_type,active,studenttypeid) VALUES (
	          '$data[0]',$data[1],$data[2],$data[3],$data[4],'$data[5]','true',$data[6]
	      ) ";
	      if($this->openqry($sql)){
	       		echo errormsg::alertmsg("Success","The successfully added..");
	      }else {
	      		echo errormsg::alertmsg('Warning', "Unable to save data.. ");
	      }
	    }
	}

	public function modify_misc_fee($des,$amount,$miscid){
		    $sql = "UPDATE miscellaneous SET name = '$des', amount = $amount WHERE miscid = $miscid ";
		    if($this->openqry($sql)){
	       		echo errormsg::alertmsg("Success","The successfully updated..");
		    }else {
		      		echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		    }
	}


	public function add_reg_fee($data){
		$sql = "SELECT * FROM registrationfee WHERE name = '$data[0]' AND amount = $data[1] AND semid = $data[2] AND syid = $data[3] AND branchid = $data[4] AND studenttypeid = $data[5] AND scholarshipid = $data[6] ";
	    if($this->notEmpty($sql)){
	       echo errormsg::alertmsg('Warning', "Unable to save data.. ");
	    }else {
	     $sql = "INSERT INTO registrationfee (name,amount,semid,syid,branchid,studenttypeid,scholarshipid) VALUES (
	          '$data[0]',$data[1],$data[2],$data[3],$data[4],$data[5],$data[6]) ";
	      if($this->openqry($sql)){
	       		echo errormsg::alertmsg("Success","The successfully added..");
		    }else {
		      		echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		    }
	    }
	}

	public function modify_reg_fee($des,$amount,$regfee){
	    $sql = "UPDATE registrationfee SET name = '$des', amount = $amount WHERE regfee = $regfee ";
	    if($this->openqry($sql)){
	       		echo errormsg::alertmsg("Success","The successfully updated..");
		    }else {
		      		echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		    }
	}

	public function add_tuition_fee($data){
		$sql = "SELECT * FROM tuition WHERE name = '$data[0]' AND amount = $data[1] AND semid = $data[2] AND syid = $data[3] AND branchid = $data[4] AND studenttypeid = $data[5] AND scholarshipid = $data[6] ";
	    if($this->notEmpty($sql)){
	        echo errormsg::alertmsg("Warning","The Tuition Fee is already inserted!..");
	    }else {
	     $sql = "INSERT INTO tuition (name,amount,semid,syid,branchid,studenttypeid,scholarshipid) VALUES (
	          '$data[0]',$data[1],$data[2],$data[3],$data[4],$data[5],$data[6]) ";
	      if($this->openqry($sql)){
	       		echo errormsg::alertmsg("Success","The successfully added..");
		    }else {
		      		echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		    }
	    }
	}

	public function modify_tuition_fee($des,$amount,$tutionid){
		$sql = "UPDATE tuition SET name = '$des', amount = $amount WHERE tutionid = $tutionid ";
	    if($this->openqry($sql)){
	       		echo errormsg::alertmsg("Success","The successfully updated..");
		    }else {
		      	echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		    }
	}
}


$fee_controller = new FeesController;

if (isset($_POST["type"])) {
	$type = $_POST["type"]; //misc / reg / tuition
	$action = $_POST["action"];

	if($type == "misc_fee"){

		if($action == "misc_add_fee"){
				
				$array = array($_POST["description"], $_POST["price"], $_POST["semid"], $_POST["syid"],$_POST["branchid"],$_POST["clienttype"],$_POST["studenttypeid"]);
				$fee_controller->add_misc_fee($array);

		} elseif ($action == "misc_modify_fee") {

			$des = $_POST["des"];
		    $amount = $_POST["amount"];
		    $miscid = $_POST["miscid"];
     		$fee_controller->modify_misc_fee($des,$amount,$miscid);

		}

	} elseif ($type == "reg_fee") {

		if($action == "reg_add_fee"){

			$array = array($_POST["description"], $_POST["price"], $_POST["semid"], $_POST["syid"],$_POST["branchid"],$_POST["studenttypeid"],$_POST["scholarshipid"]);
			$fee_controller->add_reg_fee($array);

		} elseif ($action == "reg_modify_fee") {
			
			$des = $_POST["des"];
		    $amount = $_POST["amount"];
		    $regfee = $_POST["regfee"];
     		$fee_controller->modify_reg_fee($des,$amount,$regfee);

		}

	} elseif ($type == "tuition_fee") {
		
		if ($action == "tuition_add_fee") {

			$array = array($_POST["description"], $_POST["price"], $_POST["semid"], $_POST["syid"],$_POST["branchid"],$_POST["studenttypeid"],$_POST["scholarshipid"]);
			$fee_controller->add_tuition_fee($array);

		} elseif ($action == "tui_modify_fee") {
			
			$des = $_POST["des"];
		    $amount = $_POST["amount"];
		    $tutionid = $_POST["tutionid"];
     		$fee_controller->modify_tuition_fee($des,$amount,$tutionid);

		}

	}

}
?>