<?php 
trait errormsg {
	public function alertmsg($alertType, $alertmsg){
		 $alertType;
		 $alertmsg;
		// echo "<div style='float: right; margin-left: 68%; margin-right: 10px; width: 30%; clear: right; position: fixed'>";
		switch ($alertType) {

			case "Success":
				echo 	"<div class='alert alert-success  role='alert'>
							<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
							<strong>Success!</strong> ". $alertmsg ."
						</div>";
				break;
			case "Info":
				echo	"<div class='alert alert-info role='alert>
							<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
							<strong>Info!</strong> ". $alertmsg ."
						</div>";
				break;
			case "Warning":
				echo 	"<div class='alert alert-warning role='alert id='test'>
							<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
							<strong>Warning!</strong> ". $alertmsg ."
						</div>";
				break;
			case "Danger":
				echo 	"<div class='alert alert-danger role='alert>
							<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
							<strong>Danger!</strong> ". $alertmsg ."
						</div>";
				break;
			case "empty":
				echo 	"<div class='alert alert-danger role='alert>
							<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
							<strong>Warning!</strong> ". $alertmsg ."
						</div>";
				break;
			default:
				echo "&nbsp;";
				break;
		}
		// echo "</div>"; 
    }
}
	

?>