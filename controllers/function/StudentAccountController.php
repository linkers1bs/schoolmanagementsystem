<?php 

require '../db_controller/theDBConn.php';
require '../db_controller/RegistrationHelper.php';
include('errorrmsg.php');
class StudentAccountcontroller extends DB
{
	use errormsg;

	public function add_otherpayment($others){

		$sql = "INSERT INTO othersfees (regnum,name,amount)  VALUES ($others[0],'$others[1]',$others[2])";
		 if($this->openqry($sql)) {
				$sql = "SELECT * FROM othersfees WHERE regnum = $others[0] 
					AND name = '$others[1]' AND amount = $others[2]
				";
				 $otherfeeid = $this->getVal('othersfeesid',$sql);
				 $sql = "SELECT * FROM accounttype WHERE name = 'Other fees'";
				 $actypeid = $this->getVal('actypeid',$sql);
				$sql = "INSERT INTO studentaccounts (regnum,fees,actypeid) VALUES ($others[0],$otherfeeid,$actypeid)";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","The successfully added..");
				} else {
					echo errormsg::alertmsg('Warning', "Unable to save data.. ");
				}

		} else {
			echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		}

	}


	public function modify_assement($accounttype,$amount,$accountid,$nameOthers,$amountaccountmod,$discount) {
		$bollean = false;
		if($accounttype != '') {
			for ($i=0; $i <  sizeof($accounttype); $i++) { 
				$sql = "SELECT * FROM studentaccounts 
				   LEFT JOIN accounttype USING (actypeid)
				   WHERE  accountid = $accounttype[$i]";
		   
				$name = $this->getVal('name',$sql);
		   
		   
			   if(trim($name) != 'Other fees' || trim($name) != "Other fees" || trim($name) != 'Previous Balance') {
				$sql = "UPDATE studentaccounts
						   SET discount = '$amount[$i]'
						   WHERE accountid = $accounttype[$i] ";
					   
				   if($this->openqry($sql)) {
					   $bollean = true;
				   }
			   } else {
   
				  $bollean = false;
			   }
			   
		   }
		}

		if($accountid != '') {
			for ($i=0; $i <  sizeof($accountid); $i++) { 
				
				$sql = "SELECT * FROM studentaccounts 
				   LEFT JOIN accounttype USING (actypeid)
				   WHERE  accountid = $accountid[$i]";
		   
				$name = $this->getVal('name',$sql);
		   
				$otherid = $this->getVal('fees',$sql);
		   
			   if(trim($name) == 'Other fees' || trim($name) == "Other fees") {
				$sql = "UPDATE studentaccounts
						   SET discount = '$discount[$i]'
						   WHERE accountid = $accountid[$i] ";
					   
				   if($this->openqry($sql)) {
					   

							$sql = "UPDATE othersfees
							SET amount = '$amountaccountmod[$i]' , name = '$nameOthers[$i]'
							WHERE othersfeesid = $otherid ";
							  if($this->openqry($sql)) { 
								$bollean =true;
							  } else {
								$bollean = false ;
							  }
				   } else {
					$bollean = false ;
				   }
				   
			   } 
			   
		   }
		}

		if ($bollean) {
			echo errormsg::alertmsg("Success","The Student Accounts successfully updated..");
		} else {
			echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		}

	}


	public function add_payment_account($orno,$amount,$accounttype,$length){

		$orno = trim($orno);
         $slq = "SELECT * FROM pay WHERE orno = '$orno'";
         if(!$this->notEmpty($slq)) {
           
            $n = trim($length);
            $bol = true;
            for($i = 0; $i < $n ;$i++) {
                if(trim($amount[$i]) != '' && trim($accounttype[$i]) != '' ) {
                	$id = trim($accounttype[$i]);
                	$a = $amount[$i];
                 	$sql = "INSERT INTO pay (orno,amount,createdat,accountid)
						VALUES ('$orno',$a,current_timestamp,$id)
					";
					if(!$this->openqry($sql)) {
						$bol = false;
					} 
                }
            }

	        if($bol) {
	          	echo errormsg::alertmsg("Success","The payment successfully added..");
	        } else {
	           	echo errormsg::alertmsg('Warning', "Unable to save data.. ");
	        }

       	} else {
        	echo errormsg::alertmsg('Warning', "Unable to save data.. ");
        }
      
	}


	public function modify_payment_account($payid,$amount) {
		$bol = true;
		for ($i=0; $i <  sizeof($payid); $i++) { 
		
				$sql = "UPDATE pay SET amount = '$amount[$i]', updateat = current_timestamp WHERE payid = $payid[$i]";
					
				if(!$this->openqry($sql)) {
					$bol = false;
				}
			
		}

		if($bol){
			echo errormsg::alertmsg("Success","The payment successfully modify..");
		}else {
			echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		}
	}

	public function delete_payment($payid) {
		$bol = true;
		for ($i=0; $i <  sizeof($payid); $i++) { 
		
			$sql = "DELETE FROM pay WHERE payid = $payid[$i]";
					
				if(!$this->openqry($sql)) {
					$bol = false;
				}
		}
		
		if($bol){
			echo errormsg::alertmsg("Success","The payment successfully deleted..");
		}else {
			echo errormsg::alertmsg('Warning', "Unable to save data.. ");
		}
	}
}
$studentAccount = new StudentAccountcontroller;

if (isset($_POST["type"])) {

	$type = $_POST["type"];

	if ($type == "add_otherassessment") {

		$data =array($_POST["regnumother"],$_POST["accountnameother"],$_POST["amountother"]);
		$studentAccount->add_otherpayment($data);

	} elseif ($type == "modify_assement") {

		$amountaccountmod = "";
        $discount = "";
        $nameOthers ="";
        $accountid ="";
        $accounttype = "";
        $amount ="";

		if(!empty($_POST["accounttype"]) && !empty($_POST["amountaccount"])) {
          $accounttype = $_POST["accounttype"];
          $amount = $_POST["amountaccount"];
          
        }

        if(!empty($_POST["accountid"])) {
          $amountaccountmod = $_POST["amountaccountmod"];
          $discount = $_POST["dicount"];
          $nameOthers = $_POST["nameOthers"];
          $accountid = $_POST["accountid"];
        }

        $studentAccount->modify_assement($accounttype,$amount,$accountid,$nameOthers,$amountaccountmod,$discount);

	} elseif ($type == "addpayment") {

		 $studentAccount->add_payment_account($_POST["orno"],$_POST["amount"],$_POST["accounttype"],$_POST["lens"]);

	} elseif ($type == "modifypayment") {

		$payid = $_POST["payid"];
        $amount = $_POST["amount"];
        $studentAccount->modify_payment_account($payid,$amount);

	} elseif ($type == "deletepayment") {

		$payid = $_POST["payid"];
        $studentAccount->delete_payment($payid);
	}
}
?>