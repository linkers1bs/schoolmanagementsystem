<?php 
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');

class RegistrarController extends DB 
{

	use errormsg;

	public function syInformation($post,$sy,$sycode = false){
		$sy = trim($sy);
		$sycode = trim($sycode);
		switch ($post) {
			case 'add':

				$sql = "SELECT * FROM schoolyear WHERE sy = '$sy'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "School Year already registered!");
				}else{
				$sql = "INSERT INTO schoolyear (sy) VALUES ('$sy')";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","School Year successfully added!");
						echo '
							<script>
								setTimeout(function(){ 
									$("#sy_form_add").trigger("reset");
		       						$(".wid-fix2").removeAttr("style"); 
		       					}, 2000);
							</script>';
					} else{
						echo errormsg::alertmsg('Warning', "Unable to added branch.. ");
					}
				}
				break;
			case 'update':
				$sql = "UPDATE schoolyear
						SET sy = '$sy'
						WHERE syid = '$sycode' ";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","School Year successfully updated!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to update data..");
				}
				break;
			case 'delete':
				// $check = false;
				$sql = "SELECT * FROM schoolyear WHERE syid = '$sy'";
				if($this->notEmpty($sql)){
					echo errormsg::alertmsg('Warning', "Unable to delete school year.. ");
				} else {
				$sql = "DELETE FROM schoolyear WHERE sy = '$sy'";
					if($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","School Year successfully Deleted!");
					} else {
						echo errormsg::alertmsg('Warning', "Unable to delete School Year.. ");
					}
				}
				break;
			default:
				echo "&nbsp;";
				break;
		}
	}

	public function scholarshipInformation($post,$name,$scholarcode = false){
		$name = trim($name);
		$scholarcode = trim($scholarcode);
		switch ($post) {
			case 'add':
				$sql = "SELECT * FROM scholarship WHERE name = '$name'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "Scholarship already registered!");
				}else{
				$sql = "INSERT INTO scholarship (name) VALUES ('$name')";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Scholarship successfully added!");
						echo '
							<script>
								setTimeout(function(){ 
									$("#scholar_form_add").trigger("reset");
		       						$(".wid-fix2").removeAttr("style"); 
		       					}, 2000);
							</script>';
					} else{
						echo errormsg::alertmsg('Warning', "Unable to added branch.. ");
					}
				}
				break;
			case 'update':
				$sql = "UPDATE scholarship
						SET name = '$name'
						WHERE scholarshipid = '$scholarcode' ";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Scholarship successfully updated!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to update data..");
				}
				break;
			case 'delete':
				// $check = false;
				$sql = "SELECT * FROM scholarship WHERE scholarshipid = '$name'";
				if($this->notEmpty($sql)){
					echo errormsg::alertmsg('Warning', "Unable to delete scholarship.. ");
				} else {
				$sql = "DELETE FROM scholarship WHERE name = '$name'";
					if($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Scholarship successfully Deleted!");
					} else {
						echo errormsg::alertmsg('Warning', "Unable to delete scholarship.. ");
					}
				}
				break;
			default:
				echo "&nbsp;";
				break;
		}
	}

	public function studenttypeInformation($post,$studtypename,$studtypecode = false){
		$studtypename = trim($studtypename);
		$studtypecode = trim($studtypecode);
		switch ($post) {
			case 'add':
				$sql = "SELECT * FROM studenttype WHERE name = '$studtypename'";
				if ($this->notEmpty($sql)) {
					echo errormsg::alertmsg('Warning', "Student Type already registered!");
				}else{
				 $sql = "INSERT INTO studenttype (name) VALUES ('$studtypename')";
					if ($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Student Type successfully added!");
						echo '
							<script>
								setTimeout(function(){ 
									$("#studenttype_form_add").trigger("reset");
		       						$(".wid-fix2").removeAttr("style"); 
		       					}, 2000);
							</script>';
					} else{
						echo errormsg::alertmsg('Warning', "Unable to added branch.. ");
					}
				}
				break;
			case 'update':
				$sql = "UPDATE studenttype
						SET name = '$studtypename'
						WHERE studenttypeid = '$studtypecode' ";
				if ($this->openqry($sql)) {
					echo errormsg::alertmsg("Success","Student Type successfully updated!");
				} else {
					echo errormsg::alertmsg("Warning", "Unable to update data..");
				}
				break;
			case 'delete':
				$check = false;
				$sql = "SELECT * FROM studenttype WHERE studenttypeid = '$studtypecode' ";
				if($this->notEmpty($sql)){
					$check = true;
				}
				if($this->notEmpty($sql)){
					echo errormsg::alertmsg('Warning', "Unable to delete student type.. ");
				} else {
				$sql = "DELETE FROM studenttype WHERE name = '$studtypename'";
					if($this->openqry($sql)) {
						echo errormsg::alertmsg("Success","Student type successfully Deleted!");
					} else {
						echo errormsg::alertmsg('Warning', "Unable to delete student type.. ");
					}
				}
				break;
			default:
				echo "&nbsp;";
				break;
		}
	}

	public function studentinfo_details_controller($data) {
		$studentid = explode(',', $data["studentid"]);
		$age = explode(',', $data["age"]);
		$currentaddress = explode(',', $data["currentaddress"]);
		$sex = explode(',', $data["sex"]);
		$maritalstatus = explode(',', $data["maritalstatus"]);
		$dateofbirth = explode(',', $data["dateofbirth"]);
		$mobile = explode(',', $data["mobile"]);
		$placeofbirth = explode(',', $data["placeofbirth"]);
		$home_address = explode(',', $data["home_address"]);
		$schoolstatus = explode(',', $data["schoolstatus"]);
		$scholarshipname = explode(',', $data["scholarshipname"]);
		$religion = explode(',', $data["religion"]);
		$homephone = explode(',', $data["homephone"]);
		$mothername = explode(',', $data["mothername"]);
		$motherocc = explode(',', $data["motherocc"]);
		$fathername = explode(',', $data["fathername"]);
		$fatherocc = explode(',', $data["fatherocc"]);
		$parentsadd = explode(',', $data["parentsadd"]);
		$parent_phone = explode(',', $data["parent_phone"]);
		$person_to_notify = explode(',', $data["person_to_notify"]);
		$elementary = explode(',', $data["elementary"]);
		$ele_yr_grad = explode(',', $data["ele_yr_grad"]);
		$secondary = explode(',', $data["secondary"]);
		$sec_yr_grad = explode(',', $data["sec_yr_grad"]);
		$college = explode(',', $data["college"]);
		$col_yr_grad = explode(',', $data["col_yr_grad"]);
		$sonumber = explode(',', $data["sonumber"]);
		$dategradici = explode(',', $data["dategradici"]);
		$dateso = explode(',', $data["dateso"]);
		$awards = explode(',', $data["awards"]);
		$remarks = explode(',', $data["remarks"]);
		$bol1 = false;
		for ($i=0; $i <  sizeof($studentid); $i++) {
			$dateBirth = "";
			if(!empty(trim($dateofbirth[$i]))) {
				$dateBirth = "dateofbirth = '".$dateofbirth[$i]."',";
			}
			$gradici = "";

			if(!empty(trim($dategradici[$i]))) {
				$gradici = "dategradici = '".$dategradici[$i]."',";
			}

			$so_number = "";
			if(!empty(trim($dateso[$i]))) {
				$so_number = "dateso = '".$dateso[$i]."',";
			}

            $sql = "UPDATE studentinfo 
	            SET 
	                sex                   = '$sex[$i]',
	                currentaddress        = '$currentaddress[$i]',
	                maritalstatus		  = '$maritalstatus[$i]',
	                mobile  			  = '$mobile[$i]',
	                home_address 		  = '$home_address[$i]',
	                schoolstatus 		  = '$schoolstatus[$i]',
	               	$dateBirth
	                homephone		      = '$homephone[$i]',
	                motherocc		      = '$motherocc[$i]',
	                religion      		  = '$religion[$i]',
	                mothername            = '$mothername[$i]',
	                fathername  		  = '$fathername[$i]',
	                fatherocc   		  = '$fatherocc[$i]',
	                parentsadd   		  = '$parentsadd[$i]',
	                parent_phone    	  = '$parent_phone[$i]',
	                placeofbirth   		  = '$placeofbirth[$i]',
	                person_to_notify      = '$person_to_notify[$i]',
	                elementary       	  = '$elementary[$i]',
	                ele_yr_grad       	  = '$ele_yr_grad[$i]',
	                secondary       	  = '$secondary[$i]',
	                sec_yr_grad       	  = '$sec_yr_grad[$i]',
	                college       		  = '$college[$i]',
	                col_yr_grad       	  = '$col_yr_grad[$i]',
	                sonumber       		  = '$sonumber[$i]',
	                $gradici
	                $so_number
	                awards       		  = '$awards[$i]',
	                remarks       		  = '$remarks[$i]'
	                WHERE studentid 	  = '$studentid[$i]' ";
            if($this->openqry($sql)) {
                $bol1 = true;
            }
        }

   			if ($bol1) {
				echo errormsg::alertmsg('Success', "Successfully updated..");
			} else {
				echo errormsg::alertmsg('Warning', "Unable to advise data.. ");
			}
	}

}


$action = new RegistrarController();

if (isset($_POST["file"])) {
	
	$file = $_POST["file"];

	if ($file == "sy") {
		$action->syInformation($_POST["type"],$_POST["sy"],$_POST["syid"]);
	} elseif ($file == "name") {
		$action->scholarshipInformation($_POST["type"],$_POST["name"],$_POST["scholarshipid"]);
	} elseif ($file == "studtypename") {
		$action->studenttypeInformation($_POST["type"],$_POST["studtypename"],$_POST["studenttypeid"]);
	} elseif ($file == "studentinfo_details_controller") {
		$action->studentinfo_details_controller($_POST);
		// $action->get_post($_POST);
	}

}

?>