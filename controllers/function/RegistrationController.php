<?php 
require '../db_controller/theDBConn.php';
require '../db_controller/RegistrationHelper.php';
include('errorrmsg.php');
 class RegistrationController extends RegistrationHelper
 {
 	use errormsg;
 	public function add_new($studentinfo, $curcode,$register){
      
 		    $lrnid = $studentinfo[0];
        $sql = "SELECT lrnid FROM studentinfo WHERE lrnid = '$lrnid' ";

        if($this->notEmpty($sql))  {  //checking the student if already registered

          echo errormsg::alertmsg('Warning', "The student is already registered!.. "); 

        } else  { 

           $studentid = $this->createStudetID($register[2],$register[1]);

           $sql = "INSERT INTO studentinfo (lrnid,studentid,firstname,lastname,middlename,hscard,lrncert,form137,bcrtfcate,goodmoral) VALUES (
            '$studentinfo[0]', 
            '$studentid',  
            '$studentinfo[2]', 
            '$studentinfo[1]',  
            '$studentinfo[3]',  
            '$studentinfo[5]', 
            '$studentinfo[4]', 
            '$studentinfo[6]',  
            '$studentinfo[8]', 
            '$studentinfo[7]')";

            if($this->openqry($sql)) {

                $sql = "INSERT INTO studentcourse (dateenrolled,studentstatus,curcode,studentid)
                VALUES (
                  current_timestamp,
                  'false',
                  '$curcode',
                  '$studentid'
                )";

                if ($this->openqry($sql))   {
                   
                 	 $sql = "INSERT INTO register(branchid,studentid,semid,syid,scholarshipid,studenttypeid,dateregistered,gradelevel) 
	                  VALUES (
	                    $register[0],
	                    '$studentid',
	                    $register[2],
	                    $register[1],
	                    $register[3],
	                    $register[4],
	                    current_timestamp,
	                    $register[5]
	                    )";

                    if($this->openqry($sql)) {

                        $sql = "SELECT * FROM register 
                        	WHERE branchid = $register[0] 
                        	AND studentid = '$studentid' 
                        	AND semid = $register[2] 
                        	AND syid = $register[1] 
                        	AND scholarshipid = $register[3] 
                        	AND studenttypeid = $register[4] ";

                        $getRegnum = $this->getVal('regnum',$sql);
                        $get_total_misc = $this->getMisc($register); //RETURN ARRAY MISC
                        $get_total_tuition =  $this->getTuition($register);
                        $get_total_registration =  $this->getRegistration($register);
                        $arrayregfees = array('Miscellaneous fees','Tuition fees','Registration fees');
                        $total_fees = array($get_total_misc,$get_total_tuition,$get_total_registration);
                        $bol = false;
            
  	                    for ($i=0; $i < 3 ; $i++) { 
                            for ($j=0; $j < count($total_fees[$i]) ; $j++) { 
                              $this->insertStudentAccount($arrayregfees[$i],$total_fees[$i][$j],$getRegnum);
                            }
                            $bol = true;
  	                    }

                        if ($bol) {
                          echo errormsg::alertmsg("Success","The Student successfully added..");
                          echo '
            								<script>
            									setTimeout(function(){ 
            										$("form").trigger("reset");
            	               						$(".wid-fix").removeAttr("style"); 
            	               					}, 2000);
            								 </script>';
                        } else {
                          echo errormsg::alertmsg('Warning', "Unable to save data.. ");
                        }

                    } else  {
                      echo errormsg::alertmsg('Warning', "Unable to save data.. ");
                    }

                } else  {
                  echo errormsg::alertmsg('Warning', "Unable to save data.. ");
                }

            } else  { 
              echo errormsg::alertmsg('Warning', "Unable to save data.. ");
            }

        }
 	}

 	public function add_old($studentid,$register){
 		 $sql = "SELECT * FROM register WHERE branchid = $register[0] 
			 AND studentid = '$studentid' 
			 AND semid = $register[2] 
			 AND syid = $register[1] 
			 AND studenttypeid = $register[4] ";     

        if($this->notEmpty($sql)) {
           echo errormsg::alertmsg('Warning', "The student is already registered!..."); 
        
        } else {
             $sql = "INSERT INTO register(branchid,studentid,semid,syid,scholarshipid,studenttypeid,dateregistered,gradelevel) 
                  VALUES (
                    $register[0],
                    '$studentid',
                    $register[2],
                    $register[1],
                    $register[3],
                    $register[4],
                    current_timestamp,
                    $register[5]
                    )";

                if($this->openqry($sql)) {

                  	$sql = "SELECT * FROM register WHERE branchid = $register[0] 
	                  	AND studentid = '$studentid' 
	                  	AND semid = $register[2] 
	                  	AND syid = $register[1] 
	                  	AND scholarshipid = $register[3] 
	                  	AND studenttypeid = $register[4] ";

                    $getRegnum = $this->getVal('regnum',$sql);

                    $this->getPreviousBalance($register,$getRegnum,$studentid);

                    $get_total_misc = $this->getMisc($register);
                    $get_total_tuition =  $this->getTuition($register);
                    $get_total_registration =  $this->getRegistration($register);
                    $arrayregfees = array('Miscellaneous fees','Tuition fees','Registration fees');
                    $total_fees = array($get_total_misc,$get_total_tuition,$get_total_registration);
                    $bol = false;
        
              	    for ($i=0; $i < 3 ; $i++) { 
                        for ($j=0; $j < count($total_fees[$i]) ; $j++) { 
                          $this->insertStudentAccount($arrayregfees[$i],$total_fees[$i][$j],$getRegnum);
                        }
                        $bol = true;
                    }


                    if ($bol) {
                      echo errormsg::alertmsg("Success","The Student successfully added..");
                       echo '
							<script>
								setTimeout(function(){ 
									$("form").trigger("reset");
               					}, 2000);
							 </script>';
                    } else {
                      echo errormsg::alertmsg('Warning', "The student is already registered!.. "); 
                    }

                } else  {
                    echo errormsg::alertmsg('Warning', "The student is already registered!.. "); 
                }

        }
 	}

  public function modifyRegistration($data){
    $sql = "SELECT * FROM register WHERE regnum = ".$data['regnum']."";
    $studentid = $this->getVal('studentid', $sql);

    $sql = "UPDATE studentinfo SET firstname = '".$data["fname"]."', lastname = '".$data["lname"]."', middlename = '".$data["mname"]."', lrnid = '".$data["lrn"]."' WHERE studentid = '".$studentid."' ";
    if ($this->openqry($sql)) {
      $sql = "UPDATE register SET gradelevel = ".$data['gradelvl'].", scholarshipid = ".$data['granttype']." WHERE regnum = ".$data['regnum']."";
      if ($this->openqry($sql)) {
        $sql = "UPDATE studentcourse  SET curcode = '".$data["coursecurriculum"]."' WHERE studentid = '".$studentid."'";
        if ($this->openqry($sql)) {
          echo errormsg::alertmsg("Success","The Student successfully added..");
        }
      } else {
          echo errormsg::alertmsg('Warning', "The student is already registered!.. ");   
      }
    } else {
          echo errormsg::alertmsg('Warning', "The student is already registered!.. "); 
    }
  }
    
}

$registration = new RegistrationController();
if(isset($_POST["action"])) {



	$register = array(
		$_POST["branchid"],
		$_POST["syid"],
		$_POST["semid"],
		trim($_POST["granttype"]), 
		$_POST["studenttypeid"], 
		trim($_POST["gradeLvl"]),
    trim($_POST["typeReg"])
	);

	// if($_POST["gradeLvl"] == 11 || $_POST["gradeLvl"] == "11"){
	// 	array_push($register, "New");
	// } else {
	// 	array_push($register, "Old");
	// } 

	if ($_POST['action'] == "registrationNew") {

		$studentinfo = array(
	         trim($_POST["lrn"]),
	         strtolower($_POST["lname"]),
	         strtolower($_POST["fname"]),
	         strtolower($_POST["mname"]),
	         $_POST["lrncert"],
	         $_POST["form138"],
	         $_POST["form137"],
	         $_POST["goodmoral"],
	         $_POST["nso"] 
        );
        

		$registration->add_new($studentinfo,$_POST["coursecurriculum"],$register);

	} elseif ($_POST['action'] == "registrationOld") {
		$studentid = $_POST["studentid"];
		$registration->add_old($studentid,$register);
	}
} elseif (isset($_POST["type"]) && $_POST["type"] == "modify_reg") {
  $data = [
    "regnum" => $_POST["regnum"],
    "gradelvl" => $_POST["gradelvl"],
    "granttype" => $_POST["granttype"],
    "coursecurriculum" => $_POST["coursecurriculum"],
    "lrn" => $_POST["lrn"],
    "fname" => $_POST["fname"],
    "lname" => $_POST["lname"],
    "mname" => $_POST["mname"],
    "type" => $_POST["type"]
  ];
  $registration->modifyRegistration($data);
}


?>