<?php 
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');

class GroupController extends DB
{
	use errormsg;
	public function add($groupname, $description){
	 	$sql = "SELECT * FROM grouptype WHERE name IN (SELECT name FROM grouptype WHERE name = '$groupname')";
		if ($this->notEmpty($sql)) {
			echo errormsg::alertmsg('Warning', "The group is already registered!...");
		}else {
			$sql = "INSERT INTO grouptype(name,description) VALUES('$groupname','$description')";
			if ($this->openqry($sql)) {
				echo errormsg::alertmsg('Success', "Successfully Added..");
				echo '
					<script>
						setTimeout(function(){ 
							$("#groupAdd").trigger("reset");
       						$(".wid-fix2").removeAttr("style"); 
       					}, 2000);
					 </script>';
			}
		}
	}

	public function modify($groupname,$description,$grouptypeid){
		$sql = "UPDATE grouptype
			SET name = '$groupname', description = '$description'
			 WHERE grouptypeid = $grouptypeid ";
		if ($this->openqry($sql)) {
			echo errormsg::alertmsg('Success', "Successfully Added..");
		}else {
			echo errormsg::alertmsg('Warning', "Unable to save data!...");
		}
	}

	public function delete($grouptypeid){
		$sql = "SELECT * FROM grouptype WHERE grouptypeid IN (SELECT grouptypeid FROM usergroup WHERE grouptypeid = $grouptypeid) ";
		if($this->notEmpty($sql)){
			echo errormsg::alertmsg('Warning', "Unable to delete data!...");
		} else {
			$sql = "DELETE FROM grouptype WHERE grouptypeid = $grouptypeid";
			if ($this->openqry($sql)) {
				echo errormsg::alertmsg('Success', "Successfully deleted..");
			}
		}
		
	}
}

	$group_controller = new GroupController;

	if(isset($_GET["type"])){

		if($_GET["type"] == "add"){
			$group_controller->add($_GET["groupname"], $_GET["description"]);
		} elseif ($_GET["type"] == "update") {
			$group_controller->modify($_GET["groupname"],$_GET["description"],$_GET["groupid"]);
		} else {
			$group_controller->delete($_GET["groupid"]);
		}
	}

?>