<?php
require '../db_controller/theDBCOnn.php';
include('errorrmsg.php');

class AdviserController extends DB
{
	use errormsg;

	public function controller_adviser($scheduleid,$studentid){
		$count = count($scheduleid);
		$bool = false;
		for ($i=0; $i < $count ; $i++) { 
			$data = $scheduleid[$i];
			$sql = "INSERT INTO studentgrades (scheduleid,studentid) VALUES ($data, '$studentid')";
			if ($this->openqry($sql)) {
				$bool = true;
			} 
		}

		if ($bool) {
			echo errormsg::alertmsg('Success', "Successfully updated..");
		} else {
			echo errormsg::alertmsg('Warning', "Unable to advise data.. ");
		}
	}


	public function controller_changeschedule($type,$data,$gradeid){
		$count = count($data);
		if ($count <= 1 ) {
			$row = $data[0];
			$sql = "UPDATE studentgrades
						SET scheduleid = $row
					WHERE gradeid = $gradeid";
			if ($this->openqry($sql)) {
				echo errormsg::alertmsg('Success', "Successfully updated..");
			} else {
				echo errormsg::alertmsg('Warning', "Unable to change data.. ");
			}
		}else {
			echo errormsg::alertmsg('Warning', "Only one schedule to pick..");
		}
		
	}

	public function sf1_modify_details($data){
		$studentid = explode(',', $data["studentid"]);
        $lrn = explode(',', $data["lrn"]);
        $sex = explode(',', $data["sex"]);
        $firstname = explode(',', $data["firstname"]);
        $lastname = explode(',', $data["lastname"]);
        $middlename = explode(',', $data["middlename"]);
        $dateofbirth = explode(',', $data["dateofbirth"]);
        $placeofbirth = explode(',', $data["placeofbirth"]);
        $province = explode(',', $data["province"]);
        $mothertongue = explode(',', $data["mothertongue"]);
        $ethnic = explode(',', $data["ethnic"]);
        $religion = explode(',', $data["religion"]);
        $streets = explode(',', $data["streets"]);
        $barangay = explode(',', $data["barangay"]);
        $city = explode(',', $data["city"]);
        $fathername = explode(',', $data["fathername"]);
        $mothername = explode(',', $data["mothername"]);
        $guardian_name = explode(',', $data["guardian_name"]);
        $guardian_relationship = explode(',', $data["guardian_relationship"]);
        $mother_mobile = explode(',', $data["mother_mobile"]);
        $remarks = explode(',', $data["remarks"]);
        $bol = false;

        for ($i=0; $i <  sizeof($studentid); $i++) { 
            if(empty(trim($dateofbirth[$i]))) {
                $sql = "UPDATE studentinfo 
	            SET 
	                sex                   = '$sex[$i]',
	                lrnid                 = '$lrn[$i]',
	                province              = '$province[$i]',
	                mothertongue		  = '$mothertongue[$i]',
	                ethnic  			  = '$ethnic[$i]',
	                streets 			  = '$streets[$i]',
	                barangay 			  = '$barangay[$i]',
	                city 				  = '$city[$i]',
	                guardian_name		  = '$guardian_name[$i]',
	                guardian_relationship = '$guardian_relationship[$i]',
	                mother_mobile		  = '$mother_mobile[$i]',
	                religion      		  = '$religion[$i]',
	                mothername            = '$mothername[$i]',
	                fathername  		  = '$fathername[$i]',
	                firstname   		  = '$firstname[$i]',
	                lastname   			  = '$lastname[$i]',
	                middlename    		  = '$middlename[$i]',
	                placeofbirth   		  = '$placeofbirth[$i]',
	                remarks       		  = '$remarks[$i]'
	                WHERE studentid 	  = '$studentid[$i]'";
            }else {
                  $sql = "UPDATE studentinfo 
                	SET 
                    sex        			  = '$sex[$i]',
                    lrnid 				  = '$lrn[$i]',
                    dateofbirth   		  = '$dateofbirth[$i]',
                    province      		  = '$province[$i]',
                    mothertongue		  = '$mothertongue[$i]',
                    ethnic 				  = '$ethnic[$i]',
                    streets 			  = '$streets[$i]',
                    barangay 			  = '$barangay[$i]',
                    city 				  = '$city[$i]',
                    guardian_name 		  = '$guardian_name[$i]',
                    guardian_relationship = '$guardian_relationship[$i]',
                    mother_mobile 		  = '$mother_mobile[$i]',
                    religion              = '$religion[$i]',
                    firstname             = '$firstname[$i]',
                    lastname    		  = '$lastname[$i]',
                    middlename            = '$middlename[$i]',
	                placeofbirth          = '$placeofbirth[$i]',
                    mothername            = '$mothername[$i]',
                    fathername            = '$fathername[$i]',
                    remarks               = '$remarks[$i]'
                    WHERE studentid       = '$studentid[$i]'";
            }
                if($this->openqry($sql)) {
                    $bol = true;
                }
            }

            if ($bol) {
				echo errormsg::alertmsg('Success', "Successfully updated..");
			} else {
				echo errormsg::alertmsg('Warning', "Unable to advise data.. ");
			}
	}


}



$controller = new AdviserController();


if (isset($_POST["type"])) {

	$type = $_POST["type"];
	

	if($type == "controllers"){
		$data = explode(",",$_POST["scheduleid"]);
		$controller->controller_adviser($data, $_POST["studentid"]);
	}elseif ($type == "change_schedule_controller_modify") {
		$data = explode(",",$_POST["scheduleid"]);
		$gradeid = $_POST["scheduleid_modify"];
		$controller->controller_changeschedule($type, $data, $gradeid);
	}elseif ($type == "sf1_modify_details") {
		$controller->sf1_modify_details($_POST);
	}
}
?>