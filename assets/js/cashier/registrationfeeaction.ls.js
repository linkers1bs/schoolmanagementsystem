var total_data = document.forms["reg_fee"]["number_of_data"].value;
    if(total_data != '' || total_data != 0 ){
        for(var i = 1; i < total_data; i++){
            $('#regfee_amount'+i).hide();// input hide
            $('#reg_des'+i).hide();// input hide
            console.log(i);
        }
    }
    $('input[type="checkbox"]').change(function() {
        var check = false;
           for (var i = 1 ; i <= total_data; i++){
                if ($('#regfee'+i).is(':checked')) {
                    $('#regfee_amount'+i).show();
                    $('#reg_des'+i).show();
                    $('#reg_hideamount'+i).hide();
                    $('#reg_showres'+i).hide();
                    check = true;
                }else {
                    $('#regfee_amount'+i).hide();
                    $('#reg_des'+i).hide();
                    $('#reg_hideamount'+i).show();
                    $('#reg_showres'+i).show();
                }
           }
           if (check){
                $('#check_reg_update').show();
                $('#hide_reg').hide();
                $('#show_reg').hide();
            }else{
                $('#show_reg').hide();
              $('#hide_reg').show();
              $('#check_reg_update').hide();
            }
    });

      var i=1;  
      function add_newreg(){
        i++;  
          $('#regfee_table').append('<tr  id="reg_row'+i+'"><td><input style="width:100%!important" type="text" name="description" id="description_reg"/></td><td><input type="number" name="price" id="price_reg"/></td></tr>'); 
          $('#show_reg').show();
          $('#hide_reg').hide();
      }
     function cancel_reg(){
        $('#show_reg').hide();
        $('#hide_reg').show();
        // $('#reg_row'+i).remove();
        $('#regfee_table tr#reg_row'+i).remove();
     }

     function add_newregc_data(){
          var sem = document.forms["reg_fee"]["semid"].value;
          var sy = document.forms["reg_fee"]["syid"].value;
          var branchid = document.forms["reg_fee"]["branchid"].value;
          var studenttypeid = document.forms["reg_fee"]["studenttypeid"].value;
          var scholarshipid = document.forms["reg_fee"]["scholarshipid"].value;
          var description = document.forms["reg_fee"]["description"].value;
          var price = document.forms["reg_fee"]["price"].value;
           if (description != '' ) {
                $('#description_reg').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#description_reg').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
            if (price != '' ) {
            $('#price_reg').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#price_reg').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
           
            if(price != '' && description !=''){
               $.post("controllers/function/FeesController.php",{ 
                    description: description, 
                    price: price,
                    semid: sem,
                    syid: sy,
                    branchid: branchid,
                    studenttypeid: studenttypeid,
                    scholarshipid: scholarshipid,
                    type : "reg_fee",
                    action:"reg_add_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-reg-data').hide();
                });
            }

     }


     $("#update_reg").click(function(){
        var total_data = document.forms["reg_fee"]["number_of_data"].value;
        var regfee = ""; 
        var amount = "";
        if(total_data != '' || total_data != 0 ){
            for(var i = 1; i < total_data; i++){
                regfee = document.forms["reg_fee"]["regfee"+i].value;
                des = document.forms["reg_fee"]["reg_des"+i].value;
                amount = document.forms["reg_fee"]["regfee_amount"+i].value;
                if( $('#regfee'+i).is(':checked')){
                   if (des != '' || amount != '') {
                    $('#reg_des'+i+',#regfee_amount'+i).css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
                    }
                    $.post("controllers/function/FeesController.php",{ 
                    des: des, 
                    amount: amount,
                    regfee: regfee,
                    type : "reg_fee",
                    action : "reg_modify_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-reg  -data').hide();
                });
                }
            }
            
        }
      });