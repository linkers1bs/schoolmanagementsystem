function show_Details_misc(){
  var sem = document.forms["mis_fee"]["semid"].value;
  var sy = document.forms["mis_fee"]["syid"].value;
  var branchid = document.forms["mis_fee"]["branchid"].value;
  var studenttypeid = document.forms["mis_fee"]["studenttypeid"].value;
  var clienttype = document.forms["mis_fee"]["misc_type"].value;
  if(clienttype != 'none'){
       $.ajax({
             type: "GET",
             url: "pages/cashier/forms/form_misc.php",
             data:'semid='+sem+'&syid='+sy+'&branchid='+branchid+'&studenttypeid='+studenttypeid+'&clienttype='+clienttype,
             beforeSend:function(){
              $('#action_loading').show();
             },
             success: function(data){
              $('#action_loading').hide();
               $('#show-misc-data').show();
               $("#show-misc-data").html(data);
             }
     });
   }
}

function show_Details_reg(){
  var sem = document.forms["reg_fee"]["semid"].value;
  var sy = document.forms["reg_fee"]["syid"].value;
  var branchid = document.forms["reg_fee"]["branchid"].value;
  var studenttypeid = document.forms["reg_fee"]["studenttypeid"].value;
  var scholarshipid = document.forms["reg_fee"]["scholarshipid"].value;
  if(scholarshipid != 'none'){
       $.ajax({
             type: "GET",
             url: "pages/cashier/forms/form_reg_fee.php",
             data:'semid='+sem+'&syid='+sy+'&branchid='+branchid+'&studenttypeid='+studenttypeid+'&scholarshipid='+scholarshipid,
              beforeSend:function(){
              $('#action_loading').show();
             },
             success: function(data){
              $('#action_loading').hide();
               $('#show-reg-data').show();
               $("#show-reg-data").html(data);
             }
     });
   }
}

function show_Details_tuition(){
  var sem = document.forms["tuition_fee"]["semid"].value;
  var sy = document.forms["tuition_fee"]["syid"].value;
  var branchid = document.forms["tuition_fee"]["branchid"].value;
  var studenttypeid = document.forms["tuition_fee"]["studenttypeid"].value;
  var scholarshipid = document.forms["tuition_fee"]["scholarshipid"].value;
  if(scholarshipid != 'none'){
       $.ajax({
             type: "GET",
             url: "pages/cashier/forms/form_tuition_fee.php",
             data:'semid='+sem+'&syid='+sy+'&branchid='+branchid+'&studenttypeid='+studenttypeid+'&scholarshipid='+scholarshipid,
              beforeSend:function(){
              $('#action_loading').show();
             },
             success: function(data){
              $('#action_loading').hide();
               $('#show-tuition-data').show();
               $("#show-tuition-data").html(data);
             }
     });
   }
}