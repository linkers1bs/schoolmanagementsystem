function registration_tab(type,num){
  if (type == "modify") {
    $('.tab_modify_data_table'+num).show();
    $('.table_info_show'+num).hide();
  }else if (type == "show") {
    $('.tab_modify_data_table'+num).hide();
    $('.table_info_show'+num).show();
  }
}
    var old = document.getElementById('old');
    var newstudentinfo = document.getElementById('new');
    var others =  document.getElementById('others');
    var granttype1 =  document.getElementById('granttype1');
    var granttype2 =  document.getElementById('granttype2');
    var granttype3 =  document.getElementById('granttype3');
    
    others.onclick = function() {
        document.getElementById('scholarid').style.display = 'inline';
    }
    granttype1.onclick = function() {
        document.getElementById('scholarid').style.display = 'none';
    }
    granttype3.onclick = function() {
        document.getElementById('scholarid').style.display = 'none';
    }
    granttype2.onclick = function() {
        document.getElementById('scholarid').style.display = 'none';
    }
    old.onclick = function() {
      document.getElementById('newstudentinfo').style.display = 'none';
      document.getElementById('oldstudent').style.display = 'inline'
      document.getElementById('hide').style.display = 'block';;
    }
    newstudentinfo.onclick = function() {
      document.getElementById('newstudentinfo').style.display = 'inline';
      document.getElementById('oldstudent').style.display = 'none';
      document.getElementById('hide').style.display = 'block';
    }


function registerStudent(type){
  var syid = document.forms["registrion_form"]["syid"].value;
  var branchid = document.forms["registrion_form"]["branchid"].value;
  var semid = document.forms["registrion_form"]["semid"].value;
  var studenttypeid = document.forms["registrion_form"]["studenttypeid"].value;
  var gradeLvl = document.forms["registrion_form"]["grade"].value;
 
  // var branchid = $('#branchid :selected').val();
  // var syid = $('#syid :selected').val();
  // var semid = $('#semid :selected').val();
  // var studenttypeid = $('#studenttypeid :selected').val();//student type SHS default
  // var gradeLvl = 1;
  var granttype = '';
  var typeReg = document.forms["registrion_form"]["typeReg"].value;
  var requirement = {
      lrn         : false,
      form137     : false,
      form138     : false,
      goodmoral   : false,
      nso         : false

  };
    if ($('#lrncef').is(":checked"))
    {
      requirement.lrn = true;
    }
    if ($('#form137').is(":checked"))
    {
      requirement.form137 = true;
    }    
    if ($('#form138').is(":checked"))
    {
      requirement.form138 = true;
    }
    if ($('#goodmoral').is(":checked"))
    {
      requirement.goodmoral = true;
    }   
    if ($('#nso').is(":checked"))
    {
      requirement.nso = true;
    }     

    if (gradeLvl == '0' || gradeLvl == 0 ) { 
        //if grade level equal to SELECT 
        document.getElementById('require_lvl').style.display = 'inline';
        return false;
    } 

    if($('input[type=radio][name=granttype]:checked').length == 0) {
       document.getElementById('require_type').style.display = 'inline';
      return false;
    } else {
       document.getElementById('require_type').style.display = 'none';
      granttype = document.forms["registrion_form"]["granttype"].value;
    } 

    if (granttype != '') {
    
      if(granttype == 'others') {
          granttype = $('#scholarid :selected').val(); //change value to other scholarship
      } 
    
      if (typeReg == "New")  {
        var lrn = document.forms["registrion_form"]["lrn"].value;
        var lname = document.forms["registrion_form"]["lname"].value;
        var fname = document.forms["registrion_form"]["fname"].value;
        var mname = document.forms["registrion_form"]["mname"].value;//branch
        var coursecurriculum = $('#coursecurriculum :selected').val();


        var validate =  [lrn,lname,fname,mname,coursecurriculum];//add course curriculum here
        var validatehas =  ["lrn","lname","fname","mname","coursecurriculum"];//add course curriculum here
        
        var check = false;
        for (var i = 0; i < validatehas.length ; i++) {
          
          if (validate[i] != "") {
              $('#'+validatehas[i]+'').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
          } else {
            $('#'+validatehas[i]+'').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            check = true;
          }

        }

        if (!(!(!(!(!check))))) {
         
          $.post("controllers/function/RegistrationController.php",{ 
            branchid      : branchid, 
            syid          : syid,
            semid         : semid,
            studenttypeid : studenttypeid,
            gradeLvl      : gradeLvl,
            granttype     : granttype,
            lrn           : lrn,
            lname         : lname,
            fname         : fname,
            mname         : mname,
            coursecurriculum : coursecurriculum,
            lrncert       : requirement.lrn,
            form138       : requirement.form138,
            form137       : requirement.form137,
            goodmoral     : requirement.goodmoral,
            nso           : requirement.nso,
            typeReg       : typeReg,
            action        : 'registrationNew'
            },
            function(data) {
              $("#alert-data").html(data);
              // var contents = escape(data);
              // var n = contents.search("alert-success");
              // if(n < 0){
              //   setTimeout(function(){window.location.reload(); }, 1000);
              // }
              
          });

        }

      } else if (typeReg == "Old")  {
          // var studentid = $('#studentid :selected').val(); 
          var studentid = document.forms["registrion_form"]["studentid"].value;
          $.post("controllers/function/RegistrationController.php",{ 
            branchid      : branchid, 
            syid          : syid,
            semid         : semid,
            studenttypeid : studenttypeid,
            gradeLvl      : gradeLvl,
            granttype     : granttype,
            studentid     : studentid,
            typeReg       : typeReg,
            action        : 'registrationOld'
            },
            function(data) {
              $("#alert-data").html(data);
              // var contents = escape(data);
              // var n = contents.search("alert-success");
              // if(n < 0){
              //   setTimeout(function(){window.location.reload(); }, 1000);
              // }
        });
      }

    }
}

function modifyregistration(type){
    var regnum = document.forms["modify_reg"]["regnum"].value;
    var gradeLvl = document.forms["modify_reg"]["grade"].value;
    var granttype = document.forms["modify_reg"]["granttype"].value;
    var coursecurriculum = document.forms["modify_reg"]["coursecurriculum"].value;
    var lrn = document.forms["modify_reg"]["lrn"].value;
    var fname = document.forms["modify_reg"]["fname"].value;
    var lname = document.forms["modify_reg"]["lname"].value;
    var mname = document.forms["modify_reg"]["mname"].value;
    if(granttype == 'others') {
      granttype = document.forms["modify_reg"]["scholarshipid"]; //change value to other scholarship
    } 
    var validate =  [lrn,fname,lname,mname,coursecurriculum];
    var validatehas =  ["lrn","fname","lname","mname","coursecurriculum"];
    validate_Null_value(validate,validatehas, 'modify_reg');
    if(validate_Null_value(validate,validatehas, 'modify_reg')){
      $.ajax({
        type : "POST",
        url: 'controllers/function/RegistrationController.php',
        data:'regnum='+regnum+'&gradelvl='+gradeLvl+'&granttype='+granttype+'&coursecurriculum='+coursecurriculum+'&lrn='+lrn+'&fname='+fname+'&lname='+lname+'&mname='+mname+'&type=modify_reg',
        success:function(data){
          $('#alert-data').html(data);
        }
      });
    }
}

function registration_viewInfo(type){
  $.ajax({
    type: 'POST',
    url : 'pages/cashier/forms/form_reg.php',
    data:$('#modify_reg').serialize(),
    beforeSend: function(){
      $('#action_loading').show();
    },
    success: function(data){
      $('#action_loading').hide();
      $('#show_modify_reg').html(data);
    }
  });
}