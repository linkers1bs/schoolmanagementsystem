 var total_data = document.forms["tuition_fee"]["number_of_data"].value;
    if(total_data != '' || total_data != 0 ){
        for(var i = 1; i < total_data; i++){
            $('#amount'+i).hide();// input hide
            $('#des'+i).hide();// input hide
        }
    }
    $('input[type="checkbox"]').change(function() {
        var check = false;
           for (var i = 1 ; i <= total_data; i++){
                if ($('#chek'+i).is(':checked')) {
                    $('#amount'+i).show();
                    $('#des'+i).show();
                    $('#hideamount'+i).hide();
                    $('#showres'+i).hide();
                    check = true;
                }else {
                    $('#amount'+i).hide();
                    $('#des'+i).hide();
                    $('#hideamount'+i).show();
                    $('#showres'+i).show();
                }
           }
           if (check){
                $('#check_tuition_update').show();
              $('#hide_tuition').hide();
              $('#show_tuition_button').hide();
            }else{
                $('#show_tuition_button').hide();
            $('#hide_tuition').show();
            $('#check_tuition_update').hide();
            }
    });

     var i=1;  
      function add_newtution(){
        i++;  
           $('#tuition_table').append('<tr  id="tui'+i+'"><td><input style="width:100%!important" type="text" name="description" id="description"/></td><td><input type="number" name="price" id="price"/></td></tr>'); 
          $('#show_tuition_button').show();
          $('#hide_tuition').hide();
      }
     function cancel_tuition(){
        $('#show_tuition_button').hide();
        $('#hide_tuition').show();
        $('#tuition_table tr#tui'+i).remove();
        show_Details_tuition();
     }


      function add_newtution_data(){
          var sem = document.forms["tuition_fee"]["semid"].value;
          var sy = document.forms["tuition_fee"]["syid"].value;
          var branchid = document.forms["tuition_fee"]["branchid"].value;
          var studenttypeid = document.forms["tuition_fee"]["studenttypeid"].value;
          var scholarshipid = document.forms["tuition_fee"]["scholarshipid"].value;
          var description = document.forms["tuition_fee"]["description"].value;
          var price = document.forms["tuition_fee"]["price"].value;
           if (description != '' ) {
                $('#description').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#description').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
            if (price != '' ) {
            $('#price').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#price').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
           
            if(price != '' && description !=''){
               $.post("controllers/function/FeesController.php",{ 
                    description: description, 
                    price: price,
                    semid: sem,
                    syid: sy,
                    branchid: branchid,
                    studenttypeid: studenttypeid,
                    scholarshipid: scholarshipid,
                    type : "tuition_fee",
                    action:"tuition_add_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-tuition-data').hide();
                });
            }

     }

      $("#update_tuition").click(function(){
        var total_data = document.forms["tuition_fee"]["number_of_data"].value;
        var tutionid = ""; 
        var amount = "";
        if(total_data != '' || total_data != 0 ){
            for(var i = 1; i < total_data; i++){
                tutionid = document.forms["tuition_fee"]["chek"+i].value;
                des = document.forms["tuition_fee"]["des"+i].value;
                amount = document.forms["tuition_fee"]["amount"+i].value;
                if( $('#chek'+i).is(':checked')){
                   if (des != '' || amount != '') {
                    $('#des'+i+',#amount'+i).css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
                    }
                    $.post("controllers/function/FeesController.php",{ 
                    des: des, 
                    amount: amount,
                    tutionid: tutionid,
                    type : "tuition_fee",
                    action : "tui_modify_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-tuition-data').hide();
                });
                }
            }
            
        }
      });