
  function type_Billing(type) {
    var branchid = document.forms["create_billing"]["branchid"].value;
    var syid =document.forms["create_billing"]["syid"].value;
    var semid = document.forms["create_billing"]["semid"].value;
    var type_billing = document.forms["create_billing"]["type_billing"].value;
    document.getElementById('show-bill-data').style.display = 'none';
    if(branchid != 'none' && syid != 'none' && semid != 'none' && type_billing != 'none') {
      $.ajax({
        type: 'POST',
        url: 'pages/cashier/forms/form_allbilling.php',
        data:'branchid='+branchid+'&syid='+syid+'&semid='+semid+'&type_billing='+type_billing,
        beforeSend:function(){
          $('#action_loading').show();
        },
        success: function(data){
          $('#action_loading').hide();
          if (type_billing == 'all') {
            $("#student-bill-data").hide();
            document.getElementById('show-bill-data').style.display = 'block';
             $("#show-bill-data").html(data);

          }else {
             $("#student-bill-data").show();
             $("#student-bill-data").html(data);
          }
        } 

      });
    } else {
      document.getElementById('student-bill-data').style.display = 'none';
    }
    // document.getElementById('show5-data').style.display = 'none';
        
  }

  function billStudent(name) {
    var branchid = document.forms["create_billing"]["branchid"].value;
    var syid =document.forms["create_billing"]["syid"].value;
    var semid = document.forms["create_billing"]["semid"].value;
    var type_billing = document.forms["create_billing"]["type_billing"].value;
    var regnum =  document.forms["create_billing"]["regnum"].value;
    if(regnum != 'none' || regnum != "none") {
          $.ajax({
            type: 'POST',
            url:'pages/cashier/forms/form_billing.php',
            data:'branchid='+branchid+'&syid='+syid+'&semid='+semid+'&regnum='+regnum,
            beforeSend:function(){
              $('#action_loading').show();
            },
            success: function(data){
              $('#action_loading').hide();
              $("#student-bill-data").show();
              $("#show-bill-data").show();
              $("#show-bill-data").html(data);
            }
          });

    }
}

function lvlGrade(name) {
    var branchid = document.forms["create_billing"]["branchid"].value;
    var syid =document.forms["create_billing"]["syid"].value;
    var semid = document.forms["create_billing"]["semid"].value;
    var type_billing = document.forms["create_billing"]["type_billing"].value;
    var lvl =  document.forms["create_billing"]["lvl"].value;
  if(lvl != 'none' || lvl != "none") {
      document.getElementById('show-bill-data').style.display = 'block';
    document.getElementById('student-bill-data').style.display = 'block';
        $.ajax({
            type: 'POST',
            url:'pages/cashier/forms/form_billing.php',
            data:'branchid='+branchid+'&syid='+syid+'&semid='+semid+'&lvl='+lvl,
            beforeSend:function(){
              $('#action_loading').show();
            },
            success: function(data){
              $('#action_loading').hide();
              $("#student-bill-data").show();
              $("#show-bill-data").html(data);
            }
          });
  }

}


function billScholarship(name) {
    var branchid = document.forms["create_billing"]["branchid"].value;
    var syid =document.forms["create_billing"]["syid"].value;
    var semid = document.forms["create_billing"]["semid"].value;
    var type_billing = document.forms["create_billing"]["type_billing"].value;
    var scholarshipid =  document.forms["create_billing"]["scholarshipid"].value;
  if(scholarshipid != 'none' || scholarshipid != "none") {
    document.getElementById('show-bill-data').style.display = 'block';
    document.getElementById('student-bill-data').style.display = 'block';
    $.ajax({
            type: 'POST',
            url:'pages/cashier/forms/form_billing.php',
            data:'branchid='+branchid+'&syid='+syid+'&semid='+semid+'&scholarshipid='+scholarshipid,
            beforeSend:function(){
              $('#action_loading').show();
            },
            success: function(data){
              $('#action_loading').hide();
              $("#student-bill-data").show();
              $("#show-bill-data").html(data);
            }
          });

  }

}


function billBlock(){
    var branchid = document.forms["create_billing"]["branchid"].value;
    var syid =document.forms["create_billing"]["syid"].value;
    var semid = document.forms["create_billing"]["semid"].value;
    var type_billing = document.forms["create_billing"]["type_billing"].value;
    var blockname =  document.forms["create_billing"]["blockname"].value;
    if(blockname != 'none' || blockname != "none") {
      document.getElementById('show-bill-data').style.display = 'block';
      document.getElementById('student-bill-data').style.display = 'block';
      $.ajax({
        type: 'POST',
        url:'pages/cashier/forms/form_billing.php',
        data:'branchid='+branchid+'&syid='+syid+'&semid='+semid+'&blockname='+blockname,
        beforeSend:function(){
          $('#action_loading').show();
        },
        success: function(data){
          $('#action_loading').hide();
          $("#student-bill-data").show();
          $("#show-bill-data").html(data);
        }
      });
    }
}

    // $(function() {
    //     $('#grade').on('change', function() {
    //         $(this).closest('fieldset').find('#studid').toggle(this.checked);
    //         $(this).closest('fieldset').find('#spanhidden').toggle(!this.checked);
          
    //     });
    //      $('#duo').on('change', function() {
    //         $(this).closest('fieldset').find('#date').toggle(this.checked);
          
    //     });
    // });
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}