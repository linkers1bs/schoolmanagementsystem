 var total_data = document.forms["mis_fee"]["number_of_data"].value;
    if(total_data != '' || total_data != 0 ){
        for(var i = 1; i < total_data; i++){
            $('#amount'+i).hide();// input hide
            $('#des'+i).hide();// input hide
        }
    }
    $('input[type="checkbox"]').change(function() {
        var check = false;
           for (var i = 1 ; i <= total_data; i++){
                if ($('#chek'+i).is(':checked')) {
                    $('#amount'+i).show();
                    $('#des'+i).show();
                    $('#hideamount'+i).hide();
                    $('#showres'+i).hide();
                    check = true;
                }else {
                    $('#amount'+i).hide();
                    $('#des'+i).hide();
                    $('#hideamount'+i).show();
                    $('#showres'+i).show();
                }
           }
           if (check){
               	$('#check_misc_update').show();
          		$('#hide_misc').hide();
          		$('#show_misc').hide();
            }else{
                $('#show_misc').hide();
         		$('#hide_misc').show();
         		$('#check_misc_update').hide();
            }
    });

          
      var i=1;  
      function add_newmisc(){
      	i++;  
          $('#misctableadd_table').append('<tr  id="rowmis_add'+i+'"><td><input style="width:100%!important" type="text" name="description" id="description"/></td><td><input type="number" name="price" id="price"/></td></tr>'); 
          $('#show_misc').show();
          $('#hide_misc').hide();
      }
     
    
      function cancel_mis(){
        $('#show_misc').hide();
        $('#hide_misc').show();
        $('#rowmis_add'+i).remove();
     }
      function add_newmisc_data(){
      	  var sem = document.forms["mis_fee"]["semid"].value;
		  var sy = document.forms["mis_fee"]["syid"].value;
		  var branchid = document.forms["mis_fee"]["branchid"].value;
		  var studenttypeid = document.forms["mis_fee"]["studenttypeid"].value;
		  var clienttype = document.forms["mis_fee"]["misc_type"].value;
       	  var description = document.forms["mis_fee"]["description"].value;
       	  var price = document.forms["mis_fee"]["price"].value;
       
            if (description != '' ) {
                $('#description').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#description').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
            if (price != '' ) {
            $('#price').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#price').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
            if(price != '' && description !=''){
                $.post("controllers/function/FeesController.php",{ 
                    description: description, 
                    price: price,
                    semid: sem,
                    syid: sy,
                    branchid: branchid,
                    studenttypeid: studenttypeid,
                    clienttype: clienttype,
                    type : "misc_fee",
                    action:"misc_add_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-misc-data').hide();
                    // setTimeout(function(){window.location.reload(); }, 2000);
                });
            }
      }

      $("#update_misc").click(function(){
      	 	var total_data = document.forms["mis_fee"]["number_of_data"].value;
	        var miscid = ""; 
	        var amount = "";
	        var tes = 0;
	        if(total_data != '' || total_data != 0 ){
	            for(var i = 1; i < total_data; i++){
	                miscid = document.forms["mis_fee"]["chek"+i].value;
	                des = document.forms["mis_fee"]["des"+i].value;
	                amount = document.forms["mis_fee"]["amount"+i].value;
	                if( $('#chek'+i).is(':checked')){
	                   if (des != '' || amount != '') {
	                    $('#des'+i+',#amount'+i).css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
	                    }
	                   $.post("controllers/function/FeesController.php",{ 
	                    des: des, 
	                    amount: amount,
	                    miscid: miscid,
	                    type : "misc_fee",
	                    action : "misc_modify_fee"
	                    },
	                    function(data) {
	                    $("#alert-data").html(data);
	                     $('#show-misc-data').hide();
	                    // setTimeout(function(){window.location.reload(); }, 2000);
	                });
	                }
	            }
	            
	        }
      });