function getStudentName(type) {
  var branchid = document.forms["student_account"]["branchid"].value;
  var syid = document.forms["student_account"]["syid"].value;
  var semid = document.forms["student_account"]["semid"].value;
  var scholarshipid = document.forms["student_account"]["scholarshipid"].value;
  var studenttypeid = document.forms["student_account"]["studenttypeid"].value;
  $('#show-student-data').hide();
  $('#show-accountsudent-info').hide();
  if(type == "student_getname"){
    $.ajax({
      type: 'POST',
      url : 'pages/cashier/action/get_studentaccount.php',
      data: 'branchid='+branchid+'&syid='+syid+'&semid='+semid+'&scholarshipid='+scholarshipid+'&studenttypeid='+studenttypeid+'&type='+type,
      success: function (data){
        $('#show-student-data').show();
        $('#show-student-data').html(data);
      }
    });
  }
 
}


function getStudentAccounts() {
  $('#show-accountsudent-info').hide();
  var regnum = document.forms["student_account"]["regnum"].value;
  if(regnum != 'none') {
      $.ajax({
        type : 'POST',
        url  : 'pages/cashier/forms/form_studentaccount.php',
        data : 'regnum='+regnum,
        beforeSend: function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#show-accountsudent-info').show();
          $('#action_loading').hide();
          $('#show-accountsudent-info').html(data)
        }
      }); 
  }

}





