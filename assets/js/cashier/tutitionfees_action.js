var total_data = document.forms["tuition_fee"]["number_of_data"].value;
    if(total_data != '' || total_data != 0 ){
        for(var i = 1; i < total_data; i++){
            $('#amount_tution'+i).hide();// input hide
            $('#des_tuition'+i).hide();// input hide
        }
    }
    $('input[type="checkbox"]').change(function() {
        var check = false;
           for (var i = 1 ; i <= total_data; i++){
                if ($('#tuition_chek'+i).is(':checked')) {
                    $('#amount_tution'+i).show();
                    $('#des_tuition'+i).show();
                    $('#hideamount_tuition'+i).hide();
                    $('#show_tuition'+i).hide();
                    check = true;
                }else {
                    $('#amount_tution'+i).hide();
                    $('#des_tuition'+i).hide();
                    $('#hideamount_tuition'+i).show();
                    $('#show_tuition'+i).show();
                }
           }
           if (check){
                $('#check_tuition_update').show();
                $('#hide_tuition').hide();
                $('#show_tuition_button').hide();
            }else{
                $('#show_tuition_button').hide();
                $('#hide_tuition').show();
                $('#check_tuition_update').hide();
            }
    });

     var i=1;  
      function add_newtution(){
        i++;  
           $('#tuition_table').append('<tr  id="tuition_row'+i+'"><td><input style="width:100%!important" type="text" name="description_tuition" id="description_tuition"/></td><td><input type="number" name="price_tuition" id="price_tuition"/></td></tr>'); 
          $('#show_tuition_button').show();
          $('#hide_tuition').hide();
      }
     function cancel_tuition(){
        $('#show_tuition_button').hide();
        $('#hide_tuition').show();
        $('#tuition_row'+i).remove();
     }
     
     function add_newtution_data(){
          var sem = document.forms["tuition_fee"]["semid"].value;
          var sy = document.forms["tuition_fee"]["syid"].value;
          var branchid = document.forms["tuition_fee"]["branchid"].value;
          var studenttypeid = document.forms["tuition_fee"]["studenttypeid"].value;
          var scholarshipid = document.forms["tuition_fee"]["scholarshipid"].value;
          var description = document.forms["tuition_fee"]["description_tuition"].value;
          var price = document.forms["tuition_fee"]["price_tuition"].value;
           if (description != '' ) {
                $('#description_tuition').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#description_tuition').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
            if (price != '' ) {
            $('#price_tuition').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            }else {
                $('#price_tuition').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }
           
            if(price != '' && description !=''){
               $.post("controllers/function/FeesController.php",{ 
                    description: description, 
                    price: price,
                    semid: sem,
                    syid: sy,
                    branchid: branchid,
                    studenttypeid: studenttypeid,
                    scholarshipid: scholarshipid,
                    type : "tuition_fee",
                    action:"tuition_add_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-tuition-data').hide();
                });
            }

     }

      $("#update_tuition").click(function(){
        var total_data = document.forms["tuition_fee"]["number_of_data"].value;
        var tutionid = ""; 
        var amount = "";
        if(total_data != '' || total_data != 0 ){
            for(var i = 1; i < total_data; i++){
                tutionid = document.forms["tuition_fee"]["tuition_chek"+i].value;
                des = document.forms["tuition_fee"]["des_tuition"+i].value;
                amount = document.forms["tuition_fee"]["amount_tution"+i].value;
                if( $('#tuition_chek'+i).is(':checked')){
                   if (des != '' || amount != '') {
                    $('#des_tuition'+i+',#amount_tution'+i).css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
                    }
                    $.post("controllers/function/FeesController.php",{ 
                    des: des, 
                    amount: amount,
                    tutionid: tutionid,
                    type : "tuition_fee",
                    action : "tui_modify_fee"
                    },
                    function(data) {
                    $("#alert-data").html(data);
                    $('#show-tuition-data').hide();
                });
                }
            }
            
        }
      });