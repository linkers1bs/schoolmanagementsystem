function group_manage(type) { 
  if(type == "add"){
    var name = document.forms["groupAdd"]["groupname"].value;;
    var description  = document.forms["groupAdd"]["description"].value;
    var validate =  [groupname,description];
    var validatehas =  ["groupname","description"];
    validate_Null_value(validate,validatehas, 'groupAdd');
    if(validate_Null_value(validate,validatehas, 'groupAdd')){
        $.ajax({          
          type: "GET",
          url: "controllers/function/Groupcontroller.php",
          data:$( "#groupAdd" ).serialize()+'&type='+type,
           beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
             $("#alert-data").html(data);
             $('#action_loading').hide();   
              var contents = escape(data);
              var n = contents.search("alert-warning");
              if(n < 0){
                $("#group_table").append("<tr><td class='remarks'>"+name+"</td><td class='remarks'>"+description+"</td></tr>");
                // $("#branch-list").append("<option value='"+branchname+"'>"+branchname+"</option>");
                // $("#delete_branch_id").append("<option value='"+branchname+"'>"+branchname+"</option>");
              } else {
                $(".groupAdd").find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
              }
         }
       });
    }
  } else if (type == "update") {
    var groupid = document.forms["groupModify"]["groupid"].value;
    var name = document.forms["groupModify"]["groupname"].value;
    var description  = document.forms["groupModify"]["description"].value;
    var validate =  [groupname,description];
    var validatehas =  ["groupname","description"];
    validate_Null_value(validate,validatehas, 'groupModify');
    if(validate_Null_value(validate,validatehas, 'groupModify')){
        $.ajax({          
          type: "GET",
          url: "controllers/function/Groupcontroller.php",
          data:$( "#groupModify" ).serialize()+'&type='+type,
           beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
             $("#alert-data").html(data);
             $('#action_loading').hide();   
              var contents = escape(data);
              var n = contents.search("alert-warning");
              if(n < 0){
               $("form").trigger("reset");
               $("#group-data").hide();
               $("#group-list option[value='"+groupid+"']").text(name);
               $("#delete_group_id option[value='"+groupid+"']").text(name);
              } else {
                $(".groupModify").find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
              }
         }
       });
    }

  } else {
      var groupid = document.forms["groupDel"]["groupid"].value;
      $.ajax({          
        type: "GET",
        url: "controllers/function/Groupcontroller.php",
        data:'type='+type+'&groupid='+groupid,
         beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $("#alert-data").html(data);
          $('#action_loading').hide();   
          var contents = escape(data);
          var n = contents.search("alert-warning");
          if(n < 0){
            $("form").trigger("reset"); // reset all data
            $("#delete-data-group").hide();
            $("#group-list option[value='"+groupid+"']").remove();//remove modify option 
            $("#delete_group_id option[value='"+groupid+"']").remove(); //remove delete option
            $("#group"+groupid).remove() // remove row table
          }
        }
      });
  }

}