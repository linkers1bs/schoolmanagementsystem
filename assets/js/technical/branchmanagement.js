function branch_mange(type) { 
  if(type == "add"){
    var branchname = document.forms["branchAdd"]["branchname"].value;;
    var address  = document.forms["branchAdd"]["address"].value;
    var landline = document.forms["branchAdd"]["landline"].value;
    var mobile   = document.forms["branchAdd"]["mobile"].value;
    var validate =  [branchname,address,landline,mobile];
    var validatehas =  ["branchname","address","landline","mobile"];
    validate_Null_value(validate,validatehas, 'branchAdd');
    if(validate_Null_value(validate,validatehas, 'branchAdd')){
        $.ajax({          
          type: "GET",
          url: "controllers/function/BranchController.php",
          data:$( "#branchAdd" ).serialize()+'&type='+type,
           beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
             $("#alert-data").html(data);
             $('#action_loading').hide();   
             var contents = escape(data);
              var n = contents.search("alert-warning");
              if(n < 0){
                $("#branch_table").append("<tr><td class='remarks'>"+branchname+"</td><td class='remarks'>"+address+"</td><td class='remarks'>"+landline+"</td><td class='remarks'>"+mobile+"</td></tr>");
                // $("#branch-list").append("<option value='"+branchname+"'>"+branchname+"</option>");
                // $("#delete_branch_id").append("<option value='"+branchname+"'>"+branchname+"</option>");
              } else {
                $(".branchAdd").find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
              }
         }
       });
    }
  } else if (type == "update") {
    var branchid = document.forms["branchModify"]["branchid"].value;
    var branchname = document.forms["branchModify"]["branchname"].value;
    var address  = document.forms["branchModify"]["address"].value;
    var landline = document.forms["branchModify"]["landline"].value;
    var mobile   = document.forms["branchModify"]["mobile"].value;
    var validate =  [branchname,address,landline,mobile];
    var validatehas =  ["branchname","address","landline","mobile"];
    validate_Null_value(validate,validatehas, 'branchModify');
    if(validate_Null_value(validate,validatehas, 'branchModify')){
        $.ajax({          
          type: "GET",
          url: "controllers/function/BranchController.php",
          data:$( "#branchModify" ).serialize()+'&type='+type,
           beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
             $("#alert-data").html(data);
             $('#action_loading').hide();   
              var contents = escape(data);
              var n = contents.search("alert-warning");
              if(n < 0){
               $("form").trigger("reset");
               $("#branch-data").hide();
               $("#branch-list option[value='"+branchid+"']").text(branchname);
               $("#delete_branch_id option[value='"+branchid+"']").text(branchname);
              } else {
                $(".branchModify").find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
              }
         }
       });
    }

  } else {
      var branchid = document.forms["branchDelete"]["branchid"].value;
      $.ajax({          
        type: "GET",
        url: "controllers/function/BranchController.php",
        data:'type='+type+'&branchid='+branchid,
         beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $("#alert-data").html(data);
          $('#action_loading').hide();   
          var contents = escape(data);
          var n = contents.search("alert-warning");
          if(n < 0){
            $("form").trigger("reset");
            $("#delete-branch-data").hide(); 
            $("#branch-list option[value='"+branchid+"']").remove();
            $("#delete_branch_id option[value='"+branchid+"']").remove();
            $("#subject_row"+branchid).remove()
          }
        }
      });
  }

}