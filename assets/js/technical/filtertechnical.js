////////////////////////////THIS CONTAINER FOR THE USERINFO/////////////////////////////////////////////////////////////////
// VIEW USERINFO FILTER SELECTION
function getFilter() {
   var str = $('#filter-list :selected').val();
   if (str != 'none') {
      $.ajax({          
        type: "GET",
        url: "pages/technical/forms/form_user.php",
        data:'branchname='+str,
        beforeSend: function() {
            $('#action_loading').show();    
        },
        success: function(data){
          $("#filter-data").html(data);
          $('#action_loading').hide();
          $("#filter-data").show();
        }
    });
  }
}

//GET USER INFO TO MODIFY
function getUSername() {
  var str = $('#username-list :selected').val();   
  if (str != 'none') {
    $("#username-data").show(); 
    $.ajax({          
        type: "GET",
        url: "pages/technical/forms/form_user.php",
        data:'usernameid='+str,
        beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $("#username-data").html(data);
          $('#action_loading').hide();
        }
    });
  }else {
      $("#username-data").hide(); 
  }
}
//GET USERNAME To delete
function getDeleteuser() {
  var str = $('#username-list1 :selected').val();   
  if (str != 'none') {
     $("#username-data1").show(); 
    $.ajax({          
      type: "GET",
        url: "pages/technical/forms/form_user.php",
        data:'usernameid='+str,
        beforeSend: function() {
          $('#action_loading').show();    
      },
      success: function(data){
        $("#username-data1").html(data);
        $('#action_loading').hide();
      }
    });
  }else {
    $("#username-data1").hide();
  }
}

////====================MEMBER OF===============================////
//hide MEMBEROF
$('.tab_member_data_table').hide();
$('.show_memberof').click(function(){
  $('.tab_member_data_table').show();
  $('.tab_add_data_table1').hide();
  $('.table_info_show1').hide();
  $('.tab_modify_data_table1').hide();
  $('.tab_delete_data_table1').hide();
});


function getMember() {
  var str = $('#branc-list :selected').val();   
  if (str != 'none') {      
    $('#show_member_table').hide()
      $.ajax({          
        type: "GET",
        url: "pages/technical/forms/form_user.php",
        data:'branchid='+str+'&type=memberof',
        success: function(data){
          $("#selectuser-data").html(data);
        }
    });
 }
}

function show_bydetails_member(){
  var str = $('#usernamememeber :selected').val();
  var branchid = $('#branc-list :selected').val();   
  if(str != 'none' && branchid != 'none'){
    $("#show_member_table").show(); 
    $.ajax({
        type: "GET",
        url: "pages/technical/action/view_member.php",
        data:'user='+str+'&type=show_info_member&branchid='+branchid,
        beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $("#show_member_table").html(data);
          $('#action_loading').hide();
        }
    });
  }
}

///====================END OF MEMBER============================////
////////////////////////////END ON THE CONTAINER FOR THE USERINFO/////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////THIS CONTAINER FOR THE BRANCH/////////////////////////////////////////////////////////////////
function getbranch() {
    var str = $('#branch-list :selected').val();
    $("#branch-data").hide();
    if (str != 'none') {           
        $.ajax({          
          type: "GET",
          url: "pages/technical/forms/form_branch.php",
          data:'branchid='+str,
          beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
             $("#branch-data").show();
             $("#branch-data").html(data);
             $('#action_loading').hide();
          }
        });
    }
}

function get_delete_branch(){
   var str = $('#delete_branch_id :selected').val();
    $("#delete-branch-data").hide();
    if (str != 'none') {           
        $.ajax({          
          type: "GET",
          url: "pages/technical/forms/form_branch.php",
          data:'branchid='+str,
          beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
             $("#delete-branch-data").show();
             $("#delete-branch-data").html(data);
             $('#action_loading').hide();
          }
        });
    }
}

//////////////////////////////END OF THIS CONTAINER FOR THE BRANCH/////////////////////////////////////////////////////////////////

//////////////////////////////THIS CONTAINER FOR THE GROUP MANAGEMENT/////////////////////////////////////////////////////////////////
function getGroup() {
     var str = $('#group-list :selected').val();
      if (str != 'none') {      
        $.ajax({          
                type: "GET",
                url: "pages/technical/forms/form_group.php",
                data:'grouptypeid='+str,
                 beforeSend: function() {
                  $('#action_loading').show();    
                },
                success: function(data){
                  $('#group-data').show();
                  $("#group-data").html(data);
                  $('#action_loading').hide();
                }
        });
     }else {
      $('#group-data').hide();
     }
}

function getGroup_to_Delete() {
  var str = $('#delete_group_id :selected').val();
  if (str != 'none') {    
    $.ajax({          
      type: "GET",
      url: "pages/technical/forms/form_group.php",
      data:'grouptypeid='+str,
      beforeSend: function() {
        $('#action_loading').show();    
      },
      success: function(data){
        $('#delete-data-group').show();
        $("#delete-data-group").html(data);
        $('#action_loading').hide();
      }
    });
  } else {
    $('#delete-data-group').hide();
  }
}
//////////////////////////////END OF THIS CONTAINER FOR THE GROUP MANAGEMENT/////////////////////////////////////////////////////////////////