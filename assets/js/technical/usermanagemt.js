
function user(type){
  if (type == 'add') {
    var username = document.forms["userAdd"]["username"].value;
    var pwd = document.forms["userAdd"]["pwd"].value;
    var pwd2 = document.forms["userAdd"]["pwd2"].value;
    var branchid = document.forms["userAdd"]["branchid"].value;//branch
    var grouptypeid = document.forms["userAdd"]["grouptypeid"].value;//member of
    var email = document.forms["userAdd"]["email"].value;
    var lastname = document.forms["userAdd"]["lastname"].value;
    var firstname = document.forms["userAdd"]["firstname"].value;
    var middlename = document.forms["userAdd"]["middlename"].value;
    var position = document.forms["userAdd"]["position"].value;
    var validate =  [branchid,username,pwd,pwd2,grouptypeid,lastname,firstname,middlename,email,position];
    var validatehas =  ["branchid","username","pwd","pwd2","grouptypeid","lastname","firstname","middlename","email","position"];
    validate_Null_value(validate,validatehas, 'userAdd');
    if(validate_Null_value(validate,validatehas, 'userAdd')){
        $.ajax({          
        type: "GET",
        url: "controllers/function/UserController.php",
        data:$( "#userAdd" ).serialize()+'&type='+type,
        beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $("#alert-data").html(data);
          $('#action_loading').hide();   
          var contents = escape(data);
          var n = contents.search("alert-warning");
          if(n < 0){
            $("#username-list").append("<option value='"+username+"'>"+username+"</option>");
            $("#username-list1").append("<option value='"+username+"'>"+username+"</option>");
          }
        }
        });
        if (pwd2 !=  pwd) {
          $('#pwd,#pwd2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
        }
    }

  } else if (type == 'delete') {
      var userid = document.forms["deleteUser"]["username1"].value;
       $.ajax({          
          type: "GET",
          url: "controllers/function/UserController.php",
          data:'type='+type+'&userid='+userid,
           beforeSend: function() {
            $('#action_loading').show();    
          },
          success: function(data){
            $("#alert-data").html(data);
            $('#action_loading').hide();   
            var contents = escape(data);
            var n = contents.search("alert-warning");
            if(n < 0){
              $("form").trigger("reset");
              $("#username-data1").hide(); 
              $("#username-list option[value='"+userid+"']").remove();
              $("#username-list1 option[value='"+userid+"']").remove();
            }
          }
        });
  } else {
    //UPDATE
    var userid = document.forms["modifyUser"]["username1"].value;
    var username = document.forms["modifyUser"]["username"].value;
    var branchid = document.forms["modifyUser"]["branchid"].value;//branch
    // var grouptypeid = document.forms["modifyUser"]["grouptypeid"].value;//member of
    var email = document.forms["modifyUser"]["email"].value;
    var lastname = document.forms["modifyUser"]["lastname"].value;
    var firstname = document.forms["modifyUser"]["firstname"].value;
    var middlename = document.forms["modifyUser"]["middlename"].value;
    var position = document.forms["modifyUser"]["position"].value;
    var validate =  [branchid,username,lastname,firstname,middlename,email,position];
    var validatehas =  ["branchid","username","lastname","firstname","middlename","email","position"];

    validate_Null_value(validate,validatehas, 'modifyUser');
    if(validate_Null_value(validate,validatehas,'modifyUser')){
         $.ajax({          
            type: "GET",
            url: "controllers/function/UserController.php",
            data:$( "#modifyUser" ).serialize()+'&type='+type,
             beforeSend: function() {
              $('#action_loading').show();    
            },
            success: function(data){
              $("#alert-data").html(data);
              $('#action_loading').hide();   
              var contents = escape(data);
              var n = contents.search("alert-warning");
              if(n < 0){
                $("form").trigger("reset");
                $("#username-data").hide(); 
              }
            }
          });
    }
  }

}

function action_user_member(clicked_id,type){
  if(type == "add"){
      var username = document.forms["memberof"]["usernamepk"].value;
      var grouptypeid = $('#grouptypeid-list' + clicked_id + ' :selected').val();
      var name = $( "#grouptypeid-list" + clicked_id + " option:selected" ).text();
       $.ajax({          
        type: "GET",
        url: "controllers/function/UserController.php",
        data:'type_member='+type+'&username='+username+'&gid='+grouptypeid+'&memberof=memberof',
         beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $('#action_loading').hide();   
          $("#alert-data").html(data);
          var contents = escape(data);
          var n = contents.search("alert-warning");
          var i = clicked_id + 1;
          if(n < 0){
           $('#table_member_show').append('<tr id=row'+i+'><td class="remarks">'+name+'</td><td><span title="Add Group" data-toggle="tooltip" data-placement="top"><a href="#" class="btn btn-default btn-sm"  data-toggle="modal" data-target="#addmodal'+i+''+name+'"><i class="fa fa-plus"></i></a></span> | <span title="Delete Group" data-toggle="tooltip" data-placement="top"><a href="#" class="btn btn-default btn-sm"  data-toggle="modal" data-target="#delete'+i+''+name+'"><i class="fa fa-times"></i></a></span> | <select class="btn btn-default btn-sm"  style="width: 85px!important" name="typbo'+i+'[]" id="typbo-list'+i+'"><option selected>Active</option><option selected>Inactive</option></select><span></a></span></td></tr>'); 
          }
          setTimeout(function(){
            $('#show_member_table').hide();
            $('form').trigger("reset");
          },1000);
        }
      });
  }else if(type == "action"){
    var username = document.forms["memberof"]["usernamepk"].value;
    var type_action = document.forms["memberof"]["typbo"+clicked_id].value;
    var grouptypeid = document.forms["memberof"]["grouptypeid"+clicked_id].value;
    console.log(username + ' ' + type_action + ' ' + grouptypeid);
    $.ajax({          
      type: "GET",
      url: "controllers/function/UserController.php",
      data:'type_member='+type+'&username='+username+'&gid='+grouptypeid+'&memberof=memberof&type_action='+type_action,
       beforeSend: function() {
        $('#action_loading').show();    
      },
      success: function(data){
        $('#action_loading').hide();   
        $("#alert-data").html(data);
      }
      });
  }
  else {
    //delete memberof 
      var username = document.forms["memberof"]["usernamepk"].value;
      var grouptypeid = document.forms["memberof"]["grouptypeid"+clicked_id].value;
      var name = document.forms["memberof"]["grouptypeid"+clicked_id].text;
       $.ajax({          
        type: "GET",
        url: "controllers/function/UserController.php",
        data:'type_member='+type+'&username='+username+'&gid='+grouptypeid+'&memberof=memberof',
         beforeSend: function() {
          $('#action_loading').show();    
        },
        success: function(data){
          $('#action_loading').hide();   
          $("#alert-data").html(data);
          var contents = escape(data);
          var n = contents.search("alert-warning");
          if(n < 0){
            $('#row'+clicked_id).remove();

          }
          
        }
      });
  }
}
