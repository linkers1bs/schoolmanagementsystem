
function change_shedule_name(type) {
  // alert( $('#print_cor').serialize());
  if (type == "select") {
    $.ajax({
      type : 'POST',
      url : 'pages/adviser/forms/form_change_schedule.php',
      data : $('#change_shedule').serialize()+'&type=studentname',
      beforeSend: function(){
        $('#action_loading').show();
      },
      success :function (data){
        $('#action_loading').hide();
        $('#display_change_shedule_name').html(data);
      }

    });

  } else if (type == "student_view") {
      var regnum = document.forms["change_shedule"]["regnum"].value;
      if (regnum != "none") {
         $('#display_change_shedule').show();
         $.ajax({
          type : 'POST',
          url : 'pages/adviser/forms/form_change_schedule.php',
          data : $('#change_shedule').serialize()+'&type=show_change_schedule',
          beforeSend: function(){
            $('#action_loading').show();
          },
          success :function (data){
            $('#action_loading').hide();
            $('#display_change_shedule').html(data);
          }

        });
       } else {
          $('#display_change_shedule').hide();
       }
  }
    
}

function show_cor_student(){
  // alert( $('#print_cor').serialize());
  var regnum = document.forms["print_cor"]["regnum"].value;
  if (regnum != "none") {
     $('#display_print_cor').show();
     $.ajax({
      type : 'POST',
      url : 'pages/adviser/forms/form_print_cor.php',
      data : $('#print_cor').serialize()+'&type=studentdata',
      beforeSend: function(){
        $('#action_loading').show();
      },
      success :function (data){
        $('#action_loading').hide();
        $('#display_print_cor').html(data);
      }

    });
   }else {
      $('#display_print_cor').hide();
   }
   
}


function change_shedule_name_modify_view(){
    var regnum = document.forms["change_shedule"]["scheduleid_modify"].value;
    if (regnum != "none") {
      $('#show_change_schedule_modify').show();
       $.ajax({
          type : 'POST',
          url : 'pages/adviser/forms/form_change_schedule.php',
          data : $('#change_shedule').serialize()+'&type=show_change_schedule_modify',
          beforeSend: function(){
            $('#action_loading').show();
          },
          success :function (data){
            $('#action_loading').hide();
            $('#show_change_schedule_modify').html(data);
          }

        });
     } else {
       $('#show_change_schedule_modify').hide();
     }
       
}

function change_schedule_controller(){
  var scheduleid = [];
  $.each($("input[name='checkbox_change_schedule']:checked"), function(){    
    scheduleid.push($(this).val());
  }); 
  var regnum = document.forms["change_shedule"]["scheduleid_modify"].value;
    $.ajax({
        type : 'POST',
        url  : 'controllers/function/AdviserController.php',
        data : $('#change_shedule').serialize()+'&scheduleid='+scheduleid+'&type=change_schedule_controller_modify',
        beforeSend : function(){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $("#alert-data").html(data);
        }
      });
}
