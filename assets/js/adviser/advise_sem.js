function adviser_student_details(type){
		if(type == "register_student"){
	      var data_html = "adviser_regnum_student";
	      $.ajax({
	        type : 'POST',
	        url  : 'pages/adviser/forms/forms_adviser.php',
	        data : $('#adviser_form_details').serialize()+'&type=register_student',
	        beforeSend:function (){
	          $('#action_loading').show();
	        },
	        success:function(data){
	          $('#action_loading').hide();
	          $('#'+data_html).html(data);
              $('#adviser_advice_student_details').hide();
              $('#adviser_regnum_student').show();
	        }
	      });
	    } else if (type == "student_view") {
	    	var regnum = document.forms["adviser_form_details"]["regnum"].value;
	    	var data_html = "adviser_advice_student_details";
	    	if(regnum != "none"){
	    		$('#'+data_html).show();
			      $.ajax({
			        type : 'POST',
			        url  : 'pages/adviser/forms/forms_adviser.php',
			        data : $('#adviser_form_details').serialize()+'&type=student_details',
			        beforeSend:function (){
			          $('#action_loading').show();
			        },
			        success:function(data){
			          $('#action_loading').hide();
			          $('#'+data_html).html(data);
		        }
		      });
	    	} else {
	    		$('#'+data_html).hide();
	    	}
	    	
	    }
	}
	
function show_block_offer(){
	$('#controller_save').hide();
	$('.advise_subject_new').hide();
    $.each($("input[name='scheduleid']:checked"), function(){    
        $('#show_subje'+$(this).val()).show();
        $('#scheduleid_advise'+$(this).val()).attr( 'checked', 'checked' );
        $('#controller_save').show();
    }); 
	
}

function controller_advise(){
	var scheduleid = [];
	var studentid= $('#get_advise_studentid').val();
	$.each($("input[name='get_id_scdhe']:checked"), function(){    
       scheduleid.push($(this).val());
    }); 
	
    $.ajax({
    	type : 'POST',
    	url  : 'controllers/function/AdviserController.php',
    	data : 'scheduleid='+scheduleid+'&studentid='+studentid+'&type=controllers',
    	beforeSend : function(){
    		$('#action_loading').show();
    	},
    	success : function(data){
    		$('#action_loading').hide();
    		$("#alert-data").html(data);
    		// $('#adviser_advice_student_details').hide();
    		// $('#adviser_regnum_student').hide();
    	}
    }).done(function (){
    	var regnum = document.forms["adviser_form_details"]["regnum"].value;
    	var data_html = "adviser_advice_student_details";
    	if(regnum != "none"){
    		$('#'+data_html).show();
		      $.ajax({
		        type : 'POST',
		        url  : 'pages/adviser/forms/forms_adviser.php',
		        data : $('#adviser_form_details').serialize()+'&type=student_details',
		        beforeSend:function (){
		          $('#action_loading').show();
		        },
		        success:function(data){
		          $('#action_loading').hide();
		          $('#'+data_html).html(data);
	        }
	      });
    	} else {
    		$('#'+data_html).hide();
    	}
    });
}

