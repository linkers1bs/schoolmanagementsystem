function subjectCode() {
  var subjectcode = document.forms["subject_form_modify"]["subjectcode"].value;
    if (subjectcode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/formsubject.php",
          data:'subjectcode='+subjectcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $("#show_subject_modify").show();
            $('#action_loading').hide();
            $("#show_subject_modify").html(data);
          }
        });
    } else {
       $("#show_subject_modify").hide();
    }
}

function subject_delete(){
  var subjectcode = document.forms["subject_form_delete"]["subjectcode"].value;
    if (subjectcode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/formsubject.php",
          data:'subjectcode='+subjectcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
            $("#show_subject_delete").show(); 
            $("#show_subject_delete").html(data);
          }
        });
    } else {
      $("#show_subject_delete").hide();
    }
}

function room_modify() {
  var roomcode = document.forms["room_form_update"]["roomcode_select_modify"].value;
    if (roomcode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/form_room.php",
          data:'roomcode='+roomcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
             $("#show_room_modiy").show();
            $("#show_room_modiy").html(data);
          }
        });
    } else {
      $("#show_room_modiy").hide();
    }
}

function room_delete() {
  var roomcode = document.forms["room_form_delete"]["roomcode_select_delete"].value;
    if (roomcode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/form_room.php",
          data:'roomcode='+roomcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
            $("#show_room_delete").show();
            $("#show_room_delete").html(data);
          }
        });
    } else {
      $("#show_room_delete").hide();
    }
}


function courseModify(){
   var coursecode = document.forms["form_course_update"]["coursecode_modify"].value;
    if (coursecode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/form_course.php",
          data:'coursecode='+coursecode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
             $("#show_course_modify").show();
            $("#show_course_modify").html(data);
          }
        });
    } else {
      $("#show_course_modify").hide();
    }
}

function courseDelete(){
  var coursecode = document.forms["form_course_delete"]["coursecode_delete"].value;
    if (coursecode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/form_course.php",
          data:'coursecode='+coursecode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
             $("#show_course_delete").show();
            $("#show_course_delete").html(data);
          }
        });
    } else {
      $("#show_course_delete").hide();
    }
}



function curriculum_delete(){
 var curriculumcode = document.forms["curriculum_form_delete"]["curcode_delte"].value;
    if (curriculumcode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/form_curriculum.php",
          data:'curriculumcode='+curriculumcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
             $("#show_curriculum_delete").show();
            $("#show_curriculum_delete").html(data);
          }
        });
    } else {
      $("#show_curriculum_delete").hide();
    }
}

function curriculum_modify(){
 var curriculumcode = document.forms["curriculum_form_update"]["curcode_modify"].value;
    if (curriculumcode != 'none') {      
        $.ajax({          
          type: "GET",
          url: "pages/management/forms/form_curriculum.php",
          data:'curriculumcode='+curriculumcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
             $("#show_curriculum_modify").show();
            $("#show_curriculum_modify").html(data);
          }
        });
    } else {
      $("#show_curriculum_modify").hide();
    }
}


