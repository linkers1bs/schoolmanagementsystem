function subject_controller(type){
  if (type == "add") {
    var form_name = "subject_form_add";
    var subjectInformation = {
        sub_code     : document.forms[form_name]["sub_code"].value,
        description  : document.forms[form_name]["description"].value,
        total_unit   : document.forms[form_name]["total_unit"].value,
        lecture      : document.forms[form_name]["lecture"].value,    
        lab          : document.forms[form_name]["lab"].value,
        price        : document.forms[form_name]["price"].value,  
        remarks      : document.forms[form_name]["remarks"].value,
      }
    var validate =  [subjectInformation.sub_code,subjectInformation.description,subjectInformation.total_unit,subjectInformation.lecture,subjectInformation.lab,subjectInformation.price];
    var validatehas =  ["sub_code","description","total_unit","lecture","lab","price"];

    validate_Null_value(validate,validatehas,form_name);
    if(validate_Null_value(validate,validatehas,form_name)){
        $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=subject&type='+type+'&subjectcode='+sub_code,
          beforeSend : function (){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);

            var contents = escape(data);
            var n = contents.search("alert-warning");
            if(n < 0){
              $("#subjectcode").trigger("reset");
              $("#subject_table").append("<tr id='subject_row'"+subjectInformation.sub_code+"><td class='remarks'>"+subjectInformation.sub_code+"</td><td class='remarks'>"+subjectInformation.description+"</td><td>"+subjectInformation.total_unit+"</td><td>"+subjectInformation.lecture+"</td><td>"+subjectInformation.lab+"</td><td>"+subjectInformation.price+"</td><td class='remarks'>"+subjectInformation.remarks+"</td></tr>");
              $("#subjectcode").append("<option value='"+subjectInformation.sub_code+"'>"+subjectInformation.sub_code+"</option>");
            } else {
              $("."+form_name).find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }



          }
        });
    }

  } else if (type == "update") {
    var form_name = "subject_form_modify";
    var subjectInformation = {
        sub_code     : document.forms[form_name]["sub_code"].value,
        description  : document.forms[form_name]["description"].value,
        total_unit   : document.forms[form_name]["total_unit"].value,
        lecture      : document.forms[form_name]["lecture"].value,    
        lab          : document.forms[form_name]["lab"].value,
        price        : document.forms[form_name]["price"].value,  
        remarks      : document.forms[form_name]["remarks"].value,
      }
    var validate =  [subjectInformation.sub_code,subjectInformation.description,subjectInformation.total_unit,subjectInformation.lecture,subjectInformation.lab,subjectInformation.price];
    var validatehas =  ["sub_code","description","total_unit","lecture","lab","price"];

    validate_Null_value(validate,validatehas,form_name);
    if(validate_Null_value(validate,validatehas,form_name)){
        $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=subject&type='+type,
          beforeSend : function (){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
          }
        });
    }

  } else if (type =="delete") {
    var form_name = "subject_form_delete";
    var subjectInformation = {
        sub_code     : document.forms[form_name]["sub_code"].value,
        description  : document.forms[form_name]["description"].value,
        total_unit   : document.forms[form_name]["total_unit"].value,
        lecture      : document.forms[form_name]["lecture"].value,    
        lab          : document.forms[form_name]["lab"].value,
        price        : document.forms[form_name]["price"].value,  
        remarks      : document.forms[form_name]["remarks"].value,
      }
    var validate =  [subjectInformation.sub_code,subjectInformation.description,subjectInformation.total_unit,subjectInformation.lecture,subjectInformation.lab,subjectInformation.price];
    var validatehas =  ["sub_code","description","total_unit","lecture","lab","price"];

    validate_Null_value(validate,validatehas,form_name);
    if(validate_Null_value(validate,validatehas,form_name)){
        $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=subject&type='+type,
          beforeSend : function (){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
            var contents = escape(data);
            var n = contents.search("alert-warning");
            if(n < 0){
              $("#subjectcode").trigger("reset");
              $("#show_subject_delete").hide(); 
              $("#subjectcode option[value='"+subjectInformation.sub_code+"']").remove();
              $("#subject_row"+subjectInformation.sub_code).remove()
            }
          }
        });
    }
  }
  
}


function room_manage(type){
  var form_name = "";
  if (type == "add") {
     form_name = "room_form_add";
  } else if (type =="modify") {
     form_name = "room_form_update";
  } else if (type == "delete") {
     form_name = "room_form_delete";
  }
  var roomInformation = {
      roomcode     : document.forms[form_name]["roomcode"].value,
      roomnum      : document.forms[form_name]["roomnum"].value,
      building     : document.forms[form_name]["building"].value,
      address      : document.forms[form_name]["address"].value,
      floornum     : document.forms[form_name]["floornum"].value,
      remarks      : document.forms[form_name]["remarks"].value
  };
  var validate =  [roomInformation.roomcode,
                   roomInformation.roomnum,
                   roomInformation.building,
                   roomInformation.address,
                   roomInformation.floornum
                   ];
  var validatehas =  ["roomcode","roomnum","building","address","floornum"];
  validate_Null_value(validate,validatehas,form_name);
  if(roomInformation.remarks != '') {
    $('.'+form_name).find('#remarks').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
  }

  if(validate_Null_value(validate,validatehas,form_name)){

    if (type == "add") {
        $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=room&type='+type,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
            var contents = escape(data);
            var n = contents.search("alert-warning");
            if(n < 0){
              $("#room_table").append("<tr id='room_row"+roomInformation.roomcode+"'><td class='remarks'>"+roomInformation.roomcode+"</td><td>"+roomInformation.roomnum+"</td><td>"+roomInformation.floornum+"</td><td class='remarks'>"+roomInformation.building+"</td><td class='remarks'>"+roomInformation.address+"</td><td class='remarks'>"+roomInformation.remarks+"</td></tr>");
              $("#roomcode_select_modify").append("<option value='"+roomInformation.roomcode+"'>"+roomInformation.roomcode+"</option>");
              $("#roomcode_select_delete").append("<option value='"+roomInformation.roomcode+"'>"+roomInformation.roomcode+"</option>");
            } else {
              $("."+form_name).find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            }

          }

        });
    } else if (type =="modify") {

      $.ajax({

        type : 'POST',
        url  : "controllers/function/ManagementController.php",
        data : $('#'+form_name).serialize()+'&file=room&type='+type,
        beforeSend : function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $('#alert-data').html(data);
        }

      });

    } else if (type == "delete") {
      $.ajax({
        type : 'POST',
        url  : 'controllers/function/ManagementController.php',
        data : $('#'+form_name).serialize()+'&file=room&type='+type,
        beforeSend : function(){
          $('#action_loading').show();
        },
        success: function(data){
          $('#action_loading').hide();
          $('#alert-data').html(data);
          var contents = escape(data);
          var n = contents.search("alert-warning");
            if(n < 0){
              $("#roomcode_select_delete").trigger("reset");
              $("#show_room_delete").hide(); 
              $("#roomcode_select_delete option[value='"+roomInformation.roomcode+"']").remove();
              $("#roomcode_select_modify option[value='"+roomInformation.roomcode+"']").remove();
              $("#room_row"+roomInformation.roomcode).remove()
            }
        }
      });
    }

  }
      
    
}




function course_manage(type){
   var form_name = "";
  if (type == "add") {
     form_name = "form_course_add";
  } else if (type =="update") {
     form_name = "form_course_update";
  } else if (type == "delete") {
     form_name = "form_course_delete";
  }
  var courseInformation = {
      coursename : document.forms[form_name]["coursename"].value,
      numyears   : document.forms[form_name]["numyears"].value,
      track      : document.forms[form_name]["track"].value,
      strand     : document.forms[form_name]["strand"].value,
  
  };
  var validate =  [
    courseInformation.coursename,
    courseInformation.numyears,
    courseInformation.track,
    courseInformation.strand
  ];
  var validatehas =  ["coursename","numyears","track","strand"];
  validate_Null_value(validate,validatehas,form_name);

  if(validate_Null_value(validate,validatehas,form_name)){

      if (type == "add") {

          $.ajax({
              type : 'POST',
              url  : "controllers/function/ManagementController.php",
              data : $('#'+form_name).serialize()+'&file=course&type='+type,
              beforeSend : function(){
                $('#action_loading').show();
              },
              success : function (data){
                $('#action_loading').hide();
                $('#alert-data').html(data);
                var contents = escape(data);
                var n = contents.search("alert-warning");
                if(n < 0){
                  $("#course_table").append("<tr id='course_"+courseInformation.coursename+"'><td class='remarks'>"+courseInformation.coursename+"</td><td>"+courseInformation.numyears+"</td><td class='remarks'>"+courseInformation.track+"</td><td class='remarks'>"+courseInformation.strand+"</td></tr>");
                  // $("#coursecode_modify").append("<option value='"+courseInformation.roomcode+"'>"+courseInformation.roomcode+"</option>");
                  // $("#coursecode_delete").append("<option value='"+courseInformation.roomcode+"'>"+courseInformation.roomcode+"</option>");
                  setTimeout(function(){ 
                  $("#form_course_add").trigger("reset");
                  $("#form_course_add").find(".wid-fix2").removeAttr("style");
                    }, 2000);
                } else {
                  $("."+form_name).find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
                }

              }

            });

      } else if (type =="update") {

          $.ajax({

          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=course&type='+type,
          beforeSend : function (){
            $('#action_loading').show();
          },
          success : function(data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
          }

        });

      } else if (type == "delete") {
          var id = document.forms[form_name]["coursecode_delete"].value;
          $.ajax({
          type : 'POST',
          url  : 'controllers/function/ManagementController.php',
          data : $('#'+form_name).serialize()+'&file=course&type='+type,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
            var contents = escape(data);
            var n = contents.search("alert-warning");
              if(n < 0){
                $("#form_course_delete").trigger("reset");
                $("#show_course_delete").hide(); 
                $("#coursecode_delete option[value='"+id+"']").remove();
                $("#coursecode_modify option[value='"+id+"']").remove();
                $("#course_"+id).remove()
              }
          }
        });
      }
  }
 

}





function curriculum_manage(type){
  var form_name = "";
  if (type == "add") {
     form_name = "curriculum_form_add";
  } else if (type =="update") {
     form_name = "curriculum_form_update";
  } else if (type == "delete") {
     form_name = "curriculum_form_delete";
  }

  var curriculum = {
      curricode  : document.forms[form_name]["curricode"].value,
      description: document.forms[form_name]["description"].value,
      remarks    : document.forms[form_name]["remarks"].value,
      coursecode : document.forms[form_name]["coursecode"].value,
  };

  var validate =  [
    curriculum.curricode,
    curriculum.description,
    curriculum.coursecode
  ];
  var validatehas =  ["curricode","description","coursecode"];
  validate_Null_value(validate,validatehas,form_name);

  if(validate_Null_value(validate,validatehas,form_name)){

      if (type == "add") {
          $.ajax({
            type : 'POST',
            url  : "controllers/function/ManagementController.php",
            data : $('#'+form_name).serialize()+'&file=curriculum&type='+type,
            beforeSend : function(){
              $('#action_loading').show();
            },
            success : function (data){
              $('#action_loading').hide();
              $('#alert-data').html(data);
              var contents = escape(data);
              var n = contents.search("alert-warning");
              if(n < 0){
                setTimeout(function(){ 
                $("#curriculum_form_add").trigger("reset");
                $("#curriculum_form_add").find(".wid-fix2").removeAttr("style");
                  }, 2000);
              } else {
                $("."+form_name).find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
              }

            }

          });
      } else if (type =="update") {
          $.ajax({
            type : 'POST',
            url  : "controllers/function/ManagementController.php",
            data : $('#'+form_name).serialize()+'&file=curriculum&type='+type,
            beforeSend : function (){
              $('#action_loading').show();
            },
            success : function(data){
              $('#action_loading').hide();
              $('#alert-data').html(data);
            }
        });
      } else if (type == "delete") {
        var id = document.forms[form_name]["curcode_delte"].value;
          $.ajax({
          type : 'POST',
          url  : 'controllers/function/ManagementController.php',
          data : $('#'+form_name).serialize()+'&file=curriculum&type='+type,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
            var contents = escape(data);
            var n = contents.search("alert-warning");
              if(n < 0){
                $("#curriculum_form_delete").trigger("reset");
                $("#show_curriculum_delete").hide(); 
                $("#curcode_modify option[value='"+id+"']").remove();
                $("#curcode_delte option[value='"+id+"']").remove();
                $("#curriculum_"+id).remove()
              }
          }
        });
      }

  }

 

}



function sectioning_controller(type){
  if (type == "add") {
    var form_name = "section_form_add";
    var sectionInformation = {
        section_name     : document.forms[form_name]["section_name"].value,
        adviser  : document.forms[form_name]["adviser"].value,
        task   : document.forms[form_name]["task"].value,
      }
    var validate =  [sectionInformation.section_name,sectionInformation.adviser];
    var validatehas =  ["section_name","adviser"];

    validate_Null_value(validate,validatehas,form_name);
    if(validate_Null_value(validate,validatehas,form_name)){
        $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=sectioning&type='+type,
          beforeSend : function (){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);

            // var contents = escape(data);
            // var n = contents.search("alert-warning");
            // if(n < 0){
            //   $("#subjectcode").trigger("reset");
            //   $("#subject_table").append("<tr id='subject_row'"+sectionInformation.sub_code+"><td class='remarks'>"+sectionInformation.sub_code+"</td><td class='remarks'>"+sectionInformation.description+"</td><td>"+sectionInformation.total_unit+"</td><td>"+sectionInformation.lecture+"</td><td>"+sectionInformation.lab+"</td><td>"+sectionInformation.price+"</td><td class='remarks'>"+sectionInformation.remarks+"</td></tr>");
            //   $("#subjectcode").append("<option value='"+sectionInformation.sub_code+"'>"+sectionInformation.sub_code+"</option>");
            // } else {
            //   $("."+form_name).find('.wid-fix2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
            // }



          }
        });
    }

  } 
  
}

