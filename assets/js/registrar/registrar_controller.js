function sy_controller(type){
	/*console.log($('#sy_form_add').serialize());*/
	if (type == "add") {
		var form_name = "sy_form_add";
		var syInformation = {
			sy     : document.forms[form_name]["sy"].value,
		}
		var validate = [syInformation.sy];
		var validatehas = ["sy"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=sy&type='+type,
				beforeSend : function (){
	              $('#action_loading').show();
	            },
	            success : function (data){
		        $('#action_loading').hide();
		        $('#alert-data').html(data);
	        	}
			});
		}
	} else if (type == "update") {
		// console.log($('#sy_form_modify').serialize());
		var form_name = "sy_form_modify";
		var syInformation = {
			sy     : document.forms[form_name]["sy"].value,
		}
		var validate = [syInformation.sy];
		var validatehas = ["sy"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=sy&type='+type,
				beforeSend : function (){
	            $('#action_loading').show();
	            },
	            success : function (data){
		        $('#action_loading').hide();
		        $('#alert-data').html(data);
	        }
			});
		}
	} else if (type == "delete") {
		// console.log($('#sy_form_modify').serialize());
		var form_name = "sy_form_delete";
		var syInformation = {
			sy     : document.forms[form_name]["sy"].value,
		}
		var validate = [syInformation.sy];
		var validatehas = ["sy"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=sy&type='+type,
				beforeSend : function (){
	            $('#action_loading').show();
	            },
	            success : function (data){
		        $('#action_loading').hide();
		        $('#alert-data').html(data);
	        	}
			});
		}
	}

}


function scholar_controller(type) {

	// console.log($('#scholar_form_add').serialize());
	if (type == "add") {
		var form_name = "scholar_form_add";
		var scholarshipInformation = {
			name    : document.forms[form_name]["name"].value,
		}
		var validate = [scholarshipInformation.name];
		var validatehas = ["name"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=name&type='+type,
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#alert-data').html(data);
				}
			});
		}
	} else if (type == "update") {
		// console.log($('#scholar_form_modify').serialize());
		var form_name = "scholar_form_modify";
		var scholarshipInformation = {
			name   : document.forms[form_name]["name"].value,
		}
		var validate = [scholarshipInformation.name];
		var validatehas = ["name"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : 'controllers/function/RegistrarController.php',
				data : $('#'+form_name).serialize()+'&file=name&type='+type,
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#alert-data').html(data);
				}
			});
		}
	} else if (type == "delete") {
		// console.log($('#scholar_form_delete').serialize());
		var form_name = "scholar_form_delete";
		var scholarshipInformation = {
			name   : document.forms[form_name]["name"].value,
		}
		var validate = [scholarshipInformation.name];
		var validatehas = ["name"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : 'controllers/function/RegistrarController.php',
				data : $('#'+form_name).serialize()+'&file=name&type='+type,
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#alert-data').html(data);
				}
			});
		}
	}

}


function studenttype_controller(type){

	// console.log($('#studenttype_form_add').serialize());
	if (type == "add") {
		var form_name = "studenttype_form_add";
		var studenttypeInformation = {
			studtypename    : document.forms[form_name]["studtypename"].value,
		}
		var validate = [studenttypeInformation.studtypename];
		var validatehas = ["studtypename"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=studtypename&type='+type,
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#alert-data').html(data);
				}
			});
		}
	} else if (type == "update") {
		console.log($('#studenttype_form_modify').serialize());
		var form_name = "studenttype_form_modify";
		var studenttypeInformation = {
			studtypename    : document.forms[form_name]["studtypename"].value,
		}
		var validate = [studenttypeInformation.studtypename];
		var validatehas = ["studtypename"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=studtypename&type='+type,
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#alert-data').html(data);
				}
			});
		}
	} else if (type == "delete") {
		console.log($('#studenttype_form_delete').serialize());
		var form_name = "studenttype_form_delete";
		var studenttypeInformation = {
			studtypename    : document.forms[form_name]["studtypename"].value,
		}
		var validate = [studenttypeInformation.studtypename];
		var validatehas = ["studtypename"];

		validate_Null_value(validate,validatehas,form_name);
		if (validate_Null_value(validate,validatehas,form_name)) {
			$.ajax({
				type : 'POST',
				url  : "controllers/function/RegistrarController.php",
				data : $('#'+form_name).serialize()+'&file=studtypename&type='+type,
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#alert-data').html(data);
				}
			});
		}
	}
	
}
