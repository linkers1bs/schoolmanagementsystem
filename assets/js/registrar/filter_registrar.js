function sy_Modify(){
	// console.log($('#sy_form_modify').serialize());
	var sy = document.forms["sy_form_modify"]["syid"].value;
	if (sy!= 'none') {
        $.ajax({          
          type: "GET",
          url: "pages/registrar/forms/form_sy.php",
          data:'syid='+sy,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $("#show_sy_modify").show();
            $('#action_loading').hide();
            $("#show_sy_modify").html(data);
          }
        });
    } else {
       $("#show_sy_modify").hide();
    }
}

function sy_Delete(){
	// console.log($('#sy_form_delete').serialize());
	var sy = document.forms["sy_form_delete"]["syid"].value;
	if (sy!= 'none') {
        $.ajax({          
          type: "GET",
          url : "pages/registrar/forms/form_sy.php",
          data: 'syid='+sy,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function(data){
            $("#show_sy_delete").show();
            $('#action_loading').hide();
            $("#show_sy_delete").html(data);
          }
        });
    } else {
       $("#show_sy_delete").hide();
    }
}

function scholar_Modify(){
  console.log($('#scholar_form_modify').serialize());
  var name = document.forms["scholar_form_modify"]["scholarshipid"].value;
  if (name!= 'none') {
    $.ajax({
      type : "GET",
      url  : "pages/registrar/forms/form_scholarship.php",
      data : 'scholarshipid='+name,
      beforeSend : function(){
        $('#action_loading').show();
      },
      success : function(data){
        $('#action_loading').hide();
        $('#show_scholar_modify').show();
        $('#show_scholar_modify').html(data);
      }
    });
  } else {
    $("#show_scholar_modify").hide();
  }
}

function scholar_Delete(){
  console.log($('#scholar_form_delete').serialize());
  var name = document.forms["scholar_form_delete"]["scholarshipid"].value;
  if (name!= 'none') {
    $.ajax({
      type : "GET",
      url  : "pages/registrar/forms/form_scholarship.php",
      data : 'scholarshipid='+name,
      beforeSend : function(){
        $('#action_loading').show();
      },
      success : function(data){
        $('#action_loading').hide();
        $('#show_scholar_delete').show();
        $('#show_scholar_delete').html(data);
      }
    });
  } else {
    $("#show_scholar_delete").hide();
  }
}

function studtype_Modify(){
  console.log($('#studenttype_form_modify').serialize());
  var studtypename = document.forms["studenttype_form_modify"]["studenttypeid"].value;
  if (studtypename!= 'none') {
    $.ajax({
      type : "GET",
      url  : "pages/registrar/forms/form_studenttype.php",
      data : 'studenttypeid='+studtypename,
      beforeSend : function(){
        $('#action_loading').show();
      },
      success : function(data){
        $('#action_loading').hide();
        $('#show_studtype_modify').show();
        $('#show_studtype_modify').html(data);
      }
    });
  } else {
    $("#show_studtype_modify").hide();
  }
}

function studtype_Delete(){
  console.log($('#studenttype_form_delete').serialize());
  var studtypename = document.forms["studenttype_form_delete"]["studenttypeid"].value;
  if (studtypename!= 'none') {
    $.ajax({
      type : "GET",
      url  : "pages/registrar/forms/form_studenttype.php",
      data : 'studenttypeid='+studtypename,
      beforeSend : function(){
        $('#action_loading').show();
      },
      success : function(data){
        $('#action_loading').hide();
        $('#show_studtype_delete').show();
        $('#show_studtype_delete').html(data);
      }
    });
  } else {
    $("#show_studtype_delete").hide();
  }
}