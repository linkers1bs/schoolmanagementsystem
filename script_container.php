<?php 

    if(isset($_GET["page"])) {

        if ($_GET["page"] == 'technical'){
            ?>
            <script src="assets/js/technical/filtertechnical.js"></script>
            <script src="assets/js/technical/usermanagemt.js"></script>
            <script src="assets/js/technical/groupmanagement.js"></script>
            <script src="assets/js/technical/branchmanagement.js"></script>
            <?php
        }else if ($_GET["page"] == 'cashier'){
            ?>
            <script src="assets/js/cashier/registration.js"></script>
            <script src="assets/js/cashier/studentaccount.js"></script>
            <script src="assets/js/cashier/create_billing.js"></script>
            <script src="assets/js/cashier/filterfees.js"></script>
            <?php
        }else if ($_GET["page"] == 'management'){
            ?>
            <script src="assets/js/management/management_controller.js"></script>
            <script src="assets/js/management/filter_management.js"></script>
            <?php
        } elseif ($_GET["page"] == "adviser") {
            ?>
            <script src="assets/js/adviser/advise_sem.js"></script>
            <script src="assets/js/adviser/print_cor.js"></script>
            <script src="assets/js/adviser/change_schedule.js"></script>
            <?php
        } elseif ($_GET["page"] == "registrar") {
          ?>
            <script src="assets/js/registrar/filter_registrar.js"></script>
            <script src="assets/js/registrar/registrar_controller.js"></script>
          <?php
        }
        //insert javascript here
    }

?>
