<?php 
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
include('controller_container.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SMS</title>
    <link rel="stylesheet" type="text/css" href="assets/css/app.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" >
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/registration.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/management.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/form137.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/myaccount.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/technical.css">
    <link rel="stylesheet" type="text/css" href="assets/css/v2/card.css">
</head>
<div id="loading"></div>
 
<body id="app">
    
    <div class="main_body_All">
    <header>
       	<?php include 'partials/_header.php'; ?>
    </header>
    <div id="main">
        
        <main>
        <div id="alert-data">
           
        </div>    
            <?php 

                getPage();
              
        	  ?>
        </main>
    </div>

    <footer>
    	<?php include 'partials/_footer.php' ?>
    </footer>
   
    <script src="assets/js/app.js"></script>
    <script src="assets/js/jquery.timeago.js"></script>
    <script src="assets/js/custom.js"></script> 
    <script src="assets/js/helper.js"></script>
    <?php 
      require 'script_container.php';
    ?>
    </div>
</body>
</html>
