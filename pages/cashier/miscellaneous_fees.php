<div class="tablecon mis_fee">
  <form name="mis_fee" id="mis_fee">
    <div class="enroll">
    <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
           <div class="container_select">
              <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                  }
                ?>
              </select>
            </div>
            <div class="container_select">
                <span>School Year:&nbsp;&nbsp;</span>
                <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("schoolyear");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  ?>
                </select>
            </div>
             <div class="container_select">
                <span>Semester:&nbsp;&nbsp;</span>
                <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails("semester");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                  }
                ?>
                </select>
              </div>
               <div class="container_select">
                  <span>Student Type:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("studenttype");
                    while ($r = pg_fetch_assoc($query)){
                       $select = '';
                      if($r["studenttypeid"] == "2"){
                        $select = 'selected';
                      }
                      echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
                    }
                  ?>
                  </select>
               </div>
               <div class="container_select">
                  <span>Type:&nbsp;&nbsp;</span>
                 <select class="wid-fix" style="width: 100px!important" name="misc_type" id="misc_type" style="display: inline-block;">
                      <option value="none">Select..</option>
                      <option value="New">New</option>
                      <option value="Old">Old</option>
                  </select>
               </div>
               <div class="container_select">
                 <span><button  type="button" id="select" class='button_local' onclick="show_Details_misc()">View</button></span>
               </div>
        </div>
      </div>
    </div>
 
        <div id="show-misc-data" style="display:block;width:100%;">
                  <!-- DISPLAY misc -->
        </div> 
  </form>
</div>


