<style>
    .collection_button {
      border:none!important;
      background-color: none;
    }
</style>
<div class="tablecon payment_histroy">
  <form name="payment_histroy" id="payment_histroy">
    <div class="enroll">
      <b class="line_r">Collection Report</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
             <span>This display the cashiers collections per scholl year, semester, monthly and days&nbsp;&nbsp;</span>
          </div>
        </div>
      </div>
    </div>
      <br>
        <table cellspacing="0" style="margin-bottom: 0px;  border-collapse: collapse; border :none;">
           <tr align="center">
            <th colspan="8" class="remarks" style="border:none!important;background-color: #f2f3f4;">BY SCHOOL YEAR:</th>
          </tr>
          <tr align="center">
            <th>#</th>
            <th>School Year</th>
            <th>Collectables</th>
            <th>Collected</th>
            <th>Un-Collected</th>
            <th>Previous Balance Collectables</th>
            <th>Collected Previous Balance</th>
            <th>Un-Collected Previous Balance</th>
          </tr>
           <?php
              $offer = $collection_reports->collectionRreport();
              if ($offer) {
                  $i = 0;
                  while($row = pg_fetch_assoc($offer)){
                    $i++;
                  ?>
                      <tr>
                        <td><?=$i?></td>
                        <td style="text-align: right;"><button type="button" class="removeColl collection_button"  onclick='collectionReport(this.id)' id="<?=$row['syid']?>"><?=$row['sy']?></button></td>
                        <td style="text-align: right;"><?=$row["collectables"]?></td>
                        <td style="text-align: right;"><?=$row['collected']?></td>
                        <td style="text-align: right;"><?=$row['un_collected']?></td>
                        <td style="text-align: right;"><?=$row["previous_balance_collectables"]?></td>
                        <td style="text-align: right;"><?=$row["collected_previous_balance"]?></td>
                        <td style="text-align: right;"><?=$row["un_collected_previous_balance"]?></td>
                    </tr>
                  <?php
                }
              } else {
                  ?>
                    <tr>
                      <td colspan="8">No Record found!</td>
                    </tr>
                  <?php
              }
          ?>
        </table>
      <br>
      <br>
      <div id="perSem">

        <!-- VIEW BY SY INSIDE THE TABLE BY SEMESTER -->
      </div>

      <div id="perMonths">
      <!-- view months -->
      </div>

      <div id="perDAyColee">
      <!-- view dapy -->
      </div>

      <div id="perDetails">
      <!-- view Details -->
      </div>
      
      
  </form>
</div>
<script type="text/javascript">

  function collectionReport(syid){
    // $(".removeColl").removeProperty("color");
    $('.removeColl').removeAttr('style');
    $.ajax({
        type : 'POST',
        url  : 'pages/cashier/forms/form_collectionreport.php',
        data : 'type=semester&syid='+syid,
        beforeSend: function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $('#perSem').html(data)
        }
      }); 
      $('#'+syid).css('color', 'blue');
      $('#perSem').show();
      $('#perMonths').hide();
      $('#perDAyColee').hide();
      $('#perDetails').hide();
  }

  function collectionReportSEm(semid){

     
    $.ajax({
        type : 'POST',
        url  : 'pages/cashier/forms/form_collectionreport.php',
        data : 'type=perMonths&data='+semid,
        beforeSend: function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $('#perMonths').html(data)
        }
      }); 
      $('#'+semid).css('color', 'blue');

      $('#perMonths').show();
      $('#perDAyColee').hide();
      $('#perDetails').hide();
  }

  function collectionReportDay(data){
      $('.removeDay').removeAttr('style');
      $.ajax({
        type : 'POST',
        url  : 'pages/cashier/forms/form_collectionreport.php',
        data : 'type=perDAy&data='+data,
        beforeSend: function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $('#perDAyColee').html(data)
        }
      }); 
      $('#'+data).css('color', 'blue');
      $('#perDAyColee').show();
      $('#perDetails').hide();
  }

  function collectionReportDaily(data){
     $('#perDetails').show();
      $('.removeDaily').removeAttr('style');
      $.ajax({
        type : 'POST',
        url  : 'pages/cashier/forms/form_collectionreport.php',
        data : 'type=perdetails&data='+data,
        beforeSend: function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $('#perDetails').html(data)
        }
      }); 
      $('#'+data).css('color', 'blue');
  }
 
</script>