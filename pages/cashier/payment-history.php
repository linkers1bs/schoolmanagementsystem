<div class="tablecon payment_histroy">
  <form name="payment_histroy" id="payment_histroy">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;" >
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;" onchange='paymentHisGrade(this)'>
              <option>--SELECT--</option>
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                // if($r["studenttypeid"] == "2"){
                //   $select = 'selected';
                // }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select">
             <span>Grade:&nbsp;&nbsp;</span>
              <select style="width: 100px!important"  name="p_gradelvl" id="p_gradelvl" style="display: inline-block;"  onchange='paymentGetName(this.value)'>
              <option></option>
            </select>
          </div>
           <div id="show-student-payment" style="display: inline-block;">
             
           </div>
        </div>
      </div>

    </div>

      <div id="show-accountpaymenthistory-info">
        
      </div>

    
  </form>
</div>


<script type="text/javascript">
  function paymentHisGrade(value){
    $('#show-student-payment').hide();
    var text = value.options[value.selectedIndex].text;
    selectValues = {};
    if(text == "SHS" ){
       selectValues = { "0" :  "--SELECT--" ,"11": "11", "12": "12" };
    }else if (text == "College") {
       selectValues = { "0" :  "--SELECT--" ,"1": "1st Year", "2 Year": "2nd Year","3 Year": "3rd Year","4 Year": "4th Year" };
    } else if (text == "Elementary"){
       selectValues = { 
          "0" :  "--SELECT--" ,
          "1": "1", 
          "2": "2",
          "3": "3",
          "4": "4",
          "5": "5",
          "6": "6"
        };
    }else if (text == "JHS"){
       selectValues = { 
          "0" :  "--SELECT--" ,
          "7": "7", 
          "8": "8",
          "9": "9",
          "10": "10"
        };
    }
    $('#p_gradelvl option').each(function() {
            $(this).remove();
    });
    $.each(selectValues, function(key, value) {
      $('#p_gradelvl').append($('<option>', { value : key }).text(value));
    });
    
  }

  function paymentGetName(grade){
    if(grade != 0 || grade != "0"){
      $.ajax({
        type: 'POST',
        url : 'pages/cashier/action/get_studentaccount.php',
        data: $('#payment_histroy').serialize()+'&type=paymentHistory_studentname',
        success: function (data){
          $('#show-student-payment').show();
          $('#show-accountpaymenthistory-info').hide();
          $('#show-student-payment').html(data);
        }
      });
    }
  }

  function getStudentPaymentHistory(regnum){
    // alert($('#payment_histroy').serialize());
    if(regnum != "none"){
      $.ajax({
        type: 'POST',
        url : 'pages/cashier/forms/form_paymentHistory.php',
        data: $('#payment_histroy').serialize()+'&regnum='+regnum,
        success: function (data){
          $('#show-accountpaymenthistory-info').show();
          $('#show-accountpaymenthistory-info').html(data);
        }
      });
    }
  }
</script>