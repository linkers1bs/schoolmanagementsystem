<style>
  .create {
    border-radius: 12px;
    border:1px solid rgb(217, 217, 217);
    -webkit-box-shadow: 0px 2px 15px -8px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 15px -8px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 15px -8px rgba(0,0,0,0.75);
  }
</style>
<div class="tablecon create_billing">
  <form name="create_billing" id="create_billing">
    <div class="enroll">
    <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
           <div class="container_select">
              <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                  }
                ?>
              </select>
            </div>
            <div class="container_select">
                <span>School Year:&nbsp;&nbsp;</span>
                <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("schoolyear");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  ?>
                </select>
            </div>
             <div class="container_select">
                <span>Semester:&nbsp;&nbsp;</span>
                <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails("semester");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                  }
                ?>
                </select>
              </div>
               <div class="container_select">
                 <span>Billing:&nbsp;&nbsp;</span>
                  <select class="wid-fix" style="width: 200px!important" name="type_billing" id="type_billing">
                      <option value="none">Select...</option>
                      <option value="student">Per Student</option>
                      <option value="all">For All</option>
                      <option value="blocks">By Blocks</option>
                      <option value="grade">By Grade Level</option>
                      <option value="scholarship">By Scholarship</option>
                  </select>
               </div>
               <div class="container_select">
                 <span><button  type="button" id="select" class='button_local' onclick="type_Billing(this.id)">View</button></span>
               </div>
               <div class="container_select">
                  <div id="student-bill-data" style="display: inline-block;"> 
                    
                  </div>
               </div>
        </div>
      </div>
    </div>
  </form>
      
    <div id="printableArea">
        <div id="show-bill-data" style="display:block;width:100%;">
                  <!-- DISPLAY BILLING -->
        </div> 
    </div>            
</div>
