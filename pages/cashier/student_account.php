<div class="tablecon modify_reg">
  <form name="student_account" id="modify_reg">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;" onChange="getStudentName(this.id)">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;" onChange="getStudentName(this.id)">
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;" onChange="getStudentName(this.id)">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Scholarship:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="scholarshipid" id="scholarshipid" style="display: inline-block;" onChange="getStudentName(this.id)">
            <?php
              $query = $view_option_details->optionDetails("scholarship");
              while ($r = pg_fetch_assoc($query)){
                $select = '';
                if($r["scholarshipid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['scholarshipid'].'" '.$select.' >'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;" onChange="getStudentName(this.id)">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select">
              <span>
                <button  type="button" id="student_getname" class='button_local' onclick="getStudentName(this.id)">SELECT</button>
              </span>
          </div>
           <div id="show-student-data">
             
           </div>
        
        </div>
      </div>

    </div>

    <div id="show-accountsudent-info">
          
    </div>


  </form>
</div>
