<?php

  if (session_status() == PHP_SESSION_NONE) {
  session_start();
  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';

  $view_details = new ViewCashierDetails;

  $regnum = $_POST["regnum"];
  $sql = $view_details->get_info_student_account($regnum);
  if (isset($_POST["regnum"])) {
  	
?>
<br>
  		<span style="font-size: 12px"><b>Student Information</b></span><br><br>
   
	      <table class="table table-bordered">
	        <tr>
	          <td class="remarks nb">Student ID No.</td>
	          <td class="remarks "><?php echo $view_details->getVal('studentid',$sql);?></td>
	        </tr>
	        <tr>
	          <td class="remarks nb"><label class="txt2">Student Name</label></td>
	          <td class="remarks"><label class="txt2"><?php echo ucfirst($view_details->getVal('lastname',$sql)).", ".ucfirst($view_details->getVal('firstname',$sql))." ".ucfirst($view_details->getVal('middlename',$sql)); ?></label></td>
	        </tr>
	        <tr>
	          <td class="remarks nb">Courses</td>
	          <td class="remarks"><?php echo $view_details->getVal('coursename',$sql);?></td>

	        <tr>
	          <td class="remarks nb">Scholarship</td>
	          <td class="remarks"><?php echo $view_details->getVal('scholarshipname',$sql);?></td>
	        </tr>
	        <tr>
	          <td class="remarks nb">Date Registered</td>
	          <td class="remarks"><?php
	         $date = $view_details->getVal('dateregistered',$sql);
	         $date = date_create($date);
	         //echo date('l jS \of F Y ',$date);
	          echo date_format($date, 'l jS F Y');
	         ?></td>
	        </tr>
	        <tr>
	          <td class="remarks nb">No. of Subjects</td>
	          <td class="remarks" bgcolor="#a7bfde">0</td>
	        </tr>
	      </table>

	        <form method="post" id="other_student_payment">
				<span style="font-size: 12px"><b>Student Assessment</b> 
				<button type="button" name="addson" id="addson" value="addson" class="button_local">Add New</button>
				<button type="button" name="showCancel" id="showCancel" value="showCancel" class="button_local">Cancel</button></span>

				<br><br>
				<!-- PAYMENT ADD -->
				<div id="showAdd">
				<table class="table table-bordered" id="dynamic_field">
				  <tr style="background-color: #666" align="center" class="table-heads">  
				    <th><label>Account</label>
				    <input type="hidden" name="regnum" id="add_regnumother" value="<?php echo $view_details->getVal('regnum',$sql);?>"></th>
				    
				    <th>Amount Due</th>
				  </tr>

				  
				</table>
					<div align="right" id="hideold"  style="display: block-inline;">
				      <button type="button" value="show" id="addaccount" class="button_local">Add New</button>
				 </div>
				 <div align="right" id="showold" style="display: none">
				    &emsp;<button type="button" name="addaccount" id="addother" value="Submit" class="button_local">Save</button>
				   
				 </div>
				  <br>
				</div>
					<!--END  PAYMENT ADD -->
    		</form>
      
    		<form method="post" name="action_payment">
		      	<table class="table table-bordered">
		       
		        <tr style="background-color: #666" align="center" class="table-heads">
		          <th class="remarks nb"></th>
		          <th class="remarks b"><span style="display: block;">&nbsp;</span>Payables</th>
		          <th class="pay b"><span style="display: block;">&nbsp;</span>Discount</th>
		          <th class="pay"><span style="display: block;">&nbsp;</span>Paid</th>
		          <th class="pay"><span style="display: block;">&nbsp;</span>Balance</th>
		          <th><span style="display: block;">Payment</span>
		              &nbsp;&nbsp;<input type="checkbox" class="paych" name="checkss" onclick="onlyOne(this)" id="pame" value="Add"> Add 
		              &nbsp;&nbsp;<input type="checkbox" class="paych" name="checkss" onclick="onlyOne(this)"id="modifpay" value="Modify"> Modify
		              &nbsp;&nbsp;<input type="checkbox" class="paych" name="checkss" onclick="onlyOne(this)" id="delepay" value="Delete"> Delete
		             
		            </label>
		          </th>
		         </tr>
		          <?php
		         	$sql ="SELECT 
						*,
						(payables - (discount + paid_amount)) as balance
						FROM
						(SELECT sta.regnum, sta.accountid , act.name,
							CASE 
							  WHEN act.name = 'Miscellaneous fees' THEN (SELECT amount FROM miscellaneous WHERE sta.fees = miscid)
							  WHEN act.name = 'Tuition fees' THEN (SELECT amount FROM tuition WHERE sta.fees = tutionid)
							  WHEN act.name = 'Registration fees' THEN (SELECT amount FROM registrationfee WHERE sta.fees = regfee)
							  WHEN act.name = 'Other fees' THEN (SELECT amount FROM othersfees WHERE sta.fees = othersfeesid)
							  WHEN act.name = 'Previous Balance' THEN (SELECT amount FROM previousbalance  WHERE sta.fees = pbid)
							END as payables,
							CASE 
							  WHEN act.name = 'Miscellaneous fees' THEN (SELECT name FROM miscellaneous WHERE sta.fees = miscid)
							  WHEN act.name = 'Tuition fees' THEN (SELECT name FROM tuition WHERE sta.fees = tutionid)
							  WHEN act.name = 'Registration fees' THEN (SELECT name FROM registrationfee WHERE sta.fees = regfee)
							  WHEN act.name = 'Other fees' THEN (SELECT name FROM othersfees WHERE sta.fees = othersfeesid)
							  WHEN act.name = 'Previous Balance' THEN 'Previous Balance'
							END as name_fees,
							COALESCE(sta.discount,0) as discount, 
							act.actypeid,
							COALESCE((SELECT sum(amount) FROM pay WHERE accountid = sta.accountid), 0) as paid_amount
						FROM studentaccounts sta
						INNER JOIN accounttype act USING (actypeid)
							WHERE regnum = $regnum) as tbl1
						ORDER BY name ASC";
					$queryacc = $view_details->openqry($sql);
					$getmis = 0;
					$getreg = 0;
					$gettui = 0;
					$getother = 0;
					$getprev = 0;
					$i = 1;
					$total_payable = 0;
					$total_discount = 0;
					$total_paid = 0;
					$total_balance = 0;
					while ($row = pg_fetch_assoc($queryacc)) {
						$total_payable += $row["payables"];
						$total_discount += $row["discount"];
						$total_paid += $row["paid_amount"];
						$total_balance += $row["balance"];
						if ($row["actypeid"] == '1' AND $getmis == 0) {
							$getmis++;
							echo "<tr>";
								echo "<th class='remarks' colspan=6><b>Miscellaneous fees :</b></th>";
							echo "</tr>";
						}
						if ($row["actypeid"] == '4' AND $getreg == 0) {
							$getreg++;
							echo "<tr>";
								echo "<th class='remarks' colspan=6><b>Registration fees :</b></th>";
							echo "</tr>";
						}
						if ($row["actypeid"] == '2' AND $gettui == 0) {
							$gettui++;
							echo "<tr>";
								echo "<th class='remarks' colspan=6><b>Tuition fees :</b></th>";
							echo "</tr>";
						}
						if ($row["actypeid"] == '3' AND $getother == 0) {
							$getother++;
							echo "<tr>";
								echo "<th class='remarks' colspan=6><b>Other fees :</b></th>";
							echo "</tr>";
						}
						if ($row["actypeid"] == '6' AND $getprev == 0) {
							$getprev++;
							echo "<tr>";
								echo "<th class='remarks' colspan=6><b>Previous Balance :</b></th>";
							echo "</tr>";
						}
					 	?>
					 	<tr>
					 		<!-- START FEE NAMES DELETE EDIT -->
				         	<td class="remarks nb">
				         		<span style='margin-left:1em'>
					         		
				         			<span style='posistion:relative;float: right;' >
								         		<span  title="Delete Other Payment" data-toggle="tooltip" data-placement="top" id="del<?php echo $i;?>">
								         			<a href="#"   data-toggle="modal" data-target=".bd-example-modal-sm<?php echo $i; ?>">
								                      <i class="fa fa-times"></i>
								                    </a>&nbsp;
								                </span>	
							                    <input type="checkbox" name="check<?php echo $i;?>" id="check<?php echo $i;?>" value="<?php echo $row["accountid"]; ?>" onclick="check_studentaccount_edit(this.id)">
						                    </span>
				                    <?php 
				                    	// if others fee edit the name
				                    	

				                    	if ($row["actypeid"] == '3') {

				                    		?>
				                    		<input type="text" style="width: 80%!important;" name="oth<?php echo $i;?>" id="oth<?php echo $i;?>" value="<?php echo $row["name_fees"]; ?>" >
				                    		 <span id="closedesc<?php echo $i;?>" >
				                     		 	<?=$row["name_fees"]?>
				                     		 </span>
				                    		<?php
				                    	} else {
				                    		echo $row["name_fees"];
				                    	}
				                    ?>
				                    
			                    </span>

			                    <div class="modal fade bd-example-modal-sm<?php echo $i; ?>"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-backdrop="false">
									<div class="modal-dialog" role="document">
									    <div class="_modal_notif_container" style="margin: 0px auto;margin-top: 20%;">
									      <div class="modal-content">
									      	<center>
										      <div class="title">
										      <h6>Delete Account</h6>
										      </div>
										      <div class="_modal_content">
										            <h6>Are you sure you want to delete this account? <br> <b style="color: red">
										            <?php echo $row["name_fees"]; ?></b></h6>
										      </div>
										      <div class="_modal_footer">
										        <button type="button" data-dismiss="modal">Cancel</button>&nbsp;&nbsp;&nbsp;
										        <button type="button">OK</button>
										      </div>
									      </center>
									      </div>
									    </div>
									</div>
								</div>
				         	</td>
				         	<!-- END START FEE NAMES DELETE EDIT -  --> 
				         	
				         	<td class="remarks">
				         		<?php 
				                    	// if others fee edit the name
			                    	if ($row["actypeid"] == '3') {

			                    		?>
			                    		<input type="number" style="width: 80%!important;" name="amount<?php echo $i;?>" id="amount<?php echo $i;?>" value="<?php echo $row["payables"]; ?>" >
					                    <span id="close<?php echo $i;?>" >
					                      	<?=number_format($row["payables"],2)?>
			                    		</span>
			                    		<?php
			                    	} else {
			                    		echo number_format($row["payables"],2);
			                    	}
			                    ?>
				         	</td>

				         	<td class="remarks">
				         		<span id="dishid<?php echo $i;?>" >
				         			<?=number_format($row["discount"],2)?>
				         		</span>
		                     	<input type="number" style="width: 100%!important;" name="acctount<?php echo $i;?>" id="acctount<?php echo $i;?>" value="<?php echo $row["discount"];?>">
				         	</td>

				         	<td class="remarks"><?=number_format($row["paid_amount"],2)?></td>
				         	<td class="remarks"><?=number_format($row["balance"],2)?></td>
				         	<td>
				         		<input type="number" style="width: 100%!important;" name="payadd<?php echo $i;?>" id="payadd<?php echo $i;?>"  >
				         		<?php 
		                   
				                   $gesPay = "SELECT * FROM pay 
				                   WHERE accountid  = '".$row["accountid"]."' ";
				                   $gesPays = $view_details->openqry($gesPay);
				                   $getDataMo = 0;
				                   while ($pas = pg_fetch_assoc($gesPays)) { 
				                    $getDataMo++;
				                     ?>
				                     <div class="modddd" id="paymentOrno<?php echo trim($pas["orno"]).$i; ?>" style="display:none">
				                     
				                     <input type="hidden" style="width: 100%!important;" name="payid<?php echo trim($pas["orno"]).$i;?>" id="payid<?php echo trim($pas["orno"]).$i;?>" value="<?php echo trim($pas["payid"]); ?>">
				                       
				                     
				                     
				                     <input type="number" style="width: 100%!important;" name="amountid<?php echo trim($pas["orno"]).$i;?>" id="amountid<?php echo trim($pas["orno"]).$i;?>" value="<?php echo $pas["amount"]; ?>">
				                     
				                   
				                     </div>
				                     <?php
				                    
				                   }
				                ?>
				         	</td>
				        </tr>
					 	<?php
					 	$i++;
					}
					
		         ?> 

		        <tr>
		          <td class="remarks nb"><b>TOTAL</b></td>
		          <td class="remarks">
		            <b><?php echo  number_format($total_payable, 2);?></b>
		          </td>
		          <td class="remarks"> 
		          <b><?php echo  number_format($total_discount, 2);?></b></td>
		          </td>
		          <td  class="remarks"> 
		          <b><?php echo  number_format($total_paid, 2);?></b>
		          </td>
		          <td class="remarks">
		          <b><?php 
		            //asBalance = $total - $totalPaid;
		            echo  number_format($total_balance, 2);?></b>
		          </td>
		          <td class="remarks">
		         
		         
		            <input type="hidden" style="width: 100%!important;" name="account_number_of_data" id="account_number_of_data" value="<?php echo $i; ?>" >
		            <input type="hidden" name="number_of_divsion" id="number_of_divsion" value="<?php //echo $getDataMo; ?>"/>
		          </td>
		        </tr>
		       
		      </table>

		      	<div style="display: block;margin:0px auto;text-align: right;position: relative;">
		            <span id="ors">
			            OR No.&nbsp;&nbsp;<input type="text" style="width: 15%!important;" name="orno" id="orno"  >&nbsp; 
			        </span>
			        <span id="orsmodi">
		             <?php 
						$sql2 = "SELECT pay.orno FROM register
								INNER JOIN studentaccounts  USING (regnum)
								INNER JOIN pay  USING (accountid)
								INNER JOIN accounttype USING (actypeid)
								WHERE regnum  = $regnum  GROUP BY  pay.orno ";
						$ornsss = $view_details->openqry($sql2);
						if($view_details->notEmpty($sql2)) {
							?>
							
							<b>OR No.</b>
							<select name="ornos" id="ornos" style="width: 15%!important;" >
							<?php

								echo "<option value='none'>Select Orno...</option>";
								while ($row = pg_fetch_assoc($ornsss)) {
								 
								  echo "<option value='".trim($row["orno"])."'>".trim($row["orno"])."</option>";
								}
							?>
							</select>

							<button type="button" value="modifypayment" style="display: none!important;" id="modifypayment" class="button_local">Save</button>
							<button type="button" value="deletepayment" style="display: none!important;" id="deletepayment" class="button_local">Save</button>
						<?php
						}else {
							echo "No payment Found...";
						}
		               
		             ?>
		         	</span>

			        <button type="button" value="Save" id="addassement" class="button_local">
			          	Save
			        </button>
		            <button type="button" value="addpayment" id="addpayment" class="button_local">
		          		Save
		          	</button>
		      	</div>

		      	<br><br><br>
			</form>
			
			<!-- <script type="text/javascript" src="assets/jS/cashier/student_account_action.js"></script> -->
			<script type="text/javascript">
				$('#showAdd').hide();
				$('#showCancel').hide();
				$('#addpayment').hide();

				$("#addson").click(function(){ 
					$('#showAdd').show();
					$('#addson').hide();
					$('#showCancel').show();
				});
				$("#showCancel").click(function(){ 
					$('#addson').show();
					$('#showAdd').hide();
					$('#showCancel').hide();
				});
				// ADD COLUMN 
				$('#addaccount').click(function(){  
					var i=1;  
					i++;  
					$('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="accountnameother" id="add_accountnameother" value=""/></td><td><input type="number" id="add_amountother" name="amountother" value=""/></td></tr>');  

					document.getElementById('showold').style.display = 'block';
					document.getElementById('hideold').style.display = 'none';
				});  
				//ADD OTHER PAYMENT
				$('#addother').click(function(){
					var accountnameother = document.getElementById('add_accountnameother').value;
					var amountother = document.getElementById('add_amountother').value;
					var regnumother =	document.getElementById('add_regnumother').value;
					if(accountnameother != '' && amountother != ''){
						console.log(accountnameother);
						$.ajax({
					  	type: 'POST',
					  	url : 'controllers/function/StudentAccountController.php',
					  	data: 'accountnameother='+accountnameother+'&amountother='+amountother+'&regnumother='+regnumother+'&type=add_otherassessment',
					  	success : function(data){
					  		$('#alert-data').html(data);
					  	}
				  		}).done(function () {
				  			getStudentAccounts();
				  		});
					}
					
				});
				$('#ornos').change(function(){
				    $(".modddd").css("display", "none");
				    //document.getElementsByClassName("modddd").style.display = 'none';
				    var total_data=document.getElementById('account_number_of_data').value;
				    var ornos = $('#ornos :selected').val();           
					for (let j = 1; j <= total_data ; j++) {
						$('#paymentOrno'+ornos+j).show();
					}
				});

				function onlyOne(checkbox) {
				  var checkboxes = document.getElementsByName('checkss')
				  checkboxes.forEach((item) => {
				      if (item !== checkbox) item.checked = false
				  })
				}

				$('#ors').hide();
			    $('#orsmodi').hide();

				var total_data = document.getElementById('account_number_of_data').value; //GET ROW
			    var tess = 0;
			    for (var j = 1; j <= total_data ; j++) {
			    	$('#del'+j).hide(); // ok
			    	$('#oth'+j).hide();
			    	$('#amount'+j).hide();
			        $('#payadd'+j).hide(); 
			        $('#acctount'+j).hide(); //wala
					$('#addassement').hide();
					$('#disc'+j).hide();
					$('#ors').hide();
					$('#addpayment').hide();
			    }

			    function check_studentaccount_edit(id){
					var j = id.substring(5);
						if($('#pame').is(':checked') || $('#modifpay').is(':checked') || $('#delepay').is(':checked') ) {

							if ($('#check'+j).is(':checked')) {
								$('#amount'+j).hide();
								$('#close'+j).show();
								$('#addassement').hide();
								$('#del'+j).hide();
								$('#other'+j).hide();
								$('#disc'+j).hide();
								$('#acctount'+j).hide();
								tess = 0;
							}else {
								$('#amount'+j).hide();
								$('#close'+j).show();
								$('#del'+j).hide();
								$('#other'+j).hide();
								$('#disc'+j).hide();
								$('#acctount'+j).hide();
								tess = 0;
							}

						} else {
							
							if ($('#check'+j).is(':checked')) {
								$('#acctount'+j).show();
								$('#addassement').show();
								$('#del'+j).show();
								$('#dishid'+j).hide();

								$('#amount'+j).show();
								$('#oth'+j).show();
								$('#closedesc'+j).hide();
								$('#close'+j).hide();
								$('#disc'+j).show();
								$('#ors').hide();
								tess++;
							}else {
								var checkswe = document.getElementById("other"+j);
								if(checkswe) {
									document.getElementById("other"+j).checked = false;
								}
								$('#acctount'+j).hide();
								$('#amount'+j).hide();
								$('#close'+j).show();
								$('#del'+j).hide();
								$('#other'+j).hide();
								$('#disc'+j).hide();
								$('#oth'+j).hide();
								$('#closedesc'+j).show();
								$('#dishid'+j).show();
								tess--;
							}

						} 

						if(tess == 0) {

							$('#addassement').hide();
						}
				}

				$('#addassement').click(function(){  
					var number_of_data=document.getElementById('account_number_of_data').value;
					// initialize array
					var amountaccount = [];
					var accounttype = [];


					/// initialize array other account
					var nameOthers = [];
					var dicount = [];
					var amountaccountmod = [];
					var accountid = [];
					//disc  acctount other
					// append multiple values to the array
					for (var i = 1; i <= number_of_data ; i++) {
						if ($('#check'+i).is(':checked')) {
						    
						    var checkNot = document.forms["action_payment"]['amount'+i];
							if (checkNot) {
							    //if other feessss
							    amountaccountmod.push(document.forms["action_payment"]['amount'+i].value);//amount pay
							    dicount.push(document.forms["action_payment"]['acctount'+i].value); // discount
							    nameOthers.push(document.forms["action_payment"]['oth'+i].value); //name
							    accountid.push(document.forms["action_payment"]['check'+i].value); //acountid
							}else {
							    //if not other feessss
							    amountaccount.push(document.forms["action_payment"]['acctount'+i].value); //discount
							    accounttype.push(document.forms["action_payment"]['check'+i].value);//acountid
							}
					           
					   } 
					}
					$.post("controllers/function/StudentAccountController.php",{ 
					  amountaccount  : amountaccount,
					  accounttype : accounttype,
					  amountaccountmod : amountaccountmod,
					  dicount : dicount,
					  nameOthers : nameOthers,
					  accountid : accountid,
					  type : 'modify_assement'
					    },
					    function(data) {
					    $("#alert-data").html(data);
					    //setTimeout(function(){window.location.reload(); }, 2000);
					}).done(function(){
						getStudentAccounts();
					}); 
				});  

				var allButtonsOnPage = document.getElementById("pame");
				var button = allButtonsOnPage;
				button.addEventListener('click', function() {
					if ($('#pame').is(':checked') ) {

						$('#addpayment').show();
						$('#addassement').hide();
						$('#ors').show();
						$('#modifypayment').hide();
    					$('#orsmodi').hide();
    					$(".modddd").css("display", "none");
						for (var j = 1; j <= total_data ; j++) {
							$('#payadd'+j).show();
							$('#amount'+j).hide();
							$('#close'+j).show();
							$('#acctount'+j).hide();
							$('#dishid'+j).show();

							$('#del'+j).hide();
							$('#other'+j).hide();
							$('#oth'+j).hide();
							$('#closedesc'+j).show();
							document.getElementById("check"+j).checked = false;
							var thisa = document.getElementById("other"+j);
							if (thisa) {
								document.getElementById("other"+j).checked = false;
							}
							tess = 0;
						}

						
						// $('#deletepayment').hide();
					} else {
						$('#addpayment').hide();
						$('#addassement').hide();
						$('#ors').hide();
						for (var j = 1; j <= total_data ; j++) {
							$('#payadd'+j).hide();
							document.getElementById("check"+j).checked = false;
						}
						// $('#ors').hide();
						
						// $('#orsmodi').hide();
						// $('#modifypayment').hide();
						
						// $('#deletepayment').hide();
					}

				});

				$('#addpayment').click(function(){  

					var number_of_data=document.getElementById('account_number_of_data').value;
					var orno=document.forms["action_payment"]['orno'].value;
					// initialize array
					var amount = [];
					var accounttype = [];

					// append multiple values to the array
					for (var i = 1; i <= number_of_data ; i++) {
					    if ($('#check'+i).is(':checked')) {
					        if(document.forms["action_payment"]['payadd'+i].value != '' ) {
					          amount.push(document.forms["action_payment"]['payadd'+i].value);
					          accounttype.push(document.forms["action_payment"]['check'+i].value);
					        } else {
					        	 // alert('The checkbox that you check are empty.. Please input the amount!!..');
					            // document.forms["action_payment"]['payadd'+i].style.border = "2px solid red";
					            $('#payadd'+i).css({"border":"2px solid red","box-shadow":"0 0 3px red"});
					        }
					      
					    } 
					}
					
					if(orno != '') {
					  var lens = amount.length;
					  if (lens > 0) {
					  	$.post("controllers/function/StudentAccountController.php",{ 
						      amount  : amount,
						      accounttype : accounttype,
						      orno : orno,
						      lens : lens,
						      type : 'addpayment'
						    },
						    function(data) {
						    $("#alert-data").html(data);
						    //setTimeout(function(){window.location.reload(); }, 2000);
						  }).done(function(){
						  		getStudentAccounts();
						  });
					  }
					  
					} else {
					  $('#orno').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
					}
				});  

				

				var modifpay = document.getElementById("modifpay");
				modifpay.addEventListener('click', function() {
					var ornos = $('#ornos :selected').val();           

					if ($('#modifpay').is(':checked') ) {
						$('#orsmodi').show();
						$('#modifypayment').show();
						$('#addassement').hide();
						$('#addpayment').hide();
						$('#ors').hide();
						$('#deletepayment').hide();

						for (var j = 1; j <= total_data ; j++) {
							$('#payadd'+j).hide();
							$('#amount'+j).hide();
							$('#close'+j).show();
							$('#dishid'+j).show();

							document.getElementById("check"+j).checked = false;
							$('#del'+j).hide();
							$('#acctount'+j).hide();
							$('#other'+j).hide();
							$('#oth'+j).hide();
							$('#closedesc'+j).show();
							document.getElementById("check"+j).checked = false;
							var thisa = document.getElementById("other"+j);
							if (thisa) {
								document.getElementById("other"+j).checked = false;
							}
						}

						tess = 0;
					} else {
						$('#orsmodi').hide();
						$('#modifypayment').hide();
						$('#addpayment').hide();
						$('#addassement').hide();
						$('#ors').hide();
						$('#deletepayment').hide();
						for (var j = 1; j <= total_data ; j++) {
							$('#payadd'+j).hide();
							document.getElementById("check"+j).checked = false;
							if(ornos != 'none') {
							$('#paymentOrno'+ornos+j).hide();
							}
							$('#del'+j).hide();
							$('#other'+j).hide();
							$('#oth'+j).hide();
							$('#closedesc'+j).show();
							document.getElementById("check"+j).checked = false;
							var thisa = document.getElementById("other"+j);
							if (thisa) {
							document.getElementById("other"+j).checked = false;
							}
						}

						tess = 0;
					}
				});

				$('#modifypayment').click(function(){  

					var number_of_divsion =document.getElementById('number_of_divsion').value;
					var number_of_data =document.getElementById('account_number_of_data').value;
					var ornos=document.forms["action_payment"]['ornos'].value;

					// initialize array
					var payid = [];
					var amounts = [];
					var count = 0;

					for (var i = 1; i <= number_of_data ; i++) {
						if ($('#check'+i).is(':checked')) {
					        var pay = document.getElementById('payid'+ornos+i).value;
					        var amount  =  document.getElementById('amountid'+ornos+i).value;
					        payid.push(pay);
					        amounts.push(amount);
					        count++;
						}
					}
					if(count > 0 ) {

						$.post("controllers/function/StudentAccountController.php",{ 
						      amount  : amounts,
						      payid : payid,
						      type : 'modifypayment'
						    },
						    function(data) {
						    $("#alert-data").html(data);
						}).done(function(){
							getStudentAccounts();
						});

					} else {
						alert('Please Check the checkbox to modify!!...');
					}



				});  


				var delepay = document.getElementById("delepay");
				delepay.addEventListener('click', function() {
					var ornos = $('#ornos :selected').val();           

					if ($('#delepay').is(':checked')) {

						$('#orsmodi').show();
						$('#modifypayment').hide();
						$('#addassement').hide();
						$('#addpayment').hide();
						$('#ors').hide();
						$('#deletepayment').show();

						for (var j = 1; j <= total_data ; j++) {
							$('#payadd'+j).hide();
							$('#amount'+j).hide();
							$('#close'+j).show();
							$('#dishid'+j).show();


							$('#del'+j).hide();
							$('#acctount'+j).hide();
							$('#other'+j).hide();
							$('#oth'+j).hide();
							$('#closedesc'+j).show();
							document.getElementById("check"+j).checked = false;
							var thisa = document.getElementById("other"+j);
							if (thisa) {
								document.getElementById("other"+j).checked = false;
							}
						}
						



					} else {
						$('#orsmodi').hide();
						$('#modifypayment').hide();
						$('#addpayment').hide();
						$('#addassement').hide();
						$('#ors').hide();
						$('#deletepayment').hide();
						for (var j = 1; j <= total_data ; j++) {
							$('#payadd'+j).hide();
							document.getElementById("check"+j).checked = false;
							$('#paymentOrno'+ornos+j).hide();
						}
						

					}

				});

				$('#deletepayment').click(function(){  
		        	var number_of_data =document.getElementById('account_number_of_data').value;
		          	var ornos=document.forms["action_payment"]['ornos'].value;
		         
		          // initialize array
		            var payid = [];
		         
		            var count = 0;
		          	for (var i = 1; i <= number_of_data ; i++) {
		                if ($('#check'+i).is(':checked')) {
		               
		                        var pay = document.getElementById('payid'+ornos+i).value;
		                        payid.push(pay);
		                        count++;
		                }
		                  
		                
		            }
		            alert('Are you sure you want to Delet the Payment?..');
		            if(count > 0 ) {
		                
		                	 $.post("controllers/function/StudentAccountController.php",{ 
		                      payid : payid,
		                      type : 'deletepayment'
		                    },
		                    function(data) {
		                    $("#alert-data").html(data);
		                }).done(function () {
		                	getStudentAccounts();
		                });
		  
		            } else {
		              alert('Please Check the checkbox to Delete!!...');
		            }
		           
		        });  
			</script>
 	<?php 

 	}
 	
?>



