<?php 
if (session_status() == PHP_SESSION_NONE) {
        
  session_start();

}
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  $view_details = new ViewCashierDetails;
  $sql = $view_details->studentInfoHeader($_POST,'header');
  $data = pg_fetch_assoc($sql);
?>
	<br><br>
	<div align="center" style="background-color: #F2F3F4;" class="create" >
	  <div align="center" style="padding: 10px;" >
	    <div class="header-billing" >
	        <div>
				<h4><b>STUDENT PAYMENT LOG</b></h4>
				<br>
	        </div>
	    </div>
	  <div class="info" style=""> 

		 <table bgcolor="#B0C4DE" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;">
		    <tr>
		      <td>Name :</td>
		      <td class="remarks"><b><?php echo ucwords($data["fullname"]) ?></b></td>
		      <td>Date :</td>
		      <td  class="remarks"><b><?php echo date("Y-m-d"); ?></b></td>
		    </tr>
		    <tr>
		      <td>ID No :</td>
		      <td  class="remarks"><b><?php echo $data["studentid"]; ?></b></td>
		      <td >Sem :</td>
		      <td  class="remarks"><b><?php echo $data["semester"]; ?></b></td>
		    </tr>
		    <tr>
		      <td>Course :</td>
		      <td  class="remarks"><b><?php echo $data["coursename"]; ?></b></td>
		      <td>SY :</td>
		      <td  class="remarks"><b><?php echo $data["schoolyear"]; ?></b></td>
		    </tr>
		      <tr>
		        <td >Scholarship</td>
		        <td class="remarks" bgcolor="#edf2f8"><b><?php echo $data["scholarship"]; ?></b></td>
		        <td>&nbsp;</td>
		        <td  class="remarks">&nbsp;</td>
		      </tr>
		</table>
		<br><br>
  		<table>
            <tr align="center" bgcolor="gray">
              <th rowspan="2">Subject Code</th>
              <th rowspan="2">Subject/s Taken</th>
              <th rowspan="2">Date Start</th>
              <th colspan="2">Unit </th>
              <th rowspan="2">Pice</th>
            </tr>
            <tr bgcolor="gray">
              <th>Lec</th>
              <th>Lab</th>
            </tr>
            <?php
            	$_POST["studentid"] = $data["studentid"];
            	$_POST["branchid"] = $_SESSION['branch_id'];
                $offer = $view_details->studentInfoHeader($_POST,'subjectTaken');
                $semtotallec_advise = 0;
                $semtotallab_advise = 0;
                $price = 0;
                if ($offer) {
                    while($row = pg_fetch_assoc($offer)){
                        $semtotallec_advise += $row["lec"];
                        $semtotallab_advise += $row["lab"];
                        $price += $row["price"];
                    ?>
                        <tr>
                          <td><?=$row['subjectcode']?></td>
                          <td><?=$row['subject_taken']?></td>
                          <td><?=$row["datestart"]?></td>
                          <td><?=$row['lec']?></td>
                          <td><?=$row['lab']?></td>
                          <td><?=$row["price"]?></td>
                      </tr>
                    <?php
                  }
                } else {
                    ?>
	                    <tr>
	                      <td colspan="6">No subject/s Controlled!</td>
	                    </tr>
                    <?php
                }
            ?>
            <tr>
				<td></td>
				<td></td>
				<td style="text-align: right;">TOTAL:</td>
				<td><?=$semtotallec_advise?></td>
				<td><?=$semtotallab_advise?></td>
				<td><?=number_format($price,2)?></td>
            </tr>
        </table>
        <fieldset style="margin: 0px!important;padding:0px!important">
            <div class="sasses">
		      	<table align="left" cellspacing="0" style="width: 50%; padding-top: 10px; border: none;" >
		            <tr align="center">
		              <th colspan="2"><b>SCHOOL ASSESSMENT:</b></th>
		            </tr>
		            <?php
		                $data = $view_details->schollAssessmentPayment($_POST["regnum"],"assessment");
		                $sql = $view_details->schollAssessmentPayment($_POST["regnum"],"total");
		                $total_paid = pg_fetch_assoc($sql);
		                $total = 0;
		                $discount_total = 0;
		                if ($data) {
		                    while($row = pg_fetch_assoc($data)){
		                        $total += $row["amount"];
		                        $discount_total = $row["discount"];
		                    ?>
		                        <tr>
		                          <td class='remarks'><?=$row['assessment']?></td>
		                          <td class='remarks'><?=number_format((int)$row['amount'],2)?></td>
		                      </tr>
		                    <?php
		                  }
		                  ?>
		                  <tr>
		                  	<td class='remarks'>Discount</td>
		                  	<td class='remarks'><?=number_format($discount_total,2)?></td>
		                  </tr>
		                  <tr>
		                  	<td class='remarks'>Subject Fees</td>
		                  	<td class='remarks'><?=number_format($price,2)?></td>
		                  </tr>
		                   <tr>
		                  	<td class='remarks'><b>Total Tution Fee</b></td>
		                  	<td class='remarks'><b><?=number_format($total,2)?></b></td>
		                  </tr>
		                  <tr>
		                  	<td class='remarks'><b>Total Amount Paid</b></td>
		                  	<td class='remarks'><b><?=number_format($total_paid["total"],2)?></b></td>
		                  </tr>
		                   <tr>
		                  	<td class='remarks'><b>Remaining Tution</b></td>
		                  	<td class='remarks'><b><?=number_format($total - $total_paid["total"],2)?></b></td>
		                  </tr>
		                 	<tr  align="center" style="border:none!important">
				             	<td style="border:none!important;background-color: #f2f3f4;">&nbsp;</td>
				             	<td style="border:none!important;background-color: #f2f3f4;">&nbsp;</td>
				            </tr>
		                  	<tr  align="center">
				              <th><b>No. of modules/terms for this sem:</b></th>
				               <th><b>5</b></th>
				            </tr>
				            <tr >
				              <th class="remarks" colspan="2"><b>Payment per module/term:</b></th>
				            </tr>
				            <tr>
			                  	<td class='remarks'>1st Payments Term</td>
			                  	<td class='remarks'><?=number_format($total / 5,2)?></td>
			                 </tr>
			                 <tr>
			                  	<td class='remarks'>2nd Payments Term</td>
			                  	<td class='remarks'><?=number_format($total / 5,2)?></td>
			                 </tr>
			                 <tr>
			                  	<td class='remarks'>3rd Payments Term</td>
			                  	<td class='remarks'><?=number_format($total / 5,2)?></td>
			                 </tr>
			                 <tr>
			                  	<td class='remarks'>4th Payments Term</td>
			                  	<td class='remarks'><?=number_format($total / 5,2)?></td>
			                 </tr>
			                 <tr>
			                  	<td class='remarks'>5th Payments Term</td>
			                  	<td class='remarks'><?=number_format($total / 5,2)?></td>
			                 </tr>
		                  <?php
		                } 
		            ?>

		        </table>
		    </div>

	 		<label><b>PAYMENT HISTORY:</b></label>
            <table cellspacing="0" style="width: 45%; margin-bottom: 0px;  border-collapse: collapse;">
            	<tr align="center">
	                <th colspan="2">Micellaneous Fee</th>
	                <th><?=number_format($total,2)?></th>
              	</tr>
              <tr align="center">
                <th>Date</th>
                <th>OR No.</th>
                <th>Amount Paid</th>
              </tr>
               <?php
	            $data = $view_details->schollAssessmentPayment($_POST["regnum"],"history");
	            if ($data) {
	                while($row = pg_fetch_assoc($data)){
	                ?>
	                    <tr>
	                      <td class='remarks'><?=$row['createdat']?></td>
	                      <td class='remarks'><?=$row['orno']?></td>
	                      <td class='remarks'><?=number_format((int)$row['amount'],2)?></td>
	                  </tr>
	                <?php
	              }
	          	} else {
	          		?>
	          		<tr>
	          			<td colspan="3">No record found...</td>
	          		</tr>
	          		<?php
	          	}
	            ?>

            </table><br><br><br>
		</fieldset>
	  </div>

	  </div>
	</div>