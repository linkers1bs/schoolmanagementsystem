<?php

  if (session_status() == PHP_SESSION_NONE) {
  session_start();
  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';

  $collection_reports = new ViewCashierDetails;
  

  if (isset($_POST["type"])) {
  	
  	$type = $_POST["type"];

  		if ($type == "semester") {
  			$syid = $_POST['syid']; // syid
  			?>
  				<table cellspacing="0" style="margin-bottom: 0px;  border-collapse: collapse; border :none;">
			       <tr align="center">
			        <th colspan="8" class="remarks" style="border:none!important;background-color: #f2f3f4;">BY SEMESTER:</th>
			      </tr>
			      <tr align="center">
			        <th>Sem</th>
			        <th>School Year</th>
			        <th>Collectables</th>
			        <th>Collected</th>
			        <th>Un-Collected</th>
			        <th>Previous Balance Collectables</th>
			        <th>Collected Previous Balance</th>
			        <th>Un-Collected Previous Balance</th>
			      </tr>
			      <?php
			            $offer = $collection_reports->persem($syid);
			            if ($offer) {
			                $i = 0;
			                while($row = pg_fetch_assoc($offer)){
			                  $i++;
			                ?>
			                    <tr>
			                      <td><button type="button" class="removeSEm collection_button"  onclick='collectionReportSEm(this.id)' id="<?=$row['semid']."_".$row['syid']?>"><?=$row['sem']?></button></td>
			                      <td style="text-align: right;"><?=$row['sy']?></td>
			                      <td style="text-align: right;"><?=$row["collectables"]?></td>
			                      <td style="text-align: right;"><?=$row['collected']?></td>
			                      <td style="text-align: right;"><?=$row['un_collected']?></td>
			                      <td style="text-align: right;"><?=$row["previous_balance_collectables"]?></td>
			                      <td style="text-align: right;"><?=$row["collected_previous_balance"]?></td>
			                      <td style="text-align: right;"><?=$row["un_collected_previous_balance"]?></td>
			                  </tr>
			                <?php
			              }
			            } else {
			                ?>
			                  <tr>
			                    <td colspan="8">No Record found!</td>
			                  </tr>
			                <?php
			            }
			        ?>
			      </table><br><br>
  			<?php
  		} elseif ($type == "perMonths") {
  			$data = explode('_', $_POST["data"]);
  			?>
  			<table cellspacing="0" style="margin-bottom: 0px;  border-collapse: collapse; border :none;">
		       <tr align="center">
		        <th colspan="8" class="remarks" style="border:none!important;background-color: #f2f3f4;">BY MONTH:</th>
		      </tr>
		      <tr align="center">
		        <th>#</th>
		        <th>Months</th>
		        <th>Collectables</th>
		        <th>Collected</th>
		        <th>Un-Collected</th>
		        <th>Previous Balance Collectables</th>
		        <th>Collected Previous Balance</th>
		        <th>Un-Collected Previous Balance</th>
		      </tr>
		      <?php
		            $offer = $collection_reports->permonth($data[0],$data[1]);//sem[0] | syid[1]
		            if ($offer) {
		                $i = 0;
		                while($row = pg_fetch_assoc($offer)){
		                  $i++;
		                ?>
		                    <tr>
		                      <td><?=$i?></td>
		                      <td><button type="button" class="removeDay collection_button"  onclick='collectionReportDay(this.id)' id="<?=$row['semid']."_".$row['syid']."_".$row['months']?>"><?=$row['months']?></button></td>
		                      <td style="text-align: right;"><?=$row["collectables"]?></td>
		                      <td style="text-align: right;"><?=$row['collected']?></td>
		                      <td style="text-align: right;"><?=$row['un_collected']?></td>
		                      <td style="text-align: right;"><?=$row["previous_balance_collectables"]?></td>
		                      <td style="text-align: right;"><?=$row["collected_previous_balance"]?></td>
		                      <td style="text-align: right;"><?=$row["un_collected_previous_balance"]?></td>
		                  </tr>
		                <?php
		              }
		            } else {
		                ?>
		                  <tr>
		                    <td colspan="8">No Record found!</td>
		                  </tr>
		                <?php
		            }
		        ?>
		      </table><br><br>
  			<?php
  		} elseif ($type == "perDAy") {
  			$data = explode('_', $_POST["data"]);
  			?>
  			<table cellspacing="0" style="margin-bottom: 0px;  border-collapse: collapse; border :none;">
		       <tr align="center">
		        <th colspan="3" class="remarks" style="border:none!important;background-color: #f2f3f4;">DAILY SUMMARY OF COLLECTION:</th>
		      </tr>
		      <tr align="center">
		        <th>#</th>
		        <th>Date</th>
		        <th>Collected</th>
		        <?php
		            $offer = $collection_reports->perDay($data[0],$data[1],$data[2]);
		            if ($offer) {
		                $i = 0;
		                $totalMonth = 0;
		                while($row = pg_fetch_assoc($offer)){
		                  $i++;
		                  $totalMonth += $row["collected"];
		                ?>
		                    <tr>
		                      <td><?=$i?></td>
		                      <td><button type="button" class="removeDaily collection_button"  onclick='collectionReportDaily(this.id)' id="<?=$row['semid']."_".$row['syid']."_".$row['months']."_".$row['createdat']?>"><?=$row['createdat']?></button></td>
		                      <td><?=$row["collected"]?></td>
		                  </tr>
		                <?php
		              }
		              ?>
		                 <tr>
		                    <td colspan="2" style="text-align: right;">Total:</td>
		                    <td ><?=$totalMonth?></td>
		                </tr>
		              <?php
		            } else {
		              ?>
		                <tr>
		                  <td colspan="3">No Record found!</td>
		                </tr>
		              <?php
		            }
		        ?>
		      </tr>

		      </table><br><br>

  			<?php
  		} elseif ($type == "perdetails") {
  			$data = explode('_', $_POST["data"]);
  			?>
  				<table cellspacing="0" style="margin-bottom: 0px;  border-collapse: collapse; border :none;">
		       <tr align="center">
		        <th colspan="6" class="remarks" style="border:none!important;background-color: #f2f3f4;">DETAILED COLLECTION:</th>
		      </tr>
		      <tr align="center">
		        <th>#</th>
		        <th>OR</th>
		        <th>Student Name</th>
		        <th>Payment For</th>
		        <th>Remarks</th>
		        <th>Amount</th>
		      </tr>
		      <?php
		            $offer = $collection_reports->perPerson($data[0],$data[1],$data[2],$data[3]);
		            if ($offer) {
		                $i = 0;
		                $totalDetailed = 0;
		                while($row = pg_fetch_assoc($offer)){
		                  $i++;
		                  $totalDetailed += $row["collected"];
		                ?>
		                    <tr>
		                      <td><?=$i?></td>
		                      <td class="remarks"><?=$row['orno']?></td>
		                      <td class="remarks"><?=ucwords($row["full_name"])?></td>
		                      <td class="remarks"><?=$row['assessment']?></td>
		                      <td></td>
		                      <td class="remarks"><?=$row["collected"]?></td>
		                  </tr>
		                <?php
		              }
		              ?>
		                 <tr>
		                    <td colspan="5" style="text-align: right;">Total:</td>
		                    <td class="remarks"><?=$totalDetailed?></td>
		                </tr>
		              <?php
		            } else {
		              ?>
		                <tr>
		                  <td colspan="6">No Record found!</td>
		                </tr>
		              <?php
		            }
		        ?>
		      </table><br><br>
  			<?php 

  		}

  }
?>

	



