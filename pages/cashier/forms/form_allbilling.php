<?php
  if (session_status() == PHP_SESSION_NONE) {
          
      session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  $view_details = new ViewCashierDetails;

  $branchid =  trim($_POST["branchid"]);
  $syid =  trim($_POST["syid"]);
  $semid =  trim($_POST["semid"]);
  $type_billing = trim($_POST["type_billing"]);


    if($type_billing == "student") {
        ?>
                    
        <br>
        <span id="hidename">
        Name:&nbsp;&nbsp;
        <select class="wid-fix" style="width: 255px!important" name="regnum" id="regnum" onChange="billStudent('name');">
        <?php
         
        $query = $view_details->get_perbilling($semid,$syid,$branchid,$id = 'all',$type_billing = "all");
        if($query) {
            echo "<option value='none'>Select Student..</option>";
            while ($r = pg_fetch_assoc($query)){
                                                    
                echo '<option value="'.$r['regnum'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
                                               
            } 
    
        } else {
                    
                echo "<option>No record found..</option>";
        }
               
         ?>
        </select>&nbsp;&nbsp;</span>Use ID:&nbsp;&nbsp;<input type="checkbox" name="useID" id="checkstud"/>
        <span  id='studenthide' >Student ID:&nbsp;&nbsp;<input type="text" name="studentid" id="studentid"/>
        <button type="button" id="buttstudent" class="button_local">View</button>
        </span>&nbsp; <button  type="button" onclick="printDiv('printableArea')" class="button_local">Print</button>
    
       
        
        <script type="text/javascript">
      
          $('#studenthide').hide();
          $('#hidename').show();
                
          var allButtonsOnPage = document.getElementById("checkstud");
          var button = allButtonsOnPage;
          button.addEventListener('click', function() {

          if ($('#checkstud').is(':checked')) {
            $('#studenthide').show();
            $('#hidename').hide();
            $('#show5-data').hide();
          } else {
            $('#studenthide').hide();
            $('#hidename').show();
          }
          });

                        
          $('#buttstudent').click(function(){  

            var branchid = document.forms["create_billing"]["branchid"].value;
            var syid =document.forms["create_billing"]["syid"].value;
            var semid = document.forms["create_billing"]["semid"].value;
            var type_billing = document.forms["create_billing"]["type_billing"].value;
            var studentid =  document.forms["create_billing"]["studentid"].value;
          if(type_billing != 'none' || type_billing != "none") {
            $('#show-bill-data').show();
            $.post("pages/cashier/forms/form_billing.php",{ 
                      branchid      : branchid, 
                      syid          : syid,
                      semid         : semid,
                      studentid     : studentid
                      },
                      function(data) {
                      $("#show-bill-data").html(data);
              });
          } 
                      
          });  
        
        </script>
      
        <?php
    } elseif ($type_billing == "blocks") {
     ?>
      <span>&nbsp;Block:&nbsp;&nbsp;&nbsp;</span>
      <select  class="wid-fix" name="blockname" style="width: 150px!important"  style="display: inline-block;" onChange="billBlock();" id="blockname">
      <?php
        $query =$view_details->get_type_block_billing($semid,$syid);
        if($query) {
          echo '<option value="none">Select..</option>';
          while ($r = pg_fetch_assoc($query)){
            echo '<option value="'.$r['blockname'].'">'.$r['blockname'].'</option>';
          }
        } else {
          echo '<option>No record found..</option>';
        }

      ?>
      </select>
    <?php
    }



     elseif ($type_billing == 'all' || $type_billing == "all") {
      
        $sqlopen = $view_details->get_perbilling($semid,$syid,$branchid,$id = 'all',$type_billing);
        if($sqlopen) {
        ?>
        <center style="padding-top:20px"><button  type="button" onclick="printDiv('printableArea')" class="button_local">Print</button></center>
        <?php
          while ($data = pg_fetch_assoc($sqlopen)) {
            ?>
            <style>
                b {
                    padding-bottom: 0px!important; 
                    border-bottom: none!important;
                }
                </style>
          <hr>
          <div align="center" style="background-color: #F2F3F4;" class="create" >
            <div align="center" style="padding: 10px;" >
    
                                <div class="header-billing" >
                                  <div>
                                      <h4><b>Modular Billing Statement</b></h4>
                                      <span>For Payment Terms  month of <?php echo $today = date("F Y");  ?></span>
                                  </div>
                                </div>
                          
    
                          <div class="info" style=""> 
    
                            <table bgcolor="#B0C4DE" class="table table table-bordered sal-tab1" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;">
                              <tr>
                                <td>Name :</td>
                                <td class="remarks"><b><?php echo ucfirst($data["lastname"]).", ".ucfirst($data["firstname"])." ".ucfirst($data["middlename"]); ?></b></td>
                                <td>Date :</td>
                                <td  class="remarks"><b><?php  echo $today = date("F j, Y");   ?></b></td>
                              </tr>
                              <tr>
                                <td>LRNID :</td>
                                <td  class="remarks"><b><?php echo $data["lrnid"];?></b></td>
                                <td >Sem :</td>
                                <td  class="remarks"><b><?php echo  $data["sem"];?></b></td>
                              </tr>
                              <tr>
                                <td>Course :</td>
                                <td  class="remarks"><b><?php echo $data["coursename"];?></b></td>
                                <td>SY :</td>
                                <td  class="remarks"><b><?php echo $data["sy"];?></b></td>
                              </tr>
                              <tr>
                                  <td >Scholarship</td>
                                  <td class="remarks" bgcolor="#edf2f8"><b><?php echo $data["scholarshipname"]?></b></td>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                            </table>
    
                          </div>
    
    
    
    
                            <fieldset class="man-marg">
                              
                          
                            <div class="sasses">
                              <table align="left" cellspacing="0" class=" table table-bordered sal-tab1" style="width: 50%; padding-top: 10px;" >
                                <tr align="center">
                                  <th colspan="2"><b>SCHOOL ASSESSMENT:</b></th>
                                </tr>
                               
                                <?php 
                                  $regnumid = $data["regnum"];
                                  $getAcccount = "SELECT DISTINCT * FROM register
                                  LEFT JOIN studentaccounts  USING (regnum)
                                  LEFT JOIN accounttype USING (actypeid)
                                  WHERE regnum  = $regnumid  ORDER BY accountid ASC";
                                  $queryacc = $view_details->openqry($getAcccount);
                                  $paid = 0;
                                  $totaldiscount = 0;
                                  $totalPaid =0;
                                  $total =0;
                                  $balance = 0;
                                  $totalBalance = 0;
                              
                                  while ($row = pg_fetch_assoc($queryacc)) {
                                    //get PAY using accountid 
                                    $totaldiscount += $row["discount"];
                                    $gesPay = "SELECT DISTINCT * FROM pay 
                                    WHERE accountid  = '".$row["accountid"]."' ";
                                    $queryPay = $view_details->openqry($gesPay);
                                    while ($pa = pg_fetch_assoc($queryPay)) { 
                                      $paid += $pa['amount'];
                                    }
                          
                                    $totalPaid += $paid;
                          
                                      $otherFee = trim($row["name"]);
                                   
                                      if($otherFee === "Other fees") {
                                        $otherfeid = trim($row["fees"]);
                                        $getAcccount = "SELECT DISTINCT * FROM register 
                                        LEFT JOIN othersfees USING (regnum)
                                        WHERE othersfeesid  = $otherfeid";
                                        $total += $view_details->getVal('amount',$getAcccount);
                                        $balance = $view_details->getVal('amount',$getAcccount) - $row["discount"] - $paid;
                                        $totalBalance += $balance;
                                          echo"<tr>";
                                          echo "<td class='remarks'>".$view_details->getVal('name',$getAcccount)."</td>";
                                          echo "<td class='remarks'>".number_format($view_details->getVal('amount',$getAcccount), 2)."</td>";
                                          echo"</tr>";
                                         
                                      }else {
                                        $total += $row["fees"];
                                        $balance = $row["fees"] -  $row["discount"] - $paid;
                                       $totalBalance += $balance;
                                        echo"<tr>";
                                        echo "<td class='remarks'>".$row["name"]."</td>";
                                        echo "<td class='remarks'>".number_format($row["fees"], 2)."</td>";
                                        echo"</tr>";
                                      }
                                      $paid = 0;
                                  }
                                ?>
                                </tr>
                                <tr>
                                  <td class="remarks">Total Discount</td>
                                  <td class="remarks" bgcolor="#edf2f8"><?php echo number_format($totaldiscount,2); ?></td>
                                </tr>
                                  <tr>
                                  <td class="remarks">Total Payables</td>
                                  <td class="remarks" bgcolor="#edf2f8"><?php echo number_format($total,2); ?></td>
                                </tr>
                                <tr>
                                  <td class="remarks">Payment Terms</td>
                                  <td class="remarks" bgcolor="#edf2f8">5 months</td>
                                </tr>
                                <tr>
                                  <td class="remarks"><b>Total Amount You Paid</b></td>
                                  <td class="remarks" bgcolor="#a7bfde"><b><?php echo number_format($totalPaid,2); ?></b></td>
                                </tr>
                                <tr>
                                  <td class="remarks"><b>Your Balance</b></td>
                                  <td class="remarks" bgcolor="#edf2f8"><b>
                                  <?php 
                                  
                                   echo  number_format($totalBalance, 2);
                                  ?>  
                                  
                                </b> </b>
                                  </td>
                                </tr>
                                <tr class="tests">
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td class="td1">Due Date</td>
                                  <td bgcolor="#a7bfde"><?php echo date("m/d/Y") ?></td>
                                </tr>
    
                                <tr>
                                  <td class="td1"><b>You pay at least</b></td>
                                  <td class="td4" bgcolor="#edf2f8"><b>
                                   
                                  </b></td>
                                </tr>
                                </table>
                             </div>
    
                             
                              <label><b>YOUR PREVIOUS PAYMENTS:</b></label>
                              <table cellspacing="0"  class="table table table-bordered sal-tab1" style="width: 45%; margin-bottom: 0px;  border-collapse: collapse;">
                                <tr align="center">
                                  <th>Date</th>
                                  <th>OR No.</th>
                                  <th>Amount</th>
                                </tr>
                                <?php 
                                  $regnumid = $data["regnum"];
                                  $getAcccount = "SELECT sum(amount) as total,orno, createdat  FROM register
                                  LEFT JOIN studentaccounts  USING (regnum)
                                  LEFT JOIN accounttype USING (actypeid)
                                  INNER JOIN pay USING (accountid)
                                  WHERE regnum  =  $regnumid  Group by orno,createdat ORDER BY createdat ASC";
                                  $queryacc = $view_details->openqry($getAcccount);
                                
                                if($view_details->notEmpty($getAcccount)) {
                                  while ($row = pg_fetch_assoc($queryacc)) {
                                   
                                    echo"<tr>";
                                    echo "<td class='remarks'>".$row["createdat"]."</td>";
                                    echo "<td class='remarks'>".$row["orno"]."</td>";
                                    echo "<td class='remarks'>".number_format($row["total"], 2)."</td>";
                                    echo"</tr>";
                                  
                                  }
                                } else {
                                  echo"<tr>";
                                  echo "<td colspan='3' class='remarks'>No record found...</td>";
                                  echo"</tr>";
                                }
                                ?>
                              </table><br><br><br>
    
                              
                              <div class="req">
                              <table class="table table table-bordered sal-tab1" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
                                <tr>
                                  <td><b>Please Submit the following Requirements:
                                  </b></td>
                                </tr>
                                <tr>
                                  <td><span><b>&#10003; &nbsp;</span>LRN Certificate</td>
                                </b></tr>
                                <tr>
                                  <td><span><b>&#10003; &nbsp;</span>Good Moral Character</b></td>
                                </tr>
                                <tr>
                                  <td><span><b>&#10003; &nbsp;</span>High School Card</b></td>
                                </tr>
                                <tr>
                                  <td><span><b>&#10003; &nbsp;</span>Student Permanent Record</b></td>
                                </tr>
                                <tr>
                                  <td><span><b>&#10003; &nbsp;</span>PSA Birth Certificate</td>
                                </b></tr>
                              </table>
                              </div>
                             
    
                            </fieldset>
                          </div>
           </div>
      </div>
      <?php
             }
        } else {
          echo "<br><br>No record found..";
        }
      
    } elseif ($type_billing == 'grade' || $type_billing == "grade") {
      ?>
                    
      <br>
    
      Grade Level:&nbsp;&nbsp;
      <select class="wid-fix" style="width: 100px!important" name="lvl" id="lvl" onChange="lvlGrade('name');">
        <option value="none">Select..</option>
        <option value="12">12</option>    
        <option value="11">11</option>
      </select>&nbsp;&nbsp;<button  type="button" onclick="printDiv('printableArea')" class="button_local">Print</button>
    
      <?php
    }elseif ($type_billing == 'scholarship' || $type_billing == "scholarship") {
      ?>
                    
    <br>
    
      Scholarhip:&nbsp;&nbsp;
      <select  class="wid-fix" style="width: 100px!important"  name="scholarshipid" id="scholarshipid"  onChange="billScholarship('name');">
        <option value="none">Select...</option>
          <?php

              $getsubject = "SELECT DISTINCT * FROM scholarship ORDER BY name ASC";
              $query = $view_details->openqry($getsubject);
              while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['scholarshipid'].'" '.$select.' >'.$r['name'].'</option>';
              }
          ?>
      </select> &nbsp;&nbsp;<button  type="button" onclick="printDiv('printableArea')" class="button_local">Print</button>
    
      <?php
    }


?>
