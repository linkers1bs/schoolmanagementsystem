<?php 
if (session_status() == PHP_SESSION_NONE) {
        
  session_start();

}
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  $view_details = new ViewCashierDetails;
$branchid =  trim($_POST["branchid"]);
$syid =  trim($_POST["syid"]);
$semid =  trim($_POST["semid"]);
if(isset($_POST['studentid']) || isset($_POST["regnum"])) {
    $id = isset($_POST['studentid']) ? $_POST["studentid"] : (int)$_POST["regnum"];
    $sql = $view_details->get_perbilling($semid,$syid,$branchid,$id,$type_billing = "student");
    if($sql) { 
       $data = pg_fetch_assoc($sql);
    }
   
} elseif (isset($_POST['lvl'])) {
  $sql = $view_details->get_perbilling($semid,$syid,$branchid,$id = $_POST["lvl"],$type_billing = "lvl");
    
  }elseif (isset($_POST['scholarshipid'])) {
    $sql = $view_details->get_perbilling($semid,$syid,$branchid,$id = $_POST["scholarshipid"],$type_billing = "scholarship");
  }elseif (isset($_POST['blockname'])) {
    $sql = $view_details->get_perbilling($semid,$syid,$branchid,$id = $_POST["blockname"],$type_billing = "blockname");
  }

  if($sql) { 

    if(isset($_POST['studentid']) || isset($_POST["regnum"])) {
      ?>
      <br><br>
   <div align="center" style="background-color: #F2F3F4;" class="create" >
        <div align="center" style="padding: 10px;" >

                            <div class="header-billing" >
                              <div>
                                  <h4><b>Modular Billing Statement</b></h4>
                                  <span>For Payment Terms  month of <?php echo $today = date("F Y");  ?></span>
                              </div>
                            </div>
                      

                      <div class="info" style=""> 

                        <table bgcolor="#B0C4DE" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;">
                          <tr>
                            <td>Name :</td>
                            <td class="remarks"><b><?php echo ucfirst($data["lastname"]).", ".ucfirst($data["firstname"])." ".ucfirst($data["middlename"]); ?></b></td>
                            <td>Date :</td>
                            <td  class="remarks"><b><?php  echo $today = date("F j, Y");   ?></b></td>
                          </tr>
                          <tr>
                            <td>LRNID :</td>
                            <td  class="remarks"><b><?php echo $data["lrnid"];?></b></td>
                            <td >Sem :</td>
                            <td  class="remarks"><b><?php echo $data["sem"];?></b></td>
                          </tr>
                          <tr>
                            <td>Course :</td>
                            <td  class="remarks"><b><?php echo $data["coursename"];?></b></td>
                            <td>SY :</td>
                            <td  class="remarks"><b><?php echo $data["sy"];?></b></td>
                          </tr>
                            <tr>
                              <td >Scholarship</td>
                              <td class="remarks" bgcolor="#edf2f8"><b><?php echo $data["scholarshipname"]?></b></td>
                              <td>&nbsp;</td>
                              <td  class="remarks">&nbsp;</td>
                            </tr>
                        </table>

                      </div>




                        <fieldset class="man-marg">
                          
                      
                        <div class="sasses">
                          <table align="left" cellspacing="0" style="width: 50%; padding-top: 10px;" >
                            <tr align="center">
                              <th colspan="2"><b>SCHOOL ASSESSMENT:</b></th>
                            </tr>
                           
                            <?php 
                              $regnumid = $data["regnum"];
                              $getAcccount = "SELECT DISTINCT * FROM register
                              LEFT JOIN studentaccounts  USING (regnum)
                              LEFT JOIN accounttype USING (actypeid)
                              WHERE regnum  = $regnumid  ORDER BY accountid ASC";
                              $queryacc = $view_details->openqry($getAcccount);
                              $paid = 0;
                              $totaldiscount = 0;
                              $totalPaid =0;
                              $total =0;
                              $balance = 0;
                              $totalBalance = 0;
                          
                              while ($row = pg_fetch_assoc($queryacc)) {
                                //get PAY using accountid 
                                $totaldiscount += $row["discount"];
                                $gesPay = "SELECT DISTINCT * FROM pay 
                                WHERE accountid  = '".$row["accountid"]."' ";
                                $queryPay = $view_details->openqry($gesPay);
                                while ($pa = pg_fetch_assoc($queryPay)) { 
                                  $paid += $pa['amount'];
                                }
                      
                                $totalPaid += $paid;
                      
                                  $otherFee = trim($row["name"]);
                               
                                  if($otherFee === "Other fees") {
                                    $otherfeid = trim($row["fees"]);
                                    $getAcccount = "SELECT DISTINCT * FROM register 
                                    LEFT JOIN othersfees USING (regnum)
                                    WHERE othersfeesid  = $otherfeid";
                                    $total += $view_details->getVal('amount',$getAcccount);
                                    $balance = $view_details->getVal('amount',$getAcccount) - $row["discount"] - $paid;
                                    $totalBalance += $balance;
                                      echo"<tr>";
                                      echo "<td class='remarks'>".$view_details->getVal('name',$getAcccount)."</td>";
                                      echo "<td class='remarks'>".number_format($view_details->getVal('amount',$getAcccount), 2)."</td>";
                                      echo"</tr>";
                                     
                                  }else {
                                    $total += $row["fees"];
                                    $balance = $row["fees"] -  $row["discount"] - $paid;
                                   $totalBalance += $balance;
                                    echo"<tr>";
                                    echo "<td class='remarks'>".$row["name"]."</td>";
                                    echo "<td class='remarks'>".number_format($row["fees"], 2)."</td>";
                                    echo"</tr>";
                                  }
                                  $paid = 0;
                              }
                            ?>
                            </tr>
                            <tr>
                              <td class="remarks">Total Discount</td>
                              <td class="remarks" bgcolor="#edf2f8"><?php echo number_format($totaldiscount,2); ?></td>
                            </tr>
                              <tr>
                              <td class="remarks">Total Payables</td>
                              <td class="remarks" bgcolor="#edf2f8"><?php echo number_format($total,2); ?></td>
                            </tr>
                            <tr>
                              <td class="remarks">Payment Terms</td>
                              <td class="remarks" bgcolor="#edf2f8">5 months</td>
                            </tr>
                            <tr>
                              <td class="remarks"><b>Total Amount You Paid</b></td>
                              <td class="remarks" bgcolor="#a7bfde"><b><?php echo number_format($totalPaid,2); ?></b></td>
                            </tr>
                            <tr>
                              <td class="remarks"><b>Your Balance</b></td>
                              <td class="remarks" bgcolor="#edf2f8"><b>
                              <?php 
                              
                               echo  number_format($totalBalance, 2);
                              ?>  
                              
                            </b> </b>
                              </td>
                            </tr>
                            <tr class="tests">
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="td1">Due Date</td>
                              <td bgcolor="#a7bfde"><?php echo date("m/d/Y") ?></td>
                            </tr>

                            <tr>
                              <td class="td1"><b>You pay at least</b></td>
                              <td class="td4" bgcolor="#edf2f8"><b>
                               
                              </b></td>
                            </tr>
                            </table>
                         </div>

                         
                          <label><b>YOUR PREVIOUS PAYMENTS:</b></label>
                          <table cellspacing="0" style="width: 45%; margin-bottom: 0px;  border-collapse: collapse;">
                            <tr align="center">
                              <th>Date</th>
                              <th>OR No.</th>
                              <th>Amount</th>
                            </tr>
                            <?php 
                              $regnumid =$data["regnum"];
                              $getAcccount = "SELECT sum(amount) as total,orno, createdat  FROM register
                              LEFT JOIN studentaccounts  USING (regnum)
                              LEFT JOIN accounttype USING (actypeid)
							                INNER JOIN pay USING (accountid)
                              WHERE regnum  =  $regnumid  Group by orno,createdat ORDER BY createdat ASC";
                              $queryacc = $view_details->openqry($getAcccount);
                            
                            if($view_details->notEmpty($getAcccount)) {
                              while ($row = pg_fetch_assoc($queryacc)) {
                               
                                echo"<tr>";
                                echo "<td class='remarks'>".$row["createdat"]."</td>";
                                echo "<td class='remarks'>".$row["orno"]."</td>";
                                echo "<td class='remarks'>".number_format($row["total"], 2)."</td>";
                                echo"</tr>";
                              
                              }
                            } else {
                              echo"<tr>";
                              echo "<td colspan='3' class='remarks'>No record found...</td>";
                              echo"</tr>";
                            }
                            ?>
                          </table><br><br><br>

                          
                          <div class="req">
                          <table style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
                            <tr>
                              <td><b>Please Submit the following Requirements:
                              </b></td>
                            </tr>
                            <tr>
                              <td><span><b>&#10003; &nbsp;</span>LRN Certificate</td>
                            </b></tr>
                            <tr>
                              <td><span><b>&#10003; &nbsp;</span>Good Moral Character</b></td>
                            </tr>
                            <tr>
                              <td><span><b>&#10003; &nbsp;</span>High School Card</b></td>
                            </tr>
                            <tr>
                              <td><span><b>&#10003; &nbsp;</span>Student Permanent Record</b></td>
                            </tr>
                            <tr>
                              <td><span><b>&#10003; &nbsp;</span>PSA Birth Certificate</td>
                            </b></tr>
                          </table>
                          </div>
                         

                        </fieldset>
                      </div>
       </div>
  </div>
   <?php


    } elseif (isset($_POST["lvl"]) || isset($_POST["scholarshipid"]) || isset($_POST["blockname"])) {
     
       while ($data = pg_fetch_assoc($sql)) {
      ?>
      <hr>
      <style>
          b {
              padding-bottom: 0px!important; 
              border-bottom: none!important;
          }
          </style>
    <div align="center" style="background-color: #F2F3F4;" class="create" >
      <div align="center" style="padding: 10px;" >

                          <div class="header-billing" >
                            <div>
                                <h4><b>Modular Billing Statement</b></h4>
                                <span>For Payment Terms  month of <?php echo $today = date("F Y");  ?></span>
                            </div>
                          </div>
                    

                    <div class="info" style=""> 

                      <table bgcolor="#B0C4DE" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;">
                        <tr>
                          <td>Name :</td>
                          <td class="remarks"><b><?php echo ucfirst($data["lastname"]).", ".ucfirst($data["firstname"])." ".ucfirst($data["middlename"]); ?></b></td>
                          <td>Date :</td>
                          <td  class="remarks"><b><?php  echo $today = date("F j, Y");   ?></b></td>
                        </tr>
                        <tr>
                          <td>LRNID :</td>
                          <td  class="remarks"><b><?php echo $data["lrnid"];?></b></td>
                          <td >Sem :</td>
                          <td  class="remarks"><b><?php echo  $data["sem"];?></b></td>
                        <tr>
                          <td>Course :</td>
                          <td  class="remarks"><b><?php echo $data["coursename"];?></b></td>
                          <td>SY :</td>
                          <td  class="remarks"><b><?php echo $data["sy"];?></b></td>
                        </tr>
                        <tr>
                            <td >Scholarship</td>
                            <td class="remarks" bgcolor="#edf2f8"><b><?php echo $data["scholarshipname"]?></b></td>
                            <td>&nbsp;</td>
                          <td  class="remarks">&nbsp;</td>
                          </tr>
                      </table>

                    </div>




                      <fieldset class="man-marg">
                        
                    
                      <div class="sasses">
                        <table align="left" cellspacing="0" style="width: 50%; padding-top: 10px;" >
                          <tr align="center">
                            <th colspan="2"><b>SCHOOL ASSESSMENT:</b></th>
                          </tr>
                        
                          <?php 
                            $regnumid = $data["regnum"];
                            $getAcccount = "SELECT DISTINCT * FROM register
                            LEFT JOIN studentaccounts  USING (regnum)
                            LEFT JOIN accounttype USING (actypeid)
                            WHERE regnum  = $regnumid  ORDER BY accountid ASC";
                            $queryacc = $view_details->openqry($getAcccount);
                            $paid = 0;
                            $totaldiscount = 0;
                            $totalPaid =0;
                            $total =0;
                            $balance = 0;
                            $totalBalance = 0;
                        
                            while ($row = pg_fetch_assoc($queryacc)) {
                              //get PAY using accountid 
                              $totaldiscount += $row["discount"];
                              $gesPay = "SELECT DISTINCT * FROM pay 
                              WHERE accountid  = '".$row["accountid"]."' ";
                              $queryPay = $view_details->openqry($gesPay);
                              while ($pa = pg_fetch_assoc($queryPay)) { 
                                $paid += $pa['amount'];
                              }
                    
                              $totalPaid += $paid;
                    
                                $otherFee = trim($row["name"]);
                             
                                if($otherFee === "Other fees") {
                                  $otherfeid = trim($row["fees"]);
                                  $getAcccount = "SELECT DISTINCT * FROM register 
                                  LEFT JOIN othersfees USING (regnum)
                                  WHERE othersfeesid  = $otherfeid";
                                  $total += $view_details->getVal('amount',$getAcccount);
                                  $balance = $view_details->getVal('amount',$getAcccount) - $row["discount"] - $paid;
                                  $totalBalance += $balance;
                                    echo"<tr>";
                                    echo "<td class='remarks'>".$view_details->getVal('name',$getAcccount)."</td>";
                                    echo "<td class='remarks'>".number_format($view_details->getVal('amount',$getAcccount), 2)."</td>";
                                    echo"</tr>";
                                   
                                }else {
                                  $total += $row["fees"];
                                  $balance = $row["fees"] -  $row["discount"] - $paid;
                                 $totalBalance += $balance;
                                  echo"<tr>";
                                  echo "<td class='remarks'>".$row["name"]."</td>";
                                  echo "<td class='remarks'>".number_format($row["fees"], 2)."</td>";
                                  echo"</tr>";
                                }
                                $paid = 0;
                            }
                          ?>
                          </tr>
                          <tr>
                            <td class="remarks">Total Discount</td>
                            <td class="remarks" bgcolor="#edf2f8"><?php echo number_format($totaldiscount,2); ?></td>
                          </tr>
                            <tr>
                            <td class="remarks">Total Payables</td>
                            <td class="remarks" bgcolor="#edf2f8"><?php echo number_format($total,2); ?></td>
                          </tr>
                          <tr>
                            <td class="remarks">Payment Terms</td>
                            <td class="remarks" bgcolor="#edf2f8">5 months</td>
                          </tr>
                          <tr>
                            <td class="remarks"><b>Total Amount You Paid</b></td>
                            <td class="remarks" bgcolor="#a7bfde"><b><?php echo number_format($totalPaid,2); ?></b></td>
                          </tr>
                          <tr>
                            <td class="remarks"><b>Your Balance</b></td>
                            <td class="remarks" bgcolor="#edf2f8"><b>
                            <?php 
                            
                             echo  number_format($totalBalance, 2);
                            ?>  
                            
                          </b> </b>
                            </td>
                          </tr>
                          <tr class="tests">
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td class="td1">Due Date</td>
                            <td bgcolor="#a7bfde"><?php echo date("m/d/Y") ?></td>
                          </tr>

                          <tr>
                            <td class="td1"><b>You pay at least</b></td>
                            <td class="td4" bgcolor="#edf2f8"><b>
                             
                            </b></td>
                          </tr>
                          </table>
                       </div>

                       
                        <label><b>YOUR PREVIOUS PAYMENTS:</b></label>
                        <table cellspacing="0" style="width: 45%; margin-bottom: 0px;  border-collapse: collapse;">
                          <tr align="center">
                            <th>Date</th>
                            <th>OR No.</th>
                            <th>Amount</th>
                          </tr>
                          <?php 
                            $regnumid = $data["regnum"];
                            $getAcccount = "SELECT sum(amount) as total,orno, createdat  FROM register
                            LEFT JOIN studentaccounts  USING (regnum)
                            LEFT JOIN accounttype USING (actypeid)
                            INNER JOIN pay USING (accountid)
                            WHERE regnum  =  $regnumid  Group by orno,createdat ORDER BY createdat ASC";
                            $queryacc = $view_details->openqry($getAcccount);
                          
                          if($view_details->notEmpty($getAcccount)) {
                            while ($row = pg_fetch_assoc($queryacc)) {
                             
                              echo"<tr>";
                              echo "<td class='remarks'>".$row["createdat"]."</td>";
                              echo "<td class='remarks'>".$row["orno"]."</td>";
                              echo "<td class='remarks'>".number_format($row["total"], 2)."</td>";
                              echo"</tr>";
                            
                            }
                          } else {
                            echo"<tr>";
                            echo "<td colspan='3' class='remarks'>No record found...</td>";
                            echo"</tr>";
                          }
                          ?>
                        </table><br><br><br>

                        
                        <div class="req">
                        <table style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
                          <tr>
                            <td><b>Please Submit the following Requirements:
                            </b></td>
                          </tr>
                          <tr>
                            <td><span><b>&#10003; &nbsp;</span>LRN Certificate</td>
                          </b></tr>
                          <tr>
                            <td><span><b>&#10003; &nbsp;</span>Good Moral Character</b></td>
                          </tr>
                          <tr>
                            <td><span><b>&#10003; &nbsp;</span>High School Card</b></td>
                          </tr>
                          <tr>
                            <td><span><b>&#10003; &nbsp;</span>Student Permanent Record</b></td>
                          </tr>
                          <tr>
                            <td><span><b>&#10003; &nbsp;</span>PSA Birth Certificate</td>
                          </b></tr>
                        </table>
                        </div>
                       

                      </fieldset>
                    </div>
     </div>
</div>
<?php
       }
    }
   
  }else {
    echo "No record Found......";
  }
?>
      