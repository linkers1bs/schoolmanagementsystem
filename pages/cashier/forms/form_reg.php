<?php
  if (session_status() == PHP_SESSION_NONE) {
  session_start();
  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewOptionDetails.php';
  $db = new DB;
  $optionDetails = new ViewOptionDetails;
  $branchid = $_POST["branchid"];
  $syid = $_POST["syid"];
  $semid = $_POST["semid"];
  $regnum = $_POST["studentid"];
  $sql = "SELECT DISTINCT * FROM studentinfo
  INNER JOIN register USING (studentid) 
  LEFT JOIN studentcourse USING (studentid)
  WHERE register.studentid = '$regnum' AND register.semid = $semid AND register.syid = $syid AND register.branchid = $branchid";

  if(!$db->notEmpty($sql)){
  ?>
    <br>
    <div style="font-size: 12px;"> 
      <span><b>The Student Not Registered on this semeseter or schoolyear:</b></span>
    </div>
  <?php
  }else {

  ?>
  <br>
  <div style="font-size: 12px;"> 
    <span><b> Student Information:</b></span>
  </div>

  <div class="size-16">
    <div class="we-30">
      <span>LRN ID</span>
    </div>
    <input type="text" class="wid-fix" id="lrn" name="lrn" value="<?php echo $db->getVal('lrnid',$sql);?>">
    <input type="hidden" class="wid-fix" id="regnum" name="regnum" value="<?php echo $db->getVal('regnum',$sql);?>">
  </div>
  
  <div class="size-16">
    <div class="we-30">
      <span>Lastname</span>
    </div>
    <input  type="text" class="wid-fix" id="lname" name="lname" value="<?php echo $db->getVal('lastname',$sql);?>">
  </div>

  <div class="size-16">
    <div class="we-30">
      <span>Firstname</span>
    </div>
    <input  type="text" class="wid-fix" id="fname" name="fname" value="<?php echo $db->getVal('firstname',$sql);?>">
  </div>

  <div class="size-16">
    <div class="we-30">
      <span>Middlename</span>
    </div>
    <input type="text" class="wid-fix" id="mname" name="mname" value="<?php echo $db->getVal('middlename',$sql);?>">
  </div>

  <div class="size-16" style="font-size: 12px;">
    <div class="we-30">
      <span>Grade Level</span>
    </div>
    <input id="grade" type="radio" name="grade" value="11" <?php if ( $db->getVal('gradelevel',$sql) == 11) { echo "checked";} ?> > 11 
    &emsp;<input id="grade2" type="radio" name="grade" value="12" <?php if ($db->getVal('gradelevel',$sql) == 12) { echo "checked"; } ?> > 12
  </div>

  <div class="size-16" style="font-size: 12px;">
    <div class="we-30">
        <span>Grant Type</span>
    </div>
  <input id="granttype1" type="radio" name="granttype" value="
  <?php
    $query = $optionDetails->optionDetails('scholarship');
      while ($r = pg_fetch_assoc($query)){
        if(trim($r["name"]) === "QVR") {
        echo $r["scholarshipid"];
      }
    }               
  ?>
  " <?php  
    $getscholar = "SELECT DISTINCT * FROM scholarship WHERE scholarshipid = '".$db->getVal('scholarshipid',$sql)."'";
      $query = $db->openqry($getscholar);
      while ($r = pg_fetch_assoc($query)){
        if(trim($r["name"]) === "QVR") {
        echo "checked";
      }
    }   
  ?>
  > QVR &nbsp;&nbsp;<input type="radio" name="granttype" id="granttype2"  value="<?php
    $query = $optionDetails->optionDetails('scholarship');
      while ($r = pg_fetch_assoc($query)){
        if(trim($r["name"]) === "ESC") {
        echo $r["scholarshipid"];
      }
    }               
  ?>" <?php  
    $getscholar = "SELECT DISTINCT * FROM scholarship WHERE scholarshipid = '".$db->getVal('scholarshipid',$sql)."'";
      $query = $db->openqry($getscholar);
      while ($r = pg_fetch_assoc($query)){
        if(trim($r["name"]) === "ESC") {
        echo "checked";
      }
    }   
  ?> > ESC &emsp; <input id="granttype3" type="radio" name="granttype" value="<?php
    $query = $optionDetails->optionDetails('scholarship');
      while ($r = pg_fetch_assoc($query)){
        if(trim($r["name"]) === "Paying") {
        echo $r["scholarshipid"];
      }
    }               
  ?>" <?php  
      $getscholar = "SELECT DISTINCT * FROM scholarship WHERE scholarshipid = '".$db->getVal('scholarshipid',$sql)."'";
      $query = $db->openqry($getscholar);
      while ($r = pg_fetch_assoc($query)){
        if(trim($r["name"]) === "Paying") {
        echo "checked";
      }
    }   
  ?>> Paying&emsp; 
  </span><input id="others" type="radio" name="granttype" value="others" > Others:&nbsp;&nbsp;&nbsp; 
  <select name="scholarshipid" style="display: none" id="scholarshipid">
  <?php
      $query = $optionDetails->optionDetails('scholarship');
    while ($r = pg_fetch_assoc($query)){
      echo '<option value="'.$r['scholarshipid'].'">'.$r['name'].'</option>';
    }
  ?>
  </select>
  </div>

  <div class="size-16">
    <div class="we-30">
      <span> Couser & Curriculum</span>
    </div>
    <select class="wa wid-fix" id="coursecurriculum" name="coursecurriculum" style="display: inline-block;">
    <?php
      $query = $optionDetails->optionDetails('curriculum');
      while ($r = pg_fetch_assoc($query)){
        $selsub = '';
        if ($db->getVal('curcode',$sql) == $r['curcode'])
        {
         $selsub = 'selected="selected"';
        } 

        echo '<option value="'.$r['curcode'].'"'.$selsub.'>'.$r['coursename'].' - '.$r['curcode'].'</option>';
      }
    ?>
    </select>
  </div>

  <div align="center" class="center-save" id="hide" style="height: 15vh;">
    <button  type="button" id="modify" class='button_local' onclick="modifyregistration(this.id)">Save</button>  
  </div>
  <?php
  }
  ?>