  <div class="tab_modify_data_table1">
    <div class="tablecon modify_reg">
      <form name="modify_reg" id="modify_reg">
        <div class="enroll">
          <b class="line_r">Required Option</b>
        </div>
        <?php
            require 'action/modify_reg.php';
        ?>
      </form>
    </div>
  </div>

  <div class="table_info_show1">
    <div class="tablecon">
      <div class="enroll">
        <b class="line_r">Enrollment Registration Form</b>
      </div>
       <form name="registrion_form" id="registrion_form">
              
      <div class="select-head">
        <div class="hrsb">
          <span><b> Registration for</b></span>
          <div class="selec-g">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 253px!important" name="branchid" id="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
            <span>&nbsp;&nbsp;School Year:&nbsp;&nbsp;</span>
            <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("schoolyear");
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                }
              ?>
            </select>
            <span>&nbsp;&nbsp;Semester:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("semester");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
              }
            ?>
            </select>
          </div>
        </div>
      </div>

      <!-- Student Classification: -->
      <div class="select-head">
        <div class="hrsb">
        <span><b> Student Classification:</b></span>

        <div class="size-16">
          Student Type:
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <select name="studenttypeid" id="studenttypeid" style="display: inline-block;" onchange='paymentHisGradeREG(this)'>
          <?php
            $query = $view_option_details->optionDetails('studenttype');
            while ($r = pg_fetch_assoc($query)){
              $selsub = '';
              if (trim($r['name']) == "SHS")
              {
                $selsub = 'selected="selected"';
              } 
              echo '<option value="'.$r['studenttypeid'].'"'.$selsub.'>'.$r['name'].'</option>';
            }
          ?>
          </select>
        </div>

        <div class="size-16">
          Grade Level
          &emsp;&nbsp;&emsp; 
         <!--  <div id="hide_grade_reg" style="display: inline;">
             <input id="grade" type="radio" name="grade" value="11"> 11 &emsp;
            <input id="grade" type="radio" name="grade" value="12"> 12 <span style="color: red;display: none" id="require_lvl">(Required Gradelvl for the SHS)</span>
          </div> -->
          <div id="show_grade_reg" style="display: inline;">
            <select style="width: 100px!important"  name="grade" id="grade_registration" style="display: inline;"  >
              <option value="0">--SELECT--</option>
              <option value="11">11</option>
              <option value="12">12</option>
            </select><span style="color: red;display: none" id="require_lvl">(Required Gradelvl for the SHS)</span>
          </div>
        </div>

        <div class="size-16">
          <span id="answer">
          Grant Type
          &nbsp;&nbsp;&nbsp;&emsp;&emsp; <input id="granttype1" type="radio" name="granttype" value="
          <?php
            $query = $view_option_details->optionDetails('scholarship');
            while ($r = pg_fetch_assoc($query)){
              if(trim($r["name"]) === "QVR") {
                echo $r["scholarshipid"];
              }
            }               
          ?>
          " > QVR &nbsp;&nbsp;<input type="radio" name="granttype" id="granttype2"  value="<?php
            $query = $view_option_details->optionDetails('scholarship');
            while ($r = pg_fetch_assoc($query)){
              if(trim($r["name"]) === "ESC") {
                echo $r["scholarshipid"];
              }
            }               
          ?>"  > ESC &emsp; <input id="granttype3" type="radio" name="granttype" value="<?php
          $query = $view_option_details->optionDetails('scholarship');
            while ($r = pg_fetch_assoc($query)){
              if(trim($r["name"]) === "Paying") {
                echo $r["scholarshipid"];
              }
          }               
          ?>" > Paying&emsp; 
          </span>
          <input id="others" type="radio" name="granttype" value="others" > Others:&nbsp;&nbsp;&nbsp; 
          <select name="scholarshipid" style="display: none" id="scholarid">
            <?php
              $query = $view_option_details->optionDetails('scholarship');
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['scholarshipid'].'">'.$r['name'].'</option>';
              }
            ?>
          </select>
          <span style="color: red;display: none" id="require_type">(Required)</span>
        </div>

        <div class="size-16">
          <span id="typeofstudent">
          <span>Type of Student</span>&emsp;
          <input type="radio" id="old" name="typeReg" value="Old" > Old &nbsp;&nbsp;&nbsp;
          <input type="radio" id="new" name="typeReg"  value="New"  > New &emsp;
          </span> 
        </div>
        </div>
      </div>
      <!-- END OF Student Classification: -->

      <!-- Student Information -->
      <div class="field-list" id="newstudentinfo" style="display: none;">
          <div class="mar2"> 
            <span><b> Student Information:</b></span>
          </div>
          <div class="size-16">
            <div class="we-30">
              <span>LRN ID</span>
            </div>
            <input type="text" class="wid-fix" id="lrn" name="lrn" >
          </div>
          <div class="size-16">
            <div class="we-30">
              <span>Lastname</span>
            </div>
            <input  type="text" class="wid-fix" id="lname" name="lname">
          </div>
          <div class="size-16">
            <div class="we-30">
              <span>Firstname</span>
            </div>
            <input  type="text" class="wid-fix" id="fname" name="fname" >
          </div>
          <div class="size-16">
            <div class="we-30">
              <span>Middlename</span>
            </div>
            <input type="text" class="wid-fix" id="mname" name="mname">
          </div>
          <div class="size-16">
            <div class="we-30">
              <span> Couser & Curriculum</span>
            </div>
            <select class="wa wid-fix" id="coursecurriculum" name="coursecurriculum" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails('curriculum');
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['curcode'].'">'.$r['coursename'].' - '.$r['curcode'].'</option>';
              }
            ?>
            </select>
          </div>
           <div class="size-16">
            <div class="we-30">
              <span>Section</span>
            </div>
            <select class="wa wid-fix" id="sectionid" name="sectionid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails('section');
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['classsectionid'].'">'.$r['name'].'</option>';
              }
            ?>
            </select>
          </div>
          <div class="body-reg">
            <span><b>Requirements:</b></span><br><br>
            <div class="requremen">
              <div class="requ">  
                <span><input type="checkbox" name="lrncef" id="lrncef" value="true"> Certificate of Completion (LRN Cert)</span>
              </div>
              <div class="requ">  
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="form138" name="form138" value="true"> High School Card (Form 138)</span>
              </div>
              <div class="requ">  
                <span><input type="checkbox" id="form137" name="form137" value="true"> Student Permanent Record Form 137</span>
              </div>
              <div class="requ">  
                <span><input type="checkbox" id="goodmoral"  name="goodmoral" value="true"> Certificate of Good Moral Character</span>
              </div>
              <div class="requ"> 
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="nso"  name="nso" value="true"> NSO Birth Certificate</span>
              </div>
            </div>
          </div> 
      </div>
      <!-- END OF Student Information -->

      <!------------------------------------------------------------------------------- OOLD -->
      <div class="field-list"  id="oldstudent" style="display: none">
          <div class="mar"> 
               <span><b> Student Information:</b></span>
          </div>
         <div class="pad2">
         Student Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <select class="infor wid-fix" name="studentid" id="studentid" style="display: inline-block;">
          <?php

              $query = $view_option_details->optionDetails('studentinfo');
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['studentid'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
              }
          ?>
          </select>
          </div>
          <div class="body-reg">
                <span><b>Requirements:</b></span><br><br>
            <div class="requremen">
               <div class="requ">  
                   <span><input type="checkbox" name="lrncef" value="true"> Certificate of Completion (LRN Cert)</span>
               </div>
               <div class="requ">  
                   <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="" name="form138" value="true"> High School Card (Form 138)</span>
               </div>
               <div class="requ">  
                   <span><input type="checkbox"  name="form137" value="true"> Student Permanent Record Form 137</span>
               </div>
               <div class="requ">  
                   <span><input type="checkbox"  name="goodmoral" value="true"> Certificate of Good Moral Character</span>
               </div>
                <div class="requ"> 
                    <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"  name="nso" value="true"> NSO Birth Certificate</span>
                </div>
            </div>
          </div> 
        </div>
        <!---------- END OOLD -->


      <!-- SAVE BUTTON -->
      <div align="center" class="center-save" id="hide" style="display:none;">
        <button  type="button" id="add" class='button_local' onclick="registerStudent('add')">Save</button>  
      </div>
      </form>

      </div>
  </div>

  <script type="text/javascript">

    function paymentHisGradeREG(value){

    var text = value.options[value.selectedIndex].text;
    
    selectValues = {};
    if(text == "SHS" ){
       selectValues = { "0" :  "--SELECT--" ,"11": "11", "12": "12" };
    }else if (text == "College") {
       selectValues = { "0" :  "--SELECT--" ,"1": "1st Year", "2 Year": "2nd Year","3 Year": "3rd Year","4 Year": "4th Year" };
    } else if (text == "Elementary"){
       selectValues = { 
          "0" :  "--SELECT--" ,
          "1": "1", 
          "2": "2",
          "3": "3",
          "4": "4",
          "5": "5",
          "6": "6"
        };
    }else if (text == "JHS"){
       selectValues = { 
          "0" :  "--SELECT--" ,
          "7": "7", 
          "8": "8",
          "9": "9",
          "10": "10"
        };
    }
    $('#grade_registration option').each(function() {
            $(this).remove();
    });
    $.each(selectValues, function(key, value) {
      $('#grade_registration').append($('<option>', { value : key }).text(value));
    });
    
  }
  </script>