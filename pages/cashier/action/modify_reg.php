<div class="select-head">
  <div class="hrsb">
    <div>
       <div class="container_select">
          <span>Branch:&nbsp;&nbsp;</span>
          <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails('branch');
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
              }
            ?>
          </select>
        </div>
        <div class="container_select">
            <span>School Year:&nbsp;&nbsp;</span>
            <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("schoolyear");
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                }
              ?>
            </select>
        </div>
         <div class="container_select">
            <span>Semester:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("semester");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
              }
            ?>
            </select>
          </div>
           <div class="container_select">
              <span>Name:&nbsp;&nbsp;</span>
              <select style="width: 230px!important"  name="studentid" id="studentid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('studentinfo');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['studentid'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
                  }
                ?>
              </select>
           </div>
           <div class="container_select">
             <span><button  type="button" id="view" class='button_local' onclick="registration_viewInfo(this.id)">View</button></span>
           </div>
    </div>
  </div>

</div>

  <div id="show_modify_reg">
    <!-- DISPLAY FORM -->
  </div>
