<?php

  if (session_status() == PHP_SESSION_NONE) {
  session_start();
  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  $view_details = new ViewCashierDetails;

   if(isset($_POST["type"])){


  		if($_POST["type"] == "student_getname"){

  			$semid = $_POST["semid"];
		  	$branchid = $_POST["branchid"];
		  	$syid = $_POST["syid"];
		  	$studenttypeid = $_POST["studenttypeid"];
		  	$scholarshipid = $_POST["scholarshipid"];
  			?>
  			<div class="container_select">
               <span>Name:&nbsp;&nbsp;&nbsp;</span>
                <select style="width: 230px!important"  name="regnum" id="regnum" style="display: inline-block;" onChange="getStudentAccounts()">
                  <?php
                    $query = $view_details->get_name_register($semid,$syid,$branchid,$studenttypeid,$scholarshipid);
                    if($query){
                    	echo "<option value='none'>--SELECT--</option>";
	                    while ($r = pg_fetch_assoc($query)){
	                       echo '<option value="'.$r['regnum'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
	                    }
	                } else {
	                	echo "<option value='none'>No record found...</option>";
	                }
                  ?>
                </select>
             </div>
  			<?php

  		} elseif ($_POST["type"] == "paymentHistory_studentname") {
        $semid = $_POST["semid"];
        $branchid = $_SESSION["branch_id"];
        $syid = $_POST["syid"];
        $studenttypeid = $_POST["studenttypeid"];
        $gradelevl = $_POST["p_gradelvl"];
        ?>
        <div class="container_select">
           <span>Name:&nbsp;&nbsp;&nbsp;</span>
            <select style="width: 230px!important"  name="regnum" id="regnum" style="display: inline-block;" onChange="getStudentPaymentHistory(this.value)">
              <?php
                $query = $view_details->get_name_register_new($semid,$syid,$branchid,$studenttypeid,$gradelevl);
                if($query){
                  echo "<option value='none'>--SELECT--</option>";
                  while ($r = pg_fetch_assoc($query)){
                     echo '<option value="'.$r['regnum'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
                  }
              } else {
                echo "<option value='none'>No record found...</option>";
              }
              ?>
            </select>
         </div>
        <?php

      } 

  	}
?>
  		
