<?php
    if (session_status() == PHP_SESSION_NONE) {
              
      session_start();

    }
  require '../../controllers/db_controller/theDBConn.php';
  require '../../controllers/db_controller/ViewReportDetails.php';
  $viewdetails = new ViewReportDetails;

   if(isset($_POST["type"])) {
   	if ($_POST["type"]=="enrollmentreport") {	

   		$syid = $_POST["syid"];
      $semid = $_POST["semid"];
      $branchid = $_POST["branchid"];
      $studenttypeid = $_POST["studenttypeid"];
        
      if ($_POST["studenttypeid"] == '2') {
          ?>
          <?php
  ?>

		<div class="tablecon" align="center">
			<div class="table-responsive">
			<table style="width: 100%;">
                  <thead>
                    <tr style="background-color: #666" align="center" class="table-heads">
                      <th>Categories</th>
                      <th>Grade 11</th>
                      <th>Grade 12</th>
                      <th>Total</th>
                    </tr>
                    <tr>
                     <?php
                        $offer = $viewdetails->enrollementReport();
                        if ($offer) {
                            while($row = pg_fetch_assoc($offer)){
                            ?>
                                <tr>
                                  <td><?=$row['categories']?></td>
                                  <td><?=$row['grade11']?></td>
                                  <td><?=$row["grade12"]?></td>
                                  <td><?=$row["total"]?></td>
                              </tr>
                            <?php
                          }
                        }
                    ?>
                    <tr>
                      <th>Scholarship Status: Enrolled Student Only</th>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <?php
                        $offer = $viewdetails->enrollementReportScholarship();
                        if ($offer) {
                            while($row = pg_fetch_assoc($offer)){
                            ?>
                                <tr>
                                  <td><?=$row['scholarshipname']?></td>
                                  <td><?=$row['grade11']?></td>
                                  <td><?=$row["grade12"]?></td>
                                  <td><?=$row["total"]?></td>
                              </tr>
                            <?php
                          }
                        }
                    ?>
                    <tr>
                      <th>Number of Enrolled Students by Course</th>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <?php
                        $offer = $viewdetails->enrollementReportCourse();
                        if ($offer) {
                            while($row = pg_fetch_assoc($offer)){
                            ?>
                                <tr>
                                  <td><?=$row['coursename']?></td>
                                  <td><?=$row['grade11']?></td>
                                  <td><?=$row["grade12"]?></td>
                                  <td><?=$row["total"]?></td>
                              </tr>
                            <?php
                          }
                        }
                    ?>
                    
                </table>
            </div>
                </div>

                <?php

        }





   	}
  }


  ?>

