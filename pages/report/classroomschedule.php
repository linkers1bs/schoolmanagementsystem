 <div class="tablecon classroomschedule"> 
  <form name="classroomschedule" id="classroomschedule">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
           <div class="container_select">
             <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 250px!important" name="branchid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("branch");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'">'.$r['name'].'</option>';
                  }
                ?>
              </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid"  style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid"  style="display: inline-block;" onChange="getroomname();">
                  <option value="none">--SELECT--</option>
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                // if($r["studenttypeid"] == "2"){
                //   $select = 'selected';
                // }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
         <div class="container_select">
            <div id="roomcode" style="display: inline-block;"> 
              
            </div>
         </div>
      </div>
    </div>


      <div id="display_classroomschedule" >
              <!-- DISPLAY PRINT COR -->
      </div>  
  </form>
</div>
       
<script type="text/javascript">
  
  function getroomname() { 
    $('#display_classroomschedule').hide();
     $.ajax({
          type : 'POST',
          url  : 'pages/report/roomview.php',
          data : $('#classroomschedule').serialize()+'&action=',
          beforeSend:function (){
            $('#action_loading').show();
          },
          success:function(data){
            $('#action_loading').hide();
            $('#roomcode').html(data);
        }
      });
  }

  function getroomdetails() { 
     $.ajax({
          type : 'POST',
          url  : 'pages/report/roomview.php',
          data : $('#classroomschedule').serialize()+'&action=classroomschedule',
          beforeSend:function (){
            $('#action_loading').show();
          },
          success:function(data){
            $('#action_loading').hide();
            $('#display_classroomschedule').show();
            $('#display_classroomschedule').html(data);
        }
      });
  }

   
</script>
          



