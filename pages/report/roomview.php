
<?php
    if (session_status() == PHP_SESSION_NONE) {
        
          session_start();

    }

   if(isset($_POST["action"])) {
   	if ($_POST["action"] == "classroomschedule") {	
        require '../../controllers/db_controller/theDBConn.php';
        require '../../controllers/db_controller/ViewManagementDetails.php';
        $db = new DB;
     		$syid = $_POST["syid"];
        $semid = $_POST["semid"];
        $studenttypeid = $_POST["studenttypeid"];
        $roomcode = $_POST["roomcode"];
        $branchid = $_POST["branchid"];

        $time = [
            '07:00' => [
              '07:00 07:30' => 1,
              '07:00 08:00' => 2,
              '07:00 08:30' => 3,
              '07:00 09:00' => 4,
              '07:00 09:30' => 5,
              '07:00 10:00' => 6,
              '07:00 10:30' => 7,
              '07:00 11:00' => 8,
              '07:00 11:30' => 9,
              '07:00 12:00' => 10,
              '07:00 12:30' => 11,
              '07:00 01:00' => 12,
              '07:00 01:30' => 13,
              '07:00 02:00' => 14,
              '07:00 02:30' => 15,
              '07:00 03:00' => 16,
              '07:00 03:30' => 17,
            ],
            '07:30' => [
              '07:30 08:00' => 1,
              '07:30 08:30' => 2,
              '07:30 09:00' => 3,
              '07:30 09:30' => 4,
              '07:30 10:00' => 5,
              '07:30 10:30' => 6,
              '07:30 11:00' => 7,
              '07:30 11:30' => 8,
              '07:30 12:00' => 9,
              '07:30 12:30' => 10,
              '07:30 01:00' => 11,
              '07:30 01:30' => 12,
              '07:30 02:00' => 13,
              '07:30 02:30' => 14,
              '07:30 03:00' => 15,
              '07:30 03:30' => 16,
            ],
            '08:00' => [
              '08:00 08:30' => 1,
              '08:00 09:00' => 2,
              '08:00 09:30' => 3,
              '08:00 10:00' => 4,
              '08:00 10:30' => 5,
              '08:00 11:00' => 6,
              '08:00 11:30' => 7,
              '08:00 12:00' => 8,
              '08:00 12:30' => 9,
              '08:00 01:00' => 10,
              '08:00 01:30' => 11,
              '08:00 02:00' => 12,
              '08:00 02:30' => 13,
              '08:00 03:00' => 14,
              '08:00 03:30' => 15,
            ],
            '08:30' => [
              '08:30 09:00' => 1,
              '08:30 09:30' => 2,
              '08:30 10:00' => 3,
              '08:30 10:30' => 4,
              '08:30 11:00' => 5,
              '08:30 11:30' => 6,
              '08:30 12:00' => 7,
              '08:30 12:30' => 8,
              '08:30 01:00' => 9,
              '08:30 01:30' => 10,
              '08:30 02:00' => 11,
              '08:30 02:30' => 12,
              '08:30 03:00' => 13,
              '08:30 03:30' => 14,
              '08:30 04:00' => 15,
            ],
            '09:00' => [
              '09:00 09:30' => 1,
              '09:00 10:00' => 2,
              '09:00 10:30' => 3,
              '09:00 11:00' => 4,
              '09:00 11:30' => 5,
              '09:00 12:00' => 6,
              '09:00 12:30' => 7,
              '09:00 01:00' => 8,
              '09:00 01:30' => 9,
              '09:00 02:00' => 10,
              '09:00 02:30' => 11,
              '09:00 03:00' => 12,
              '09:00 03:30' => 13,
              '09:00 04:00' => 14,
              '09:00 04:30' => 15,
            ],
            '09:30' => [
              '09:30 10:00' => 1,
              '09:30 10:30' => 2,
              '09:30 11:00' => 3,
              '09:30 11:30' => 4,
              '09:30 12:00' => 5,
              '09:30 12:30' => 6,
              '09:30 01:00' => 7,
              '09:30 01:30' => 8,
              '09:30 02:00' => 9,
              '09:30 02:30' => 10,
              '09:30 03:00' => 11,
              '09:30 03:30' => 12,
              '09:30 04:00' => 13,
              '09:30 04:30' => 14,
              '09:30 05:00' => 15,
            ],
            '10:00' => [
              '10:00 10:30' => 1,
              '10:00 11:00' => 2,
              '10:00 11:30' => 3,
              '10:00 12:00' => 4,
              '10:00 12:30' => 5,
              '10:00 01:00' => 6,
              '10:00 01:30' => 7,
              '10:00 02:00' => 8,
              '10:00 02:30' => 9,
              '10:00 03:00' => 10,
              '10:00 03:30' => 11,
              '10:00 04:00' => 12,
              '10:00 04:30' => 13,
              '10:00 05:00' => 14,
              '10:00 05:30' => 15,
            ],
            '10:30' => [
              '10:30 11:00' => 1,
              '10:30 11:30' => 2,
              '10:30 12:00' => 3,
              '10:30 12:30' => 4,
              '10:30 01:00' => 5,
              '10:30 01:30' => 6,
              '10:30 02:00' => 7,
              '10:30 02:30' => 8,
              '10:30 03:00' => 9,
              '10:30 03:30' => 10,
              '10:30 04:00' => 11,
              '10:30 04:30' => 12,
              '10:30 05:00' => 13,
              '10:30 05:30' => 14,
              '10:30 06:00' => 15,
            ],
            '11:00' => [
              '11:00 11:30' => 1,
              '11:00 12:00' => 2,
              '11:00 12:30' => 3,
              '11:00 01:00' => 4,
              '11:00 01:30' => 5,
              '11:00 02:00' => 6,
              '11:00 02:30' => 7,
              '11:00 03:00' => 8,
              '11:00 03:30' => 9,
              '11:00 04:00' => 10,
              '11:00 04:30' => 11,
              '11:00 05:00' => 12,
              '11:00 05:30' => 13,
              '11:00 06:00' => 14,
              '11:00 06:30' => 15,
            ],
            '11:30' => [
              '11:30 12:00' => 1,
              '11:30 12:30' => 2,
              '11:30 01:00' => 3,
              '11:30 01:30' => 4,
              '11:30 02:00' => 5,
              '11:30 02:30' => 6,
              '11:30 03:00' => 7,
              '11:30 03:30' => 8,
              '11:30 04:00' => 9,
              '11:30 04:30' => 10,
              '11:30 05:00' => 11,
              '11:30 05:30' => 12,
              '11:30 06:00' => 13,
              '11:30 06:30' => 14,
              '11:30 07:00' => 15,
            ],
            '12:00' => [
              '12:00 12:30' => 1,
              '12:00 01:00' => 2,
              '12:00 01:30' => 3,
              '12:00 02:00' => 4,
              '12:00 02:30' => 5,
              '12:00 03:00' => 6,
              '12:00 03:30' => 7,
              '12:00 04:00' => 8,
              '12:00 04:30' => 9,
              '12:00 05:00' => 10,
              '12:00 05:30' => 11,
              '12:00 06:00' => 12,
              '12:00 06:30' => 13,
              '12:00 07:00' => 14,
            ],
            '12:30' => [
              '12:30 01:00' => 1,
              '12:30 01:30' => 2,
              '12:30 02:00' => 3,
              '12:30 02:30' => 4,
              '12:30 03:00' => 5,
              '12:30 03:30' => 6,
              '12:30 04:00' => 7,
              '12:30 04:30' => 8,
              '12:30 05:00' => 9,
              '12:30 05:30' => 10,
              '12:30 06:00' => 11,
              '12:30 06:30' => 12,
              '12:30 07:00' => 13,
            ],
            '01:00' => [
              '01:00 01:30' => 1,
              '01:00 02:00' => 2,
              '01:00 02:30' => 3,
              '01:00 03:00' => 4,
              '01:00 03:30' => 5,
              '01:00 04:00' => 6,
              '01:00 04:30' => 7,
              '01:00 05:00' => 8,
              '01:00 05:30' => 9,
              '01:00 06:00' => 10,
              '01:00 06:30' => 11,
              '01:00 07:00' => 12,
            ],
            '01:30' => [
              '01:30 02:00' => 1,
              '01:30 02:30' => 2,
              '01:30 03:00' => 3,
              '01:30 03:30' => 4,
              '01:30 04:00' => 5,
              '01:30 04:30' => 6,
              '01:30 05:00' => 7,
              '01:30 05:30' => 8,
              '01:30 06:00' => 9,
              '01:30 06:30' => 10,
              '01:30 07:00' => 11,
            ],
            '02:00' => [
              '02:00 02:30' => 1,
              '02:00 03:00' => 2,
              '02:00 03:30' => 3,
              '02:00 04:00' => 4,
              '02:00 04:30' => 5,
              '02:00 05:00' => 6,
              '02:00 05:30' => 7,
              '02:00 06:00' => 8,
              '02:00 06:30' => 9,
              '02:00 07:00' => 10,
            ],
            '02:30' => [
              '02:30 03:00' => 1,
              '02:30 03:30' => 2,
              '02:30 04:00' => 3,
              '02:30 04:30' => 4,
              '02:30 05:00' => 5,
              '02:30 05:30' => 6,
              '02:30 06:00' => 7,
              '02:30 06:30' => 8,
              '02:30 07:00' => 9,
            ],
            '03:00' => [
              '03:00 03:30' => 1,
              '03:00 04:00' => 2,
              '03:00 04:30' => 3,
              '03:00 05:00' => 4,
              '03:00 05:30' => 5,
              '03:00 06:00' => 6,
              '03:00 06:30' => 7,
              '03:00 07:00' => 8,
            ],
            '03:30' => [
              '03:30 04:00' => 1,
              '03:30 04:30' => 2,
              '03:30 05:00' => 3,
              '03:30 05:30' => 4,
              '03:30 06:00' => 5,
              '03:30 06:30' => 6,
              '03:30 07:00' => 7,
            ],
            '04:00' => [
              '04:00 04:30' => 1,
              '04:00 05:00' => 2,
              '04:00 05:30' => 3,
              '04:00 06:00' => 4,
              '04:00 06:30' => 5,
              '04:00 07:00' => 6,
            ],
            '04:30' => [
              '04:30 05:00' => 1,
              '04:30 05:30' => 2,
              '04:30 06:00' => 3,
              '04:30 06:30' => 4,
              '04:30 07:00' => 5,
            ],
            '05:00' => [
              '05:00 05:30' => 1,
              '05:00 06:00' => 2,
              '05:00 06:30' => 3,
              '05:00 07:00' => 4,
            ],
            '05:30' => [
              '05:30 06:00' => 1,
              '05:30 06:30' => 2,
              '05:30 07:00' => 3,
            ],
            '06:00' => [
              '06:00 06:30' => 1,
              '06:00 07:00' => 2,
            ],
            '06:30' => [
              '06:30 07:00' => 1,
            ],
        ];


        $time_schedule = [
          '07:00 AM - 07:30 AM',
          '07:30 AM - 08:00 AM',
          '08:00 AM - 08:30 AM',
          '08:30 AM - 09:00 AM',
          '09:00 AM - 09:30 AM',
          '09:30 AM - 10:00 AM',
          '10:00 AM - 10:30 AM',
          '10:30 AM - 11:00 AM',
          '11:00 AM - 11:30 AM',
          '11:30 AM - 12:00 PM',
          '12:00 PM - 12:30 PM',
          '12:30 PM - 01:00 PM',
          '01:00 PM - 01:30 PM',
          '01:30 PM - 02:00 PM',
          '02:00 PM - 02:30 PM',
          '02:30 PM - 03:00 PM',
          '03:00 PM - 03:30 PM',
          '03:30 PM - 04:00 PM',
          '04:00 PM - 04:30 PM',
          '04:30 PM - 05:00 PM',
          '05:00 PM - 05:30 PM',
          '05:30 PM - 06:00 PM',
          '06:00 PM - 06:30 PM',
          '06:30 PM - 07:00 PM',
        ];
        $count_schedule = count($time_schedule);

?>
    <br>
    <div class="table-responsive">
				<table>
            <tr align="center">
                <th rowspan="2">Time</th>
                <th colspan="8" >Classrooms - <b><?php echo $_POST["roomcode"]?></b></th>
            </tr>
            <tr align="center">
                <td>Mon</td>
                <td>Tue</td>
                <td>Wed</td>
                <td>Thur</td>
                <td>Fri</td>
                <td>Sat</td>
                <td>Sun</td>
            </tr>
            
              <?php

                $sql = "SELECT branchid FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                              WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' 
                                ORDER BY starttime ASC";
                if ($db->notEmpty($sql)) {
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%M%'
                              ORDER BY starttime ASC";
                    $mon_schedule = [];// done
                    $tue_schedule = [];
                    $we_schedule = [];
                    $thur_schedule = [];//done
                    $fri_schedule = [];
                    $sta_schedule = [];
                    $sun_schedule = [];
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($mon_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }

                    //tuesday
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%T%'
                              ORDER BY starttime ASC";
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($tue_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }
                    //tuesday

                    //wednesday
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity  FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%W%'
                              ORDER BY starttime ASC";
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($we_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }
                    //end wednesday

                    //thursday
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%H%'
                              ORDER BY starttime ASC";
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($thur_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }
                    //eendthursday

                     //friday
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%F%'
                              ORDER BY starttime ASC";
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($fri_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }
                    //eend friday

                     //sta_schedule
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity  FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%S%'
                              ORDER BY starttime ASC";
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($sta_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }
                    
                    //eend sta_schedule

                     //sun_schedule
                    $sql = "SELECT CONCAT(starttime, ' ', endtime) as scheduletime, starttime, endtime, subjectcode,blockname,capacity FROM schedules
                            INNER JOIN classschedules USING (scheduleid)
                            WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND roomcode = '$roomcode' AND studenttypeid = $studenttypeid
                            AND days LIKE '%U%'
                              ORDER BY starttime ASC";
                    $query = $db->openqry($sql);
                    while ($data = pg_fetch_assoc($query)){
                      $starttime = date('h:i', strtotime($data["starttime"]));
                      $endtime = date('h:i', strtotime($data["endtime"]));
                      $scheduletime = $starttime.' '.$endtime;
                      array_push($sun_schedule, [ $scheduletime,$starttime,$endtime,$data["subjectcode"], $data["blockname"], $data["capacity"] ]);
                    }
                    //eend sun_schedule

                      $getrowspan_monday = 0;
                      $getrowspan_tuesday = 0;
                      $getrowspan_thurday = 0;
                      $getrowspan_wed = 0;
                      $getrowspan_fri = 0;
                      $getrowspan_saturday = 0;
                      $getrowspan_sunday = 0;
                      for ($i = 0; $i < $count_schedule; $i++) { 
                          echo "<tr>";
                          echo "<td>".$time_schedule[$i]."</td>";
                          //FOR MONDAY 
                          if (count($mon_schedule) > 0) {
                            for ($mon=0; $mon < count($mon_schedule) ; $mon++) { 
                                if ($mon_schedule[$mon][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$mon_schedule[$mon][1]][$mon_schedule[$mon][0]]."'>".$mon_schedule[$mon][3]."<br>".$mon_schedule[$mon][4]."<br>10/".$mon_schedule[$mon][5]."</td>";
                                  $getrowspan_monday = $time[$mon_schedule[$mon][1]][$mon_schedule[$mon][0]];
                                }
                            }
                          }
                          if ($getrowspan_monday == 0 ) {
                            echo "<td></td>";
                          }
                          //END FOR MONDAY

                          //tuesday
                          if (count($tue_schedule) > 0) {
                            for ($tue=0; $tue < count($tue_schedule) ; $tue++) { 
                                if ($tue_schedule[$tue][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$tue_schedule[$tue][1]][$tue_schedule[$tue][0]]."'>".$tue_schedule[$tue][3]."<br>".$tue_schedule[$tue][4]."<br>10/".$tue_schedule[$tue][5]."</td>";
                                  $getrowspan_tuesday = $time[$tue_schedule[$tue][1]][$tue_schedule[$tue][0]];
                                }
                            }
                          }
                          if ($getrowspan_tuesday == 0 ) {
                            echo "<td></td>";
                          }
                          //end of tuesday

                          //wednesday
                          if (count($we_schedule) > 0) {
                            for ($wed=0; $wed < count($we_schedule) ; $wed++) { 
                                if ($we_schedule[$wed][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$we_schedule[$wed][1]][$we_schedule[$wed][0]]."'>".$we_schedule[$wed][3]."<br>".$we_schedule[$wed][4]."<br>10/".$we_schedule[$wed][5]."</td>";
                                  $getrowspan_wed = $time[$we_schedule[$wed][1]][$we_schedule[$wed][0]];
                                }
                            }
                          }
                          if ($getrowspan_wed == 0 ) {
                            echo "<td></td>";
                          }
                          //end of wednesday


                          //thursday
                          if (count($thur_schedule) > 0) {
                            for ($thur=0; $thur < count($thur_schedule) ; $thur++) { 
                                if ($thur_schedule[$thur][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$thur_schedule[$thur][1]][$thur_schedule[$thur][0]]."'>".$thur_schedule[$thur][3]."<br>".$thur_schedule[$thur][4]."<br>10/".$thur_schedule[$thur][5]."</td>";
                                  $getrowspan_thurday = $time[$thur_schedule[$thur][1]][$thur_schedule[$thur][0]];
                                }
                            }
                          }
                          if ($getrowspan_thurday == 0 ) {
                            echo "<td></td>";
                          }
                          //end of thursday


                          //fri_schedule
                          if (count($fri_schedule) > 0) {
                            for ($frid=0; $frid < count($fri_schedule) ; $frid++) { 
                                if ($fri_schedule[$frid][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$fri_schedule[$frid][1]][$fri_schedule[$frid][0]]."'>".$fri_schedule[$frid][3]."<br>".$fri_schedule[$frid][4]."<br>10/".$fri_schedule[$frid][5]."</td>";
                                  $getrowspan_fri = $time[$fri_schedule[$frid][1]][$fri_schedule[$frid][0]];
                                }
                            }
                          }
                          if ($getrowspan_fri == 0 ) {
                            echo "<td></td>";
                          }
                          //end of fri_schedule

                          
                          //sta_schedule
                          if (count($sta_schedule) > 0) {
                            for ($sta=0; $sta < count($sta_schedule) ; $sta++) { 
                                if ($sta_schedule[$sta][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$sta_schedule[$sta][1]][$sta_schedule[$sta][0]]."'>".$sta_schedule[$sta][3]."<br>".$sta_schedule[$sta][4]."<br>10/".$sta_schedule[$sta][5]."</td>";
                                  $getrowspan_saturday = $time[$sta_schedule[$sta][1]][$sta_schedule[$sta][0]];
                                }
                            }
                          }
                          if ($getrowspan_saturday == 0 ) {
                            echo "<td></td>";
                          }
                          //end of sta_schedule


                          //sta_schedule
                          if (count($sun_schedule) > 0) {
                            for ($sun=0; $sun < count($sun_schedule) ; $sun++) { 
                                if ($sun_schedule[$sun][1] == substr($time_schedule[$i], 0,5)) {
                                  echo "<td style='background-color:#948D8D;font-size: 12px;font-weight: bold;' rowspan='".$time[$sun_schedule[$sun][1]][$sun_schedule[$sun][0]]."'>".$sun_schedule[$sun][3]."<br>".$sun_schedule[$sun][4]."<br>10/".$sun_schedule[$sun][5]."</td>";
                                  $getrowspan_sunday = $time[$sun_schedule[$sun][1]][$sun_schedule[$sun][0]];
                                }
                            }
                          }
                          if ($getrowspan_sunday == 0 ) {
                            echo "<td></td>";
                          }
                          //end of sta_schedule

                          echo "</tr>";
                          

                          //decress the td
                          if ($getrowspan_monday ==0) {
                              $getrowspan_monday;
                          }else {
                             $getrowspan_monday--;
                          }

                          if ($getrowspan_tuesday ==0) {
                              $getrowspan_tuesday;
                          }else {
                             $getrowspan_tuesday--;
                          }

                          if ($getrowspan_wed ==0) {
                              $getrowspan_wed;
                          }else {
                             $getrowspan_wed--;
                          }

                          if ($getrowspan_thurday ==0) {
                              $getrowspan_thurday;
                          }else {
                             $getrowspan_thurday--;
                          }

                          if ($getrowspan_fri ==0) {
                              $getrowspan_fri;
                          }else {
                             $getrowspan_fri--;
                          }

                          if ($getrowspan_saturday ==0) {
                              $getrowspan_saturday;
                          }else {
                             $getrowspan_saturday--;
                          }

                          if ($getrowspan_sunday ==0) {
                              $getrowspan_sunday;
                          }else {
                             $getrowspan_sunday--;
                          }

                      } 

                } else {

                  //if no data the room
                 for ($i=0; $i < $count_schedule; $i++) { 
                  echo "<tr>";
                    echo "<td>".$time_schedule[$i]."</td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                    echo "<td></td>";
                  echo "</tr>";
                  } 
                }
              ?>
        
      </table>
    </div>

<?php 
  }else {
        require '../../controllers/db_controller/theDBConn.php';
        require '../../controllers/db_controller/ViewManagementDetails.php';
        $db = new DB;
        $syid = $_POST["syid"];
        $semid = $_POST["semid"];
        $branchid = $_POST["branchid"];
        $studenttypeid = $_POST["studenttypeid"];
      ?>

      <span>Room:&nbsp;&nbsp;</span>
        <select style="width: 100px!important"  name="roomcode"  style="display: inline-block;" onChange="getroomdetails();">
              <option value="none">--SELECT--</option>
        <?php
          $sql = "SELECT roomcode FROM schedules
                  INNER JOIN classschedules USING (scheduleid)
                 WHERE semid = $semid AND syid = $syid AND branchid = $branchid AND studenttypeid = $studenttypeid
                    GROUP BY roomcode ORDER BY roomcode ASC";
          $query = $db->openqry($sql);
          while ($r = pg_fetch_assoc($query)){
             echo '<option value="'.$r['roomcode'].'" >'.$r['roomcode'].'</option>';
          }

        ?>
        </select>
      <?php
  } 
} ?>