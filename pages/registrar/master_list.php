<div class="tab_add_data_table5">
    <?php require 'class_roster.php' ?>
</div>

<div class="tab_modify_data_table5">
  <div class="tablecon">
     <b style="font-size: 20px; margin-left: 41%">Student's List</b>
     <?php require 'forms/student_list.php';  ?>
  </div>
</div>

<div class="table_info_show5">
  <div class="tablecon master_list_forms">
    <form name="master_list_forms" id="master_list_forms">
      <div class="enroll">
      <b class="line_r">Master List Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
             <div class="container_select">
                <span>Branch:&nbsp;&nbsp;</span>
                <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails('branch');
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                </select>
              </div>
              <div class="container_select">
                  <span>School Year:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("schoolyear");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                      }
                    ?>
                  </select>
              </div>
               <div class="container_select">
                  <span>Semester:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("semester");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                <div class="container_select">
                  <span>Type:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("studenttype");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['studenttypeid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                 <div class="container_select">
                   <span><button  type="button" id="roster_table" class='button_local' onclick="master_list(this.id)">SELECT</button></span>
                 </div>

                <div id="show-masterlist" style="display:block;width:100%;">
                  <!-- DISPLAY CLASS ROSTER TABLE -->
                </div>
          </div>
        </div>
      </div>
    </form>
    <bR>
     
  </div>
</div>

<script type="text/javascript">

  function master_list(type){
    // alert( $('#class_roster_form').serialize());
    if(type == "roster_table"){
      var data_html = "show-masterlist";
      $.ajax({
        type : 'POST',
        url  : 'pages/registrar/forms/master_list_forms.php',
        data : $('#master_list_forms').serialize()+'&type=roster_table',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function (data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    }
  }


</script>