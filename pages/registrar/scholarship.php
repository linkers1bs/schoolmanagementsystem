<div class="tab_add_data_table2">
  <div class="adds scholar_form_add">
    <form name="scholar_form_add" id="scholar_form_add">
     <b>Add New Scholarship</b>
      <?php
          require 'forms/form_scholarship.php';
        ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" value="add"  onclick="scholar_controller(this.id)">Save</button>  
      </div>
    </form>
  </div>
</div>

<div class="tab_modify_data_table2">
  <div class="adds scholar_form_modify">
    <form name="scholar_form_modify" id="scholar_form_modify">
      <b>Required Option</b>
          <div class="size-16">
             <div class="we-40">
                 <span>Select Scholarship:</span>
             </div>
            <select class="wid-fix2"  name="scholarshipid" id="scholarshipid" onChange="scholar_Modify();">
                 <?php
                  $sql = $view_details->scholar_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                        echo '<option value="'.$r['scholarshipid'].'">'.$r['name'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
            </select>
        </div>

        <div class="requroption">
          <span>Scholarship Information</span>
        </div>
        <div id="show_scholar_modify">
          
        </div>
        <div class="buttonsave">
          <button  type="button" class='button_local' value="update" id='update' onclick="scholar_controller(this.id)"> Update </button>
      </div>
    </form>
  </div>
</div>

<div class="tab_delete_data_table2">
  <div class="adds scholar_form_delete">
    <form name="scholar_form_delete" id="scholar_form_delete">
      <b>Required Option</b>
          <div class="size-16">
             <div class="we-40">
                 <span>Select Scholarship:</span>
             </div>
            <select class="wid-fix2"  name="scholarshipid" id="scholarshipid" onChange="scholar_Delete();">
                 <?php
                  $sql = $view_details->scholar_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                        echo '<option value="'.$r['scholarshipid'].'">'.$r['name'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
            </select>
        </div>

        <div class="requroption">
          <span>Scholarship Information</span>
        </div>
        <div id="show_scholar_delete">
          
        </div>
        <div class="buttonsave">
          <button  type="button" class='button_local' value="delete" id='delete' onclick="scholar_controller(this.id)"> Delete </button>
      </div>
    </form>
  </div>
</div>

<div class="table_info_show2">
  <div class="tablecon">
      <div class="requroption">
        <span><b>List of Scholarship's</b></span>
      </div>
        <div class="spacetable">
          <div class="table-responsive">
          <table>
          <thead>
            <tr style="background-color: #666" align="center" class="table-heads">
              <th>Scholarship</th>
              <th>Description</th>
            </tr>
          </thead>
            <?php

            $sql = $view_details->scholar_details();
            if ($sql) {
                while($r = pg_fetch_assoc($sql)){
             ?>
                <tr>
                  <td class="remarks"><?php echo $r["name"]; ?></td>
                  <td class="remarks"></td>
                </tr>
            <?php
                }
                // end while
            }else {
              echo "<td  class='remarks' colspan=2 >No Data Found...</td>";
            }
          ?>
            
          </table>
        </div>
    </div>
  </div>
</div>