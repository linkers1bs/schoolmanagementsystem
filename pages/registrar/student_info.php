<div class="table_info_show6">
  <div class="tablecon">
    <div class="requroption">
      <span><b>Required Option</b></span><br><br>
        <span>Select Student Name:</span>&nbsp;&nbsp;
          <select class="wid-fix" name="studentid" id="info-studentid"  onChange="studentInfoSel('info');">
             <?php
                $query = $view_option_details->optionDetails("studentinfo");
                echo "<option value='none'>Select name..</option>";
                while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['studentid'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename'][0]).'.</option>';
               }
            ?>
          </select>
     <hr>
     
     <div id="studentinfo-data">

     </div>
    
    </div>
  </div>
</div>


<script type="text/javascript">
  
  function studentInfoSel(name) {
        var studentid = $('#'+name+'-studentid :selected').val();
        document.getElementById('studentinfo-data').style.display = 'none';

        if(studentid != 'none' || studentid != "none") {
          document.getElementById('studentinfo-data').style.display = 'block';
          $.post("pages/registrar/forms/form_studentinfo.php",{ 
                studentid      : studentid, 
                action        : 'studentinfo'
                },
                function(data) {
                $("#studentinfo-data").html(data);
          });
         
        }else {
          document.getElementById('studentinfo-data').style.display = 'none';
        }
        // document.getElementById('show5-data').style.display = 'none';
        
  }

</script>