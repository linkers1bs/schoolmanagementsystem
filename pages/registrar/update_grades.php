<div class="tablecon grademanage"> 
  <form name="grademanage" id="grademanage">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 180px!important" name="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid"  style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid"  style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select" style="display: inline-block;">
            <span><button  type="button" id="gradeblock" class='button_local' onclick="show_grade_manage(this.id)">SELECT</button></span>
          </div>
          <div class="container_select" style="display: inline-block;">
            <div id="grade_form" style="display: inline-block;"> 
              <!-- DISPLAYT NAME OF REGISTER -->
            </div>
          </div>
        </div>
      </div>

    </div>
      <div id="display_grademanage" >
              <!-- DISPLAY PRINT COR -->
      </div>  
  </form>
</div>

<script type="text/javascript">
  
function show_grade_manage(type) {
   if (type == "gradeblock") {
    var data_html = "grade_form";
    $.ajax({
      type : 'POST',
      url : 'pages/registrar/forms/updategrades_forms.php',
      data : $('#grademanage').serialize()+'&type=gradeblock',
      beforeSend: function(){
        $('#action_loading').show();
      },
      success :function (data){
        $('#action_loading').hide();
        $('#'+data_html).html(data);
      }
    });
} else if (type == "grademanagelist") {
  var data_html = "display_grademanage";
     $.ajax({
      type : 'POST',
      url : 'pages/registrar/forms/updategrades_forms.php',
      data : $('#grademanage').serialize()+'&type=grademanagelist',
      beforeSend: function(){
        $('#action_loading').show();
      },
      success :function (data){
        $('#action_loading').hide();
        $('#'+data_html).html(data);
      }
    });
  }
 }  


 function AddInputs()
{
  var number_of_data=document.getElementById('number_of_data_grade').value;
    for (var a = 1; a <= number_of_data; a++) {
      $('#add_grade'+a).hide();      
                  
        var total = 0;
        var coll = document.getElementsByClassName("first_grade"+a);
        for ( var i = 0; i<coll.length; i++)
        {
            var ele = coll[i];
            total += parseInt(ele.value);
          
        }
      
        if(total > 100) {
            total /= 2;
        }
        var Display = document.getElementById("Display"+a);
        if(total ) {
         Display.innerHTML = total;
        }

    }
  
}

function grademanagelist_table() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInputGrade");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTableGrade");
  tr = table.getElementsByTagName("tr");
  for (var i = 1; i < tr.length; i++) {
    var tds = tr[i].getElementsByTagName("td");
    var flag = false;
    for(var j = 0; j < tds.length; j++){
      var td = tds[j];
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        flag = true;
      } 
    }
    if(flag){
        tr[i].style.display = "";
    }
    else {
        tr[i].style.display = "none";
    }
  }
}

 

</script>