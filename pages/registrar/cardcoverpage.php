<div class="tab_add_data_table8">
    <?php //require 'secondcardpage.php';?>
</div>

<div class="table_info_show8">
  <div class="tablecon cardcover_form_show">
    <form name="cardcover_form_show" id="cardcover_form_show">
      <div class="enroll">
        <b class="line_r">Required Option</b>
      </div>
      
      <div class="select-head">
        <div class="hrsb">
             <div class="container_select">
                <span>Branch:&nbsp;&nbsp;</span>
                <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails('branch');
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                </select>
              </div>
              <div class="container_select">
                  <span>School Year:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("schoolyear");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                      }
                    ?>
                  </select>
              </div>
               <div class="container_select">
                  <span>Semester:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("semester");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                <div class="container_select">
                  <span>Scholarship:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="scholarshipid" id="scholarshipid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("scholarship");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['scholarshipid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                 <div class="container_select">
                   <span><button  type="button" id="cardpage" class='button_local' onclick="show_studname(this.id)">SELECT</button></span>
                 </div>
                 <div class="container_select">
                   <div id="show_students">
                     <!-- DISPLAY STUDENT CATEGORY -->
                   </div>
                 </div>
        </div>
      </div>
    </form>
    <br>
    <div id="show_card_students" style="display:block;width:100%;">
              <!-- DISPLAY CARD -->
    </div> 
  </div>
</div>

<script type="text/javascript">

  function show_studname(type){
    // alert( $('#cardcover_form_show').serialize() );
    if (type == "cardpage") {
      var data_html = "show_students";
      $.ajax({
        type : 'POST',
        url  : 'pages/registrar/forms/form_cardcoverpage.php',
        data : $('#cardcover_form_show').serialize()+'&type=cardpage',
        beforeSend : function (){
          $('#action_loading').show();
        },
        success : function (data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    } else if (type == "show_cardpage"){
      var data_html = "show_card_students";
      $.ajax({
        type : 'POST',
        url  : 'pages/registrar/forms/form_cardcoverpage.php',
        data : $('#cardcover_form_show').serialize()+'&type=show_cardpage',
        beforeSend : function (){
          $('#action_loading').show();
        },
        success : function (data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    }

  }

</script>