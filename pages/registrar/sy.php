<div class="tab_add_data_table1">
  <div class="adds sy_form_add">
    <form name="sy_form_add" id="sy_form_add">
     <b>Add New School Year</b>
      <?php
          require 'forms/form_sy.php';
        ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" value="add"  onclick="sy_controller(this.id)">Save</button>  
      </div>
    </form>
  </div>
</div>

<div class="tab_modify_data_table1">
  <div class="adds sy_form_modify">
    <form name="sy_form_modify" id="sy_form_modify">
      <b>Required Option</b>
          <div class="size-16">
             <div class="we-40">
                 <span>Select School Year:</span>
             </div>
            <select class="wid-fix2"  name="syid" id="syid" onChange="sy_Modify();">
                 <?php
                  $sql = $view_details->sy_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                        echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
            </select>
        </div>

        <div class="requroption">
          <span>School Year Information</span>
        </div>
        <div id="show_sy_modify">
          
        </div>
        <div class="buttonsave">
          <button  type="button" class='button_local' value="update" id='update' onclick="sy_controller(this.id)"> Update </button>
      </div>
    </form>
  </div>
</div>


<div class="tab_delete_data_table1">
  <div class="adds sy_form_delete">
    <form name="sy_form_delete" id="sy_form_delete">
      <b>Required Option</b>
          <div class="size-16">
             <div class="we-40">
                 <span>Select School Year:</span>
             </div>
            <select class="wid-fix2"  name="syid" id="syid" onChange="sy_Delete();">
                 <?php
                  $sql = $view_details->sy_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                        echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
            </select>
        </div>

        <div class="requroption">
          <span>School Year Information</span>
        </div>
        <div id="show_sy_delete">
          
        </div>
        <div class="buttonsave">
          <button  type="button" class='button_local' value="delete" id='delete' onclick="sy_controller(this.id)"> Delete </button>
      </div>
    </form>
  </div>
</div>



<div class="table_info_show1">
  <div class="tablecon">
      <div class="requroption">
        <span><b>List of Semester's</b></span>
      </div>
      <div class="spacetable">
          <div class="table-responsive">
            <table id="sy_table">
              <thead>
                <tr style="background-color: #666" align="center" class="table-heads">
                  <th>Semester</th>
                  <th>Description</th>
                </tr>
              </thead>
              <?php

                $sql = $view_details->sy_details();
                if ($sql) {
                    while($r = pg_fetch_assoc($sql)){
                 ?>
                      <tr id='sy_row<?php echo $r["syid"]; ?>'>
                      <td class="remarks"><?php echo $r["sy"]; ?></td>
                      <td class="remarks"></td>
                    </tr>
             <?php
                    }
                    // end while
                }else {
                  echo "<td  class='remarks' colspan=2 >No Data Found...</td>";
                }
              ?>
            </table>
          </div>
      </div>
  </div>
</div>