  <div class="tablecon class_roster_form">
    <form name="class_roster_form" id="class_roster_form">
      <div class="enroll">
      <b class="line_r">Class Roster Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
             <div class="container_select">
                <span>Branch:&nbsp;&nbsp;</span>
                <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails('branch');
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                </select>
              </div>
              <div class="container_select">
                  <span>School Year:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("schoolyear");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                      }
                    ?>
                  </select>
              </div>
               <div class="container_select">
                  <span>Semester:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("semester");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                <div class="container_select">
                  <span>Type:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("studenttype");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['studenttypeid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                 <div class="container_select">
                   <span><button  type="button" id="class_table" class='button_local' onclick="show_classroster(this.id)">SELECT</button></span>
                 </div>

                <div id="show-table-roster" style="display:block;width:100%;">
                  <!-- DISPLAY CLASS ROSTER TABLE -->
                </div>
          </div>
        </div>
      </div>
    </form>
    <bR>
     
  </div>

<script type="text/javascript">
  function show_classroster(type){
    if(type == "class_table"){
      var data_html = "show-table-roster";
      $.ajax({
        type : 'POST',
        url  : 'pages/registrar/forms/class_roster_forms.php',
        data : $('#class_roster_form').serialize()+'&type=class_table',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function (data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    }
  }
</script>