<?php 
	if (session_status() == PHP_SESSION_NONE) {
			        
	  session_start();

	}
	require '../../../controllers/db_controller/theDBConn.php';
	require '../../../controllers/db_controller/ViewRegistrarDetails.php';
	$viewdetails = new ViewRegistrarDetails;

	if (isset($_POST["type"])) {
		
		if ($_POST["type"] == "class_table") {
			$sy = $_POST["syid"];
			$sem = $_POST["semid"];
			$branchid = $_POST["branchid"];
			$schoolyear = "SELECT * FROM  schoolyear WHERE syid = $sy";
			$semester = "SELECT * FROM  semester WHERE semid = $sem";

			$sql = "SELECT * FROM course ORDER BY coursename ASC";
			
?>

			<div onchange="show_classroster(this.id);">
			    <center>
			        <p>Master List</p>
			        <p>School Year <?php echo $viewdetails->getVal('sy', $schoolyear);?></p>
			        <p>
			        	<?php 
			            	if($viewdetails->getVal('sem', $semester) == '1') { echo "1st";} else { echo '2nd';} 
			        	?> 
			        Semester</p>
			    </center>

			    <b>Choose the Subject you want to view:</b>

			    <div class="table-responsive">
			    	<hr>
				    <?php  

				    $sql = "SELECT scheduleid,subjectcode,blockname,sub.description as description 
							FROM schedules
							INNER JOIN subjects sub USING(subjectcode)
								WHERE semid = $sem AND syid = $sy AND branchid = $branchid
							ORDER BY subjectcode ASC";


					if ($viewdetails->notEmpty($sql)) {
						$openqry = $viewdetails->openqry($sql);

						while ($r = pg_fetch_assoc($openqry)) {
					?>

					<div>
						<a href="#offer" data-toggle="modal" data-target="#offer<?=$r["scheduleid"]?>"><?= $r['subjectcode'];?><?=$r['blockname'];?> - English for Academic and Professional Purposes</a>

						<div class="modal fade offer" id="offer<?=$r["scheduleid"]?>"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"data-backdrop="false">
						  <div class="modal-dialog" role="document" style="margin: 0px auto;margin-right: 42%;">
						    <div class="_modal_notif_container" style="margin-top: 10%;width: 800px">
						      <div class="modal-content">
						      <div class="title">
						      	<br><br><br>
						      <center>
							        <p><b>Class List</b></p>
							        <p><b>School Year <?php echo $viewdetails->getVal('sy', $schoolyear);?></b></p>
							        <p>
							        	<b>
								        	<?php 
								            	if($viewdetails->getVal('sem', $semester) == '1') { echo "1st";} else { echo '2nd';} 
								        	?>Semester
							        	</b>
							        </p>
							        <br>
							        <b><?= $r['subjectcode']?> - English for Academic and Professional Purposes</b>
							    </center>
						      </div>
						      <div class="_modal_content">

						        <?php
						        	$scheduleid = $r["scheduleid"];
						        	$sql1 = "SELECT * FROM studentgrades
											INNER JOIN studentinfo USING(studentid)
											INNER JOIN schedules USING(scheduleid)
											INNER JOIN classschedules USING(scheduleid)
											where scheduleid = $scheduleid
											";

									if ($viewdetails->notEmpty($sql1)) {
										$openqry1 = $viewdetails->openqry($sql1);

										if ($r = pg_fetch_assoc($openqry1)) {
											$i = 1;
								?>
								<table style="border: none;">
						        	<tr>
						        		<td style="border: none;">Block: <b><?=$r['blockname']?></b></td>
						        		<td style="border: none;">Room: <b><?=$r['roomcode']?></b></td>
						        	</tr>
						        	<tr>
						        		<td style="border: none;">Day: <b><?=$r['days']?></b></td>
						        		<td style="border: none;">Time: <b><?=$r['starttime']?>-<?=$r['endtime']?></b></td>
						        	</tr>
						        	<tr>
						        		<td style="border: none;">Start Date: <b><?=$r['datestart']?></b></td>
						        		<td style="border: none;">Instructor: <b><?=$r['username']?></b></td>
						        	</tr>
						        	<tr>
						        		<td style="border: none;">End Date: <b><?=$r['dateend']?></b></td>
						        	</tr>
						        </table>
						        <?php
										}
									}
						        ?>
						        <br>
						        <span style="color: red">Please Note</span> : &nbsp;
						        <span>Student in red names have problems on their accounts. As management policy, you can allow them to enter your class until only, 3 days from day start, Advise and remind student to settle their account to continue their study. If can't, then advise student to withdraw the subject</span><br>
						        <hr>
						        <table>
						        	<tr>
						        		<th></th>
						        		<th>Last Name</th>
						        		<th>First Name</th>
						        		<th>Middle Name</th>
						        		<th>ID Number</th>
						        	</tr>
						        	<?php
							        	if ($viewdetails->notEmpty($sql1)) {
											while ($r = pg_fetch_assoc($openqry1)) {
											$i = 1;
							        	?>
							        	<tr>
							        		<td><?= $i++?></td>
							        		<td><?= $r['lastname']?></td>
							        		<td><?= $r['firstname']?></td>
							        		<td><?= $r['middlename']?></td>
							        		<td><?= $r['studentid']?></td>
							        	</tr>
						        		<?php  
						        			}
						        		} else {
						        			echo "<table>
	 												<tr>
	 													<td><b>No record found...</b></tr>
	 												</tr>
						        				  </table>";
						        		}
					        		?>
						        </table>
						      </div>
						      <div class="_modal_footer">
						        <button type="button" data-dismiss="modal" >Cancel</button>
						      </div>
						      </div>
						    </div>
						  </div>
						</div>
					</div>

				<?php
					}
				} else {
					echo "<b>No record found...</b>";
				}
			    ?>
			</div>


			</div>


<?php
	} 
}
?>