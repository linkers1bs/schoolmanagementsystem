<?php 
	if (session_status() == PHP_SESSION_NONE) {
			        
	  session_start();

	}
	require '../../../controllers/db_controller/theDBConn.php';
	require '../../../controllers/db_controller/ViewRegistrarDetails.php';
	$viewdetails = new ViewRegistrarDetails;

	if (isset($_POST["type"])) {
		
		if ($_POST["type"] == "roster_table") {
			$sy = $_POST["syid"];
			$sem = $_POST["semid"];
			$schoolyear = "SELECT * FROM  schoolyear WHERE syid = $sy";
			$semester = "SELECT * FROM  semester WHERE semid = $sem";

			$sql = "SELECT * FROM course ORDER BY coursename ASC";
			
?>

			<div onchange="master_list(this.id);">
			    <center>
			        <p>Master List</p>
			        <p>School Year <?php echo $viewdetails->getVal('sy', $schoolyear);?></p>
			        <p>
			        	<?php 
			            	if($viewdetails->getVal('sem', $semester) == '1') { echo "1st";} else { echo '2nd';} 
			        	?> 
			        Semester</p>
			    </center>

			    <div class="table-responsive">
			    <hr>
			    <?php
			    	if ($viewdetails->notEmpty($sql)) {
				    		$openqry = $viewdetails->openqry($sql);
				        	$total = 0;
				       	while($r = pg_fetch_assoc($openqry)){
			            $coursecode = $r["coursecode"];
			            $sql2 = "SELECT * FROM register 
					            LEFT JOIN studentinfo USING(studentid)
					            LEFT JOIN studentcourse USING (studentid)
					            LEFT JOIN curriculum USING (curcode)
					            LEFT JOIN course USING (coursecode)
					            WHERE register.semid = $sem AND register.syid = $sy AND course.coursecode = '$coursecode' ";
					    $openqry2 = $viewdetails->openqry($sql2);
			    ?>
			                <table  style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
			                  <thead>
			                    <tr style="background-color: #666" align="center" class="table-heads">
			                     
			                      <th  class="remarks"><?php echo $r["numofyear"].' Year - '.$r["coursename"]; ?></th>
			                    </tr>
			                  </thead>
			                  
			                </table>
			             
			            <div style="position: relative;display:block;margin:0px auto; width:90%;">
			          
			              <div style="padding-top:12px;padding-bottom:12px;">
			                  <span><b>Count: <?php echo $count = pg_num_rows($openqry2); ?></b></span>
			              </div>
			              <table style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
			              <?php 
			               

			                  if($viewdetails->notEmpty($sql2)){
			                    $i = 0;
			                    while($r2 = pg_fetch_assoc($openqry2)){
			                        $i++;

			            ?>
			               
			                    <tr>
			                        <td class="remarks"><?php echo $i;?></td>
			                        <td class="remarks"><?php echo ucfirst(strtolower(trim($r2['lastname'])));?></td>
			                        <td class="remarks"><?php echo ucfirst(strtolower($r2['firstname']));?></td>
			                        <td class="remarks"><?php echo ucfirst(strtolower($r2['middlename']));?></td>
			                        <td class="remarks"><?php echo $r2['studentid'];?></td>
			                    </tr>
			                  
			               
			                <?php 
			                    }
			                    echo " </table>";
			                } else {
			                ?>
			                <table style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
			                  <thead>
			                    <tr>
			                        <td class='remarks'>No record found..</td>
			                    </tr>
			                  </thead>
			                  
			                </table>
			                <?php
			                }
			                ?>
			                </div>

			                
			            <br>

				    <?php 
				    $total += $count;
				        }
				    } else {
				    	echo "<b>No Record Found...</b>";
				    }
				    ?>
			    	<b><p>Last count as of <?php date_default_timezone_set('Asia/Manila'); echo date('l jS \of F Y h:i:s A')?>: <?php echo $total ?> enrollees</p><b>
				</div>
			</div>


<?php
		} else {
			echo "<b>No record found...</b>";
		}
	}
?>