
 <?php
    if (session_status() == PHP_SESSION_NONE) {
        
          session_start();

    }
 require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewAdviserDetails.php';
  require '../../../controllers/db_controller/ViewInstructorDetails.php';
  
  $view_details = new ViewAdviserDetails;
  $view_details_instructor = new ViewInstructorDetails;

if (isset($_POST["type"])) {  

    if ($_POST["type"] == "gradeblock") {

    ?> 
      <span>&nbsp;Block & Subject:&nbsp;&nbsp;&nbsp;</span>
      <select class="wid-fix" name="scheduleid" style="width: 130px!important" onchange="show_grade_manage(this.id);" id="grademanagelist">
        <?php
            $query = $view_details_instructor->get_block_subject($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
            if($query) {
              echo '<option value="none">Select..</option>';
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].'</option>';
              }
            } else {
              echo '<option>No record found..</option>';
            }

          ?>
      </select>

    <?php
    
  } elseif ($_POST['type'] == 'grademanagelist') {
      $blockname = $_POST["scheduleid"];
    ?>
    <div class="spacetable">
      <style>
        #myInputGrade {
        
            width: 100%;
            font-size: 12px;
            padding: 6px;
            border: 1px solid #ddd;
            margin-top: 6px;
            margin-bottom: 8px;
            }

      </style>
      <input type="text" id="myInputGrade" onkeyup="grademanagelist_table()" placeholder="Search for names.." title="Type in a name">
      <div class="table-responsive">     
        <table id="myTableGrade">
        <thead>
          <tr style="background-color: #666" align="center" class="table-heads">
            <th>#</th>
            <th>Student ID</th>
            <th>Lastname</th>
            <th>Firstname</th>
            <th>MI</th>   
            <th>1st<br> Quarter</th>
            <th>2nd<br> Quarter</th>
            <th>Average <br> Grade</th>
            <th>Remarks</th>
          </tr>
        </thead>
        <?php 

        $details = $view_details_instructor->get_details_classlist($blockname);
        if ($details) {
          $i =1;
          $average_persubject = 0;
          while($row = pg_fetch_assoc($details)) {
            ?>
            <tr>
            <input type="hidden"  name="gradeid" id="gradeid<?php echo $i?>" value="<?php echo $row["gradeid"]; ?>" />
              <td class="remarks"><?php echo $i; ?></td>
              <td class="remarks"><?php echo $row["studentid"]; ?></td>
              <td class="remarks"><?php echo ucfirst($row["lastname"]); ?></td>
              <td class="remarks"><?php echo ucfirst($row["firstname"]); ?></td>
              <td><?php echo ucfirst($row["middlename"][0]); ?>.</td>
              <td>
                <input type="number" onkeyup="AddInputs()" style="width: 80px;" name="first_grade" id="first_grademange<?php echo $i?>"  class="first_grade<?php echo $i ?>" 
              value="<?php echo $row["first"] ?>" onKeyPress="if(this.value.length==2) return false;" min="0"/>
              </td>
              <td>
                <input type="number" onkeyup="AddInputs()" style="width: 80px;" name="second_grade" id="second_grademange<?php echo $i?>" class="first_grade<?php echo $i ?>"  
              value="<?php echo $row["second"] ?>" onKeyPress="if(this.value.length==2) return false;" min="0"/>
              </td>
              <td>
                <span id="Display<?php echo $i ?>">
                  <?php 
                    $average_persubject = ($row["first"] + $row["second"]);
                    echo $average_persubject > 100 ? $average_persubject / 2 : "";
                    
                  ?>
                </span>
                <span id="add_grade<?php echo $i ?>"></span>
              </td>
              <td><input type="text" name="remarks_grade" id="remarks_grade<?php echo $i?>" value="<?=$row["gremarks"]?>"/></td>
            </tr>
            <?php
            $i++;
          }
        } else {
          echo "<tr><td class='remarks' colspan=8>No record found..<td/</tr>";
        }
        ?><input type="hidden" id="number_of_data_grade" value="<?php echo $i;?>"/>
        </table>
      </div>
      <div style="position: relative;text-align:right"> 
      <button  type="button" name="add_sub" value="add" id='btns' class='button_local' onclick="add_grade_management()"> Save </button>
      </div>
    </div>

    <script type="text/javascript">
      function add_grade_management(){      
        var number_of_data_grade=document.getElementById('number_of_data_grade').value;
        // initialize array
        var first = [];
        var second = [];
        var remarks = [];
        var gradeid = [];          

          // append multiple values to the array
          for (var i = 1; i < number_of_data_grade ; i++) {
                first.push(document.getElementById('first_grademange'+i).value);
                second.push(document.getElementById('second_grademange'+i).value);
                remarks.push(document.getElementById('remarks_grade'+i).value);
                gradeid.push(document.getElementById('gradeid'+i).value);
          }
          $.post("controllers/function/InstructorController.php",{ 
            first  : first,
            second : second,
            remarks : remarks,
            gradeid : gradeid,
            type : 'add_grade_management'
            },
            function(data) {
              $("#alert-data").html(data);
            }); 
         } 
    </script>
    <?php
    }
  }
  ?>
