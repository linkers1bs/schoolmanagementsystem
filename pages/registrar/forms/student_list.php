

<div>
  <table class="table table table-bordered">
    <thead>
      <tr align="center" bgcolor="gray">
        <th>No.</th>
        <th>Student ID</th>
        <th>Last Name</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Course</th>
        <th>Date of Registration</th>
      </tr>
    </thead>
  <?php
      //require 'forms/student_list.php';
    $sql = $view_details->studentlist_details();
    if ($sql) {
      $i = 1;
      while ($r = pg_fetch_assoc($sql)) {
  ?>
    <tr>
      <td><?php echo $i++ ?></td>
      <td><?php echo $r['studentid']?></td>
      <td><?php echo $r['lastname']?></td>
      <td><?php echo $r['firstname']?></td>
      <td><?php echo $r['middlename']?></td>
      <td><?php echo $r['coursename']?></td>
      <td><?php echo $r['dateenrolled']?></td>
    </tr>
  <?php
      }
    } else {
      echo "<tr>";
      echo "<td><b>No recoord found...</b></td>";
      echo "</tr>";
    }
    ?>
  </table>
</div>