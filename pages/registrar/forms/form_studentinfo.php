<style>
    .remarks2 {
        text-align: right;
        width:27%!important;
       
    }
    .studentcontainer {
      border-radius: 12px;
     
      padding:20px;
      border-radius: 12px;
    }
    .studentprofile {
     
      display:inline-block;
      position: relative;
     
      width:68%;
      height: 30.5vh;
    }
    .s22 {
      width:26%;
      margin-right: 5%;
      border-radius: 12px;
      -webkit-box-shadow: 0px 2px 15px -8px rgba(0,0,0,0.75);
      -moz-box-shadow: 0px 2px 15px -8px rgba(0,0,0,0.75);
      box-shadow: 0px 2px 15px -8px rgba(0,0,0,0.75);
    }
    .tdb {
      background-color: white;
      border-left:2px solid white!important;
    }
 
    .tr1 {
      border-top:2px solid white;
    }
    .whit {
      background-color: white!important;
    }
</style>
 <?php
    if (session_status() == PHP_SESSION_NONE) {
        
          session_start();

    }
    require '../../../controllers/db_controller/theDBConn.php';
    require '../../../controllers/db_controller/ViewRegistrarDetails.php';
    $view_details = new ViewRegistrarDetails;

    $studentid = $_POST["studentid"];

     $sql = "SELECT scholarship.name AS scholarshipname,  * FROM studentinfo
              LEFT JOIN register USING (studentid) 
              LEFT JOIN schoolyear USING (syid) 
              LEFT JOIN semester USING (semid) 
              LEFT JOIN scholarship USING (scholarshipid)
              LEFT JOIN studentcourse USING (studentid)
              LEFT JOIN curriculum USING (curcode)
              LEFT JOIN course USING (coursecode)
              WHERE studentinfo.studentid = '$studentid' ";
    ?>

<script>
    $('.inpt_hide').each(function(){
        $(this).hide();
     });

    function studentinfoModify() {
      $('#savemodify').show();
      $('#modify').hide();
      $('.inpt_hide').each(function(){
        $(this).show();
      });
      $('.inpt_details').each(function(){
        $(this).hide();
      });
    }

    function saveModifystudentinfo() {
      $('#savemodify').hide();
      $('#modify').show();

      $('.inpt_details').each(function(){
        $(this).show();
      });
      var studentid = [];
      var age = [];
      var currentaddress = [];
      var sex = [];
      var maritalstatus = [];
      var dateofbirth = [];
      var mobile = [];
      var placeofbirth = [];
      var home_address = [];
      var schoolstatus = [];
      var scholarshipname = [];
      var religion = [];
      var homephone = [];
      var mothername = [];
      var motherocc = [];
      var fathername = [];
      var fatherocc = [];
      var parentsadd = [];
      var parent_phone = [];
      var person_to_notify = [];
      var elementary = [];
      var ele_yr_grad = [];
      var secondary = [];
      var sec_yr_grad = [];
      var college = [];
      var col_yr_grad = [];
      var sonumber = [];
      var dategradici = [];
      var dateso = [];
      var awards = [];
      var remarks = [];
      $('.inpt_hide').each(function(){
        $(this).hide();
          if (this.name == "studentid") {
            studentid.push($(this).val());
          } else if (this.name == "age") {
            age.push($(this).val());
          }else if (this.name == "currentaddress") {
            currentaddress.push($(this).val());
          } else if (this.name == "sex") {
            sex.push($(this).val());
          } else if (this.name == "maritalstatus") {
            maritalstatus.push($(this).val());
          } else if (this.name == "dateofbirth") {
            dateofbirth.push($(this).val());
          } else if (this.name == "mobile") {
            mobile.push($(this).val());
          } else if (this.name == "placeofbirth") {
            placeofbirth.push($(this).val());
          } else if (this.name == "home_address") {
            home_address.push($(this).val());
          } else if (this.name == "schoolstatus") {
            schoolstatus.push($(this).val());
          } else if (this.name == "scholarshipname") {
            scholarshipname.push($(this).val());
          } else if (this.name == "religion") {
            religion.push($(this).val());
          } else if (this.name == "homephone") {
            homephone.push($(this).val());
          } else if (this.name == "mothername") {
            mothername.push($(this).val());
          } else if (this.name == "motherocc") {
            motherocc.push($(this).val());
          } else if (this.name == "fathername") {
            fathername.push($(this).val());
          } else if (this.name == "fatherocc") {
            fatherocc.push($(this).val());
          } else if (this.name == "parentsadd") {
            parentsadd.push($(this).val());
          } else if (this.name == "parent_phone") {
            parent_phone.push($(this).val());
          } else if (this.name == "person_to_notify") {
            person_to_notify.push($(this).val());
          } else if (this.name == "elementary") {
            elementary.push($(this).val());
          } else if (this.name == "ele_yr_grad") {
            ele_yr_grad.push($(this).val());
          } else if (this.name == "secondary") {
            secondary.push($(this).val());
          } else if (this.name == "sec_yr_grad") {
            sec_yr_grad.push($(this).val());
          } else if (this.name == "college") {
            college.push($(this).val());
          } else if (this.name == "col_yr_grad") {
            col_yr_grad.push($(this).val());
          } else if (this.name == "sonumber") {
            sonumber.push($(this).val());
          } else if (this.name == "dategradici") {
            dategradici.push($(this).val());
          } else if (this.name == "dateso") {
            dateso.push($(this).val());
          } else if (this.name == "awards") {
            awards.push($(this).val());
          } else if (this.name == "remarks") {
            remarks.push($(this).val());
          }
      });
      $.ajax({
        type : 'POST',
        url  : 'controllers/function/RegistrarController.php',
        data : 'currentaddress='+currentaddress+'&file=studentinfo_details_controller&studentid='+studentid+'&age='+age+'&sex='+sex+'&maritalstatus='+maritalstatus+'&dateofbirth='+dateofbirth+'&mobile='+mobile+'&placeofbirth='+placeofbirth+'&home_address='+home_address+'&schoolstatus='+schoolstatus+'&scholarshipname='+scholarshipname+'&religion='+religion+'&homephone='+homephone+'&mothername='+mothername+'&motherocc='+motherocc+'&fathername='+fathername+'&fatherocc='+fatherocc+'&parentsadd='+parentsadd+'&parent_phone='+parent_phone+'&person_to_notify='+person_to_notify+'&elementary='+elementary+'&ele_yr_grad='+ele_yr_grad+'&secondary='+secondary+'&sec_yr_grad='+sec_yr_grad+'&college='+college+'&col_yr_grad='+col_yr_grad+'&sonumber='+sonumber+'&dategradici='+dategradici+'&dateso='+dateso+'&awards='+awards+'&remarks='+remarks,
        beforeSend : function(){
          $('#action_loading').show();
        },
        success : function(data) {
          $('#action_loading').hide();
          $('#studentin-show').html(data);
        }
      });
    }
  </script>

  <div class="studentcontainer">
    <div id="studentin-show">
      
    </div>

    <div class="studentprofile s22">

    </div>

    <div class="studentprofile">
      <table bgcolor="#B0C4DE" class="table table table-bordered sal-tab1" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;">
        <tr class="trs tr1">
          <td class="remarks2 tdb" >Name :</td>
          <td class="remarks">
            <span class="inpt_details">
              <?php echo ucfirst($view_details->getVal('lastname',$sql)).", ".ucfirst($view_details->getVal('firstname',$sql))." ".ucfirst($view_details->getVal('middlename',$sql)[0]).'.'; ?>
            </span>
            <span>
              <input type="text" name="lastname" placeholder="Lastname" class="inpt_hide"  style="width:32%" value="<?php echo ucfirst($view_details->getVal('lastname',$sql)); ?>"/>
              <input type="text" name="firstname" placeholder="Firstname" class="inpt_hide" style="width:33%" value="<?php echo ucfirst($view_details->getVal('firstname',$sql)); ?>"/>
              <input type="text" name="middlename" placeholder="Middlename" class="inpt_hide"  style="width:33%" value="<?php echo ucfirst($view_details->getVal('middlename',$sql)); ?>"/>
            </span>
          </td>
        </tr>
        <tr class="trs">
          <td class="remarks2 tdb">ID No. :</td>
          <td class="remarks">
            <span class="inpt_details">
              <?php echo $view_details->getVal('studentid',$sql);?>
            </span>
            <span>
              <input type="text" name="studentid" class="inpt_hide" value="<?php echo $view_details->getVal('studentid',$sql); ?>"/>
            </span>
          </td>
        </tr>
        <tr class="trs">
          <td class="remarks2 tdb">Course :</td>
          <td class="remarks">
            <span class="inpt_details">
              <?php echo $view_details->getVal('coursename',$sql);?>
            </span>
            <span>
              <select name="coursename" class="inpt_hide" style="width:100%;" value="<?php echo $view_details->getVal('coursename',$sql); ?>">
                  <option><?php echo $view_details->getVal('coursename',$sql); ?></option>
                  <option>SELECT</option>
                  <option value="Accounting Technology">Accounting Technology</option>
                  <option value="Accounting, Business and Management">Accounting, Business and Management</option>
                  <option value="Computer Engineering">Computer Engineering</option>
                  <option value="Culinary Arts">Culinary Arts</option>
                  <option value="Digital Media Arts">Digital Media Arts</option>
                  <option value="Electrical Installation and Maintenance">Electrical Installation and Maintenance</option>
                  <option value="General Academic Strand">General Academic Strand</option>
                  <option value="Hotel and Restaurant Management">Hotel and Restaurant Management</option>
                  <option value="Humanities and Social Science">Humanities and Social Science</option>
                  <option value="Information Technology">Information Technology</option>
                  <option value="Office Management">Office Management</option>
                  <option value="Practical Nursing">Practical Nursing</option>
                  <option value="Science, Technology, Engineering and Mathematics">Science, Technology, Engineering and Mathematics</option>
              </select>
            </span>
          </td>
        </tr>
        <tr class="trs">
          <td class="remarks2 tdb">Date :</td>
          <td class="remarks">
            <span>
              <?php  echo $today = date("F j, Y");?>
            </span>
          </td>
        </tr>
        <tr class="trs">
          <td class="remarks2 tdb">Semester :</td>
          <td  class="remarks">
            <span>
              <?php echo $view_details->getVal('sem',$sql);?>
            </span>
          </td>
        </tr>
        <tr class="trs">
          <td class="remarks2 tdb">School Year :</td>
          <td  class="remarks"><?php echo $view_details->getVal('sy',$sql);?></td>
        </tr>
      </table>
    </div>
<hr>
    <table  class="table table table-bordered" id="myTable">
        <thead>
          <tr style="background-color: #666" align="center" class="table-heads">
            <th colspan="4" class="remarks">Student Information</th>
          </tr>
        </thead>
          <tr>
              <td class="remarks2 whit">Age :</td>
              <td class="remarks whit" >
                <span class="inpt_details"><?php echo ucfirst($view_details->getVal('age',$sql)); ?></span>
                <span> 
                  <input type="number" name="age" class="inpt_hide" value="<?php echo ucfirst($view_details->getVal('age',$sql)); ?>"/>
                <span>
              </td>
              <td class="remarks2 whit">Current Address :</td>
              <td class="remarks whit">
                <span class="inpt_details"><?php echo ucfirst($view_details->getVal('currentaddress',$sql)); ?></span>
                <span> 
                  <input type="text" name="currentaddress" class="inpt_hide" value="<?php echo ucfirst($view_details->getVal('currentaddress',$sql)); ?>"/>
                <span>
              </td>
          </tr>
          <tr>
              <td class="remarks2">Gender :</td>
              <td class="remarks"><span class="inpt_details"><?php echo ucfirst($view_details->getVal('sex',$sql)); ?></span>
                <span class="inpt_hide"> 
                  <select name="sex" style="width:100%;">
                     <option <?php if($view_details->getVal('sex',$sql) == 'Male' ) { echo 'selected';} ?> value="Male">Male</option>
                     <option <?php if($view_details->getVal('sex',$sql) == 'Female' ) { echo 'selected';} ?> value="Female">Female</option>
                  </select>
                </span>
              </td>
              <td class="remarks2">Marital Status :</td>
              <td class="remarks"><span class="inpt_details"><?php echo ucfirst($view_details->getVal('maritalstatus',$sql)); ?></span>
                <span class="inpt_hide"> 
                  <select name="maritalstatus" style="width:100%;">
                     <option <?php if($view_details->getVal('maritalstatus',$sql) == 'Single' ) { echo 'selected';} ?> value="Single">Single</option>
                     <option <?php if($view_details->getVal('maritalstatus',$sql) == 'Married' ) { echo 'selected';} ?> value="Married">Married</option>
                     <option <?php if($view_details->getVal('maritalstatus',$sql) == 'Widowed' ) { echo 'selected';} ?> value="Widowed">Widowed</option>
                  </select>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2 whit">Date of Birth :</td>
              <td class="remarks whit"><span class="inpt_details"><?php echo $view_details->getVal('dateofbirth',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="date" name="dateofbirth" style="width:100%;" value="<?php echo $view_details->getVal('dateofbirth',$sql); ?>"/>
                </span>
              </td>
              <td class="remarks2 whit">Mobile No. :</td>
              <td class="remarks whit"><span class="inpt_details"><?php echo $view_details->getVal('mobile',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="number" name="mobile" style="width:100%;" value="<?php echo $view_details->getVal('mobile',$sql); ?>"/>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2">Place of Birth :</td>
              <td class="remarks"><span class="inpt_details"><?php echo $view_details->getVal('placeofbirth',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="text" name="placeofbirth" style="width:100%;" value="<?php echo $view_details->getVal('placeofbirth',$sql); ?>"/>
                </span>
              </td>
              <td class="remarks2">Home Addres :</td>
              <td class="remarks"><span class="inpt_details"><?php echo $view_details->getVal('home_address',$sql); ?></span>  
                <span class="inpt_hide"> 
                  <input type="text" name="home_address" style="width:100%;" value="<?php echo $view_details->getVal('home_address',$sql); ?>"/>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2 whit">School Status :</td>
              <td class="remarks whit"><span class="inpt_details"><?php  echo $view_details->getVal('schoolstatus',$sql)?></span>
                <span class="inpt_hide"> 
                  <select name="schoolstatus" style="width:100%;">
                     <option <?php if($view_details->getVal('schoolstatus',$sql) == 'Old' ) { echo 'selected';} ?> value="Old">Old</option>
                     <option <?php if($view_details->getVal('schoolstatus',$sql) == 'New' ) { echo 'selected';} ?> value="New">New</option>
                     <option <?php if($view_details->getVal('schoolstatus',$sql) == 'Transferee' ) { echo 'selected';} ?> value="Transferee">Transferee</option>
                  </select>
                </span>
              </td>
              <td class="remarks2 whit">Academic Status :</td>
              <td class="remarks whit"><span class="inpt_details"><?php  echo $view_details->getVal('scholarshipname',$sql);?></span> 
                <span class="inpt_hide"> 
                  <select style="width: 100%" name="scholarid" value="<?php echo $view_details->getVal('scholarshipname',$sql); ?>">
                      <option><?php echo $view_details->getVal('scholarshipname',$sql); ?></option>
                      <option value="CHED">CHED</option>
                      <option value="DOST">DOST</option>
                      <option value="ESC">ESC</option>
                      <option value="MAYOR">MAYOR</option>
                      <option value="OWWA">OWWA</option>
                      <option value="Paying">Paying</option>
                      <option value="PESPA">PESPA</option>
                      <option value="QVR">QVR</option>
                  </select>
                </span>
            </td>
          </tr>
          <tr>
              <td class="remarks2">Religion :</td>
              <td class="remarks"><span class="inpt_details"><?php echo $view_details->getVal('religion',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="text" name="religion" style="width:100%;" value="<?php echo $view_details->getVal('religion',$sql); ?>"/>
                </span>
              </td>
              <td class="remarks2">Home Phone :</td>
              <td class="remarks"><span class="inpt_details"><?php echo $view_details->getVal('homephone',$sql); ?></span> 
                <span class="inpt_hide"> 
                  <input type="number" name="homephone" style="width:100%;" value="<?php echo $view_details->getVal('homephone',$sql); ?>"/>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2 whit">Mother's Name :</td>
              <td class="remarks whit"><span class="inpt_details"><?php echo ucfirst($view_details->getVal('mothername',$sql)); ?></span>
                <span class="inpt_hide"> 
                  <input type="text" name="mothername" style="width:100%;" value="<?php echo ucfirst($view_details->getVal('mothername',$sql)); ?>"/>
                </span>
              </td>
              <td class="remarks2 whit">Mother's Occupation :</td>
              <td class="remarks whit"><span class="inpt_details"><?php echo ucfirst($view_details->getVal('motherocc',$sql)); ?></span>   
                <span class="inpt_hide"> 
                  <input type="text" name="motherocc" style="width:100%;" value="<?php echo ucfirst($view_details->getVal('motherocc',$sql)); ?>"/>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2">Father's Name :</td>
              <td class="remarks"><span class="inpt_details"><?php echo ucfirst($view_details->getVal('fathername',$sql)); ?></span>
                <span class="inpt_hide"> 
                  <input type="text" name="fathername" style="width:100%;" value="<?php echo ucfirst($view_details->getVal('fathername',$sql)); ?>"/>
                </span>
              </td>
              <td class="remarks2">Father's Occupation :</td>
              <td class="remarks"><span class="inpt_details"><?php echo ucfirst($view_details->getVal('fatherocc',$sql)); ?></span>  
                <span class="inpt_hide"> 
                  <input type="text" name="fatherocc" style="width:100%;" value="<?php echo ucfirst($view_details->getVal('fatherocc',$sql)); ?>"/>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2 whit">Parent's Address :</td>
              <td class="remarks whit"><span class="inpt_details"><?php echo $view_details->getVal('parentsadd',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="text" name="parentsadd" style="width:100%;" value="<?php echo ucfirst($view_details->getVal('parentsadd',$sql)); ?>"/>
                </span>
              </td>
              <td class="remarks2 whit">Phone :</td>
              <td class="remarks whit"><span class="inpt_details"><?php echo $view_details->getVal('parent_phone',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="number" name="parent_phone" style="width:100%;" value="<?php echo $view_details->getVal('parent_phone',$sql); ?>"/>
                </span>
              </td>
          </tr>
          <tr>
              <td class="remarks2" >Person to Notify in case of emergency :</td>
              <td class="remarks" colspan="3"><span class="inpt_details"><?php echo $view_details->getVal('person_to_notify',$sql); ?></span>
                <span class="inpt_hide"> 
                  <input type="text" name="person_to_notify" style="width:100%;" value="<?php echo $view_details->getVal('person_to_notify',$sql); ?>"/>
                </span>
              </td>
          </tr>
      </table>
<hr>
      <table  class="table table table-bordered" id="myTable">
        <thead>
          <tr style="background-color: #666" align="center" class="table-heads">
            <th colspan="4" class="remarks">Educational Background</th>
          </tr>
        </thead>
      </table>
             
      <table  class="table table table-bordered" id="myTable"  style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;" >
        <thead>
          <tr style="background-color: #666" align="center" class="table-heads">
            <th colspan="2">School</th>
            <th class="remarks">Year Graduated</th>
          </tr>
        </thead>
        <tr>
            <td class="remarks2 whit">Elementary :</td>
            <td class="remarks whit">
              <span class="inpt_details">
                <?php echo $view_details->getVal('elementary',$sql); ?>
              </span>
              <span class="inpt_hide"> 
                <input type="text" name="elementary" style="width:100%;" value="<?php echo $view_details->getVal('elementary',$sql); ?>"/>
              </span>
            </td>
            <td class="remarks whit">
              <span class="inpt_details">
                <?php echo $view_details->getVal('ele_yr_grad',$sql); ?>
              </span>
              <span class="inpt_hide"> 
                <input type="text" name="ele_yr_grad" style="width:100%;" value="<?php echo $view_details->getVal('ele_yr_grad',$sql); ?>"/>
              </span>
            </td>
        </tr>
        <tr>
            <td class="remarks2" >Secondary :</td>
            <td class="remarks" >
              <span class="inpt_details">
                <?php echo $view_details->getVal('secondary',$sql); ?>
              </span>
              <span class="inpt_hide"> 
                <input type="text" name="secondary" style="width:100%;" value="<?php echo $view_details->getVal('secondary',$sql); ?>"/>
              </span>
            </td>
            <td class="remarks"><span class="inpt_details"><?php echo $view_details->getVal('sec_yr_grad',$sql); ?></span> 
              <span class="inpt_hide"> 
                <input type="text" name="sec_yr_grad" style="width:100%;" value="<?php echo $view_details->getVal('sec_yr_grad',$sql); ?>"/>
              </span>
            </td>
        </tr>
        <tr>
            <td class="remarks2 whit">College :</td>
            <td class="remarks whit">
              <span class="inpt_details"><?php echo $view_details->getVal('college',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="text" name="college" style="width:100%;" value="<?php echo $view_details->getVal('college',$sql); ?>"/>
              </span>
            </td>
            <td class="remarks whit">
              <span class="inpt_details"><?php echo $view_details->getVal('col_yr_grad',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="text" name="col_yr_grad" style="width:100%;" value="<?php echo $view_details->getVal('col_yr_grad',$sql); ?>"/>
              </span>
            </td>
        </tr>
      </table>
<hr>
      <table  class="table table table-bordered" id="myTable">
        <tr>
            <td class="remarks2">SO Number :</td>
            <td class="remarks">
              <span class="inpt_details"><?php echo $view_details->getVal('sonumber',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="text" name="sonumber" style="width:100%;" value="<?php echo $view_details->getVal('sonumber',$sql); ?>"/>
              </span>
            </td>
            <td class="remarks2">Date Graduated in ICI :</td>
            <td class="remarks">
              <span class="inpt_details"><?php echo $view_details->getVal('dategradici',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="date" name="dategradici" style="width:100%;" value="<?php echo $view_details->getVal('dategradici',$sql); ?>"/>
              </span>
            </td>
        </tr>
        <tr>
            <td class="remarks2 whit">SO Date Issued :</td>
            <td class="remarks whit" >
              <span class="inpt_details"><?php echo $view_details->getVal('dateso',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="date" name="dateso" style="width:100%;" value="<?php echo $view_details->getVal('dateso',$sql); ?>"/>
              </span>
            </td>
            <td class="remarks2 whit">Honors/Awards Received :</td>
            <td class="remarks whit">
              <span class="inpt_details"><?php echo $view_details->getVal('awards',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="text" name="awards" style="width:100%;" value="<?php echo $view_details->getVal('awards',$sql); ?>"/>
              </span>
            </td>
        </tr>
        <tr>
            <td class="remarks2">Remarks :</td>
            <td  colspan="3"  class="remarks">
              <span class="inpt_details"><?php echo $view_details->getVal('remarks',$sql); ?></span>
              <span class="inpt_hide"> 
                <input type="text" name="remarks" style="width:100%;" value="<?php echo $view_details->getVal('remarks',$sql); ?>"/>
              </span>
            </td>
        </tr>
      </table>

      <div class="body-reg">
          <span><b>Requirements/Credentials:</b></span><br><br>
        <div class="requremen">
         <div class="requ">  
             <span><input type="checkbox" name="lrncef" value="true" disabled> Certificate of Completion (LRN Cert)</span>
         </div>
         <div class="requ">  
             <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="" name="form138" value="true" disabled> High School Card (Form 138)</span>
         </div>
         <div class="requ">  
             <span><input type="checkbox"  name="form137" value="true" disabled> Student Permanent Record Form 137</span>
         </div>
         <div class="requ">  
             <span><input type="checkbox"  name="goodmoral" value="true" disabled> Certificate of Good Moral Character</span>
         </div>
          <div class="requ"> 
              <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"  name="nso" value="true" disabled> NSO Birth Certificate</span>
          </div>
        </div>
      </div> 
         
  <center>
    <script>
       $('#savemodify').hide();
    </script>
    <span id="modifyhide"><button type="button" id="modify" class="button_local" onclick="studentinfoModify()">Modify</button></span>
    <span id="showsave"><button type="button" id="savemodify" class="button_local" onclick="saveModifystudentinfo()">Save</button></span>
  </center>            
  </div>

  


