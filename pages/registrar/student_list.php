<?php 
  if (session_status() == PHP_SESSION_NONE) {
              
    session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewRegistrarDetails.php';
  $viewdetails = new ViewRegistrarDetails;
?>

<div>
  <table class="table table table-bordered">
    <thead>
    <tr align="center" bgcolor="gray">
      <th>No.</th>
      <th>Student ID</th>
      <th>Last Name</th>
      <th>First Name</th>
      <th>Middle Name</th>
      <th>Course</th>
      <th>Date of Registration</th>
    </tr>
    <thead>
    <?php
            $sql = "SELECT 
                    studentid,
                    lastname,
                    firstname,
                    middlename,
                    dateenrolled,
                    coursename
                    FROM studentinfo 
                    INNER JOIN studentcourse using(studentid)
                    INNER JOIN curriculum using(curcode)
                    INNER JOIN course USING(coursecode)
                     ORDER BY lastname";
                    
            if ($viewdetails->notEmpty($sql)) {
              $openqry = $viewdetails->openqry($sql);
              $x=1;
            while($r = pg_fetch_assoc($openqry)){
          ?>
              <tr>
                <td class='remarks'><?=$x?></td>
                <td class='remarks'><?=$r['studentid']?></td>
                <td class='remarks'><?=$r['lastname']?></td>
                <td class='remarks'><?=$r['firstname']?></td>
                <td class='remarks'><?=$r['middlename']?></td>
                <td class='remarks'><?=$r['coursename']?></td> 
                <td class='remarks'><?=$r['dateenrolled']?></td>
              </tr>
          <?php
          $x++;
            }// end while
          } else {
            echo "<b>No record found...</b>";
          }
            
          ?>
  </table>
</div>