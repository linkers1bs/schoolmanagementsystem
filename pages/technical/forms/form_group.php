<?php

  if (isset($_GET['grouptypeid'])) {  
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
    require '../../../controllers/db_controller/theDBConn.php';
    $db = new DB;
    $grouptypeid = $_GET['grouptypeid'];
    $sql = "SELECT * FROM grouptype WHERE grouptypeid IN (SELECT grouptypeid FROM grouptype WHERE grouptypeid = $grouptypeid)";
  } 
?>
  <div class="size-16">
    <div class="we-40">
      <span>Group Name</span>
    </div>
    <input type="text" name="groupname" class="wid-fix2" value="<?php if (isset($_GET['grouptypeid'])) {echo $db->getVal('name',$sql);}?>" id="groupname">
  </div>

  <div class="size-16">
    <div class="we-40">
      <span>Description</span>
    </div>
    <input type="text" name="description" class="wid-fix2" value="<?php if (isset($_GET['grouptypeid'])) {echo $db->getVal('description',$sql);}?>" id="description">
  </div>
