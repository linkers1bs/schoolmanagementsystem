<?php
// DISPLAY TABLE
if (isset($_GET["branchname"])) {
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }
  require '../../../controllers/db_controller/theDBCOnn.php';
  require '../../../controllers/db_controller/ViewTechnicalDetails.php';
  $view_details = new ViewDetails();
  $var = $_GET["branchname"];
  ?>
  <div class="spacetable">
    <div class="table-responsive">
        <table>
        <thead>
          <tr style="background-color: #666" align="center" class="table-heads">
            <th>Branch Name</th>
            <th>Username</th>
            <th>Fullname</th>
            <th>Member of Group</th>
            <th>Position</th>   
          </tr>
        </thead>
        <tbody>
        <?php
          $view_user_details = $view_details->view_user_details($var);
          if ($view_user_details) {
              while($row = pg_fetch_assoc($view_user_details)){
              ?>
                <tr>
                  <td class="remarks"><?php echo $row["branchname"]; ?></td>
                  <td class="remarks"><?php echo $row["user"]; ?></td>
                  <td class="remarks"><?php echo ucfirst($row["lastname"]).", ".ucfirst($row["firstname"])." ".ucfirst($row["middlename"][0])."."; ?></td>
                  <td class="remarks"><?php echo $row["member_of_group"]; ?></td>
                  <td class="remarks"><?php echo $row["position"]; ?></td>
                </tr>
              <?php
              }
          }else {
            echo "<td colspan=5 class='remarks'>No Data Found...</td>";
          }
        ?>
        </tbody>
      </table>
    </div>
  </div>
  <?php
}elseif  (isset($_GET["branchid"]) && isset($_GET["type"]) && $_GET["type"] == "memberof") {
  ///table 
  if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }
    require '../../../controllers/db_controller/theDBCOnn.php';
    require '../../../controllers/db_controller/ViewTechnicalDetails.php';
    $view_details = new ViewDetails();
    $var = $_GET["branchid"];
    ?>
    <span>Username:</span>&nbsp;&nbsp;&nbsp;
    <select class="wid-fix" style="width: 240px!important" name="usernamememeber" id="usernamememeber" onChange="show_bydetails_member();">
      <?php
        $sql = "  SELECT branch.name as branchname, userinfo.username as user, * FROM branch
                  LEFT JOIN userinfo USING (branchid) ";
        if($var != "all"){
          $sql .= "WHERE branch.branchid = ".$var."";
        }
        $view = $view_details->has_data_db($sql);
        if($view){
          ?>
            <option value="none">Select Username..</option>
          <?php
          while ($r = pg_fetch_assoc($view)){
            $selsub = '';
             if (!empty($_POST['usernamememeber']) && $_POST['usernamememeber'] == $r['username'])
             {
              $selsub = 'selected="selected"';
             } 
        
             echo '<option value="'.$r['username'].'"'.$selsub.'>'.$r['username'].'</option>';
          }
        }else {
          echo '<option>No record found..</option>';
        }
      ?>
    </select>
  <?php
} elseif (isset($_GET["usernameid"]) ){
  session_start();
  require '../../../controllers/db_controller/theDBCOnn.php';
  require '../../../controllers/db_controller/ViewTechnicalDetails.php';
  $view_details = new ViewDetails();;
  
  $username = $_GET["usernameid"];
   $sql = "
    SELECT branch.name as branchname, userinfo.username as user, grouptype.name as groupname, * FROM userinfo
    LEFT JOIN branch USING(branchid)
    LEFT JOIN usergroup USING (username)
    LEFT join grouptype USING (grouptypeid)
    WHERE userinfo.username = '$username'
    ";   
    ?>
    <div class="size-16">
       <div class="we-40">
           <span>Branch</span>
       </div> 
      <select class="wid-fix2"  name="branchid" id="branchid">
           <?php
              $show_db = "SELECT branchid, name FROM branch ORDER BY name ASC ";
              $view = $view_details->has_data_db($show_db);
              if($view){
                while ($r = pg_fetch_assoc($view)){
                   $selsub = '';
                   if ($view_details->getVal('branchid', $sql) == $r['branchid'])
                   {
                      $selsub = 'selected="selected"';
                   } 
                  echo '<option value="'.$r['branchid'].'" '.$selsub.'>'.$r['name'].'</option>';
                }
              }else {
                echo '<option>No record found..</option>';
              }
          ?>
        </select>
    </div>
    <div class="size-16">
     <div class="we-40">
         <span>Username</span>
     </div>
       <input type="text" name="username" class="wid-fix2" id="username" value="<?php echo $view_details->getVal('username',$sql);?>">
      <input type="hidden" name="username1" class="wid-fix2" id="username1" value="<?php echo $view_details->getVal('username',$sql);?>">
    </div>
  
   <!-- <div class="size-16">
     <div class="we-40">
         <span>Member of Group</span>
     </div>
        <select class="wid-fix2"  name="grouptypeid" id="grouptypeid">
       <?php
          // $show_db = "SELECT grouptypeid,name FROM grouptype ORDER BY name ASC ";
          // $view = $view_details->has_data_db($show_db);
          // if($view){
          //   while ($r = pg_fetch_assoc($view)){
          //      $selsub = '';
          //                  if ($view_details->getVal('grouptypeid', $sql) == $r['grouptypeid'])
          //                  {
          //                   $selsub = 'selected="selected"';
          //                  } 
                  
          //            echo '<option value="'.$r['grouptypeid'].'"'.$selsub.'>'.$r['name'].'</option>';
          //   }
          // }else {
          //   echo '<option>No record found..</option>';
          // }
          ?>
        </select>
    </div> -->
  
    <div class="size-16">
     <div class="we-40">
         <span>Email</span>
     </div>
       <input type="email" name="email" class="wid-fix2"  id="email" value="<?php echo $view_details->getVal('email',$sql);?>">
      
    </div>
  <div class="size-16">
     <div class="we-40">
         <span>Lastname</span>
     </div>
         <input type="text" name="lastname" class="wid-fix2"  id="lastname" value="<?php echo $view_details->getVal('lastname',$sql);?>">
    </div>
    <div class="size-16">
     <div class="we-40">
         <span>Firstname</span>
     </div>
         <input type="text" name="firstname"  class="wid-fix2" id="firstname" value="<?php echo $view_details->getVal('firstname',$sql);?>">
    </div>
     <div class="size-16">
     <div class="we-40">
         <span>Middlename</span>
     </div>
         <input type="text" name="middlename"  class="wid-fix2" id="middlename" value="<?php echo $view_details->getVal('middlename',$sql);?>">
    </div>
     <div class="size-16">
     <div class="we-40">
         <span>Position</span>
     </div>
         <input type="text" name="position"  class="wid-fix2" id="position" value="<?php echo $view_details->getVal('position',$sql);?>">
    </div>
                       

    <?php


}else {
  //ADD USER FORM
   if (isset($_GET['grouptypeid'])) {  
    session_start();
    require '../../../controllers/db_controller/theDBConn.php';
    require '../../../controllers/function/errorrmsg.php';
    $grouptypeid = $_GET['grouptypeid'];
    // $sql = "SELECT * FROM grouptype WHERE grouptypeid IN (SELECT grouptypeid FROM grouptype WHERE grouptypeid = $grouptypeid)";
   
  }
?> 
    <div class="size-16">
     <div class="we-40">
         <span>Branch</span>
     </div> 
      <select class="wid-fix2"  name="branchid" id="branchid">
           <?php
              $getsubject = "SELECT * FROM branch ORDER BY name ASC ";
             if ($view_details->notEmpty($getsubject)) {
              ?><option value="none">Select Group..</option><?php
               $query = $view_details->openqry($getsubject);
                while ($r = pg_fetch_assoc($query)){
                  $selsub = '';
                   if (!empty($_POST['branchid']) && $_POST['branchid'] == $r['branchid'])
                   {
                    $selsub = 'selected="selected"';
                   } 
                  echo '<option value="'.$r['branchid'].'"'.$selsub.'>'.$r['name'].'</option>';
                }
             }else {
              echo '<option value="none">No data found</option>';
             }
          ?>
        </select>
    </div>
    <div class="size-16">
     <div class="we-40">
         <span>Username</span>
     </div>
       <input type="text" name="username" class="wid-fix2" id="username">
      
    </div>
    <div class="size-16">
     <div class="we-40">
         <span>Password</span>
     </div>
       <input type="password" name="pwd" class="wid-fix2"  id="pwd">
    </div>
     <div class="size-16">
     <div class="we-40">
         <span>Retype Password</span>
     </div>
       <input type="password" name="pwd2" class="wid-fix2"  id="pwd2">
    </div>
   <div class="size-16">
     <div class="we-40">
         <span>Member of Group</span>
     </div>
        <select class="wid-fix2"  name="grouptypeid" id="grouptypeid">
           <?php
              $getsubject = "SELECT * FROM grouptype ORDER BY name ASC ";
             if ($view_details->notEmpty($getsubject)) {
              ?><option value="none">Select Group..</option><?php
                $query = $view_details->openqry($getsubject);
                while ($r = pg_fetch_assoc($query)){
                  $selsub = '';
                   if (!empty($_POST['grouptypeid']) && $_POST['grouptypeid'] == $r['grouptypeid'])
                   {
                    $selsub = 'selected="selected"';
                   } 
                echo '<option value="'.$r['grouptypeid'].'"'.$selsub.'>'.$r['name'].'</option>';
                }
             }else {
              echo '<option  value="none">No data found</option>';
             }
          ?>
        </select>
    </div>
  
    <div class="size-16">
     <div class="we-40">
         <span>Email</span>
     </div>
       <input type="email" name="email" class="wid-fix2"  id="email">
      
    </div>
  <div class="size-16">
     <div class="we-40">
         <span>Lastname</span>
     </div>
         <input type="text" name="lastname" class="wid-fix2"  id="lastname">
    </div>
    <div class="size-16">
     <div class="we-40">
         <span>Firstname</span>
     </div>
         <input type="text" name="firstname"  class="wid-fix2" id="firstname">
    </div>
     <div class="size-16">
     <div class="we-40">
         <span>Middlename</span>
     </div>
         <input type="text" name="middlename"  class="wid-fix2" id="middlename">
    </div>
     <div class="size-16">
     <div class="we-40">
         <span>Position</span>
     </div>
         <input type="text" name="position"  class="wid-fix2" id="position">
    </div>
 <?php 
}


 ?>