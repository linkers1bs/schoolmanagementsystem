<div class="modal fade" id="<?php echo $modaladd[$i].$j.$gname; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="_modal_notif_container" style="margin: 0px auto;margin-top: 20%;">
      <div class="modal-content">
      <div class="title">
      <h6>Add Group</h6>
      </div>
      <div class="_modal_content">
        <input type="hidden" name="username" class="wid-fix2" value="<?php echo $row["username"];?>">
        <select class="wid-fix2"  name="grouptypeid[]" id="grouptypeid-list<?php echo $j; ?>">
        <option value="none">Select Group..</option>
        <?php
            $getsubject = "SELECT grouptypeid,name FROM grouptype ORDER BY name ASC ";
           if ($view_details->has_data_db($getsubject)) {
            $query = $view_details->has_data_db($getsubject);
            while ($r = pg_fetch_assoc($query)){
                $selsub = '';
                 if ($view_details->getVal('grouptypeid', $sql) == $r['grouptypeid'])
                 {
                  $selsub = 'selected="selected"';
                 } 
                echo '<option value="'.$r['grouptypeid'].'"'.$selsub.'>'.$r['name'].'</option>';
              }
           }else {
            echo '<option>No data found</option>';
           }
        ?>
        </select>
      </div>
      <div class="_modal_footer">
        <button type="button" data-dismiss="modal"  id="<?php echo $j;?>">Cancel</button>&nbsp;&nbsp;&nbsp;
        <button type="button" data-dismiss="modal" id="<?php echo $j;?>" onclick="action_user_member(this.id,'add',)" >OK</button>
      </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="<?php echo $delete[$i].$j.$gname; ?>"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false" >
  <div class="modal-dialog" role="document">
    <div class="_modal_notif_container" style="margin: 0px auto;margin-top: 20%;">
      <div class="modal-content">
      <div class="title">
      <h6>Delete Group</h6>
      </div>
      <div class="_modal_content">
            <h6>Are you sure you want to delete this group member? <br> <b style="color: red"><?php echo $row["name"];?></b></h6>
      </div>
      <div class="_modal_footer">
        <button type="button" data-dismiss="modal"  id="<?php echo $j;?>">Cancel</button>&nbsp;&nbsp;&nbsp;
        <button type="button" data-dismiss="modal"  id="<?php echo $j;?>" onclick="action_user_member(this.id,'delete')" >OK</button>
      </div>
      </div>
    </div>
  </div>
</div>

