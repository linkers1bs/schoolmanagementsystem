<div class="tablecon">
	<div class="requroption" >
		<span><b>List of User's</b></span><br><br>
		<span>Branch name:</span>&nbsp;&nbsp;&nbsp;
         	<select class="wid-fix" style="width: 240px!important" id="branc-list" onChange="getMember();">
                <?php
                  $sql = "SELECT * FROM branch ORDER BY name ASC ";
                  $view = $view_details->has_data_db($sql);
                  if($view){
                    ?>
                      <option value="none">Select branches..</option>
                      <option value="all">All</option>
                    <?php
                    while ($r = pg_fetch_assoc($view)){
                      $selsub = '';
                       if (!empty($_POST['branch']) && $_POST['branch'] == $r['branchid'])
                       {
                        $selsub = 'selected="selected"';
                       } 
                  
                       echo '<option value="'.$r['branchid'].'"'.$selsub.'>'.$r['name'].'</option>';
                    }
                  }else {
                    echo '<option>No record found..</option>';
                  }
                ?>
          	</select>

          	<div id="selectuser-data" style="display: inline-block;">
                <!--DISPLAY USERNAME OPTION  -->
            </div>
            <br>
            <div id="show_member_table" style="display: inline-block;width: 100%">
                <!--DISPLAY USERNAME INFO MEMBER   -->
            </div >
	</div>
</div>