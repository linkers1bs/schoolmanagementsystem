
  <div class="tab_add_data_table1">
    <div class="adds userAdd">
       <form name="userAdd" id="userAdd">
         <b>Add New User</b>
           <?php
             require 'forms/form_user.php';
            ?>
          <div class="buttonsave">
            <button  type="button" class='button_local' onclick="user('add')">Save</button>  
          </div>
       </form>
    </div>
  </div>


  <div class="tab_modify_data_table1">
      <div class="adds modifyUser">
        <form name="modifyUser" id="modifyUser">
        <b>Required Option</b>
            <?php
            require 'action/modify_user.php';
            ?>
          <div class="buttonsave">
            <button  type="button" class='button_local'  onclick="user('update')"> Update </button>
          </div>
        </form>
      </div>
  </div>

  <div class="tab_delete_data_table1">
      <div class="adds deleteUser">
          <form name="deleteUser" id="deleteUser">
          <b>Required Option</b>
           <?php
              require 'action/delete_user.php';
            ?>
          <div class="buttonsave">
            <button  type="button" class='button_local'  onclick="user('delete')"> Delete </button>
          </div>
          </form>
      </div>
  </div>

  <div class="tab_member_data_table">
      <form name="memberof">
       <?php
          require 'member_of.php';
        ?>
      </form>
  </div>

  <div class="table_info_show1">
    <div class="tablecon">
      <div class="requroption">
        <span><b>List of User's</b></span><br><br>
         <span>Branch name:</span>&nbsp;&nbsp;&nbsp;
         <select class="wid-fix" style="width: 240px!important" id="filter-list" onChange="getFilter();">
               <?php
                  $sql = "SELECT * FROM branch ORDER BY name ASC ";
                  $view = $view_details->has_data_db($sql);
                  if($view){
                    ?>
                      <option value="none">Select branches..</option>
                      <option value="all">All</option>
                    <?php
                    while ($r = pg_fetch_assoc($view)){
                      $selsub = '';
                       if (!empty($_POST['branch']) && $_POST['branch'] == $r['branchid'])
                       {
                        $selsub = 'selected="selected"';
                       } 
                  
                       echo '<option value="'.$r['branchid'].'"'.$selsub.'>'.$r['name'].'</option>';
                    }
                  }else {
                    echo '<option>No record found..</option>';
                  }
                ?>
          </select>
      </div>
      <div id="filter-data">
       <!-- DISPLAY RABLE OF USER INFORMATION -->
      </div>
    </div>
  </div>
       
