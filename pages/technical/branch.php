<div class="tab_add_data_table2">
  <div class="adds branchAdd" >
      <form name="branchAdd" id="branchAdd">
       <b>Add New Group</b>
        <?php
          require 'forms/form_branch.php';
          ?>
        <div class="buttonsave">
          <button  type="button" class='button_local' id="add" onclick="branch_mange(this.id)">Save</button>  
        </div>
      </form>
  </div>
</div>


<div class="tab_modify_data_table2">
    <div class="adds branchModify">
      <form name="branchModify" id="branchModify">
        <b>Required Option</b>
            <?php
             require 'action/modify_branch.php';
            ?>
          <div class="buttonsave">
            <button  type="button" class='button_local' id="update" onclick="branch_mange(this.id)"> Update </button>
          </div>
        </form>
    </div>
</div>

<div class="tab_delete_data_table2">
    <div class="adds branchDelete">
        <form name="branchDelete" id="branchDelete">
           <b>Required Option</b>
            <?php
             require 'action/delete_branch.php';
            ?>
          <div class="buttonsave">
            <button  type="button" class='button_local' id="delete" onclick="branch_mange(this.id)"> Delete </button>
          </div>
        </form>
    </div>
</div>

<div class="table_info_show2">
  <div class="tablecon">
      <div class="requroption">
        <span><b>List of Branches</b></span>
      </div>
      <div class="spacetable">
      <div class="table-responsive">
      <table id="branch_table">
      <thead>
        <tr style="background-color: #666" align="center" class="table-heads">
          <th>Branch Name</th>
          <th>Current Address</th>
          <th>Landline</th>
          <th>Mobile Number</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $sql = $view_details->view_branch_details();
        if ($sql) {
          while($row = pg_fetch_assoc($sql)){
        ?>
          <tr id='rowbranch<?php echo $row["branchid"]; ?>'>
             <td class="remarks"><?php echo $row["name"] ?></td>
             <td class="remarks"><?php echo $row["address"] ?></td>
             <td class="remarks"><?php echo $row["landline"] ?></td>
             <td class="remarks"><?php echo $row["mobile"] ?></td>
           </tr>
          
        <?php
          }
        }else {
          echo "<td colspan=4 class='remarks'>No Data Found.</td>";
        }
      ?>
      </tbody>
    </table>
      </div>
    </div>
  </div>
</div>
