<div class="tab_add_data_table3">
  <div class="adds groupAdd">
    <form name="groupAdd" id="groupAdd">
      <b>Add New Group</b>
      <?php
        require 'forms/form_group.php';
      ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" onclick="group_manage(this.id)">Save</button>  
      </div>
    </form>
  </div>
</div>

<div class="tab_modify_data_table3">
  <div class="adds groupModify">
     <form name="groupModify" id="groupModify">
      <b>Required Option</b>
      <?php
        require 'action/modify_group.php';
      ?>
      <div class="buttonsave">
        <button  type="button" class='button_local'  id='update' onclick="group_manage(this.id)"> Update </button>
      </div>
    </form>
  </div>
</div>

<div class="tab_delete_data_table3">
  <div class="adds groupDel">
    <form name="groupDel" id="groupDel">
      <b>Required Option</b>
      <?php
        require 'action/delete_group.php';
      ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id='delete' onclick="group_manage(this.id)"> Delete </button>
      </div>
    </form>
  </div>
</div>


<div class="table_info_show3">
  <div class="tablecon">
    <div class="requroption">
      <span><b>List of Group's</b></span>
    </div>
    <div class="spacetable">
      <div class="table-responsive">
        <table id="group_table">
          <thead>
            <tr style="background-color: #666" align="center" class="table-heads">
              <th>Group Name</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <?php
              if ($view_details->view_group_details()) {
                $qry = $view_details->view_group_details();
                while($row = pg_fetch_assoc($qry)){
                ?>
                  <tr id="group<?php echo $row["grouptypeid"]?>">
                    <td class="remarks"><?php echo $row["name"]; ?></td>
                    <td class="remarks"><?php echo $row["description"]; ?></td>
                  </tr>

                <?php
                }
              }else {
              echo "<td  class='remarks' colspan=2 >No Data Found...</td>";
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>



