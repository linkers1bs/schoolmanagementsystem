<?php 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }
  require '../../../controllers/db_controller/theDBCOnn.php';
  require '../../../controllers/db_controller/ViewTechnicalDetails.php';
  $view_details = new ViewDetails();

?>
<div class="spacetable">
	<table  class="table table table-bordered" id="table_member_show">
	<thead>
		<tr style="background-color: #666" align="center" class="table-heads">
			<th>Member of Group</th>
			<th>Action</th>   
		</tr>
	</thead>
	<input type="hidden" name="usernamepk" id="usernamepk" class="wid-fix2" value="<?php echo $_GET["user"];?>">
	<input type="hidden" name="active" class="wid-fix2" value="active">
	<?php
	$details = $view_details->view_member_details($_GET["user"],$_GET["branchid"]);
	if($details){
		$modaladd = array( 'addmodal');// Set the array
		$delete = array('delete');// Set the array
		$active = array('active');
		$i = 0;
		$j = 0;
		while($row = pg_fetch_assoc($details ) ){
			$users  = $row["username"];
			$gname = strtolower($row["groupname"]);
			$gname = str_replace(' ', '', $gname );
		?>
			<input type="hidden" value="<?php echo $row["grouptypeid"];?>"  name="grouptypeid<?php echo $j; ?>" >
			<tr id='row<?php echo $j?>'>
				<td class="remarks"><?php echo $row["groupname"]; ?></td>
				<td>
					<span title="Add Group" data-toggle="tooltip" data-placement="top"><a href="#" class="btn btn-default btn-sm"  data-toggle="modal" data-target="#<?php echo $modaladd[$i].$j.$gname; ?>"><i class="fa fa-plus"></i></a></span>
					| 
					<span title="Delete Group" data-toggle="tooltip" data-placement="top"><a href="#" class="btn btn-default btn-sm"  data-toggle="modal" data-target="#<?php echo $delete[$i].$j.$gname; ?>">
					<i class="fa fa-times"></i>
					</a></span> | <select class="btn btn-default btn-sm"  style="width: 85px!important" name="typbo<?php echo $j; ?>" id="<?php echo $j; ?>" onChange="action_user_member(this.id,'action');">
					<option <?php if ($row["type"] == 't' )  {
					echo "selected";
					}  ?> >Active</option>
					<option <?php if ($row["type"] == 'f')  {
					echo "selected";
					}  ?> >Inactive</option>
					</select>
					<span>
					</a></span> 
					<?php include '../forms/modal_usermember.php'; ?>
				</td>
			</tr>
			<?php
			$j++;
		}
		?>
		<input type="hidden" name="number_of_data" id="number_of_data" class="wid-fix2" value="<?php echo $j;?>">
		<?php
	}
	?>
	</table>
</div>
