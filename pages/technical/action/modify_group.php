  <div class="size-16">
    <div class="we-40">
      <span>Select Group:</span>
    </div>
    <select class="wid-fix2"  name="groupid" id="group-list" onChange="getGroup();">
      <?php
        $view = $view_details->view_group_details();
        if($view){
          ?>
             <option value="none">Select group..</option>
          <?php
            while ($r = pg_fetch_assoc($view)){
                $selsub = '';
                if (!empty($_POST['groupid']) && $_POST['groupid'] == $r['grouptypeid']){
                  $selsub = 'selected="selected"';
                } 
                echo '<option value="'.$r['grouptypeid'].'"'.$selsub.'>'.$r['name'].'</option>';
            }
        }else {
          echo '<option>No record found..</option>';
        }
      ?>
    </select>
  </div>

  <div class="requroption">
    <span>Group Information</span>
  </div>

  <div id="group-data">

  </div>
