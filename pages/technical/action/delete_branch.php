<div class="size-16">
  <div class="we-40">
    <span>Select Branch:</span>
  </div>
  <select class="wid-fix2"  name="branchid" id="delete_branch_id" onChange="get_delete_branch();">
    <?php
      $view = $view_details->view_branch_details();
      if($view){
        ?>
            <option value="none">Select branches..</option>
        <?php
          while ($r = pg_fetch_assoc($view)){
              $selsub = '';
              if (!empty($_POST['branch']) && $_POST['branch'] == $r['branchid']){
                $selsub = 'selected="selected"';
              } 
              echo '<option value="'.$r['branchid'].'"'.$selsub.'>'.$r['name'].'</option>';
          }
      }else {
        echo '<option>No record found..</option>';
      }
    ?>
  </select>
</div>

<div class="requroption">
  <span>Branch Information</span>
</div>
<div id="delete-branch-data">

</div>
               