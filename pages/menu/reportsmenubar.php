<?php
  $view_option_details = new ViewOptionDetails();
?>
<div class="_body_container" id="_body_container">

  <div class="_menu_box_container _menu_my_acc">
      <a href="#" class="show_report" id="report1">
      <img src="./assets/image/enrollmentsystem-report.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Enrollment System Report</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc">
     <a href="#" class="show_report" id="report2">
      <img src="./assets/image/classroom-schedule.png">
        <div class="_box_body_content">
          <div class="_box_title"></div>
          <div class="_box_content">
          <h6>Classroom Schedule</h6>
          </div>
        </div>
      </a>
  </div>

</div>
<?php  include './partials/_sidebar.php'; ?>

<div class="_body_container">

  <div class="_pop_window _drag_me report1" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','1')" >Enrollment System Report</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_1" onclick="close_report('report1','minimize_1','expand_1')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/report/enrollmentreport.php'; ?>
    </div>
  </div>


  <div class="_pop_window _drag_me report2" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','2')" >Classroom Schedule</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/report/classroomschedule.php'; ?>
    </div>
  </div>

</div>

<!--   <div class="container-nor">
  <div class="nor">
  <div class="nor-row">
      <div class="imgnor">
        <a href="?page=report">
        <img src="image/Subject-icon.png">
      <p>Enrollment Sysytem Report</p></a>               
      </div>
      <div class="imgnor"> 
        <a href="?page=classchedule">
        <img src="image/Classroom-icon.png">
      <p>Classroom Schedule</p></a>                
      </div>
      
  </div>
</div>
 -->