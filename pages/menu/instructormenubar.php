<?php
  $view_option_details = new ViewOptionDetails();
?>
<div class="_body_container" id="_body_container">

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report1">
      <img src="./assets/image/student-type.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Class List</h6>
          </div>
        </div>
      </a>
  </div>
  

<div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report2">
      <img src="./assets/image/form-138.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Attendance</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report3">
      <img src="./assets/image/enrollmentsystem-report.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Grade Management</h6>
          </div>
        </div>
      </a>
  </div>
  
</div>


<?php  include './partials/_sidebar.php'; ?>


<div class="_body_container">

  

  <div class="_pop_window _drag_me report1" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" >Classlist</a></h6>
        </div>
       
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_1" onclick="close_report('report1','minimize_1','expand_1')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/instructor/classlist.php'; ?>
    </div>
  </div>

   <div class="_pop_window _drag_me report2" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table2" onclick="show_tab_menu('show','2')" >Attendance</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table2" onclick="show_tab_menu('add','2')" >Check Attendance</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table2" onclick="show_tab_menu('modify','2')" >Detailed</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/instructor/attendance.php'; ?>
    </div>
  </div>

   <div class="_pop_window _drag_me report3" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#">Grade Management</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_3" onclick="expand_role('minimize_3','report3','expand_3')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_3" style="display:none;" onclick="minimize_role('minimize_3','report3','expand_3')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_3" onclick="close_report('report3','minimize_3','expand_3')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/instructor/grademanagement.php'; ?>
    </div>
  </div>

</div>