<?php
  $view_option_details = new ViewOptionDetails();
  $view_details = new ViewManagementDetails();
?>

<div class="_body_container" id="_body_container">
  
  <div class="_menu_box_container _menu_my_acc updatesIN">
    <a href="#" class="show_report" id="report1">
      <img src="./assets/image/subjects.png">
      <div class="_box_body_content">
        <div class="_box_content">
        <h6>Subjects</h6>
        </div>
      </div>
    </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report2">
      <img src="./assets/image/rooms.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Rooms</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report3">
      <img src="./assets/image/instructor-management.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Instructor</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report4">
      <img src="./assets/image/courses.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Courses</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report5">
      <img src="./assets/image/curriculum.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Curriculum</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report6">
      <img src="./assets/image/block-schedule.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Block & Schedules</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report7">
      <img src="./assets/image/class-section.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Class Sectioning</h6>
          </div>
        </div>
      </a>
  </div>

 <!--  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report8">
      <img src="./assets/image/courses.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Curriculum Details</h6>
          </div>
        </div>
      </a>
  </div> -->

</div>





<?php  include './partials/_sidebar.php'; ?>



<div class="_body_container">

  <div id="action_loading"></div> 

  <div class="_pop_window _drag_me report1" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','1')" >Subject</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table1" onclick="show_tab_menu('add','1')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table1" onclick="show_tab_menu('modify','1')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table1" onclick="show_tab_menu('delete','1')" >Delete</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_1" onclick="close_report('report1','minimize_1','expand_1')"><i class="fa fa-times"></i></a>
      </div>     
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/subjectin.php'; ?>
    </div>
  </div>


  <div class="_pop_window _drag_me report2" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table2" onclick="show_tab_menu('show','2')" >Room</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table2" onclick="show_tab_menu('add','2')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table2" onclick="show_tab_menu('modify','2')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table2" onclick="show_tab_menu('delete','2')" >Delete</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/room.php'; ?>
    </div>
  </div>

   <div class="_pop_window _drag_me report3" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table3" onclick="show_tab_menu('show','3')" >Instructor's</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_3" onclick="expand_role('minimize_3','report3','expand_3')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_3" style="display:none;" onclick="minimize_role('minimize_3','report3','expand_3')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_3" onclick="close_report('report3','minimize_3','expand_3')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/instructor.php'; ?> 
    </div>
  </div> 

<div class="_pop_window _drag_me report4" id='_pop_window' >
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table4" onclick="show_tab_menu('show','4')" >Courses</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table4" onclick="show_tab_menu('add','4')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table4" onclick="show_tab_menu('modify','4')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table4" onclick="show_tab_menu('delete','4')" >Delete</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_4" onclick="expand_role('minimize_4','report4','expand_4')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_4" style="display:none;" onclick="minimize_role('minimize_4','report4','expand_4')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_4" onclick="close_report('report4','minimize_4','expand_4')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/course.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report5" id='_pop_window' >
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table5" onclick="show_tab_menu('show','5')" >Curriculum</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table5" onclick="show_tab_menu('add','5')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table5" onclick="show_tab_menu('modify','5')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table5" onclick="show_tab_menu('delete','5')" >Delete</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_details_table5" onclick="show_tab_menu('details','5')" >Details</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_5" onclick="expand_role('minimize_5','report5','expand_5')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_5" style="display:none;" onclick="minimize_role('minimize_5','report5','expand_5')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_5" onclick="close_report('report5','minimize_5','expand_5')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/curriculum.php'; ?>
    </div>
  </div>



  <div class="_pop_window _drag_me report6" id='_pop_window' >
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table6" onclick="show_tab_menu('show','6')" >Block & Schedules</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table6" onclick="show_tab_menu('add','6')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table6" onclick="show_tab_menu('modify','6')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table6" onclick="show_tab_menu('delete','6')" >Delete</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_6" onclick="expand_role('minimize_6','report6','expand_6')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_6" style="display:none;" onclick="minimize_role('minimize_6','report6','expand_6')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_6" onclick="close_report('report6','minimize_6','expand_6')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/block.php'; ?>
    </div>
  </div>


  <div class="_pop_window _drag_me report7" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table7" onclick="show_tab_menu('show','7')" >Sectioning</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table7" onclick="show_tab_menu('add','7')" >Add</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_7" onclick="expand_role('minimize_7','report7','expand_7')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_7" style="display:none;" onclick="minimize_role('minimize_7','report7','expand_7')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_7" onclick="close_report('report7','minimize_7','expand_7')"><i class="fa fa-times"></i></a>
      </div>
    </div>
    <div class="_pop_body">
      <?php require 'pages/management/sectioning.php'; ?>
    </div>
  </div>


 <!--  <div class="_pop_window _drag_me report8" id='_pop_window' >
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table8" onclick="show_tab_menu('show','8')" >Curriculum Details</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_8" onclick="expand_role('minimize_8','report8','expand_8')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_8" style="display:none;" onclick="minimize_role('minimize_8','report8','expand_8')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_8" onclick="close_report('report8','minimize_8','expand_8')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
        <?php //require 'pages/management/curriculum_details.php'; ?>
    </div>
  </div> -->

</div>


