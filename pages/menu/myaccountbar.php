
<?php
$view_details_myaccount  = new ViewMyaccountDetails;

 ?>

<div class="_body_container" id="_body_container"> 
	<div class="_menu_box_container _menu_my_acc updatesIN">
		<a href="#" class="show_report" id="report1">
			<img src="./assets/image/stud-account.png">
			<div class="_box_body_content">
				<div class="_box_content">
					<h6>My Profile</h6>
				</div>
			</div>
		</a>
	</div>
  


	<div class="_menu_box_container _menu_my_acc updatesIN">
	    <a href="#" class="show_report" id="report2">
	    	<img src="./assets/image/changepass.png">
				<div class="_box_body_content">
					<div class="_box_content">
	        			<h6>Change Password</h6>
					</div>
				</div>
		</a>
  	</div>
  

</div>

<?php  include './partials/_sidebar.php'; ?>


<div class="_body_container">

<div id="action_loading"></div>

	<div class="_pop_window _drag_me report1" id='_pop_window'>
		<div class="_pop_title">
			<h6>My Profile</h6>
			<div class="_pop_action">
		        <a href="?"><i class="fa fa-home"></i></a>
		        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
		        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
		        <a href="#" class="close_report_1" onclick="close_report('report1','minimize_1','expand_1')"><i class="fa fa-times"></i></a>
		      </div>			
		</div>
		<div class="_pop_body">
			
			<?php require 'pages/myaccount/myprofile.php'; ?>
		</div>
	</div>
	

	<div class="_pop_window _drag_me report2" id='_pop_window'>
		<div class="_pop_title">
			<h6>Change Password</h6>
			<div class="_pop_action">
		        <a href="?"><i class="fa fa-home"></i></a>
		        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
		        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
		        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
		      </div>			
		</div>
		<div class="_pop_body">
		<?php require 'pages/myaccount/mychangepass.php'; ?>
		</div>
	</div>

</div>


