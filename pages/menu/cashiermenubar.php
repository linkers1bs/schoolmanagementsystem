<?php
  $view_option_details = new ViewOptionDetails();
  $collection_reports = new ViewCashierDetails();
?>
<div class="_body_container" id="_body_container">

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report1">
      <img src="./assets/image/registration.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Registration</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report2">
      <img src="./assets/image/stud-account.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Students Account</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report3">
      <img src="./assets/image/create-bill.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Create Billing</h6>
          </div>
        </div>
      </a>
  </div>

   <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report4">
      <img src="./assets/image/mescellaneous.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Mescellaneous Fees</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report5">
      <img src="./assets/image/register-fee.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Registration Fees</h6>
          </div>
        </div>
      </a>
  </div>

   <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report6">
      <img src="./assets/image/tuitionfee.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Tuition Fees</h6>
          </div>
        </div>
      </a>
  </div>

   <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report7">
      <img src="./assets/image/payment.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Payment History</h6>
          </div>
        </div>
      </a>
  </div>

<div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report8">
      <img src="./assets/image/account-receive.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Account Receivables</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report9">
      <img src="./assets/image/collection-reports.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Collection Reports</h6>
          </div>
        </div>
      </a>
  </div>

</div>

<?php include './partials/_sidebar.php'; ?>



<div class="_body_container" >
<div id="action_loading"></div> 

  <div class="_pop_window _drag_me report1" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="registration_tab('show','1')">Registration</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table1" onclick="registration_tab('modify','1')" >Modify</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_1" onclick="close_report('report1','minimize_1','expand_1')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php  require 'pages/cashier/registration.php'; ?>
    </div>
  </div>

   <div class="_pop_window _drag_me report2" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table2" onclick="show_tab_menu('show','2')">Student Account</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/student_account.php';?>
    </div>
  </div>

  <div class="_pop_window _drag_me report3" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table3" onclick="show_tab_menu('show','3')">Create Billing</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_3" onclick="expand_role('minimize_3','report3','expand_3')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_3" style="display:none;" onclick="minimize_role('minimize_3','report3','expand_3')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_3" onclick="close_report('report3','minimize_3','expand_3')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/create_billing.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report4" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table4" onclick="show_tab_menu('show','4')">Mescellaneous Fees</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_4" onclick="expand_role('minimize_4','report4','expand_4')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_4" style="display:none;" onclick="minimize_role('minimize_4','report4','expand_4')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_4" onclick="close_report('report4','minimize_4','expand_4')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/miscellaneous_fees.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report5" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table5" onclick="show_tab_menu('show','5')">Registration Fees</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_5" onclick="expand_role('minimize_5','report5','expand_5')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_5" style="display:none;" onclick="minimize_role('minimize_5','report5','expand_5')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_5" onclick="close_report('report5','minimize_5','expand_5')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/registration_fees.php'; ?>
    </div>
  </div>

<div class="_pop_window _drag_me report6" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table6" onclick="show_tab_menu('show','6')" >Tuition Fees</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_6" onclick="expand_role('minimize_6','report6','expand_6')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_6" style="display:none;" onclick="minimize_role('minimize_6','report6','expand_6')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_6" onclick="close_report('report6','minimize_6','expand_6')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/tuition_fee.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report7" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table6" onclick="show_tab_menu('show','7')" >Payment History</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_7" onclick="expand_role('minimize_7','report7','expand_7')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_7" style="display:none;" onclick="minimize_role('minimize_7','report7','expand_7')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_7"  onclick="close_report('report7','minimize_7','expand_7')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/payment-history.php'; ?>
    </div>
  </div>
<!--
  <div class="_pop_window _drag_me report8" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table8" onclick="show_tab_menu('show','8')" >Account Recievable</a></h6>
        </div>
      </div>
     <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_8" onclick="expand_role('minimize_8','report8','expand_8')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_8" style="display:none;" onclick="minimize_role('minimize_8','report8','expand_8')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_8" onclick="close_report('report8')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php //require 'pages/cashier/account-recievables.php'; ?>
    </div>
  </div> -->

  <div class="_pop_window _drag_me report9" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table9" onclick="show_tab_menu('show','9')" >Collection Report</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_9" onclick="expand_role('minimize_9','report9','expand_9')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_9" style="display:none;" onclick="minimize_role('minimize_9','report9','expand_9')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_9"  onclick="close_report('report9','minimize_9','expand_9')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/cashier/collection-report.php'; ?>
    </div>
  </div>



</div>





















