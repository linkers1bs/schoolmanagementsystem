
<div class="_body_container" id="_body_container">

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report_1">
      <img src="./assets/image/result-entry.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Result Entry</h6>
          </div>
        </div>
      </a>
  </div>

    <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report_2">
      <img src="./assets/image/result-summary.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Result Summary</h6>
          </div>
        </div>
      </a>
  </div>

</div>
<?php  include './partials/_sidebar.php'; ?>


<div class="_body_container">

  <div class="_pop_window _drag_me report1" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="close_report('report1')" >Result Entry</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table1" onclick="close_report('report1')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table1" onclick="close_report('report1')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table1" onclick="close_report('report1')" >Delete</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_1"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_1" style="display:none;"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_1"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/testcenter/result-entry.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report2" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="close_report('report2')" >Result Entry</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table1" onclick="close_report('report2')" >Add</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_modify_table1" onclick="close_report('report2')" >Modify</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_delete_table1" onclick="close_report('report2')" >Delete</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_2"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_2" style="display:none;"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_2"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/testcenter/testcenter.php'; ?>
    </div>
  </div>


</div>





  <!-- <div class="container-nor">
  <div class="nor">
  <div class="nor-row">
      <div class="imgnor">
        <a href="?page=entry">
        <img src="image/Subject-icon.png">
      <p>Result Entry</p></a>               
      </div>
      <div class="imgnor"> 
        <a href="?page=summary-result">
        <img src="image/Classroom-icon.png">
      <p>Result Summary</p></a>                
      </div>
      
  </div>
</div> -->
