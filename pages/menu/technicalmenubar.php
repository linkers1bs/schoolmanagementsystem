<?php
	$view_details = new ViewDetails();
?>
<div class="_body_container" id="_body_container">

  <div class="_menu_box_container _menu_my_acc updatesIN">
  	<a href="#" class="show_report" id="report1">
  	<img src="./assets/image/technical-user.png">
        <div class="_box_body_content">
          <div class="_box_content">
          	<h6>User's</h6>
          </div>
        </div>
   	</a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
    <a href="#" class="show_report" id="report2">
	<img src="./assets/image/branchmanage.png">
      <div class="_box_body_content">
        <div class="_box_content">
        <h6>Branch Management</h6>
        </div>
      </div>
    </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
    <a href="#" class="show_report" id="report3">
	<img src="./assets/image/groupmanage.png">
      <div class="_box_body_content">
        <div class="_box_content">
        <h6>Group Management</h6>
        </div>
      </div>
    </a>
  </div>
  
</div>



<?php  
	include './partials/_sidebar.php'; 
?>

<div class="_body_container">
	
	<div id="action_loading"></div>	
	<div class="_pop_window _drag_me report1 " id='_pop_window'>
		<div class="_pop_title">
			<div class="_tab_title_tab">
				<div class="_main_tab_one">
					<h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','1','tab_member_data_table')" >User Information</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_add_table1" onclick="show_tab_menu('add','1','tab_member_data_table')" >Add</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_modify_table1" onclick="show_tab_menu('modify','1','tab_member_data_table')" >Modify</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_delete_table1" onclick="show_tab_menu('delete','1','tab_member_data_table')" >Delete</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_memberof" >Member of</a></h6>
				</div>
			</div>
			<div class="_pop_action">
		        <a href="?"><i class="fa fa-home"></i></a>
		        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
		        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
		        <a href="#" class="close_report_1" onclick="close_report('report1','minimize_1','expand_1')"><i class="fa fa-times"></i></a>
		      </div>			
		</div>
			<div class="_pop_body">
      			<?php require 'pages/technical/user.php'; ?>
			</div>
	</div>
	
	<div class="_pop_window _drag_me report2" id='_pop_window'>
		<div class="_pop_title">
			<div class="_tab_title_tab">
				<div class="_main_tab_one">
					<h6><a href="#" class="show_info_table2" onclick="show_tab_menu('show','2')" >Branch Management</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_add_table2" onclick="show_tab_menu('add','2')" >Add</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_modify_table2" onclick="show_tab_menu('modify','2')" >Modify</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_delete_table2" onclick="show_tab_menu('delete','2')" >Delete</a></h6>
				</div>
			</div>
			<div class="_pop_action">
		        <a href="?"><i class="fa fa-home"></i></a>
		        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
		        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
		        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
		      </div>			
		</div>
		<div class="_pop_body">
      		<?php require 'pages/technical/branch.php'; ?>
		</div>
	</div>

  	<div class="_pop_window _drag_me report3" id='_pop_window'>
		<div class="_pop_title">
			<div class="_tab_title_tab">
				<div class="_main_tab_one">
					<h6><a href="#" class="show_info_table3" onclick="show_tab_menu('show','3')" >Group Management</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_add_table3" onclick="show_tab_menu('add','3')" >Add</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_modify_table3" onclick="show_tab_menu('modify','3')" >Modify</a></h6>
				</div>
				<div class="_main_tab_one">
					<h6><a href="#" class="show_delete_table3" onclick="show_tab_menu('delete','3')" >Delete</a></h6>
				</div>
			</div>
			<div class="_pop_action">
		        <a href="?"><i class="fa fa-home"></i></a>
		        <a href="#" class="expand_3" onclick="expand_role('minimize_3','report3','expand_3')"><i class="fa fa-expand"></i></a>
		        <a href="#" class="minimize_3" style="display:none;" onclick="minimize_role('minimize_3','report3','expand_3')"><i class="fa fa-compress"></i></a>
		        <a href="#" class="close_report_3" onclick="close_report('report3','minimize_3','expand_3')"><i class="fa fa-times"></i></a>
		      </div>
		</div>
		<div class="body_white_body">
			<div class="_pop_body">
				<?php require 'pages/technical/groupmanagement.php'; ?>
			</div>
		</div>
	</div>

</div>



