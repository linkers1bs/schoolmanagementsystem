<?php
  $view_option_details = new ViewOptionDetails();
?>

<div class="_body_container" id="_body_container">

<!--   <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report1">
      <img src="./assets/image/advise-sem1-student.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Advise Sem1 Student</h6>
          </div>
        </div>
      </a>
  </div> -->

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report2">
      <img src="./assets/image/advise-student.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Advise Student</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report3">
      <img src="./assets/image/print-cor.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Print COR</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report4">
      <img src="./assets/image/change-schedule.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Change Schedules</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report5">
      <img src="./assets/image/assign-sections.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Sections</h6>
          </div>
        </div>
      </a>
  </div>

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report6">
      <img src="./assets/image/sf1.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>SF1</h6>
          </div>
        </div>
      </a>
  </div>

 

  <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report7">
      <img src="./assets/image/gradesform-138.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>Grades - Form 138</h6>
          </div>
        </div>
      </a>
  </div>

  
 <div class="_menu_box_container _menu_my_acc updatesIN">
      <a href="#" class="show_report" id="report8">
      <img src="./assets/image/sf2.png">
        <div class="_box_body_content">
          <div class="_box_content">
          <h6>SF2</h6>
          </div>
        </div>
      </a>
  </div>
  

</div>


<?php include './partials/_sidebar.php'; ?>



<div class="_body_container">

  <div id="action_loading"></div>

  <!-- <div class="_pop_window _drag_me report1" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','1')" >Advise Sem1 Student</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_1" onclick="expand_role('minimize_1','report1','expand_1')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_1" style="display:none;" onclick="minimize_role('minimize_1','report1','expand_1')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_1" onclick="close_report('report1')"><i class="fa fa-times"></i></a>
      </div>     
    </div>
    <div class="_pop_body">
      <?php //require 'pages/adviser/advise-sem-student.php'; ?>
    </div>
  </div> -->

  <div class="_pop_window _drag_me report2" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table2" onclick="show_tab_menu('show','2')" >Advise Student</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_2" onclick="expand_role('minimize_2','report2','expand_2')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_2" style="display:none;" onclick="minimize_role('minimize_2','report2','expand_2')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_2" onclick="close_report('report2','minimize_2','expand_2')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/adviser/advise_sem.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report3" id='_pop_window'>
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table3" onclick="show_tab_menu('show','3')">Print COR</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_3" onclick="expand_role('minimize_3','report3','expand_3')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_3" style="display:none;" onclick="minimize_role('minimize_3','report3','expand_3')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_3" onclick="close_report('report3','minimize_3','expand_3')"><i class="fa fa-times"></i></a>
      </div>     
    </div>
    <div class="_pop_body">
      <?php require 'pages/adviser/print-cor.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report4" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table4" onclick="show_tab_menu('show','4')">Schedule Revision Form</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_4" onclick="expand_role('minimize_4','report4','expand_4')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_4" style="display:none;" onclick="minimize_role('minimize_4','report4','expand_4')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_4" onclick="close_report('report4','minimize_4','expand_4')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/adviser/change_schedule.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report5" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','5')">Sections</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_5" onclick="expand_role('minimize_5','report5','expand_5')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_5" style="display:none;" onclick="minimize_role('minimize_5','report5','expand_5')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_5" onclick="close_report('report5','minimize_5','expand_5')"><i class="fa fa-times"></i></a>
      </div>      
    </div>
    <div class="_pop_body">
      <?php require 'pages/adviser/assign_sections.php'; ?>
    </div>
  </div>

  <div class="_pop_window _drag_me report6" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','6')">SF1</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_6" onclick="expand_role('minimize_6','report6','expand_6')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_6" style="display:none;" onclick="minimize_role('minimize_6','report6','expand_6')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_6" onclick="close_report('report6','minimize_6','expand_6')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/adviser/sf1.php'; ?>
    </div>
  </div>

 <!--  <div class="_pop_window _drag_me report7" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table1" onclick="show_tab_menu('show','7')">Grades - Form 138</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_7" onclick="expand_role('minimize_7','report7','expand_7')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_7" style="display:none;" onclick="minimize_role('minimize_7','report7','expand_7')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_7" onclick="close_report('report7','minimize_7','expand_7')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php //require 'pages/adviser/form_138.php'; ?>
    </div>
  </div> -->

  <div class="_pop_window _drag_me report7" id='_pop_window' style="display: none;">
    <div class="_pop_title">
      <div class="_tab_title_tab">
        <div class="_main_tab_one">
          <h6><a href="#" class="show_info_table7" onclick="show_tab_menu('show','7')">Grades - Form 138</a></h6>
        </div>
        <div class="_main_tab_one">
          <h6><a href="#" class="show_add_table7" onclick="show_tab_menu('add','7')">2nd Page</a></h6>
        </div>
      </div>
      <div class="_pop_action">
        <a href="?"><i class="fa fa-home"></i></a>
        <a href="#" class="expand_7" onclick="expand_role('minimize_7','report7','expand_7')"><i class="fa fa-expand"></i></a>
        <a href="#" class="minimize_7" style="display:none;" onclick="minimize_role('minimize_7','report7','expand_7')"><i class="fa fa-compress"></i></a>
        <a href="#" class="close_report_7"  onclick="close_report('report7','minimize_7','expand_7')"><i class="fa fa-times"></i></a>
      </div>       
    </div>
    <div class="_pop_body">
      <?php require 'pages/adviser/form_138.php'; ?>
    </div>
  </div>

</div>
