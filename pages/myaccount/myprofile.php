
<?php 
        $name = $_SESSION["theUID"];
        $query = $view_details_myaccount->view_myaccount_details($name);
        $row = pg_fetch_assoc($query);
?>

<style type="text/css">
.buploadprof span {
    bottom: -4px;
    position: relative;
}
</style>
<div class="containerP" style='padding-top:50px;'>
    <div class="form-container">
        <div align="center">
            <div class="Cbody style" style="" align="center">
           
            <div id="alert-data">
                
            </div>
            <form  method="POST" enctype="multipart/form-data" >

        <div style="display: flex;">

            <div class="im" id="forimage">
                <img class="profimg"  id="output" src='<?php if($row["userprofile"] == '' || $row["userprofile"] == null){ echo "assets/image/emptyimage.png";}else {echo $row["userprofile"];} ?>' >

                <div class="profupload" style="padding-top: 16px;">
                    <input type="file"  accept="image/jpeg, image/png" name="photo" id="file" style="display: none;"   onchange="loadFile(event)">
                    <div class="buploadprof">
                       <label for="file" style="cursor: pointer;">
                                <span>Upload Image</span>
                            </label>
                    </div>
                    
                </div>
            </div>

            <div align="left">
                <div class="form profile"> 
                    <div class="from-inline row">
                        <label class="col-sm-3 prof">First Name</label>
                        <input type="text" name="firstname" id="firstname" class="form-control col-sm-7 inputprof" placeholder="Type here..." value="<?php echo $row["firstname"] ?>">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof ">Last Name</label>
                        <input type="text" name="lastname" id="lastname" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["lastname"] ?>">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof">E-mail</label>
                        <input type="email" name="email" id="email" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["email"] ?>" required>
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof">Phone Number</label>
                        <input type="number" name="phone" id="mobile" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["mobileno"] ?>">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof">Job Description</label>
                        <input type="text" name="jobdescription" id="description" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["position"] ?>">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof">Date of Birth</label>
                        <input type="date" name="dateofbirth" id="dateofbirth" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["dateofbirth"] ?>">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof">Gender</label>
                        <input type="text" name="gender" id="gender" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["gender"] ?>">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof">Department</label>
                        <input type="text" name="department" id="department" class="form-control col-sm-7 inputprof" placeholder="Type here..."  value="<?php echo $row["department"] ?>">
                    </div>
                </div>

                    <div>
                        <div align="right" class="bprofile">
                             <button type="submit" class="btn bsaveprof" name="add">SAVE</button>
                         </div>
                    </div>
    </div>
        </div>
</form>


        </div> 
        </div>
    </div>
</div>

    
<script>
var loadFile = function(event) {
    var image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};

</script>

<?php
    if (isset($_POST["add"])) {
     
        if (empty($_POST["firstname"])) {$firstname = ""; }else {$firstname = $_POST["firstname"];}
            if (empty($_POST["lastname"])) {$lastname = ""; }else {$lastname = $_POST["lastname"];}
            if (empty($_POST["email"])) {$email = ""; }else {$email = $_POST["email"];}
            if (empty($_POST["phone"])) {$mobileno = 0; }else {$mobileno = $_POST["phone"];}
            if (empty($_POST["description"])) {$description = ""; }else {$description = $_POST["description"];}    
            if (empty($_POST["dateofbirth"])) {$dateofbirth = ""; }else {$dateofbirth = $_POST["dateofbirth"];} 
            if (empty($_POST["gender"])) {$gender = ""; }else {$gender = $_POST["gender"];} 
            if (empty($_POST["department"])) {$department = ""; }else {$department = $_POST["department"];} 
            $photo_tmp = $_FILES['photo']['tmp_name'];
            $photo_filename = $_FILES['photo']['name'];
            $destination = "assets/image/image_user_profile/".$name.'-'.$photo_filename;
               if (move_uploaded_file($photo_tmp, $destination)) {
                $sql = "UPDATE userinfo
                SET firstname = '$firstname', lastname = '$lastname', email = '$email', mobileno = $mobileno, userprofile = '$destination', dateofbirth = '$dateofbirth', gender = '$gender', department = '$department'
                WHERE username = '$name'";
                    if ($view_details_myaccount->openqry($sql)) {
                        
                        echo $view_details_myaccount->alertmsg('Success', "Successfully updated..");
                    }
               }else {
                $sql = "UPDATE userinfo
                SET firstname = '$firstname', lastname = '$lastname', email = '$email', mobileno = $mobileno,
                dateofbirth = '$dateofbirth', gender = '$gender', department = '$department'
                WHERE username = '$name'";
                 if ($view_details_myaccount->openqry($sql)) {
                    echo $view_details_myaccount->alertmsg('Success', "Successfully updated..");
                    
                }
            }
            unset($_POST['add']);
            echo "<script> window.location='index.php?page=myaccount';  </script>";
    }       

 ?>
