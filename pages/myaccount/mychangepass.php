

        <form name="myForm" method="POST" id="addFormrest">
        <div align="center" class="mainpass">
          <center>
            <div class="form pass"> 
                <div class="from-inline row">
                        <label class="col-sm-3 prof">New Password</label>
                        <input type="password" name="pwd" id="pwd" class="form-control col-sm-6 inputprof" placeholder="Type here...">
                    </div>
                    <div class="from-group row">
                        <label class="col-sm-3 prof ">Confirm Password</label>
                        <input type="password" name="pwd2" id="pwd2" class="form-control col-sm-6 inputprof" placeholder="Type here...">
                    </div>
            </div>
          </center>
            <div>
                 <div align="center" class="bpass">
                    <button type="button" class="bsavepass"  onclick="change_Pass()">Save</button>
                    
                 </div>
            </div>
        </div>

        </form>
 

 <script type="text/javascript">
function change_Pass() {

        var pwd = document.forms["myForm"]["pwd"].value;
        var pwd2 = document.forms["myForm"]["pwd2"].value;
        if (pwd == "" && pwd2 == "" ) {
          $('#pwd,#pwd2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
          return false;
        }else {
          $('#pwd,#pwd2').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
        }
        if (pwd == "" ) {
          $('#pwd').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
          return false;
        }else {
          $('#pwd').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
        }
        if (pwd2 == "" ) {
          $('#pwd2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
          return false;
        }else {
          $('#pwd2').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
        }
        if (pwd != "" && pwd2 != "" ) {
            $('#pwd2').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            $('#pwd').css({"border":"2px solid #5bc0de","box-shadow":"0 0 3px #5bc0de"});
            $.ajax({          
                type: "GET",
                url  : "controllers/function/MyaccountController.php",
                data:'pwd2='+pwd2+'&pwd='+pwd+'&type=change_pass',
                beforeSend:function(){
                  $('#action_loading').show();
                },
                success: function(data){
                    $('#action_loading').hide();
                    $("#alert-data").html(data);
                    if (pwd != pwd2) {
                      $('#pwd,#pwd2').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
                    }else {
                      $("#addFormrest")[0].reset();
                    }
                    
                }
          });  
        }
}
 </script>