
 <?php
    if (session_status() == PHP_SESSION_NONE) {
        
          session_start();

    }
  require '../../../backends/db/theDBConn.php';
  require '../../../backends/function/errorrmsg.php';
  

  if(isset($_POST['action']) && $_POST['action'] == 'grademanagement') {

            $first = $_POST["first"];
            $second = $_POST["second"];
            $remarks = $_POST["remarks"];   
            $gradeid = $_POST["gradeid"];   
            $sem = $_POST['sem'];
            $name = $_SESSION["theUID"];
            $sql = "SELECT * FROM semester where semid = $sem";
            $getsem = getVal('sem',$sql);
        
            if($getsem == 1 || $getsem == "1") {

                for ($i=0; $i <  sizeof($gradeid); $i++) { 
                    if($first[$i] == '') {
                        $first[$i] = 0;
                    } 
                    if($second[$i] == ''){
                        $second[$i] = 0;
                    }
                
                $sql = "UPDATE studentgrades SET first = $first[$i], second = $second[$i], remarks = '$remarks[$i]', createdby = '$name', createat = current_timestamp   WHERE gradeid = $gradeid[$i]";
                    if(openqry($sql)) {
                        $bol = true;
                    }
                }      
                    if($bol) {
                        echo alertmsg("Success","The successfully updated..");
                    }else {
                        echo alertmsg('Danger', "Unable to save data.. ");
                    }
                
        
            
            } elseif ($getsem == "2" || $getsem == 2) {
                for ($i=0; $i <  sizeof($gradeid); $i++) { 
                    if($first[$i] == '') {
                        $first[$i] = 0;
                    } 
                    if($second[$i] == ''){
                        $second[$i] = 0;
                    }
                
                    $sql = "UPDATE studentgrades SET third = $first[$i], forth = $second[$i], remarks = $remarks[$i], createdby = '$name', createat = current_timestamp   WHERE gradeid = $gradeid[$i]";
                    if(openqry($sql)) {
                        $bol = true;
                    }
                }      
                    if($bol) {
                        echo alertmsg("Success","The successfully updated..");
                    }else {
                        echo alertmsg('Danger', "Unable to save data.. ");
                    }
                
            }

  
  }elseif (isset($_POST['action']) && $_POST['action'] == 'classlist') {
     $sy = $_POST["syid"];
    $sem = $_POST["semid"];
    $action = $_POST["action"];   
    $name = $_SESSION["theUID"];
    $sql = "  SELECT * FROM schedules 
    LEFT JOIN subjects USING (subjectcode)
         WHERE schedules.username = '$name' AND schedules.semid = $sem AND schedules.syid = $sy ";

   
        ?>
   
        <span> Block & Subject:</span>&nbsp;&nbsp;<select class="wid-fix" style="width: 150px!important" name="block" id="block" onChange="classlist();">
                                                 <?php
                                                 
                                                    $query = openqry($sql);
                                                    if(notEmpty($sql)) {
                                                      echo "<option value='none'>Select...</option>";
                                                        while ($r = pg_fetch_assoc($query)){
                                                       
                                                            echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].' </option>';
                                                        
                                                         }
        
                                                    }  else {
                                                        echo "<option>No record found..</option>";
                                                    }
                                                  
                                                ?>
                                            </select>
                <?php

  }elseif (isset($_POST['action']) && $_POST['action'] == 'attendace') {
    $sy = $_POST["syid"];
    $sem = $_POST["semid"];
    $action = $_POST["action"];   
    $name = $_SESSION["theUID"];
    $sql = "  SELECT * FROM schedules 
    LEFT JOIN subjects USING (subjectcode)
         WHERE schedules.username = '$name' AND schedules.semid = $sem AND schedules.syid = $sy ";
    if($_POST["type"] == 'check') {
        ?>
   
        <span> Block & Subject:</span>&nbsp;&nbsp;<select class="wid-fix" style="width: 150px!important" name="block" id="checkblock" onChange="getattendace('check');">
                                                 <?php
                                                 
                                                    $query = openqry($sql);
                                                    if(notEmpty($sql)) {
                                                      echo "<option value='none'>Select...</option>";
                                                        while ($r = pg_fetch_assoc($query)){
                                                       
                                                            echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].' </option>';
                                                        
                                                         }
        
                                                    }  else {
                                                        echo "<option>No record found..</option>";
                                                    }
                                                  
                                                ?>
                                            </select><span> Attendace for the Date of:</span>&nbsp;&nbsp;
                                        <input type="date" id="currentDate" class="wid-fix" style="width: 150px!important" value=''/>
        <script type="text/javascript">
            var date = new Date();
            var currentDate = date.toISOString().slice(0,10);
            document.getElementById('currentDate').value = currentDate;
        </script>
                <?php
    }elseif ($_POST['type'] == 'showtable') {
       
            $scheduleid = $_POST["block"];
        ?>
            <span><b>List of Students</b></span>
                  </div>
                  <div class="spacetable">
                  <style>
                    #myInput {
                    
                        width: 100%;
                        font-size: 12px;
                        padding: 6px;
                        border: 1px solid #ddd;
                        margin-top: 6px;
                        margin-bottom: 8px;
                        }

                </style>
                <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
                <div class="table-responsive">     
                  <table>
                  <thead>
                    <tr style="background-color: #666" align="center" class="table-heads">
                      <th>#</th>
                      <th>Student ID</th>
                      <th>Lastname</th>
                      <th>Firstname</th>
                      <th>MI</th>   
                      <th>Attended<br> <input type="checkbox"  onclick="toggle(this);"/></th>
                   
                      <th>Remarks</th>
                    </tr>
                  </thead>
                <?php 
                $sql = "SELECT *, studentgrades.remarks AS studentremarks FROM studentgrades 
                LEFT JOIN schedules USING (scheduleid)
                LEFT JOIN studentinfo USING(studentid) WHERE scheduleid = $scheduleid";
                $sqlop = openqry($sql);
                $i = 0;
                while($row = pg_fetch_assoc($sqlop)) {
                  $i++;
                  $gradeid = $row["gradeid"];
                  ?>
                  <tr>
                  <input type="hidden"  id="gradeid<?php echo $i?>" value="<?php echo $gradeid; ?>" />
                    <td class="remarks"><?php echo $i; ?></td>
                    <td class="remarks"><?php echo $row["studentid"]; ?></td>
                    <td class="remarks"><?php echo ucfirst($row["lastname"]); ?></td>
                    <td class="remarks"><?php echo ucfirst($row["firstname"]); ?></td>
                    <td><?php echo ucfirst($row["middlename"][0]); ?>.</td>
                    <td><input type="checkbox"  id="check<?php echo $i?>" value="true"/></td>
             
                    <td><input type="text" id="remarks<?php echo $i?>"/></td>
                </tr>
                  <?php
                }
                
                ?><input type="hidden" id="number_of_data" value="<?php echo $i;?>"/>
                </table>
              </div>
              </div>
           
              <script>

function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}


</script>
           <?php
        } elseif  ($_POST['type'] == 'summary') {
            ?>
             <span> Block & Subject:</span>&nbsp;&nbsp;<select class="wid-fix" style="width: 200px!important" name="block" id="block" onChange="viewAttndance();">
                                                 <?php
                                                 
                                                    $query = openqry($sql);
                                                    if(notEmpty($sql)) {
                                                      echo "<option value='none'>Select...</option>";
                                                        while ($r = pg_fetch_assoc($query)){
                                                       
                                                            echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].' </option>';
                                                        
                                                         }
        
                                                    }  else {
                                                        echo "<option>No record found..</option>";
                                                    }
                                                  
                                                ?>
                                            </select>
            <?php
        }elseif  ($_POST['type'] == 'summarttable') {
            $scheduleid = $_POST["block"];
        ?>
            <span><b>Student Class Attendance</b></span>
                  </div>
                  <div class="spacetable">
                  <style>
                    #myInput {
                    
                        width: 100%;
                        font-size: 12px;
                        padding: 6px;
                        border: 1px solid #ddd;
                        margin-top: 6px;
                        margin-bottom: 8px;
                        }

                </style>
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">
                <div class="table-responsive">     
                  <table>
                  <thead>
                    <tr style="background-color: #666" align="center" class="table-heads">
                      <th>#</th>
                      <th>Student ID</th>
                      <th>Names</th>
                      <th>Attendace<br>Present | Absent</th>
                    </tr>
                  </thead>
                <?php 
                 $sql = "SELECT *, studentgrades.remarks AS studentremarks FROM studentgrades 
                LEFT JOIN schedules USING (scheduleid)
               
                LEFT JOIN studentinfo USING(studentid) WHERE scheduleid = $scheduleid";
                $sqlop = openqry($sql);
                $i = 0;
                while($row = pg_fetch_assoc($sqlop)) {
                  $i++;
                  $gradeid = $row["gradeid"];
                   $sq = "SELECT *
                  FROM attendance WHERE gradeid = $gradeid ";
                  $count = openqry($sq);
                  $absent = 0;
                  $atted = 0;
                  while($j = pg_fetch_assoc($count)) {
                      if($j["attended"] == 't') {
                          $atted++;
                      }else {
                          $absent++;
                      }
                  }
                  ?>
                  <tr>
               
                    <td class="remarks"><?php echo $i; ?></td>
                    <td class="remarks"><?php echo $row["studentid"]; ?></td>
                    <td class="remarks"><?php echo ucfirst($row["lastname"]).', '.ucfirst($row["firstname"]).' '.ucfirst($row["middlename"][0]).'.'; ?></td>
                    <td ><?php echo $atted?> | <?php echo $absent?> </td>
                   
                </tr>
                  <?php 
                }
                
                ?><input type="hidden" id="number_of_data" value="<?php echo $i;?>"/>
                </table>
              </div>
              </div>
            <?php
        }elseif  ($_POST['type'] == 'detailedblock') {
            ?>
            <span> Block & Subject:</span>&nbsp;&nbsp;<select class="wid-fix" style="width: 150px!important" name="block" id="checkDetailblock" onChange="studentDetail('checkDetail');">
                                                <?php
                                                
                                                   $query = openqry($sql);
                                                   if(notEmpty($sql)) {
                                                     echo "<option value='none'>Select...</option>";
                                                       while ($r = pg_fetch_assoc($query)){
                                                      
                                                           echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].' </option>';
                                                       
                                                        }
       
                                                   }  else {
                                                       echo "<option>No record found..</option>";
                                                   }
                                                 
                                               ?>
                                           </select>
           <?php

         }elseif  ($_POST['type'] == 'perstudent') {
            $scheduleid = $_POST["block"];
             $sql = "SELECT *, studentgrades.remarks AS studentremarks FROM studentgrades 
            LEFT JOIN schedules USING (scheduleid)
            LEFT JOIN studentinfo USING(studentid) WHERE scheduleid = $scheduleid";
            ?>
            <span> Student Name:</span>&nbsp;&nbsp;<select class="wid-fix" style="width: 230px!important" name="gradeid" id="checkDetailgradeid" onChange="infoDetail('checkDetail');">
                                                <?php
                                                
                                                   $query = openqry($sql);
                                                   if(notEmpty($sql)) {
                                                     echo "<option value='none'>Select...</option>";
                                                       while ($r = pg_fetch_assoc($query)){
                                                      
                                                           echo '<option value="'.$r['gradeid'].'">'.ucfirst($r["lastname"]).', '.ucfirst($r["firstname"]).' '.ucfirst($r["middlename"]).'.'.' </option>';
                                                       
                                                        }
       
                                                   }  else {
                                                       echo "<option>No record found..</option>";
                                                   }
                                                 
                                               ?>
                                           </select>
           <?php 
         }elseif  ($_POST['type'] == 'showinfostudent') {
            $gradeid = $_POST["gradeid"];
            $scheduleid = $_POST["block"];
         
            $sq = "SELECT *
           FROM attendance WHERE gradeid = $gradeid ORDER BY atdate ASC";
           $count = openqry($sq);
           $absent = 0;
           $atted = 0;
           
            ?>  
            <style type="text/css">
                .sum {
                    width:44%;
                    display: inline-block;
                    position:relative;
                }
            </style>
            <center>
            <div class="sum">
               Student Class Attendance
               <div class="table-responsive">
                <table>
                  <thead>
                    <tr style="background-color: #666" align="center" class="table-heads">
                      <th>Dates</th>
                      <th>Attended</th>
                      <th>Remarks</th>
                     
                    </tr>
                  </thead>

                  <?php 
                    while($j = pg_fetch_assoc($count)) {
                     ?>
                        <tr>
                            <td><?php echo $j['atdate']; ?></td>
                            <td><?php  if($j["attended"] == 't') {
                                echo "Present";
                                $atted++;
                                }else {
                                   echo "Absent";
                                   $absent++;
                                } ?></td>
                            <td><?php echo $j["remarks"] ?></td>
                        </tr>
                     <?Php
                        
                    }
                  
                  ?>
                </table>
              </div>
            </div>
            <div class="sum">
           Student Summary of Attendance
            <table  class="table table table-bordered" id="myTable">
                  <thead>
                    <tr style="background-color: #666" align="center" class="table-heads">
                      <th>Marks</th>
                      <th>Attendace</th>
                    </tr>
                  </thead>
                  <tr>
                      <td>Absent</td>
                      <td><?php echo $absent; ?></td>
                  </tr>
                  <tr>
                      <td>Present</td>
                      <td><?php echo $atted; ?></td>
                  </tr>
                </table>
            </div>
            <div style="display: block;">
            Student Class Schedule<br>
          <div class="table-responsive">
            <table  class="table table table-bordered" id="myTable">
                  <thead>
                    <tr style="background-color: #666" align="center" class="table-heads">
                      <th>Room</th>
                      <th>Block</th>
                      <th>Subject</th>
                      <th>Day</th>
                      <th>Time</th>
                      <th>Start</th>
                      <th>End</th>
                      <th>Enrollee</th>
                    </tr>
                  </thead>
                  <?php 
                     $sqlsc = "SELECT * FROM schedules  
                    left join classschedules USING (scheduleid)
                   WHERE scheduleid = $scheduleid";
                     $ssc = openqry($sqlsc);
                    while($s = pg_fetch_assoc($ssc)) {
                     ?>
                        <tr>
                            <td><?php echo $s['roomcode']; ?></td>
                            <td><?php echo $s['blockname']; ?></td>
                            <td><?php echo $s['subjectcode']; ?></td>
                            <td><?php echo $s['days']; ?></td>
                            <td><?php echo $s['starttime'].' - '.$s['endtime']; ?></td>
                            <td><?php echo $s['datestart']; ?></td>
                            <td><?php echo $s['dateend']; ?></td>
                            <td><?php echo '3/'.$s["capacity"] ?></td>
                        </tr>
                     <?Php
                        
                    }
                  
                  ?>
                </table>
              </div>
            </div>
</center>
            <?php
         }
    }
?>