<?php 
	if (session_status() == PHP_SESSION_NONE) {
			        
	  session_start();

	}
	require '../../../controllers/db_controller/theDBConn.php';
	require '../../../controllers/db_controller/ViewAdviserDetails.php';
	require '../../../controllers/db_controller/ViewInstructorDetails.php';
  
	$view_details = new ViewAdviserDetails;
	$view_details_instructor = new ViewInstructorDetails;

if (isset($_POST["type"])) {
	
	if ($_POST["type"] == "attendance_name") {
	?>

	<span>&nbsp;Block & Subject:&nbsp;&nbsp;&nbsp;</span>
	<select class="wid-fix" name="scheduleid" style="width: 130px!important" onchange="attendance_filter_controller(this.id);" id="attendance_form">
		<?php
	      $query = $view_details_instructor->get_block_subject($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
	      if($query) {
	        echo '<option value="none">Select..</option>';
	        while ($r = pg_fetch_assoc($query)){
	          echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].'</option>';
	        }
	      } else {
	        echo '<option>No record found..</option>';
	      }

	    ?>
	</select>

	<?php
	} elseif ($_POST["type"] == "attendance_form") {
		$blockname = $_POST["scheduleid"];
	?>
	<br>
	<span><b>Check Attendance</b></span>
	<div class="spacetable">
	<div class="table-responsive">     
		<table>
			<thead>
				<tr style="background-color: #666" align="center" class="table-heads">
					<th>#</th>
					<th>Student ID</th>
					<th>Names</th>
					<th>Attendace<br>Present | Absent</th>
				</tr>
			</thead>

			<?php 
				$sql = $view_details_instructor->attendace_details($blockname); 
				if($sql){
					$i = 1;
					while ($r = pg_fetch_assoc($sql)) {
					$gradeid = $r["gradeid"];
					$sq = "SELECT * FROM attendance WHERE gradeid = $gradeid ";
					$count = $view_details->openqry($sq);
					$absent = 0;
					$atted = 0;

					while($j = pg_fetch_assoc($count)) {
						if($j["attended"] == 't') {
							$atted++;
						}else {
							$absent++;
						}
				}
			?>
			<tr>
				<td class="remarks"><?php echo $i++; ?></td>
				<td class="remarks"><?php echo $r["studentid"]; ?></td>
				<td class="remarks"><?php echo ucfirst($r["lastname"]).', '.ucfirst($r["firstname"]).' '.ucfirst($r["middlename"][0]).'.'; ?></td>
				<td ><?php echo $atted?> | <?php echo $absent?> </td>
			</tr>
			<?php 
				}
			} else {
				echo "<tr>
					  	<td colspan='4'><b>No record found...</b></td>
					  </tr>";
			}
			?>
			<input type="hidden" id="number_of_data" value="<?php echo $i;?>"/>
		</table>
	</div>
	</div>

<?php
	} elseif ($_POST["type"] == "attendance_name_check") {
	?>
	<span>Block & Subject:&nbsp;&nbsp;</span>
	<select class="wid-fix" name="scheduleid" style="width: 150px!important" onchange="attendance_filter_controller_check(this.id);" id="attendance_form">
		<?php
	      $query = $view_details_instructor->get_block_subject($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
	      if($query) {
	        echo '<option value="none">Select..</option>';
	        while ($r = pg_fetch_assoc($query)){
	          echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].'</option>';
	        }
	      } else {
	        echo '<option>No record found..</option>';
	      }

	    ?>
	</select>
	<span> Attendace for the Date of:</span>&nbsp;&nbsp;
    <input type="date" id="currentDate" class="wid-fix" style="width: 150px!important" value=''/>
    <span>
      <button  type="button" id="attendance_name_add_check" class='button_local' onclick="attendance_filter_controller_check(this.id)">Save</button>
    </span>
    <script type="text/javascript">
        var date = new Date();
        var currentDate = date.toISOString().slice(0,10);
        document.getElementById('currentDate').value = currentDate;
    </script>
	<?php
	} elseif ($_POST["type"] =="attendance_form_check") {
		$blockname = $_POST["scheduleid"];
	?>
	<br>
	<span><b>Check Attendance</b></span>
	<div class="spacetable">
		<style>
        #myInputcheck {
        
            width: 100%;
            font-size: 12px;
            padding: 6px;
            border: 1px solid #ddd;
            margin-top: 6px;
            margin-bottom: 8px;
            }

      </style>
      <input type="text" id="myInputcheck" onkeyup="check_attendance_table()" placeholder="Search for names.." title="Type in a name">
		<div class="table-responsive">     
			<table id="myTableAttendaace">
				<thead>
					<tr style="background-color: #666" align="center" class="table-heads">
						<th>#</th>
						<th>Student ID</th>
						<th>Names</th>
						<th>Attended<br> <input type="checkbox"  onclick="toggle(this);"/></th>
						<th>Remarks</th>
					</tr>
				</thead>

				<?php 
					$sql = $view_details_instructor->attendace_details($blockname); 
					if($sql){
						$i = 1;
						while ($r = pg_fetch_assoc($sql)) {
						$gradeid = $r["gradeid"];
					?>
					<tr>
						<input type="hidden"  id="gradeid_check<?php echo $i?>" value="<?php echo $gradeid; ?>" />
						<td class="remarks"><?php echo $i; ?></td>
						<td class="remarks"><?php echo $r["studentid"]; ?></td>
						<td class="remarks">
							<?php echo ucfirst($r["lastname"]).', '.ucfirst($r["firstname"]).' '.ucfirst($r["middlename"][0]).'.'; ?>
						</td>
						<td><input type="checkbox"  id="check_check<?php echo $i?>" value="true"/></td>
	                    <td><input type="text" id="remarks_check<?php echo $i?>"/></td>
					</tr>
					<?php 
					$i++;
						}
					} else {
						echo "<tr>
							  	<td colspan='4'><b>No record found...</b></td>
							  </tr>";
					}
				?>
				<input type="hidden" id="number_of_data_checkattendace" value="<?php echo $i;?>"/>
			</table>
		</div>
	</div>
	<script>
	function toggle(source) {
	    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
	    for (var i = 0; i < checkboxes.length; i++) {
	        if (checkboxes[i] != source)
	            checkboxes[i].checked = source.checked;
	    }
	}
	</script>
	<?php
	} elseif ($_POST["type"] == "attendance_form_details") {
		?>
		<span>&nbsp;Block & Subject:&nbsp;&nbsp;&nbsp;</span>
		<select class="wid-fix" name="scheduleid" style="width: 130px!important" onchange="attendance_filter_details(this.id);" id="attendance_form_details_name">
			<?php
		      $query = $view_details_instructor->get_block_subject($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
		      if($query) {
		        echo '<option value="none">Select..</option>';
		        while ($r = pg_fetch_assoc($query)){
		          echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].'</option>';
		        }
		      } else {
		        echo '<option>No record found..</option>';
		      }

		    ?>
		</select>
		<?php
	}elseif ($_POST["type"] == "attendance_form_details_name") {
		?>
		<span>&nbsp;Block & Subject:&nbsp;&nbsp;&nbsp;</span>
		<select class="wid-fix" name="scheduleid" style="width: 130px!important" onchange="attendance_filter_details(this.id);" id="attendance_form_details_name">
			<?php
		      $query = $view_details_instructor->get_block_subject($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
		      if($query) {
		        echo '<option value="none">Select..</option>';
		        while ($r = pg_fetch_assoc($query)){
		          echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].'</option>';
		        }
		      } else {
		        echo '<option>No record found..</option>';
		      }

		    ?>
		</select>
		<?php
	}
} 
?>
	
