 <?php
    if (session_status() == PHP_SESSION_NONE) {
        
          session_start();

    }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewAdviserDetails.php';
  require '../../../controllers/db_controller/ViewInstructorDetails.php';

  $view_details = new ViewAdviserDetails;
  $view_details_instructor = new ViewInstructorDetails;

if (isset($_POST["type"])) {  

    if ($_POST["type"] == "studentblock") {

  ?>
    <span>&nbsp;Block & Subject:&nbsp;&nbsp;&nbsp;</span>
    <select class="wid-fix" style="width: 130px!important" name="scheduleid" id="scheduleid" onchange="show_block_name(this.id)">

    <?php
      $query = $view_details_instructor->get_block_subject($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
      if($query) {
        echo '<option value="none">Select..</option>';
        while ($r = pg_fetch_assoc($query)){
          echo '<option value="'.$r['scheduleid'].'">'.$r['blockname'].' - '.$r["subjectcode"].'</option>';
        }
      } else {
        echo '<option>No record found..</option>';
      }

    ?>
    </select>
  <?php
  } elseif ($_POST['type'] == 'studentlist') {
      $blockname = $_POST["scheduleid"]; //student id
      $sql = $view_details_instructor->get_details_classlist($blockname);
      if($sql){
        $data = pg_fetch_assoc($sql);
      ?>
      <style>
      b {
          padding-bottom: 0px!important; 
          border-bottom: none!important;
      }
      .remarks2 {
          text-align: right;
          background-color: #f2f3f4!important;
        
      }
      </style>
      <br>
      <div align="center" style="background-color: #F2F3F4;" class="create" >
        <div align="center" style="padding: 10px;" >
          <b>Class List</b><br>
          <span>School Year</span>
            <div class="info" style=""> 
              <table bgcolor="#B0C4DE" class="table table table-bordered sal-tab1" style="width: 100%; margin-bottom: 0px;  border-collapse: collapse;">
                <tr>
                  <td class="remarks2">Block  :</td>
                  <td class="remarks"><b><?php echo $data["blockname"];?></b></td>
                  <td  class="remarks2">Subject   :</td>
                  <td  class="remarks"><b><?php echo $data["subjectcode"];?></b></td>
                </tr>
                <tr>
                  <td  class="remarks2">Day  :</td>
                  <td  class="remarks"><b><?php echo $data["days"];?></b></td>
                  <td  class="remarks2">Room  :</td>
                  <td  class="remarks"><b><?php echo $data["roomcode"];?></b></td>
                <tr>
                  <td  class="remarks2">Start Date :</td>
                  <td  class="remarks"><b><?php echo $data["datestart"];?></b></td>
                  <td  class="remarks2">Time  :</td>
                  <td  class="remarks"><b><?php echo $data["starttime"].'-'.$data["endtime"].'.';?></b></td>
                </tr>
                <tr>
                    <td  class="remarks2">End Date :</td>
                    <td class="remarks" bgcolor="#edf2f8"><b><?php echo $data["dateend"];?></b></td>
                    <td  class="remarks2">Instructor :</td>
                  <td  class="remarks"><b><?php echo ucwords($data["instructorname"]);?></b></td>
                  </tr>
            </table>
          
            <div class="spacetable">
                <style>
                #myInput {

                    width: 100%;
                    font-size: 12px;
                    padding: 6px;
                    border: 1px solid #ddd;
                    margin-top: 6px;
                    margin-bottom: 8px;
                    }

                </style>
                <div class="table-responsive">
                    <table>
                    <thead>
                      <tr style="background-color: #666" align="center" class="table-heads">
                        <th>#</th>
                        <th>Last Name</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>ID Number</th>
                      </tr>
                    </thead>
                    <?php 
                    $details = $view_details_instructor->get_details_classlist($blockname);
                    if ($details) {
                        $i =1;
                        while($r = pg_fetch_assoc($details)) {
                        ?>
                        <tr>
                          <td class="remarks"><?php echo $i++; ?></td>
                          <td class="remarks"><?php echo ucfirst($r["lastname"]); ?></td>
                          <td class="remarks"><?php echo ucfirst($r["firstname"]); ?></td>
                          <td class="remarks"><?php echo ucfirst($r["middlename"])?> </td>
                          <td class="remarks"><?php echo $r["studentid"]?> </td>
                      </tr>
                        <?php
                        }
                      }
                    ?>
                  </table>
                </div>
            </div>
          </div> 
        </div>
      </div>
  <?php  
      }else {
        echo "No enroll on this Subject...";
      }
    }
  }  
?>