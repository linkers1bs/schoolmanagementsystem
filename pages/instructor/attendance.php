
<!-- ATTENDANCE SUMMARY -->
<div class="table_info_show2">
  <div class="tablecon attendance_form_view">
    <form name="attendance_form_view" id="attendance_form_view">
      <div class="enroll">
        <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
            <div class="container_select">
              <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 180px!important" name="branchid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                  }
                ?>
              </select>
            </div>
            <div class="container_select">
               <span>School Year:&nbsp;&nbsp;</span>
                <select style="width: 100px!important" name="syid" style="display: inline-block;" >
                  <?php
                    $query = $view_option_details->optionDetails("schoolyear");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  ?>
                </select>
            </div>
           <div class="container_select">
            <span>Semester:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="semid"  style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("semester");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
              }
            ?>
            </select>
           </div>
            <div class="container_select">
              <span>Type:&nbsp;&nbsp;</span>
              <select style="width: 100px!important"  name="studenttypeid"style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("studenttype");
                while ($r = pg_fetch_assoc($query)){
                   $select = '';
                  if($r["studenttypeid"] == "2"){
                    $select = 'selected';
                  }
                  echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
                }
              ?>
              </select>
           </div>
            <div class="container_select">
                <span>
                  <button  type="button" id="attendance_name" class='button_local' onclick="attendance_filter_controller(this.id)">SELECT</button>
                </span>
            </div>
            <div class="container_select">
               <div id="show-block-data-attendance">
                  <!-- DISPLAY BLOCK HERE -->
               </div>
            </div>
          </div>
        </div>

      </div>

      <div id="show-attendance-info">
            <!-- DISPLAY SF1 HERE -->
      </div>


    </form>
  </div>
</div>



<!-- CHECK ATTENDACE -->
<div class="tab_add_data_table2">
  <div class="tablecon attendance_form_check_atte">
    <form name="attendance_form_check_atte" id="attendance_form_check_atte">
      <div class="enroll">
        <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
            <div class="container_select">
              <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 240px!important" name="branchid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                  }
                ?>
              </select>
            </div>
            <div class="container_select">
               <span>School Year:&nbsp;&nbsp;</span>
                <select style="width: 140px!important" name="syid" style="display: inline-block;" >
                  <?php
                    $query = $view_option_details->optionDetails("schoolyear");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  ?>
                </select>
            </div>
           <div class="container_select">
            <span>Semester:&nbsp;&nbsp;</span>
            <select style="width: 140px!important"  name="semid"  style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("semester");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
              }
            ?>
            </select>
           </div>
            <div class="container_select">
              <span>Type:&nbsp;&nbsp;</span>
              <select style="width: 140px!important"  name="studenttypeid"style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("studenttype");
                while ($r = pg_fetch_assoc($query)){
                   $select = '';
                  if($r["studenttypeid"] == "2"){
                    $select = 'selected';
                  }
                  echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
                }
              ?>
              </select>
           </div>
            <div class="container_select">
                <span>
                  <button  type="button" id="attendance_name_check" class='button_local' onclick="attendance_filter_controller_check(this.id)">SELECT</button>
                </span>
            </div>
            <div class="container_select">
               <div id="show-block-data-attendance-check">
                  <!-- DISPLAY BLOCK HERE -->
               </div>
            </div>
          </div>
        </div>

      </div>

      <div id="show-attendance-info-check">
            <!-- DISPLAY SF1 HERE -->
      </div>


    </form>
  </div>
</div>
<!-- END OF CHECK ATTENDACE CONTAINER -->


<!-- MODIFY CONTAINER -->
<div class="tab_modify_data_table2">
  <div class="tablecon attendance_form_view_details">
    <form name="attendance_form_view_details" id="attendance_form_view_details">
      <div class="enroll">
        <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
            <div class="container_select">
              <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 180px!important" name="branchid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                  }
                ?>
              </select>
            </div>
            <div class="container_select">
               <span>School Year:&nbsp;&nbsp;</span>
                <select style="width: 100px!important" name="syid" style="display: inline-block;" >
                  <?php
                    $query = $view_option_details->optionDetails("schoolyear");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  ?>
                </select>
            </div>
           <div class="container_select">
            <span>Semester:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="semid"  style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("semester");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
              }
            ?>
            </select>
           </div>
            <div class="container_select">
              <span>Type:&nbsp;&nbsp;</span>
              <select style="width: 100px!important"  name="studenttypeid"style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("studenttype");
                while ($r = pg_fetch_assoc($query)){
                   $select = '';
                  if($r["studenttypeid"] == "2"){
                    $select = 'selected';
                  }
                  echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
                }
              ?>
              </select>
           </div>
            <div class="container_select">
                <span>
                  <button  type="button" id="attendance_name_details" class='button_local' onclick="attendance_filter_details(this.id)">SELECT</button>
                </span>
            </div>
            <div class="container_select">
               <div id="show-block-data-attendance-details">
                  <!-- DISPLAY BLOCK HERE -->
               </div>
            </div>
          </div>
        </div>

      </div>

      <div id="show-attendance-info-details">
            <!-- DISPLAY SF1 HERE -->
      </div>


    </form>
  </div>
</div>
<!-- END OF DELETE MODIFY -->

<script type="text/javascript">
  
  function attendance_filter_controller(type){
 
    if(type == "attendance_name"){
      var data_html = "show-block-data-attendance";
      $.ajax({
        type : 'POST',
        url  : 'pages/instructor/forms/attendance_form.php',
        data : $('#attendance_form_view').serialize()+'&type=attendance_name',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    } else if (type == "attendance_form") {
      var data_html = "show-attendance-info";
      $.ajax({
        type : 'POST',
        url  : 'pages/instructor/forms/attendance_form.php',
        data : $('#attendance_form_view').serialize()+'&type=attendance_form',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    }
  } 


  function attendance_filter_controller_check(type){
    if (type == "attendance_form") {
      var data_html = "show-attendance-info-check";
      $.ajax({
        type : 'POST',
        url  : 'pages/instructor/forms/attendance_form.php',
        data : $('#attendance_form_check_atte').serialize()+'&type=attendance_form_check',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
          $('#'+data_html).show();
          $('#'+data_html).html(data);
        }
      });
    } else if (type == "attendance_name_check"){
      var data_html = "show-block-data-attendance-check";
      $.ajax({
        type : 'POST',
        url  : 'pages/instructor/forms/attendance_form.php',
        data : $('#attendance_form_check_atte').serialize()+'&type=attendance_name_check',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
           $('#'+data_html).show();
          $('#'+data_html).html(data);
        }
      });
    } else if (type == "attendance_name_add_check") { 
       var data_html = "show-block-data-attendance-check";
        var number_of_data=document.getElementById('number_of_data_checkattendace').value;
        var currentdate   =document.getElementById('currentDate').value;
        var gradeid = [];          
        var remarks = [];
        var attended = [];
        // append multiple values to the array
        for (var i = 1; i < number_of_data ; i++) {
         
            if ($('#check_check'+i).is(':checked')) {
                gradeid.push(document.getElementById('gradeid_check'+i).value);
                remarks.push(document.getElementById('remarks_check'+i).value);
                attended.push('true');
            } else {
                gradeid.push(document.getElementById('gradeid_check'+i).value);
                remarks.push(document.getElementById('remarks_check'+i).value);
                attended.push('false');
            }
        }
       
        $.post("controllers/function/instructorController.php",{ 
            gradeid         : gradeid,
            remarks         : remarks,
            attended        : attended,
            currentdate       : currentdate,
            type : "check_attendance"
            },
            function(data) {
            $("#alert-data").html(data);
            $('#'+data_html).hide();
             $('#show-attendance-info-check').hide();
        });
    }
  }

  function attendance_filter_details(type){
    if (type == "attendance_name_details") {
      var data_html = "show-block-data-attendance-details";
      $.ajax({
        type : 'POST',
        url  : 'pages/instructor/forms/attendance_form.php',
        data : $('#attendance_form_view_details').serialize()+'&type=attendance_form_details',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
          $('#'+data_html).show();
          $('#'+data_html).html(data);
        }
      });
    }else if (type == "attendance_form_details_name") {
      attendance_filter_details
    }
  }

  function check_attendance_table() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInputcheck");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTableAttendaace");
  tr = table.getElementsByTagName("tr");
  for (var i = 1; i < tr.length; i++) {
    var tds = tr[i].getElementsByTagName("td");
    var flag = false;
    for(var j = 0; j < tds.length; j++){
      var td = tds[j];
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        flag = true;
      } 
    }
    if(flag){
        tr[i].style.display = "";
    }
    else {
        tr[i].style.display = "none";
    }
  }
}
</script>