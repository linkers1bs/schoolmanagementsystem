<div class="tablecon classlist"> 
  <form name="classlist" id="classlist">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 180px!important" name="branchid"  style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid"  style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select" style="display: inline-block;">
            <span><button  type="button" id="studentblock" class='button_local' onclick="show_block_name(this.id)">SELECT</button></span>
          </div>
          <div class="container_select" style="display: inline-block;">
            <div id="classlist_form" style="display: inline-block;"> 
              <!-- DISPLAYT NAME OF REGISTER -->
            </div>
          </div>
        </div>
      </div>

    </div>
    
        
          <div id="display_classlist" >
                    <!-- DISPLAY PRINT COR -->
          </div>  
    

  </form>
</div>

<script type="text/javascript">
  
function show_block_name(type) {
   if (type == "studentblock") {
    var data_html = "classlist_form";
    $.ajax({
      type : 'POST',
      url : 'pages/instructor/forms/classlist_form.php',
      data : $('#classlist').serialize()+'&type=studentblock',
      beforeSend: function(){
        $('#action_loading').show();
      },
      success :function (data){
        $('#action_loading').hide();
        $('#'+data_html).html(data);
      }
    });
} else if (type == "scheduleid") {
  var data_html = "display_classlist";
     $.ajax({
      type : 'POST',
      url : 'pages/instructor/forms/classlist_form.php',
      data : $('#classlist').serialize()+'&type=studentlist',
      beforeSend: function(){
        $('#action_loading').show();
      },
      success :function (data){
        $('#action_loading').hide();
        $('#'+data_html).html(data);
      }
    });
  }
 }  




</script>