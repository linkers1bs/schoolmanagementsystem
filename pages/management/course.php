<div class="tab_add_data_table4">
  <div class="adds form_course_add">
      <form name="form_course_add" id="form_course_add">
       <b>Add New Course</b>
        <?php
          require 'forms/form_course.php';
          ?>
        <div class="buttonsave">
          <button  type="button" class='button_local' id="add" value="add"  onclick="course_manage(this.id)">Save</button>  
        </div>
    </form>
  </div>
</div>

<div class="tab_modify_data_table4">
    <div class="adds form_course_update">
      <form name="form_course_update" id="form_course_update">
        <b>Required Option</b>
            <div class="size-16">
              <div class="we-40">
                <span>Select Course:</span>
              </div>
              <select class="wid-fix2"  name="coursecode_modify" id="coursecode_modify" onChange="courseModify();">
               <?php
                  $sql = $view_details->course_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                       echo '<option value="'.$r['coursecode'].'">'.$r['coursename'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
              </select>
            </div>

            <div class="requroption">
            <span>Course Information</span>
            </div>
            <div id="show_course_modify">
              <!-- DISPLAY FORM MODIFY -->
            </div>
          <div class="buttonsave">
            <button  type="button" class='button_local' value="update" id='update' onclick="course_manage(this.id)"> Update </button>
          </div>
        </form>
    </div>
</div>


<div class="tab_delete_data_table4">
    <div class="adds form_course_delete">
      <form name="form_course_delete" id="form_course_delete">
       <b>Required Option</b>
          <div class="size-16">
            <div class="we-40">
              <span>Select Course:</span>
            </div>
            <select class="wid-fix2"  name="coursecode_delete" id="coursecode_delete" onChange="courseDelete();">
             <?php
                $sql = $view_details->course_details();
                if ($sql) {
                  ?>
                  <option value="none">Select group..</option>
                  <?php
                  while ($r = pg_fetch_assoc($sql)){
                     echo '<option value="'.$r['coursecode'].'">'.$r['coursename'].'</option>';
                  }
                } else {
                  echo '<option>No record found..</option>';
                }
              ?>
            </select>
          </div>

          <div class="requroption">
          <span>Course Information</span>
          </div>
          <div id="show_course_delete">
            <!-- DISPLAY FORM DELETE -->
          </div>
        <div class="buttonsave">
          <button  type="button" class='button_local'  value="delete" id='delete' onclick="course_manage(this.id)"> Delete </button>
        </div>
       </form>
    </div>
</div>
           

<!--TABLE INFORMATION  -->
<div class="table_info_show4">
  <div class="tablecon">
    <div class="requroption">
        <span><b>List of Courses</b></span>
    </div>
      <div class="spacetable">
      <div class="table-responsive">
        <table id="course_table">
          <thead>
            <tr style="background-color: #666" align="center" class="table-heads">
              <th>Course Name</th>
              <th style="width: 140px"># of Years</th>
              <th>Track</th>
              <th>Strand</th>
            </tr>
          </thead>
            <?php
              $sql = $view_details->course_details();
              if ($sql) {
                while($row = pg_fetch_assoc($sql)){
              ?>
                <tr id='course_<?php echo $row["coursecode"]; ?>'>
                    <td class="remarks"><?=ucwords($row['coursename'])?></td>
                    <td><?=$row['numofyear']?></td>
                    <td  class="remarks"><?=ucwords($row['track'])?></td>
                    <td class="remarks"><?=ucwords($row['strand'])?></td>
                 </tr>
                
              <?php
                }
              }else {
                echo "<td colspan=7 class='remarks'>No Data Found.</td>";
              }
            ?>
        </table>
      </div>
    </div>
  </div>
</div>
