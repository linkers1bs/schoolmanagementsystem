    <div class="tab_add_data_table1">
      <div class="adds subject_form_add">
        <form name="subject_form_add" id="subject_form_add">
         <b>Add New Subject</b>
          <?php
              require 'forms/formsubject.php';
            ?>
          <div class="buttonsave">
            <button  type="button" class='button_local' id="add" value="add"  onclick="subject_controller(this.id)">Save</button>  
          </div>
        </form>
      </div>
    </div>

    <div class="tab_modify_data_table1">
        <div class="adds subject_form_modify">
          <form name="subject_form_modify" id="subject_form_modify">
            <b>Required Option</b>
                <div class="size-16">
                   <div class="we-40">
                       <span>Select Subject Code:</span>
                   </div>
                  <select class="wid-fix2"  name="subjectcode" id="subjectcode" onChange="subjectCode();">
                       <?php
                        $sql = $view_details->subject_details();
                        if ($sql) {
                          ?>
                          <option value="none">Select group..</option>
                          <?php
                          while ($r = pg_fetch_assoc($sql)){
                              echo '<option value="'.$r['subjectcode'].'">'.$r['subjectcode'].' - '.$r['description'].'</option>';
                          }
                        } else {
                          echo '<option>No record found..</option>';
                        }
                      ?>
                  </select>
              </div>

              <div class="requroption">
                <span>Subject Information</span>
              </div>
              <div id="show_subject_modify">
                
              </div>
              <div class="buttonsave">
                <button  type="button" class='button_local' value="update" id='update' onclick="subject_controller(this.id)"> Update </button>
            </div>
          </form>
        </div>
    </div>

    <div class="tab_delete_data_table1">
        <div class="adds subject_form_delete">
          <form name="subject_form_delete" id="subject_form_delete">
              <b>Required Option</b>
                <div class="size-16">
                   <div class="we-40">
                       <span>Select Subject Code:</span>
                   </div>
                  <select class="wid-fix2"  name="subjectcode" id="subjectcode" onChange="subject_delete();">
                       <?php
                        $sql = $view_details->subject_details();
                        if ($sql) {
                          ?>
                          <option value="none">Select group..</option>
                          <?php
                          while ($r = pg_fetch_assoc($sql)){
                              echo '<option value="'.$r['subjectcode'].'">'.$r['subjectcode'].' - '.$r['description'].'</option>';
                          }
                        } else {
                          echo '<option>No record found..</option>';
                        }
                      ?>
                  </select>
              </div>

              <div class="requroption">
                <span>Subject Information</span>
              </div>
              <div id="show_subject_delete">
                
              </div>
                        
              <div class="buttonsave">
                <button  type="button" class='button_local'  value="delete" id='delete' onclick="subject_controller(this.id)"> Delete </button>
              </div>
          </form>
        </div>
    </div>
        
    <div class="table_info_show1">
      <div class="tablecon">
          <div class="requroption">
            <span><b>List of Subjects</b></span>
          </div>
          <div class="spacetable">
       <div class="table-responsive">
          <table id="subject_table">
          <thead>
            <tr style="background-color: #666" align="center" class="table-heads">
              <th>Code</th>
              <th>Description</th>
              <th>Unit</th>
              <th>Lec</th>
              <th>Lab</th>   
              <th>Price</th>
              <th class="remarks">Remarks</th>
            </tr>
          </thead>
           <?php
            $sql = $view_details->subject_details();
            if ($sql) {
              while($row = pg_fetch_assoc($sql)){
            ?>
              <tr id='subject_row<?php echo $row["subjectcode"]; ?>'>
                  <td class="remarks"><?=$row['subjectcode']?></td>
                  <td class="remarks"><?=$row['description']?></td>
                  <td><?=$row['numofunit']?></td>
                  <td><?=$row['lec']?></td>
                  <td><?=$row['lab']?></td>
                  <td><?=$row['price']?></td>
                  <td class="remarks"><?=$row['remarks']?></td>
               </tr>
              
            <?php
              }
            }else {
              echo "<td colspan=7 class='remarks'>No Data Found.</td>";
            }
          ?>
        </table>
      </div>
          </div>
      </div>
    </div>

  