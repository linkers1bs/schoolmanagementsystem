<div class="tab_add_data_table6">
  <div class="tablecon block_form_add">
     <form id="block_form_add" name="block_form_add">
      <div class="requroption block_form_add">
        <span><b>Add New Schedule's</b></span>
      </div>
      <?php
        require 'forms/form_block.php';
        ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" value="add"  onclick="block_scedule_management(this.id)">Save</button>  
      </div>
      </form>
  </div>
</div>


<!-- MODIFY BLOCKKKK -->
<div class="tab_modify_data_table6">
    <div class="tablecon block_form_modify">
      <form name="block_form_modify" id="block_form_modify">
        <div class="enroll">
        <b class="line_r">Required Option</b>
        </div>
        <div class="select-head">
          <div class="hrsb">
            <div>
               <div class="container_select">
                  <span>Branch:&nbsp;&nbsp;</span>
                  <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails('branch');
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                      }
                    ?>
                  </select>
                </div>
                <div class="container_select">
                    <span>School Year:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                      <?php
                        $query = $view_option_details->optionDetails("schoolyear");
                        while ($r = pg_fetch_assoc($query)){
                          echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                        }
                      ?>
                    </select>
                </div>
                 <div class="container_select">
                    <span>Semester:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("semester");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                      }
                    ?>
                    </select>
                  </div>
                  <div class="container_select">
                    <span>Type:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("studenttype");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['studenttypeid'].'" >'.$r['name'].'</option>';
                      }
                    ?>
                    </select>
                  </div>
                   <div class="container_select">
                     <span><button  type="button" id="modify" class='button_local' onclick="getBlockDetail(this.id)">SELECT</button></span>
                   </div>
                    <div class="container_select">
                     <div id="block_name_id_modify">
                       <!-- DISPLAY BLOCK CATEGORY -->
                     </div>
                   </div>
                   <div class="container_select">
                     <div id="subject_name_id_modify">
                       <!-- DISPLAY BLOCK CATEGORY -->

                     </div>
                   </div>
            </div>
          </div>
        </div>
       </form>
      <bR>
      <form name="block_form_modify_data" id="block_form_modify_data">
        
      <div id="show_data_block_modify" style="display:block;width:100%;">
                <!-- DISPLAY INFO BLOCK -->
      </div> 
      </form>

     <!--  <div class="buttonsave">
        <button  type="button" class='button_local' value="update" id='update' onclick="roomModify()"> Update </button>
      </div> -->
      
    </div>
</div>
<!-- END MODIFY BLOCKKKK -->



<!-- DELETE BLOCKKKK -->
<div class="tab_delete_data_table6">
    <div class="tablecon block_form_delete">
      <form name="block_form_delete" id="block_form_delete">
        <div class="enroll">
        <b class="line_r">Required Option</b>
        </div>
        <div class="select-head">
          <div class="hrsb">
            <div>
               <div class="container_select">
                  <span>Branch:&nbsp;&nbsp;</span>
                  <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails('branch');
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                      }
                    ?>
                  </select>
                </div>
                <div class="container_select">
                    <span>School Year:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                      <?php
                        $query = $view_option_details->optionDetails("schoolyear");
                        while ($r = pg_fetch_assoc($query)){
                          echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                        }
                      ?>
                    </select>
                </div>
                 <div class="container_select">
                    <span>Semester:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("semester");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                      }
                    ?>
                    </select>
                  </div>
                  <div class="container_select">
                    <span>Type:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("studenttype");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['studenttypeid'].'" >'.$r['name'].'</option>';
                      }
                    ?>
                    </select>
                  </div>
                   <div class="container_select">
                     <span><button  type="button" id="delete" class='button_local' onclick="getBlockDetail(this.id)">SELECT</button></span>
                   </div>
                    <div class="container_select">
                     <div id="block_name_id_delete">
                       <!-- DISPLAY BLOCK CATEGORY -->
                     </div>
                   </div>
                   <div class="container_select">
                     <div id="subject_name_id_delete">
                       <!-- DISPLAY BLOCK CATEGORY -->

                     </div>
                   </div>
            </div>
          </div>
        </div>
       </form>
      <bR>
      <form name="block_form_delete_data" id="block_form_delete_data">
        
      <div id="show_data_block_delete" style="display:block;width:100%;">
                <!-- DISPLAY INFO BLOCK -->
      </div> 
      </form>

     <!--  <div class="buttonsave">
        <button  type="button" class='button_local' value="update" id='update' onclick="roomModify()"> Update </button>
      </div> -->
      
    </div>
</div>
<!-- END DELETE BLOCKKKK -->



<!--TABLE INFORMATION  -->
<div class="table_info_show6">
  <div class="tablecon block_form_show">
    <form name="block_form_show" id="block_form_show">
      <div class="enroll">
      <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
             <div class="container_select">
                <span>Branch:&nbsp;&nbsp;</span>
                <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails('branch');
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                </select>
              </div>
              <div class="container_select">
                  <span>School Year:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("schoolyear");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                      }
                    ?>
                  </select>
              </div>
               <div class="container_select">
                  <span>Semester:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("semester");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                <div class="container_select">
                  <span>Type:&nbsp;&nbsp;</span>
                  <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
                  <?php
                    $query = $view_option_details->optionDetails("studenttype");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['studenttypeid'].'" >'.$r['name'].'</option>';
                    }
                  ?>
                  </select>
                </div>
                 <div class="container_select">
                   <span><button  type="button" id="select" class='button_local' onclick="getBlockDetail(this.id)">SELECT</button></span>
                 </div>
                  <div class="container_select">
                   <div id="block_name_id">
                     <!-- DISPLAY BLOCK CATEGORY -->
                   </div>
                 </div>
          </div>
        </div>
      </div>
    </form>
    <bR>
    <div id="show_data_block" style="display:block;width:100%;">
              <!-- DISPLAY INFO BLOCK -->
    </div> 
  </div>
</div>


<script type="text/javascript">
  function getBlockDetail(type){

    if (type == "modify") {
      var form_name = "block_form_modify";
      var semid = document.forms[form_name]["semid"].value;
      var syid = document.forms[form_name]["syid"].value;
      var branchid = document.forms[form_name]["branchid"].value;
      var studenttypeid = document.forms[form_name]["studenttypeid"].value;
 
      if (semid != "none" && syid != "none" && branchid != "none" && studenttypeid != "none") {
         $('#show_data_block_modify').hide();
         $('#subject_name_id_modify').hide();
         $('#block_name_id_modify').show();
         $.ajax({
            type : 'POST',
            url  : "pages/management/forms/form_block.php",
            data : $('#'+form_name).serialize()+'&type=block_name_modify',
            beforeSend : function (){
              $('#action_loading').show();
            },
            success : function (data){
              $('#action_loading').hide();
              $("#block_name_id_modify").html(data);
            }

         });
      } else {
        $('#show_data_block_modify').hide();
        $('#block_name_id_modify').hide();
        $('#subject_name_id_modify').hide();
      }
    } else if (type == "delete") {
      var form_name = "block_form_delete";
      var semid = document.forms[form_name]["semid"].value;
      var syid = document.forms[form_name]["syid"].value;
      var branchid = document.forms[form_name]["branchid"].value;
      var studenttypeid = document.forms[form_name]["studenttypeid"].value;
 
      if (semid != "none" && syid != "none" && branchid != "none" && studenttypeid != "none") {
         $('#show_data_block_delete').hide();
         $('#subject_name_id_delete').hide();
         $('#block_name_id_delete').show();
         $.ajax({
            type : 'POST',
            url  : "pages/management/forms/form_block.php",
            data : $('#'+form_name).serialize()+'&type=block_name_delete',
            beforeSend : function (){
              $('#action_loading').show();
            },
            success : function (data){
              $('#action_loading').hide();
              $("#block_name_id_delete").html(data);
            }

         });
      } else {
        $('#show_data_block_delete').hide();
        $('#block_name_id_delete').hide();
        $('#subject_name_id_delete').hide();
      }

    }else {
      var semid = document.forms["block_form_show"]["semid"].value;
      var syid = document.forms["block_form_show"]["syid"].value;
      var branchid = document.forms["block_form_show"]["branchid"].value;
      var studenttypeid = document.forms["block_form_show"]["studenttypeid"].value;

      if (semid != "none" && syid != "none" && branchid != "none" && studenttypeid != "none") {
         $('#show_data_block').hide();
         $('#block_name_id').show();
         $.ajax({
            type : 'POST',
            url  : "pages/management/forms/form_block.php",
            data : $('#block_form_show').serialize()+'&type=block_name',
            beforeSend : function (){
              $('#action_loading').show();
            },
            success : function (data){
              $('#action_loading').hide();
              $("#block_name_id").html(data);
            }

         });
      } else {
        $('#show_data_block').hide();
        $('#block_name_id').hide();
      }
  }
}

  function getInfo_block(){
    // alert('dasfdsafasdfasd');
    var block = document.forms["block_form_show"]["blockname"].value;
    if(block != 'none'){
      $('#show_data_block').show();
       $.ajax({
            type : 'POST',
            url  : "pages/management/forms/form_block.php",
            data : $('#block_form_show').serialize()+'&type=block_table',
            beforeSend : function (){
              $('#action_loading').show();
            },
            success : function (data){
              $('#action_loading').hide();
              $("#show_data_block").html(data);
            }

         });
    }else {
      $('#show_data_block').hide();
    }


  }

  function block_scedule_management(type){
    var form_name = "";

    if (type == "add") {
        form_name = "block_form_add";
        console.log($('#'+form_name).serialize());
        var block_details = {
        block  : document.forms[form_name]["block"].value,
        coursecode: document.forms[form_name]["coursecode"].value,
        subjectcode : document.forms[form_name]["subjectcode"].value,
        studenttypeid : document.forms[form_name]["studenttypeid"].value,
        capacity : document.forms[form_name]["capacity"].value,
        sem : document.forms[form_name]["sem"].value,
        sy : document.forms[form_name]["sy"].value,
        instructor : document.forms[form_name]["instructor"].value
      };

      var validate =  [
        block_details.block,
        block_details.coursecode,
        block_details.subjectcode,
        block_details.studenttypeid,
        block_details.capacity,
        block_details.sem,
        block_details.sy,
        block_details.instructor
      ];
      var validatehas =  ["block","coursecode","subjectcode","studenttypeid","capacity","sem","sy","instructor"];
      validate_Null_value(validate,validatehas,form_name);
      if (!$('#restric').is(':checked')) {
         $("."+form_name).find('#restric').css({"border":"2px solid red","box-shadow":"0 0 3px red"});

      }else {

        if (validate_Null_value(validate,validatehas,form_name)) {

             $.ajax({
              type : 'POST',
              url  : "controllers/function/ManagementController.php",
              data : $('#'+form_name).serialize()+'&file=block&type='+type,
              beforeSend : function(){
                $('#action_loading').show();
              },
              success : function (data){
                $('#action_loading').hide();
                $('#alert-data').html(data);
                var contents = escape(data);
                var n = contents.search("alert-warning");
                if(n < 0){
                  setTimeout(function(){ 
                  $("#"+form_name).trigger("reset");
                  $("#"+form_name).find(".wid-fix").removeAttr("style");
                    }, 2000);
                } else {
                  $("."+form_name).find('.wid-fix').css({"border":"2px solid red","box-shadow":"0 0 3px red"});
                }

              }

            });
        }

      }

    } else if (type =="update") {
       form_name = "block_form_modify_data";
       console.log($('#'+form_name).serialize());
       $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=block&type='+type,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
          }

        });
    } else if (type == "delete") {
       form_name = "block_form_delete_data";
       console.log($('#'+form_name).serialize());
       $.ajax({
          type : 'POST',
          url  : "controllers/function/ManagementController.php",
          data : $('#'+form_name).serialize()+'&file=block&type='+type,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#alert-data').html(data);
            $('#block_name_id_delete').hide();
            $('#subject_name_id_delete').hide();
            $('#show_data_block_delete').hide();
          }

        });
    }
   

    
    // var curriculum = {
    //     curricode  : document.forms[form_name]["curricode"].value,
    //     description: document.forms[form_name]["description"].value,
    //     remarks    : document.forms[form_name]["remarks"].value,
    //     coursecode : document.forms[form_name]["coursecode"].value,
    // };

    // var validate =  [
    //   curriculum.curricode,
    //   curriculum.description,
    //   curriculum.coursecode
    // ];
    // var validatehas =  ["curricode","description","coursecode"];
    // validate_Null_value(validate,validatehas,form_name);
  }
  function get_block_modify_subject(){
    // alert($('#block_form_modify').serialize());
    $.ajax({
      type : 'POST',
      url  : "pages/management/forms/form_block.php",
      data : $('#block_form_modify').serialize()+'&type=blockname_subject_code',
      beforeSend : function (){
        $('#action_loading').show();
      },
      success : function (data){
        $('#action_loading').hide();
        $('#subject_name_id_modify').show();
        $('#show_data_block_modify').hide();
        $("#subject_name_id_modify").html(data);
      }
   });
  }

  function get_block_delete_subject(){
     $.ajax({
      type : 'POST',
      url  : "pages/management/forms/form_block.php",
      data : $('#block_form_delete').serialize()+'&type=blockname_subject_code_delete',
      beforeSend : function (){
        $('#action_loading').show();
      },
      success : function (data){
        $('#action_loading').hide();
        $('#subject_name_id_delete').show();
        $('#show_data_block_delete').hide();
        $("#subject_name_id_delete").html(data);
      }
   });
  }

 function getInfo_block_delete(){
  $.ajax({
      type : 'POST',
      url  : "pages/management/forms/form_modify_delete.php",
      data : $('#block_form_delete').serialize()+'&action=delete',
      beforeSend : function (){
        $('#action_loading').show();
      },
      success : function (data){
        $('#action_loading').hide();
        $('#show_data_block_delete').show();
        $("#show_data_block_delete").html(data);
      }
   });
 }
  function getInfo_block_modify(){
     $.ajax({
      type : 'POST',
      url  : "pages/management/forms/form_modify_delete.php",
      data : $('#block_form_modify').serialize()+'&action=',
      beforeSend : function (){
        $('#action_loading').show();
      },
      success : function (data){
        $('#action_loading').hide();
        $('#show_data_block_modify').show();
        $("#show_data_block_modify").html(data);
      }
   });
  }
</script>
