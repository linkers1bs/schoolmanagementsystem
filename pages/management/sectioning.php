<div class="tab_add_data_table7">
  <div class="adds section_form_add">
    <form name="section_form_add" id="section_form_add">
     <b>Add New Sections and Adviser</b>
      <?php
          require 'forms/form_sectioning.php';
        ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" value="add"  onclick="sectioning_controller(this.id)">Save</button>  
      </div>
    </form>
  </div>
</div>


<div class="table_info_show7">
  <div class="tablecon">
      <div class="requroption">
        <span><b>Sections and Adviser Information</b></span>
      </div>
      <div class="spacetable">
   <div class="table-responsive">
      <table id="section_table">
      <thead>
        <tr style="background-color: #666" align="center" class="table-heads">
          <th>Section Name</th>
          <th>Adviser</th>
          <th>Task</th>
        </tr>
      </thead>
       <?php
        $sql = $view_details->sectioning_details();
        if ($sql) {
          while($row = pg_fetch_assoc($sql)){
        ?>
          <tr id='subject_row<?php echo $row["classsectionid"]; ?>'>
              <td class="remarks"><?=$row['name']?></td>
              <td class="remarks"><?=$row['fullname']?></td>
              <td class="remarks"><?=$row['task']?></td>
           </tr>
          
        <?php
          }
        }else {
          echo "<td colspan=3 class='remarks'>No Data Found.</td>";
        }
      ?>
    </table>
  </div>
      </div>
  </div>
</div>