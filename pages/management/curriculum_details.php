<div class="table_info_show8">
  <div class="tablecon curriculum_details">
    <form name="curriculum_details" id="curriculum_details">
      <div class="enroll">
      <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
             <div class="container_select">
                  <select class="wid-fix2"  name="curcode_modify" id="curcode_modify" onChange="curriculum_modify();">
                    <?php
                      $sql = $view_details->curriculum_details();
                      if ($sql) {
                        ?>
                        <option value="none">Select group..</option>
                        <?php
                        while ($r = pg_fetch_assoc($sql)){
                            echo '<option value="'.$r['curcode'].'">'.$r['description'].'</option>';
                        }
                      } else {
                        echo '<option>No record found..</option>';
                      }
                    ?>
                </select>
              </div>
                 <div class="container_select">
                   <span><button  type="button" id="select" class='button_local' onclick="curriculum_details_show(this.id)">SELECT</button></span>
                 </div>
          </div>
        </div>
      </div>
    </form>
    <bR>
    <div id="show_details_currilum" style="display:block;width:100%;">
              <!-- DISPLAY INFO curriculum_detais -->
    </div> 
  </div>
</div>


<script>
function curriculum_details_show() {
  var curcode = document.forms["curriculum_details"]["curcode_modify"].value;
    if (curcode != 'none') {    
        $('#show_details_currilum').show();  
         $.ajax({
          type : 'POST',
          url  : 'pages/management/forms/form_curriculum_details.php',
          data : 'curriculumcode='+curcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function (data){
            $('#action_loading').hide();
            $('#show_details_currilum').html(data);
          }
         });

    } else {
        $('#show_details_currilum').hide();
    }
}
</script>