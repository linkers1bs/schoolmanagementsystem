
<div class="tab_add_data_table2">
  <div class="adds room_form_add">
     <form name="room_form_add" id="room_form_add">
     <b>Add New Room</b>
        <?php
         require 'forms/form_room.php';
        ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" value="add"  onclick="room_manage(this.id)">Save</button>  
      </div>
      </form>
  </div>
</div>

<div class="tab_modify_data_table2">
    <div class="adds room_form_update">
      <form name="room_form_update" id="room_form_update">
      <b>Required Option</b>
           <div class="size-16">
             <div class="we-40">
                 <span>Select Course:</span>
             </div>
              <select class="wid-fix2"  name="roomcode_select_modify" id="roomcode_select_modify" onChange="room_modify();">
                  <?php
                    $sql = $view_details->rooms_details();
                    if ($sql) {
                      ?>
                      <option value="none">Select group..</option>
                      <?php
                      while ($r = pg_fetch_assoc($sql)){
                          echo '<option value="'.$r['roomcode'].'">'.$r['roomcode'].'</option>';
                      }
                    } else {
                      echo '<option>No record found..</option>';
                    }
                  ?>
                </select>
            </div>

          <div class="requroption">
          <span>Course Information</span>
          </div>
          <div id="show_room_modiy">

          </div>
        <div class="buttonsave">
          <button  type="button" class='button_local' value="update" id='modify' onclick="room_manage(this.id)"> Update </button>
        </div>
       </form>
    </div>
</div>

<div class="tab_delete_data_table2">
    <div class="adds room_form_delete">
       <form name="room_form_delete" id="room_form_delete">
       <b>Required Option</b>
        <div class="size-16">
             <div class="we-40">
                 <span>Select Course:</span>
             </div>
              <select class="wid-fix2"  name="roomcode_select_delete" id="roomcode_select_delete" onChange="room_delete();">
                  <?php
                    $sql = $view_details->rooms_details();
                    if ($sql) {
                      ?>
                      <option value="none">Select group..</option>
                      <?php
                      while ($r = pg_fetch_assoc($sql)){
                          echo '<option value="'.$r['roomcode'].'">'.$r['roomcode'].'</option>';
                      }
                    } else {
                      echo '<option>No record found..</option>';
                    }
                  ?>
                </select>
            </div>

          <div class="requroption">
          <span>Course Information</span>
          </div>
          <div id="show_room_delete">

          </div>
        <div class="buttonsave">
          <button  type="button" class='button_local'  value="delete" id='delete' onclick="room_manage(this.id)"> Delete </button>
        </div>
      </form>
    </div>
</div>

<div class="table_info_show2">
  <div class="tablecon">
      <div class="requroption">
        <span><b>List of Rooms</b></span>
      </div>
      <div class="spacetable">
       <div class="table-responsive">
         <table id="room_table">
          <thead>
            <tr style="background-color: #666" align="center" class="table-heads">
              <th>Room Code</th>
              <th style="width: 100px;">Room #</th>
              <th>Floor</th>
              <th>Bldg</th>
              <th>Address</th>
              <th style="width: 200px;">Remarks</th>
            </tr>
          </thead>
            <?php
              $sql = $view_details->rooms_details();
              if ($sql) {
                while($row = pg_fetch_assoc($sql)){
              ?>
                <tr id='room_row<?php echo $row["roomcode"]; ?>'>
                    <td class="remarks"><?=$row['roomcode']?></td>
                    <td><?=$row['roomnum']?></td>
                    <td><?=$row['floornum']?></td>
                    <td  class="remarks"><?=$row['building']?></td>
                    <td class="remarks"><?=$row['address']?></td>
                    <td class="remarks"><?=$row['remarks']?></td>
                 </tr>
                
              <?php
                }
              }else {
                echo "<td colspan=7 class='remarks'>No Data Found.</td>";
              }
            ?>
        </table>
      </div>
      </div>
  </div>
</div>
