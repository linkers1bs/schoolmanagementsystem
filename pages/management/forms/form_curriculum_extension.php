<?php

if (isset($_POST['curcode'])) {  

    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }

      $curriculumcode = trim($_POST["curcode"]);
      require '../../../controllers/db_controller/theDBConn.php';
      require '../../../controllers/db_controller/ViewManagementDetails.php';
      $view_details = new ViewManagementDetails();
}

?>

 <div class="size-16">
    <div>
      <span>Subjects:</span>
    </div>
     <select class="wid-fix2"  name="cdetailedid" id="cdetailedid" >
         <?php
          $sql = $view_details->curriculum_details($curriculumcode,$_POST["sem"]);
          if ($sql) {
            ?>
            <?php
            while ($r = pg_fetch_assoc($sql)){
                echo '<option value="'.$r['cdetailedid'].'">'.$r['subjectcode'].' - '.$r['description'].'</option>';
            }
          } else {
            echo '<option>No record found..</option>';
          }
        ?>
    </select>
</div>