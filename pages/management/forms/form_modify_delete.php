<?php 
	if (session_status() == PHP_SESSION_NONE) {
        session_start();
     }
    require '../../../controllers/db_controller/theDBConn.php';
    require '../../../controllers/db_controller/ViewManagementDetails.php';
    require '../../../controllers/db_controller/ViewOptionDetails.php';
    $db = new DB;
    $view_details = new ViewManagementDetails();
    $view_option_details = new ViewOptionDetails();
    $sy = $_POST["syid"];
	$sem = $_POST["semid"];
	$studenttypeid = $_POST["studenttypeid"];
	$block = $_POST["blockname"];
	$subject = $_POST["subjectcode"];
	$sql = "SELECT DISTINCT * FROM schedules
	LEFT JOIN userinfo USING (username)
	LEFT JOIN classschedules USING(scheduleid)
	LEFT JOIN room USING (roomcode)
	WHERE schedules.scheduleid = $subject
	ORDER BY roomcode ASC";
?>
 <input type="hidden" name="scheduleid"  value="<?php  echo $db->getVal('scheduleid',$sql);  ?>" class="wid-fix" >
<div class="size-16">
	<div class="we-20">
		<span>Branch:</span>
	</div>
	<select class="wid-fix" name="branchid" id="branchid" >
	    <?php
	      $query = $view_option_details->optionDetails('branch');
	      while ($r = pg_fetch_assoc($query)){
	      	$select ="";
	      	if ($db->getVal('branchid', $sql) == $r["branchid"]) {
	      		$select = 'selected';
	      	}
	        echo '<option value="'.$r['branchid'].'" '. $select.'>'.$r['name'].'</option>';
	      }
	    ?>
	 </select>
</div>

	
<div class="size-16">
	<div class="we-20">
	   <span>Block:</span>
	</div>
	  <input type="text" name="block"  value="<?php if(isset($_POST["action"])){  echo $db->getVal('blockname',$sql); } ?>" class="wid-fix" id="block">
</div>

<div class="size-16">
   <div class="we-20">
       <span>Block for:</span>
   </div>
  <select class="wid-fix" name="coursecode" id="coursecode" style="display: inline-block;">
        <?php
        	$query = $view_option_details->optionDetails("course");
        	if($query){

        		while ($r = pg_fetch_assoc($query)){
    				$select ="";
			      	if ($db->getVal('coursecode', $sql) == $r["coursecode"]) {
			      		$select = 'selected';
			      	}
                	echo '<option value="'.$r['coursecode'].'"'.$select.'>'.$r['coursename'].'</option>';
        		}
        	}else {
        		echo "<option>No record found..</option>";
        	}
        ?>
    </select>
</div>

<div class="size-16">
	<div class="checkbox">
		<input type="checkbox" required name ="restric" id="restric" value="false" style="width: 20px;margin:1px;">
		<label style="position: relative;">Do not allow other student, not belong to this course, to enroll on this block</label>
	</div>
</div>

<div class="size-16">
	<div class="we-20">
	   <span>Subject:</span>
	</div>
	<select class="wid-fix" required name="subjectcode" id="subjectcode">
	    <?php
	   		$query = $view_option_details->optionDetails("subject");
        	if($query){
        		while ($r = pg_fetch_assoc($query)){
        			$select ="";
			      	if ($db->getVal('subjectcode', $sql) == $r["subjectcode"]) {
			      		$select = 'selected';
			      	}
                	echo '<option value="'.$r['subjectcode'].'" '.$select.'>'.$r['subjectcode'].' - '.$r['description'].'</option>';
        		}
        	}else {
        		echo "<option>No record found..</option>";
        	}
	    ?>
	</select>
</div>

<div class="size-16">
	<div class="we-20">
		<span>Student Type:</span>
	</div>
	<select class="wid-fix" required name="studenttypeid" id="studenttypeid" >
		<?php
	   		$query = $view_option_details->optionDetails("studenttype");
        	if($query){
        		while ($r = pg_fetch_assoc($query)){
                	$select ="";
			      	if ($db->getVal('studenttypeid', $sql) == $r["studenttypeid"]) {
			      		$select = 'selected';
			      	}
                    echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
        		}
        	}else {
        		echo "<option>No record found..</option>";
        	}
	    ?>
    </select>
</div>

<div class="size-16">
	<div class="we-20">
	   <span>Capacity:</span>
	</div>
    <input type="text" value="<?php if(isset($_POST["action"])){  echo $db->getVal('capacity',$sql); } ?>" name="capacity"  id="capacity"  required class="wid-fix">
</div>




<div class="size-16">
	<div class="we-20">
		<span>Semester:</span>
	</div>
	<select class="wid-fix" name="sem" id="sem" >
	     <?php
	   		$query = $view_option_details->optionDetails("semester");
        	if($query){
        		while ($r = pg_fetch_assoc($query)){
        			$select ="";
			      	if ($db->getVal('semid', $sql) == $r["semid"]) {
			      		$select = 'selected';
			      	}
                	echo '<option value="'.$r['semid'].'" '.$select.'>'.$r['sem'].'</option>';
        		}
        	}else {
        		echo "<option>No record found..</option>";
        	}
	    ?>
	</select>
</div>

<div class="size-16">
	<div class="we-20">
		<span>School Year:</span>
	</div>
	<select class="wid-fix"  name="sy" id="sy" >
      <?php
	   		$query = $view_option_details->optionDetails("schoolyear");
        	if($query){
        		while ($r = pg_fetch_assoc($query)){
        			$select ="";
			      	if ($db->getVal('syid', $sql) == $r["syid"]) {
			      		$select = 'selected';
			      	}
                	echo '<option value="'.$r['syid'].'" '.$select.'>'.$r['sy'].'</option>';
        		}
        	}else {
        		echo "<option>No record found..</option>";
        	}
	    ?>
	</select> 
</div>

<div class="size-16">
	<div class="we-20">
		<span>Instructor:</span>
	</div>
	<select class="wid-fix" required name="instructor" id="instructor" >
	<option value="TBA">TBA</option>
			<?php
		   		$query = $view_option_details->optionDetails("instructor");
	        	if($query){
	        		while ($r = pg_fetch_assoc($query)){
	        			$select ="";
				      	if ($db->getVal('username', $sql) == $r["username"]) {
				      		$select = 'selected';
				      	}
	                	echo '<option value="'.$r['username'].'" '.$select.'>'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
	        		}
	        	}else {
	        		echo "<option>No record found..</option>";
	        	}
		    ?>
	</select>
</div>
                      
<div class="requroption">
	<span><b>Rooming Assignment and Scheduling</b></span>
</div>
<br>
<div style="margin:0px auto;width: 70%" >
 <fieldset class="field-m">
     <legend class="legend-s">Note</legend>
     <p>Use the following letters as:</p>
     <p>M - Monday, 
     T - Tuesday,
     W - Wednesday,
     H - Thursday,
     F - Friday,
     S - Saturday,
     U - Sunday</p>
     <p>Write no space like TWS </p>
     
 </fieldset>
</div>
<table  id="block_schdule_add" >
    <tr>
      <th>Room</th>
      <th>Time Start</th>
      <th>Time End</th>
      <th>Day</th>
      <th >Date Start</th>
      <th>Date End</th>
    </tr>
    <tr id="add_schedule'+i+'">
    	<td>
	    	<select name="roomcode" id="roomcode" >
	    	<?php 
				$query = $view_option_details->optionDetails("room");
				if($query){
				    while ($r = pg_fetch_assoc($query)){
				    	$select ="";
				      	if ($db->getVal('roomcode', $sql) == $r["roomcode"]) {
				      		$select = 'selected';
				      	}
				   		echo '<option value="'.$r['roomcode'].'" '.$select.'>'.$r['roomcode'].'</option>';
					}
				}else {
					echo "<option>No record found..</option>";
				}
			?>
			</select>
		</td>
		<td>
			<input type="time" name="timestart" style="margin-bottom: 2px"  id="timestart" value="<?php echo $db->getVal('starttime',$sql); ?>">
		</td>
		<td>
			<input type="time" name="timeend"  style="margin-bottom: 2px" id="timeend" value="<?php echo $db->getVal('endtime',$sql); ?>">
		</td>
		<td>
			<input type="text" name="day" class="td-wid" style="margin-bottom: 2px" id="day" value="<?php echo $db->getVal('days',$sql); ?>">
		</td>
		<td>
			<input type="date" id="datestart" name="datestart" style="margin-bottom: 2px;width:165px!important"  value="<?php echo $db->getVal('datestart',$sql); ?>">
		</td>
		<td>
			<input type="date" id="dateend" name="dateend" style="margin-bottom: 2px;width:165px!important" value="<?php echo $db->getVal('dateend',$sql); ?>">
		</td>
	</tr>
</table>   
<center>
	<br>
	<?php 
	if (isset($_POST["action"]) && $_POST["action"] == "delete") {
		?>
		<button  type="button" class='button_local' id="delete" value="delete"  onclick="block_scedule_management(this.id)">DELETE</button>
		<?php
	}else {
		?>
		<button  type="button" class='button_local' id="update" value="update"  onclick="block_scedule_management(this.id)">UPDATE</button>
		<?php
	}
	?>
</center>


