<?php

if (isset($_POST['curriculumcode'])) {  

    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }

      $curriculumcode = trim($_POST["curriculumcode"]);
      require '../../../controllers/db_controller/theDBConn.php';
      require '../../../controllers/db_controller/ViewManagementDetails.php';
      $view_details = new ViewManagementDetails();
      $openqry =$view_details->curriculum_details($curriculumcode);
      $row = pg_fetch_assoc($openqry);

    }

?>
<div id='show-detail-data'>
    <div class="currihead">
      <div class="subcurri"><?=$row["description"]?></div><br>
      <div class="subcurri" style="margin-top: -29px">Curriculum Code: <?=$row["curcode"]?></div>
      <div class="subcurri2">Applied to Course: <?=$row["coursename"]?></div>
    </div>

    <div style="float: right; margin-top: 3px">
      <button class="buttonsize" data-toggle="modal" data-target="#addsubject">Add Subject</button>
      <button class="buttonsize"data-toggle="modal" data-target="#deletesubject">Delete Subject</button>
      <button class="buttonsize">Pre-requisite</button>
      <button class="buttonsize">Co-requisite</button>
      <button class="buttonsize">Modify Sub Type</button>
    </div>
    <br><br>

    <?php
    $totallec = 0;
    $totallab = 0;
    for ($i=1; $i <= 4; $i++) { 
      $semtotallec = 0;
      $semtotallab = 0;
      ?>
      <span class="subcurri2">Semester <?=$i?></span>

      <table style="margin-top: 5px">
        <tr>
          <th rowspan="2">Subject Code </th>
          <th rowspan="2">Description</th>
          <th colspan="2">Unit</th>
          <th rowspan="2">Pre-requisite</th>
          <th rowspan="2">Co-requisite </th>
          <th rowspan="2">Subject Type</th>
        </tr>
        <tr>
          <th>Lec</th>
          <th>Lab</th>
        </tr>
        <?php
          $details =$view_details->curriculum_details($curriculumcode,$i);
          if ($details) {
           
            while ($data = pg_fetch_assoc($details)) {
              $semtotallec += $data["lec"];
              $semtotallab += $data["lab"];
             ?>
              <tr>
                <td class="remarks"><?=$data["subjectcode"]?></td>
                <td class="remarks"><?=$data["description"]?></td>
                <td><?=$data["lec"]?></td>
                <td><?=$data["lab"]?></td>
                <td><?=$data["prerequisite"]?></td>
                <td><?=$data["corerequisite"]?></td>
                <td  class="remarks"><?=$data["subjecttype"]?></td>
              </tr>
            <?php
            }
            
          }else {
            echo "<tr><td class='remarks' colspan=7>No record found...</td></tr>";
          }
        ?>

    
        <tr>
          <td colspan="2" class="subcurri2 remarks">Total</td>
          <td><?=$semtotallec?></td>
          <td><?=$semtotallab?></td>
          <td colspan="3"></td>
        </tr>
      </table>
       <br>
      <?php
      $totallec += $semtotallec;
      $totallab += $semtotallab;
    }

    ?>

    

    <div style="width: 18%;  font-size: 13px">
      <span class="subcurri2">Total Unit: <?=($totallab + $totallec)?></span>
      <span class="subcurri2">Lec: <?=$totallec?></span>
      <span class="subcurri2">Lab: <?=$totallab?></span>
    </div>

</div>
    
     <!--ADD SUBJECT MODAL  -->
<div class="modal fade" id="addsubject"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="_modal_notif_container" style="margin: 0px auto;margin-top: 20%;">
      <div class="modal-content">
      <div class="title">
         <h6>Add Subject</h6>
      </div>
      <div class="_modal_content curr_details_addsubject">
         <div class="subcurri">Curriculum Code: <?=$row["curcode"]?></div>
        <form name="curr_details_addsubject" id="curr_details_addsubject">
            <input type="hidden" name="curcode" value="<?=$row["curcode"]?>">
          <div class="size-16">
              <div>
                <span>Semester:</span>
              </div>
              <select name="sem" id="sem" class="wid-fix2" >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
          </div>
           <div class="size-16">
              <div>
                <span>Subjects:</span>
              </div>
               <select class="wid-fix2"  name="subjectcode" id="subjectcode" >
                   <?php
                    $sql = $view_details->subject_details();
                    if ($sql) {
                      ?>
                      <option value="none">Select group..</option>
                      <?php
                      while ($r = pg_fetch_assoc($sql)){
                          echo '<option value="'.$r['subjectcode'].'">'.$r['subjectcode'].' - '.$r['description'].'</option>';
                      }
                    } else {
                      echo '<option>No record found..</option>';
                    }
                  ?>
              </select>
          </div>
             <div class="size-16">
              <div>
                <span>Subjec Type:</span>
              </div>
               <select class="wid-fix2"  name="subjecttype" id="subjecttype">
                   <option value="">none</option>
                   <option value="CORE">CORE</option>
                   <option value="APPLIED">APPLIED</option>
                   <option value="SPECIALIZED">SPECIALIZED</option>
              </select>
          </div>
        </form>
      </div>
      <div class="_modal_footer">
        <button type="button" data-dismiss="modal"  id="Cancel">Cancel</button>&nbsp;&nbsp;&nbsp;
        <button type="button" data-dismiss="modal"  id="add" onclick="curr_details_controller(this.id)" >OK</button>
      </div>
      </div>
    </div>
  </div>
</div>
<!--END ADD SUBJECT MODAL  -->

<!-- DELETE SUBJECT DETAILS -->
<div class="modal fade" id="deletesubject"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="_modal_notif_container" style="margin: 0px auto;margin-top: 20%;">
      <div class="modal-content">
      <div class="title">
         <h6>Delete Subject</h6>
      </div>
      <div class="_modal_content curr_details_deletesubject">
         <div class="subcurri">Curriculum Code: <?=$row["curcode"]?></div>
        <form name="curr_details_deletesubject" id="curr_details_deletesubject">
            <input type="hidden" name="curcode" value="<?=$row["curcode"]?>">
          <div class="size-16">
              <div>
                <span>Semester:</span>
              </div>
              <select name="sem" id="sem" class="wid-fix2" onChange="get_subject_details()">
                <option value="none">Select subject...</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
              </select>
          </div>
           <div id="display_subject_details">
              <!-- DISPLAY SELECTION OPTION -->
           </div>
        </form>
      </div>
      <div class="_modal_footer">
        <button type="button" data-dismiss="modal"  id="Cancel">Cancel</button>&nbsp;&nbsp;&nbsp;
        <button type="button" data-dismiss="modal"  id="delete" onclick="curr_details_controller(this.id)" >OK</button>
      </div>
      </div>
    </div>
  </div>
</div>
<!-- END OF DELETE SUBJECT DETAILS -->

<script type="text/javascript">
  function curr_details_controller(type){
    var form_name = "";
    if (type == "add") {
      form_name = "curr_details_addsubject";
    } else if (type == "delete") {
      form_name = "curr_details_deletesubject";
    }
    // alert($('#'+form_name).serialize());
      $.ajax({
        type : 'POST',
        url  : "controllers/function/ManagementController.php",
        data : $('#'+form_name).serialize()+'&file=curriculum_details&type='+type,
        beforeSend : function (){
          $('#action_loading').show();
        },
        success : function(data){
          $('#action_loading').hide();
          $('#alert-data').html(data);
        }
       });
     
  }

  function get_subject_details(){
    $.ajax({
      type : 'POST',
      url  : "pages/management/forms/form_curriculum_extension.php",
      data : $('#curr_details_deletesubject').serialize(),
      success : function(data){
        $('#display_subject_details').html(data);
      }
     });
  }

</script> 