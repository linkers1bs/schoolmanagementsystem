<?php 
if(isset($_POST["type"])){
	if (session_status() == PHP_SESSION_NONE) {
        session_start();
     }
    require '../../../controllers/db_controller/theDBConn.php';
    require '../../../controllers/db_controller/ViewManagementDetails.php';
    $view_details = new ViewManagementDetails();
    $db = new DB;
	$type = $_POST["type"];
	$sy = $_POST["syid"];
	$sem = $_POST["semid"];
	$studenttypeid = $_POST["studenttypeid"];
	$branchid = $_POST["branchid"];

	if ($type == "block_name") {


		?>
			<span>&nbsp;Block:&nbsp;&nbsp;&nbsp;</span>
			<select  class="wid-fix" name="blockname" style="width: 150px!important"  style="display: inline-block;" onChange="getInfo_block();" id="blockname">
			<?php
				$query =$view_details->get_block($sem,$sy,$studenttypeid,$branchid);
				if($query) {
					echo '<option value="none">Select..</option>';
					while ($r = pg_fetch_assoc($query)){
						echo '<option value="'.$r['blockname'].'">'.$r['blockname'].'</option>';
					}
				} else {
					echo '<option>No record found..</option>';
				}

			?>
			</select>
		<?php

	} elseif ($type == "block_table") {

		$block = $_POST["blockname"];
     ?>
      <div class="requroption">
          <span><b>List of Schedule for Block</b></span>
          <br> <br>
        </div>
      <table class="table table table-bordered">
        <thead>
          <tr style="background-color: #666" align="center" class="table-heads">
            <th>Room</th>
            <th>Block</th>
            <th >Subject</th>
            <th>Day</th>
            <th >Time</th>
            <th>Start</th>
            <th>Instructor</th>
            <th style="width: 120px">Enrolle</th>
          </tr>
        </thead>
        <?php
				$query =$view_details->get_block_info_table($block,$sem,$sy,$branchid,$studenttypeid);
				if($query) {
					 while($row = pg_fetch_assoc($query)){
					 	$datestart = $row["starttime"];
					 	$dateend = $row["endtime"];
				        ?>
				            <tr>
					            <td><?=$row['roomcode']?></td>
					            <td><?=$row['blockname']?></td>
					            <td><?=$row['subjectcode']?></td>
					            <td><?=$row['days']?></td>
					            <td><?=date('h:i A', strtotime($datestart)).' - '.date('h:i A', strtotime($dateend))?></td>
					            <td><?=$row['datestart']?></td>
					            <td>
					            	<?php
					            		if($row["name"] === "TBA"){
					            			echo "<input type='checkbox' name='tbche' id='tbche'/>".$row["name"];
					            		} else{
					            			echo ucfirst($row['lastname']).', '.ucfirst($row['firstname']).' '.ucfirst($row['middlename']);
					            		}
					            
					            	?>
					            		
					            </td>
					            <td><?='20 / '.$row['capacity']?></td>
				          </tr>
				        <?php
				     }
				}else {
					echo '<tr><td colspan=8>No record found..</td></tr>';
				}
			?>
      </table>

     <?php 

	} elseif ($type == "block_name_modify") {

		?>
			<span>&nbsp;Block:&nbsp;&nbsp;&nbsp;</span>
			<select  class="wid-fix" name="blockname" style="width: 150px!important"  style="display: inline-block;" onChange="get_block_modify_subject();" id="blockname">
			<?php
				$query =$view_details->get_block($sem,$sy,$studenttypeid,$branchid);
				if($query) {
					echo '<option value="none">Select..</option>';
					while ($r = pg_fetch_assoc($query)){
						echo '<option value="'.$r['blockname'].'">'.$r['blockname'].'</option>';
					}
				} else {
					echo '<option>No record found..</option>';
				}

			?>
			</select>
		<?php
	} elseif ($type == "block_name_delete") {

		?>
			<span>&nbsp;Block:&nbsp;&nbsp;&nbsp;</span>
			<select  class="wid-fix" name="blockname" style="width: 150px!important"  style="display: inline-block;" onChange="get_block_delete_subject();" id="blockname">
			<?php
				$query =$view_details->get_block($sem,$sy,$studenttypeid,$branchid);
				if($query) {
					echo '<option value="none">Select..</option>';
					while ($r = pg_fetch_assoc($query)){
						echo '<option value="'.$r['blockname'].'">'.$r['blockname'].'</option>';
					}
				} else {
					echo '<option>No record found..</option>';
				}

			?>
			</select>
		<?php
	} 

	elseif ($type == "blockname_subject_code") {
		$blockname = $_POST["blockname"]
		?>
			<span>Subject:&nbsp;&nbsp;&nbsp;</span>
			<select  class="wid-fix" name="subjectcode" style="width: 150px!important"  style="display: inline-block;" onChange="getInfo_block_modify();" id="subjectcode">
			<?php
				$query =$view_details->get_block_info_table($blockname,$sem,$sy,$branchid,$studenttypeid);
				if($query) {
					echo '<option value="none">Select..</option>';
					while ($r = pg_fetch_assoc($query)){
						echo '<option value="'.$r['scheduleid'].'">'.$r['subjectcode'].'</option>';
					}
				} else {
					echo '<option>No record found..</option>';
				}

			?>
			</select>
		<?php
	} elseif ($type == "blockname_subject_code_delete") {
		$blockname = $_POST["blockname"]
		?>
			<span>Subject:&nbsp;&nbsp;&nbsp;</span>
			<select  class="wid-fix" name="subjectcode" style="width: 150px!important"  style="display: inline-block;" onChange="getInfo_block_delete();" id="subjectcode">
			<?php
				$query =$view_details->get_block_info_table($blockname,$sem,$sy,$branchid,$studenttypeid);
				if($query) {
					echo '<option value="none">Select..</option>';
					while ($r = pg_fetch_assoc($query)){
						echo '<option value="'.$r['scheduleid'].'">'.$r['subjectcode'].'</option>';
					}
				} else {
					echo '<option>No record found..</option>';
				}

			?>
			</select>
		<?php
	}


}else {
	?>

	<div class="size-16">
		<div class="we-20">
			<span>Branch:</span>
		</div>
		<select class="wid-fix" name="branchid" id="branchid" >
		    <?php
		      $query = $view_option_details->optionDetails('branch');
		      while ($r = pg_fetch_assoc($query)){
		        echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
		      }
		    ?>
		 </select>
	</div>

		
	<div class="size-16">
		<div class="we-20">
		   <span>Block:</span>
		</div>
		  <input type="text" name="block"  value="<?php if(isset($_POST["action"])){  echo getVal('blockname',$sql); } ?>" class="wid-fix" id="block">
	</div>

	<div class="size-16">
	   <div class="we-20">
	       <span>Block for:</span>
	   </div>
      <select class="wid-fix" name="coursecode" id="coursecode" style="display: inline-block;">
	        <?php
	        	$query = $view_option_details->optionDetails("course");
	        	if($query){

	        		while ($r = pg_fetch_assoc($query)){
	                	echo '<option value="'.$r['coursecode'].'"'.$selsub.'>'.$r['coursename'].'</option>';
	        		}
	        	}else {
	        		echo "<option>No record found..</option>";
	        	}
	        ?>
        </select>
	</div>

	<div class="size-16">
		<div class="checkbox">
			<input type="checkbox" required name ="restric" id="restric" value="false" style="width: 20px;margin:1px;">
			<label style="position: relative;">Do not allow other student, not belong to this course, to enroll on this block</label>
		</div>
	</div>

	<div class="size-16">
		<div class="we-20">
		   <span>Subject:</span>
		</div>
		<select class="wid-fix" required name="subjectcode" id="subjectcode">
		    <?php
		   		$query = $view_option_details->optionDetails("subject");
	        	if($query){
	        		while ($r = pg_fetch_assoc($query)){
	                	 echo '<option value="'.$r['subjectcode'].'">'.$r['subjectcode'].' - '.$r['description'].'</option>';
	        		}
	        	}else {
	        		echo "<option>No record found..</option>";
	        	}
		    ?>
		</select>
	</div>

	<div class="size-16">
		<div class="we-20">
			<span>Student Type:</span>
		</div>
		<select class="wid-fix" required name="studenttypeid" id="studenttypeid" >
			<?php
		   		$query = $view_option_details->optionDetails("studenttype");
	        	if($query){
	        		while ($r = pg_fetch_assoc($query)){
	                	$select = '';
	                    if($r["studenttypeid"] == "2"){
	                        $select = 'selected';
	                    }
	                    echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
	        		}
	        	}else {
	        		echo "<option>No record found..</option>";
	        	}
		    ?>
	    </select>
	</div>

	<div class="size-16">
		<div class="we-20">
		   <span>Capacity:</span>
		</div>
	    <input type="text" value="<?php if(isset($_POST["action"])){  echo getVal('capacity',$sql); } ?>" name="capacity"  id="capacity"  required class="wid-fix">
	</div>


	

	<div class="size-16">
		<div class="we-20">
			<span>Semester:</span>
		</div>
		<select class="wid-fix" name="sem" id="sem" >
		     <?php
		   		$query = $view_option_details->optionDetails("semester");
	        	if($query){
	        		while ($r = pg_fetch_assoc($query)){
	                	echo '<option value="'.$r['semid'].'">'.$r['sem'].'</option>';
	        		}
	        	}else {
	        		echo "<option>No record found..</option>";
	        	}
		    ?>
		</select>
	</div>

	<div class="size-16">
		<div class="we-20">
			<span>School Year:</span>
		</div>
		<select class="wid-fix"  name="sy" id="sy" >
	      <?php
		   		$query = $view_option_details->optionDetails("schoolyear");
	        	if($query){
	        		while ($r = pg_fetch_assoc($query)){
	                	echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
	        		}
	        	}else {
	        		echo "<option>No record found..</option>";
	        	}
		    ?>
		</select> 
	</div>

	<div class="size-16">
		<div class="we-20">
			<span>Instructor:</span>
		</div>
		<select class="wid-fix" required name="instructor" id="instructor" >
		<option value="TBA">TBA</option>
				<?php
			   		$query = $view_option_details->optionDetails("instructor");
		        	if($query){
		        		while ($r = pg_fetch_assoc($query)){
		                	echo '<option value="'.$r['username'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
		        		}
		        	}else {
		        		echo "<option>No record found..</option>";
		        	}
			    ?>
		</select>
	</div>
                          
	<div class="requroption">
		<span><b>Rooming Assignment and Scheduling</b></span>
	</div>
	<br>
	<div style="margin:0px auto;width: 70%" >
     <fieldset class="field-m">
         <legend class="legend-s">Note</legend>
         <p>Use the following letters as:</p>
         <p>M - Monday, 
         T - Tuesday,
         W - Wednesday,
         H - Thursday,
         F - Friday,
         S - Saturday,
         U - Sunday</p>
         <p>Write no space like TWS </p>
         
     </fieldset>
    </div>
    <table  id="block_schdule_add" >
        <tr>
          <th></th>
          <th>Room</th>
          <th>Time Start</th>
          <th>Time End</th>
          <th>Day</th>
          <th >Date Start</th>
          <th>Date End</th>
        </tr>
    </table>   
      <button class="col-sm-2" type="button" name="add" id="add_schedule" onclick="add_schedule_row()" style="background-color: #2C373A;color:white;border:none"> Add Schedule</button> 

	<script type="text/javascript"> 
	function add_schedule_row(){
		var i=1;  
		$('#block_schdule_add').append('<tr id="add_schedule'+i+'"><td><button id="add_schedule'+i+'" type="button" onclick="delete_schedule_row(this.id)">X</button> </td><td><select name="roomcode[]" id="roomcode" ><?php 
			$query = $view_option_details->optionDetails("room");
		if($query){
		    while ($r = pg_fetch_assoc($query)){
		   		echo '<option value="'.$r['roomcode'].'">'.$r['roomcode'].'</option>';
			}
		}else {
			echo "<option>No record found..</option>";
		}
		?>
		</select></td><td><input type="time" name="timestart[]" style="margin-bottom: 2px"  id="timestart" ></td><td> <input type="time" name="timeend[]" style="margin-bottom: 2px" id="timeend" ></td><td><input type="text" name="day[]" class="td-wid" style="margin-bottom: 2px" id="day" placeholder="MTWHF"></td><td><input type="date" id="datestart" name="datestart[]" style="margin-bottom: 2px;width:165px!important" ></td><td><input type="date" id="dateend" name="dateend[]" style="margin-bottom: 2px;width:165px!important"></td></tr>');  
		}
	function delete_schedule_row(id){
		$('#'+id).remove();
	}
	</script>   
	<?php
}



?>