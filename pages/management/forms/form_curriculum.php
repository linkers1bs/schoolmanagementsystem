<?php 

if (isset($_GET['curriculumcode'])) {  
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
      }
      $curriculumcode = trim($_GET["curriculumcode"]);
      require '../../../controllers/db_controller/theDBConn.php';
      require '../../../controllers/db_controller/ViewManagementDetails.php';
      $view_details = new ViewManagementDetails();
      $db = new DB;
      $sql = "SELECT DISTINCT  * FROM curriculum  WHERE curcode = '$curriculumcode' ";
      $openqry = $db->openqry($sql);
      $row = pg_fetch_assoc($openqry);
    }
?>

<div class="size-16">
   <div class="we-40">
       <span>Curriculum code</span>
   </div>
     <input type="text" name="curricode" id="curricode" class="wid-fix2"  value="<?php if (isset($_GET['curriculumcode'])) {echo $row['curcode'];}?>">
  </div>
<div class="size-16">
   <div class="we-40">
       <span>Curriculum Description</span>
   </div>
      <input type="text" name="description" id="description"  class="wid-fix2"  value="<?php if (isset($_GET['curriculumcode'])) {echo $row['description'];}?>">
  </div>
<div class="size-16">
   <div class="we-40">
       <span>Remarks</span>
   </div>
       <input type="text" name="remarks" id="remarks"  class="wid-fix2"  value="<?php if (isset($_GET['curriculumcode'])) {echo $row['remarks'];}?>">
  </div>
  <div class="size-16">
   <div class="we-40">
       <span>Course</span>
   </div>
       <select name="coursecode" id="coursecode"  class="wid-fix2" >
          <?php
            $sql = $view_details->course_details();
            if ($sql) {
              ?>
              <option value="none">Select group..</option>
              <?php
              while ($r = pg_fetch_assoc($sql)){
                  $selsub = '';
                   if ($row['coursecode'] == $r['coursecode'])
                   {
                    $selsub = 'selected="selected"';
                   } 
                  echo '<option value="'.$r['coursecode'].'"'.$selsub.'>'.$r['coursename'].'</option>';
              }
            } else {
              echo '<option>No record found..</option>';
            }
          ?>
        </select>
    </div>

                    

