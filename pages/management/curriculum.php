<!-- ADD CONTAINER -->
<div class="tab_add_data_table5">
  <div class="adds curriculum_form_add">
     <form name="curriculum_form_add" id="curriculum_form_add">
     <b>Add New Curriculum</b>
      <?php
          require 'forms/form_curriculum.php';
      ?>
      <div class="buttonsave">
        <button  type="button" class='button_local' id="add" value="add"  onclick="curriculum_manage(this.id)">Save</button>  
      </div>
      </form>
  </div>
</div>
<!-- END OF ADD CONTAINER -->


<!-- MODIFY CONTAINER -->
<div class="tab_modify_data_table5">
    <div class="adds curriculum_form_update">
      <form name="curriculum_form_update" id="curriculum_form_update">
      <b>Required Option</b>
        <div class="size-16">
           <div class="we-40">
               <span>Select Curriculum:</span>
           </div>
            <select class="wid-fix2"  name="curcode_modify" id="curcode_modify" onChange="curriculum_modify();">
                <?php
                  $sql = $view_details->curriculum_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                        echo '<option value="'.$r['curcode'].'">'.$r['description'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
            </select>
          </div>
        <div class="requroption">
          <span>Curriculum Information</span>
        </div>
        <div id="show_curriculum_modify">
          <!-- DISPLAY CURRICULUM DETAIL TO MODIFY -->
        </div>
      <div class="buttonsave">
        <button  type="button" class='button_local' value="update" id='update' onclick="curriculum_manage(this.id)"> Update </button>
      </div>
      </form>
    </div>
</div>
<!-- END OF DELETE MODIFY -->


<!-- DETAILS CONTAINER -->
<div class="tab_details_data_table5">
  <div class="tablecon curriculum_details">
    <form name="curriculum_details" id="curriculum_details">
      <div class="enroll">
      <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
             <div class="container_select">
                  <select class="wid-fix2"  name="curcode_modify" id="curcode_modify" onChange="curriculum_modify();">
                    <?php
                      $sql = $view_details->curriculum_details();
                      if ($sql) {
                        ?>
                        <option value="none">Select group..</option>
                        <?php
                        while ($r = pg_fetch_assoc($sql)){
                            echo '<option value="'.$r['curcode'].'">'.$r['description'].'</option>';
                        }
                      } else {
                        echo '<option>No record found..</option>';
                      }
                    ?>
                </select>
              </div>
                 <div class="container_select">
                   <span><button  type="button" id="select" class='button_local' onclick="curriculum_details_show(this.id)">SELECT</button></span>
                 </div>
          </div>
        </div>
      </div>
    </form>
    <bR>
    <div id="show_details_currilum" style="display:block;width:100%;">
              <!-- DISPLAY INFO curriculum_detais -->
    </div> 
  </div>
</div>
<!-- END OF DETAILS CONTAINER -->


<!--DELETE CONTAINER  -->
<div class="tab_delete_data_table5">
    <div class="adds curriculum_form_delete">
      <form name="curriculum_form_delete" id="curriculum_form_delete">
       <b>Required Option</b>
          <div class="size-16">
           <div class="we-40">
               <span>Select Curriculum:</span>
           </div>
            <select class="wid-fix2"  name="curcode_delte" id="curcode_delte" onChange="curriculum_delete();">
                <?php
                  $sql = $view_details->curriculum_details();
                  if ($sql) {
                    ?>
                    <option value="none">Select group..</option>
                    <?php
                    while ($r = pg_fetch_assoc($sql)){
                        echo '<option value="'.$r['curcode'].'">'.$r['description'].'</option>';
                    }
                  } else {
                    echo '<option>No record found..</option>';
                  }
                ?>
            </select>
          </div>
            <div class="requroption">
              <span>Curriculum Information</span>
            </div>
            <div id="show_curriculum_delete">
              <!-- DISPLAY CURRICULUM DETAIL TO DELETE -->
            </div>
      <div class="buttonsave">
        <button  type="button" class='button_local'  value="delete" id='delete' onclick="curriculum_manage(this.id)"> Delete </button>
      </div>
      </form>
    </div>
</div>
<!-- END OF THE DELETE CONTAINER -->

<!-- DETAILS CONTIANER TABLE -->
  <div class="table_info_show5">
    <div class="tablecon">
      <div class="requroption">
        <span><b>List of curriculum</b></span>
      </div>
      <div class="spacetable">
        <div class="table-responsive">
          <table id="curriculum_table">
            <thead>
              <tr style="background-color: #666" align="center" class="table-heads">
                <th>Curriculum</th>
                <th>Course Applied</th>
                <th>Description</th>
                <th>Last Uploaded</th>
                <th class="remarks">Remarks</th>
              </tr>
            </thead>
             <?php
              $sql = $view_details->curriculum_details();
              if ($sql) {
                while($row = pg_fetch_assoc($sql)){
              ?>
                <tr id='curriculum_<?php echo $row["curcode"]; ?>'>
                    <td class="remarks"><?=$row['curcode']?></td>
                    <td class="remarks"><?=$row['coursename']?></td>
                    <td class="remarks"><?=$row['description']?></td>
                    <td><?=$row['datecreated']?></td>
                    <td><?=$row['remarks']?></td>
                 </tr>
                
              <?php
                }
              }else {
                echo "<td colspan=5 class='remarks'>No Data Found.</td>";
              }
            ?>
          </table>
        </div>
      </div>
    </div>
  </div>
<!-- END OF THE CONTAINER DETAILS TABLE -->

<script>
function curriculum_details_show() {
  var curcode = document.forms["curriculum_details"]["curcode_modify"].value;
    if (curcode != 'none') {    
        $('#show_details_currilum').show();  
         $.ajax({
          type : 'POST',
          url  : 'pages/management/forms/form_curriculum_details.php',
          data : 'curriculumcode='+curcode,
          beforeSend : function(){
            $('#action_loading').show();
          },
          success: function (data){
            $('#action_loading').hide();
            $('#show_details_currilum').html(data);
          }
         });

    } else {
        $('#show_details_currilum').hide();
    }
}
</script>
