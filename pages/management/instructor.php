<div class="table_info_show3">
  <div class="tablecon">
      <div class="requroption">
        <span><b>List of Instructor's</b></span>
      </div>
      <div class="spacetable">
        <div class="table-responsive">
          <table class="subject_table">
            <thead>
              <tr style="background-color: #666" align="center" class="table-heads">
                <th style="width: 300px">Last Name</th>
                <th style="width: 300px">First Name</th>
                <th style="width: 300px">Middle Name</th>
                <th style="width: 300px">Age</th>
                <th style="width: 300px">Sex</th>
                <th style="width: 300px">Department</th>
              </tr>
            </thead>
            <?php
              $sql = $view_details->instructor_details();
              if ($sql) {
                while($row = pg_fetch_assoc($sql)){
              ?>
                <tr id='instructor<?php echo ucwords($row["username"]); ?>'>
                    <td class="remarks"><?=ucfirst($row['lastname'])?></td>
                    <td class="remarks"><?=ucfirst($row['firstname'])?></td>
                    <td class="remarks"><?=ucfirst($row['middlename'][0])?>.</td>
                    <td class="remarks"><?=ucfirst($row['age'])?></td>
                    <td class="remarks"><?=ucfirst($row['gender'])?></td>
                    <td class="remarks"><?=ucfirst($row['department'])?></td>
                 </tr>
                
              <?php
                }
              }else {
                echo "<td colspan=7 class='remarks'>No Data Found.</td>";
              }
            ?>
          </table>
        </div>
      </div>
  </div>
</div>


