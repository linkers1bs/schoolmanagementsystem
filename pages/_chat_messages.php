<div class="_messages_container">
	
	<div class="_user_list_column">
		<div class="_title_header">
			<h6>Chats</h6>
		</div>
		
		<div class="_chat_search">
			<input type="search" name="">
			<i class="fa fa-search"></i>
		</div>
		<div class="_user_list_container">
			
			<div class="_user_list_scroll ">

				<?php for ($x = 0; $x <= 15; $x++): ?>
				<a class="_chat_box_user" >
					<div class="_user_profile">
						<img src="./assets/image/test.jpg">
						<i class="fa fa-circle"></i>
					</div>
					<div class="_username">
						<h6>Janet Fowler <?=$x?></h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
					<div class="_timeago">
					 	<time class="timeago" datetime="2020-05-13 12:00:00"></time>
					</div>
				</a>
				<?php endfor; ?>

			</div>

		</div>
	</div>


	<div class="_messages_content_column">
		<div class="_mcc_header">
			<img src="./assets/images/test.jpg">
			<h6>Janet Fowler</h6>
		</div>

		<div class="_cbs_message_content">

			<div class="_left_user_messages">
				<div class="_l_u_m_icon">
					<img src="./assets/images/test.jpg">
				</div>
				<div class="_l_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
					<time class="timeago" datetime="2020-05-12 12:00:00"></time>
				</div>
			</div>

			<div class="_left_user_messages">
				<div class="_l_u_m_icon">
					<img src="./assets/images/test.jpg">
				</div>
				<div class="_l_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
					<time class="timeago" datetime="2020-05-12 12:00:00"></time>
				</div>
			</div>

			<div class="_right_user_messages">
				<div class="_r_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
				</div>
			</div>

			<div class="_right_user_messages">
				<div class="_r_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
				</div>
			</div>

		</div>
		<div class="_input_message_area">
			<div class="_input_ma dropdown">
				<button class="fa fa-smile-o"></button>
				<textarea></textarea>
				<button class="_mic fa fa-microphone"></button>
				<button class="_mic fa fa-ellipsis-h " id="menu_icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
				<div class="dropdown-menu menu_icons" aria-labelledby="menu_icons">
					<div class="container_sub_option">
				    	<a><i class="fa fa-camera"></i> <h6>Take Photo</h6></a>
				    	<a><i class="fa fa-picture-o"></i> <h6>Add Image</h6></a>
				    	<a><i class="fa fa-envelope"></i> <h6>Send E-mail</h6></a>
				    	<a><i class="fa fa-video-camera"></i> <h6>Add Video</h6></a>
					</div>
			  	</div>
			</div>
		</div>
	</div>

	<div class="_user_options_column">
		<div class="_uoc_header">
			<button class="fa fa-video-camera"></button>
			<button class="fa fa-phone"></button>
			<button class="fa fa-user-plus"></button>
		</div>
		<div class="_user_profile_photo">
			<img src="./assets/images/test.jpg">
			<div class="_name">
				<h6>Janet Fowler</h6> <i class="fa fa-circle"></i>
			</div>
			<h5>Active Now</h5>
		</div>	
		<div class="_some_option">
			<button class="share_file">Shared Files</button>
			<button class="share_photo">Shared Photo</button>
			<button class="change_nickname">Change Nickanem</button>
		</div>
		<div class="_settings_area">
			<button class="privacy_support">Privacy and Support</button>
			<button class="_settings_">Settings</button>
		</div>
	</div>


</div>