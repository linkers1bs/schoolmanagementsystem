
<div class="_body_container">
	
	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/accountweb.png">
			<a href="?page=myaccount">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>MY ACCOUNT</h6>
					</div>
					<div class="_box_content">
							<!-- <a href="#" class="show_report_1 text_W"></a> -->
							<p>My Profile</p>
							<!-- <a href="#" class="show_report_2 text_W"></a> -->
							<p>Change Password</p>
					</div>
				</div>
			</a>
	</div>

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/tasklist.png">
		<a href="?page=tasklist">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>MY TASK LIST</h6>
				</div>
				<div class="_box_content">
					<!-- <a href="#" class="show_report_1 text_W"></a> -->
					<p>Soon..</p>
				</div>
			</div>
		</a> 
	</div>

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/student-type.png">
		<a href="?page=adviser">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>ADVISER</h6>
				</div>
				<div class="_box_content">
					<div class="scroll-bar-wrap">
						<div class="scroll-box">
							<!-- <a href="#" class="show_report_1 text_W"> -->
							<p>Advise Sem1 Student</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Advise Student</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_3 text_W"> -->
							<p>Print COR</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_4 text_W"> -->
							<p>Change Schedules</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_5 text_W"> -->
							<p>Assign Sections</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_6 text_W"> -->
							<p>SF1</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_7 text_W"> -->
							<p>Grades - Form 138</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_8 text_W"> -->
							<p>SF2</p>
							<!-- </a> -->
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>


	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/report.png">
		<a href="?page=report-menubar">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>REPORTS</h6>
				</div>
				<div class="_box_content">
					<!-- <a href="#" class="show_report_1 text_W"> -->
					<p>Enrollment Sysytem Report</p>
					<!-- </a> -->
					<!-- <a href="#" class="show_report_2 text_W"> -->
					<p>Classroom Schedule</p>
					<!-- </a> -->
				</div>
			</div>
		</a>
	</div>

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/registrar.png">
		<a href="?page=registrar">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>REGISTRAR</h6>
				</div>
				<div class="_box_content">
					<div class="scroll-bar-wrap">
						<div class="scroll-box">
							<!-- <a href="#" class="show_report_1 text_W"> -->
							<p>School Year</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Scholarship</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_3 text_W"> -->
							<p>Student Type</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_4 text_W"> -->
							<p>Submission of Grades</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_5 text_W"> -->
							<p>Set Grade Deadline</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_6 text_W"> -->
							<p>Class Roster</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_7 text_W"> -->
							<p>Student Information</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_8 text_W"> -->
							<p>Update Grades</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_9 text_W"> -->
							<p>Form 137</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_10 text_W"> -->
							<p>Form 138</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_11 text_W"> -->
							<p>SF1</p>
							<!-- </a> -->
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/testcenter.png">
		<a href="?page=tcenter-menubar">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>TEST CENTER</h6>
				</div>
				<div class="_box_content">
					<!-- <a href="#" class="show_report_1 text_W"> -->
					<p>Result Entry</p>
					<!-- </a> -->
					<!-- <a href="#" class="show_report_2 text_W"> -->
					<p>Result Summary</p>
					<!-- </a> -->
				</div>
			</div>
		</a>
	</div>
	
	

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/guidance.png">
		<a href="?page=guidance">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>GUIDANCE</h6>
				</div>
				<div class="_box_content">
					<!-- <a href="#" class="show_report_1 text_W"> -->
					<p>Guidance Info</p>
					<!-- </a> -->
				</div>
			</div>
		</a>
	</div>


	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/management.png">
		<a href="?page=management">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>MANAGEMENT</h6>
				</div>
				<div class="_box_content">
					<div class="scroll-bar-wrap">
						<div class="scroll-box">
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Subjects</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_3 text_W"> -->
							<p>Rooms</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_4 text_W"> -->
							<p>Instructor</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_5 text_W"> -->
							<p>Courses</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_6 text_W"> -->
							<p>Curriculum</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_7 text_W"> -->
							<p>Blocks & Schedules</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_8 text_W"> -->
							<p>Class Sectioning</p>
							<!-- </a> -->
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>
	

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/instructor.png">
		<a href="?page=instructor">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>INSTRUCTOR</h6>
				</div>
				<div class="_box_content">
					<!-- <a href="#" class="show_report_1 text_W"> -->
					<p>Class List</p>
					<!-- </a> -->
					<!-- <a href="#" class="show_report_2 text_W"> -->
					<p>Attendance</p>
					<!-- </a> -->
					<!-- <a href="#" class="show_report_3 text_W"> -->
					<p>Grade Management</p>
					<!-- </a> -->
				</div>
			</div>
		</a>
	</div>

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/cashier.png">
		<a href="?page=cashier">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>CASHIER</h6>
				</div>
				<div class="_box_content">
					<div class="scroll-bar-wrap">
						<div class="scroll-box">
							<!-- <a href="#" class="show_report_1 text_W"> -->
							<p>Registration</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Student Account</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_3 text_W"> -->
							<p>Create Billing</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_4 text_W"> -->
							<p>Mescellaneous Fees</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_5 text_W"> -->
							<p>Registration Fees</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_6 text_W"> -->
							<p>Tuition Fees</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_7 text_W"> -->
							<p>Payment History</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_8 text_W"> -->
							<p>Account Receivables</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_9 text_W"> -->
							<p>Collection Reports</p>
							<!-- </a> -->
						</div>
					</div>
				</div>
			</div>
		</a>
	</div>

	<div class="_menu_box_container _menu_my_acc updatesIN">
		<img src="./assets/image/technical.png">
		<a href="?page=technical">
			<div class="_box_body_content">
				<div class="_box_title">
					<h6>TECHNICAL</h6>
				</div>
				<div class="_box_content">
					<!-- <a href="#" class="show_report_1 text_W"> -->
					<p>User's</p>
					<!-- </a> -->
					<!-- <a href="#" class="show_report_2 text_W"> -->
					<p>Branch Management</p>
					<!-- </a> -->
					<!-- <a href="#" class="show_report_3 text_W"> -->
					<p>Group Management</p>
					<!-- </a> -->
				</div>
			</div>
		</a>
	</div>

</div>
<div class="clear-fixed"></div>