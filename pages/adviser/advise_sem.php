 <div class="tablecon adviser_form_details">
  <form name="adviser_form_details" id="adviser_form_details">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 270px!important" name="branchid" id="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 140px!important" name="syid" id="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 140px!important"  name="semid" id="semid" style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 140px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select">
              <span>
                <button  type="button" id="register_student" class='button_local' onclick="adviser_student_details(this.id)">SELECT</button>
              </span>
          </div>
          <div class="container_select">
             <div id="adviser_regnum_student">
             	
			    <!-- DISPLAY STUDENT -->
             </div>
          </div>
        </div>
      </div>

    </div>
    <div id="printadvise">
    	<div id="adviser_advice_student_details">
          
    	</div>

    </div>
    

  </form>
</div>
<script>
  function adviseprint() {
    var divToPrint = document.getElementById('printadvise');
    var htmlToPrint = '' +
    '<style type="text/css">' +
      'table {' +
        'margin-top: 20px;' +
        'margin-bottom: 1rem;' +
        'width:100%' +
        'border-collapse:collapse!important;' +
      '}' +

      'tr td {' +
        'border:1px solid gray;' +
        'padding: 4px;' + 
        'font-size: 10px'+ 
      '}' +

      'th {' +
        'border:1px solid gray;' + 
        'font-size: 11px;'+ 
      '}' +

      '#displaynone_print2 {' +
        'display:none;'+
        '}'+

      '#displaynone_print3 {' +
        'display:none;'+
        '}'+

        '#displaynone_print5 {' +
        'display:none;'+
        '}'+

      '.table-bordered td {' +
        'border: 1px solid #dee2e6;'+
        'padding: 6px 17px;' +
        '}'+

        '.nb {' +
        'background-color: white!important;'+
        'width: 25%;' +
        'border-top: 1px solid white!important;'+
        'border-left: 1px solid white!important;'+
        '}'+

        '.remarks'+
        'text-align: left;'+
        '}'+

        '.headadvise {' +
        'font-size: 12px;'+
        '}'+
      '</style>';
    htmlToPrint += divToPrint.outerHTML;//table
    newWin = window.open("");
    newWin.document.write("<img src='./assets/image/logo.png'>");
    // newWin.document.write("<img src='./assets/image/logo.png'>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
    }
  </script>