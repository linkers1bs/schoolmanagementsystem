   <div class="tablecon form_138_details">
    <form name="form_138_details" id="form_138_details">
      <div class="enroll">
        <b class="line_r">Required Option</b>
      </div>
      <div class="select-head">
        <div class="hrsb">
          <div>
            <div class="container_select">
              <span>Branch:&nbsp;&nbsp;</span>
              <select style="width: 270px!important" name="branchid" id="branchid" style="display: inline-block;">
                <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                  }
                ?>
              </select>
            </div>
            <div class="container_select">
               <span>School Year:&nbsp;&nbsp;</span>
                <select style="width: 140px!important" name="syid" id="syid" style="display: inline-block;" >
                  <?php
                    $query = $view_option_details->optionDetails("schoolyear");
                    while ($r = pg_fetch_assoc($query)){
                      echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                    }
                  ?>
                </select>
            </div>
           <div class="container_select">
            <span>Semester:&nbsp;&nbsp;</span>
            <select style="width: 120px!important"  name="semid" id="semid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("semester");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
              }
            ?>
            </select>
           </div>
            <div class="container_select">
              <span>Type:&nbsp;&nbsp;</span>
              <select style="width: 120px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("studenttype");
                while ($r = pg_fetch_assoc($query)){
                   $select = '';
                  if($r["studenttypeid"] == "2"){
                    $select = 'selected';
                  }
                  echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
                }
              ?>
              </select>
           </div>
           <div class="container_select">
              <span>Grade lvl:&nbsp;&nbsp;</span>
              <select style="width: 80px!important"  name="gradelvl" id="gradelvl" style="display: inline-block;">
              	<option value="11">11</option>
              	<option value="12">12</option>
              </select>
           </div>
           <div class="container_select">
              <span>Course:&nbsp;&nbsp;</span>
              <select style="width: 320px!important"  name="coursecode" id="coursecode" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails("course");
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['coursecode'].'">'.$r['coursename'].'</option>';
                }
              ?>
              </select>
           </div>
            <div class="container_select">
                <span>
                  <button  type="button" id="select_name" class='button_local' onclick="form_138_controller(this.id)">SELECT</button>
                </span>
            </div>
            <div class="container_select">
               <div id="form138_regnum_student">             	
  			         <!-- DISPLAY STUDENT -->
               </div>
            </div>
          </div>
        </div>

      </div>

      <div id="form138_print">

      	<div id="adviser_form138_student_details">
            
      	</div>

      </div>

    </form>
  </div>

<script type="text/javascript">
function printFrom138() {
    var divToPrint = document.getElementById('form138_print');
    var htmlToPrint = '' +
        '<style type="text/css">' +
          'table, td{' +
              'border: 1px solid black;' +
              'border-collapse: collapse;' +
              'padding: 6px;' +
          '}' +

          'th {' +
              'background-color:gray;' +
          '}' +

          '.f138-txtsize {' +
              'font-size: 12px;' +
          '}' +

          '.f138-txtsize2 {' +
              'font-size: 12px;' +
          '}' +

          '.center {' +
              'text-align:center;' +
          '}' +

          '.center2 {' +
              'text-align:center;' +
              'font-size: 11px;' +
          '}' +

          '.border {' +
              'border:none!important' +
          '}' +

          '.align-left {' +
              'text-align:left' +
          '}' +

        '</style>';
    htmlToPrint += divToPrint.outerHTML;//table
    newWin = window.open("");
    // newWin.document.write("<img src='./assets/image/logo.png'>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
}

	function form_138_controller(type){
		var form = "form_138_details";

		if (type == "select_name") {
			var id = "form138_regnum_student";
			$.ajax({
				type : 'POST',
				url  : 'pages/adviser/forms/form_secondpage138.php',
				data : $('#'+form).serialize()+'&type=view_name',
				beforeSend : function (){
					$('#action_loading').show();
				},
				success : function (data){
					$('#action_loading').hide();
					$('#'+id).html(data);
				}
			});
		} else if (type == "regnum"){
       	var id = "adviser_form138_student_details";
        $.ajax({
          type : 'POST',
          url  : 'pages/adviser/forms/form_secondpage138.php',
          data : $('#'+form).serialize()+'&type=show_details_grades',
          beforeSend : function (){
            $('#action_loading').show();
          },
          success : function (data){
            $('#action_loading').hide();
            $('#'+id).html(data);
            $('#adviser_form138_student_details').html(data);
          }
        });
    }

	}



	  
</script>
