<div class="tablecon print_cor"> 
  <form name="print_cor" id="print_cor">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 270px!important" name="branchid" id="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 140px!important" name="syid" id="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 140px!important"  name="semid" id="semid" style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 140px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select" style="display: inline-block;">
            <span><button  type="button" id="select" class='button_local' onclick="show_name_student(this.id)">SELECT</button></span>
          </div>
          <div class="container_select" style="display: inline-block;">
            <div id="print_corname_student" style="display: inline-block;"> 
              <!-- DISPLAYT NAME OF REGISTER -->
            </div>
          </div>
        </div>
      </div>

    </div>
    <div id="prtinKquery">
      
        <div id="display_print_cor" >
                  <!-- DISPLAY PRINT COR -->
        </div>  

    </div>
    

  </form>
</div>

<script type="text/javascript">
  function printsss() {
    var divToPrint = document.getElementById('prtinKquery');
    var htmlToPrint = '' +
        '<style type="text/css">' +

          'table {' +
              // 'border:1px solid #000;' +
              'border-collapse:collapse;' +
          '}' +

          '.print-cent {' +
              'margin-left:-23px;' +
          '}' +

          '.text-b {' +
              'font-weight:bold;' +
          '}' +

          'print-table1 {' +
              'border:none!important' +
          '}' +

          '.table2 {' +
              'width:100%;' +
              'border:1px solid #000;' +
          '}' +

          '.table2 th{' +
              'border:1px solid #000;' +
          '}' +

          '.table2 td{' +
              'border:1px solid #000;' +
          '}' +

          'th {' +
              // 'border:1px solid #000;' +
              'border-collapse:collapse;' +
              'padding:5px;' +
              'background-color:#898282;' +
          '}' +

          'th p{' +
              'font-size:14px;' +
          '}' +

          '.print-txt{' +
              'font-size:13px;' +
          '}' +

          '.wid {' +
              'width:45%;' +
          '}' +

          '.wid1 {' +
              'width:16%;' +
          '}' +

          '.wid2 {' +
              'width:25%;' +
          '}' +

          '.wid3 {' +
              'width:20%;' +
          '}' +

          'tr {' +
              // 'border:1px solid #000;' +
              'border-collapse:collapse;' +
          '}' +

          'td {' +
              // 'border:1px solid #000;' +
              'border-collapse:collapse;' +
              'padding:5px;' +
              'text-align:center;' +
          '}' +

          '.bg-th-color {' +
              'background-color:gray;' +
          '}' +

        '</style>';
    htmlToPrint += divToPrint.outerHTML;//table
    newWin = window.open("");
    newWin.document.write("<img src='./assets/image/logo.png'>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
}
</script>