<div class="tablecon view_sectioning"> 
  <form name="view_sectioning" id="view_sectioning">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 220px!important" name="branchid" id="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
         <div class="container_select">
            <span>Section:&nbsp;&nbsp;</span>
            <select style="width: 140px!important"  name="sectionid" id="sectionid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("section");
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['classsectionid'].'">'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select" style="display: inline-block;">
            <span><button  type="button" id="select" class='button_local' onclick="view_setioning_details()">SELECT</button></span>
          </div>
        </div>
      </div>

    </div>
      
    <div id="display_section_details" >
              <!-- DISPLAY PRINT COR -->
    </div>  

  </form>
</div>

<script type="text/javascript">
  
    function view_setioning_details(){
      var form = "view_sectioning";
      $('#display_section_details').show();
        $.ajax({
          type : 'POST',
          url  : 'pages/adviser/forms/form_section.php',
          data : $('#'+form).serialize()+'&type=view_details',
          beforeSend:function (){
            $('#action_loading').show();
          },
          success:function(data){
            $('#action_loading').hide();
            $('#display_section_details').html(data);
        }
      });
    }

</script>
