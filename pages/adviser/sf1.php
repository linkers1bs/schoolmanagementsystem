 <style type="text/css">
    
 </style>
 <div class="tablecon sf1_form_view">
  <form name="sf1_form_view" id="sf1_form_view">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 100px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select">
              <span>
                <button  type="button" id="block_name" class='button_local' onclick="sf1_filter_controller(this.id)">SELECT</button>
              </span>
          </div>
          <div class="container_select">
             <div id="show-block-data-sf1">
                <!-- DISPLAY BLOCK HERE -->
             </div>
          </div>
        </div>
      </div>

    </div>
    <div id="printssf">
      <div id="show-accountsudent-info">
            <!-- DISPLAY SF1 HERE -->
      </div>
    </div>


  </form>
</div>




<script type="text/javascript">
  // function printsf1()
  // {
  //    var divToPrint=document.getElementById("table-responsive-x");
  //    newWin= window.open("");
  //    newWin.document.write(divToPrint.outerHTML);
  //    newWin.print();
  //    newWin.close();
  // }
  function printsf1() {

    var wrapper = document.getElementById('wrapper');
    var content = document.getElementById('content');
    var scrollPos;
    var divToPrint = document.getElementById('printssf');
    var htmlToPrint = '' +
        '<style type="text/css">' +

        'table {' +
          'margin-top: 20px;' + 
          'border-collapse:collapse;' +
          'padding: 0;' +
        '}' +

        '.title-print {'+
          'font-size:20px;'+
        '}'+

        '.sf1-subtitle span {'+
          'font-size:14px;'+
        '}'+

        '.table_print {'+
          'width:auto;'+
        '}'+

        '.table_first {'+
          'width:27%!important;'+
        '}'+

        'tr td {' +
          'border:1px solid black;' +
          'padding: 5px;' +  
        '}' +

        'th {' +
          'border:1px solid black;' +
          'background-color: gray;' + 
        '}' +

        '.lists {' +
          'font-size:10px;' +
          'margin-bottom: -20px;' +
          'margin-left: -35px;' +
          'width: 50%;' +
        '}' +

        '.sf1-text {' +
          'font-size:14px;' +
        '}' +

        '.sf1-textss {' +
          'font-size:12px;' +
        '}' +

        '.sf1-head {' +
          'padding: 2px;' +
          'width: 46%;' +
          'margin: 0 auto;' +
        '}' +

        '.sf1-head .sf1-title {' +
          'width: 99%;' +
          'margin: 0 auto;' +
          'text-align: center;' +
          'font-size: 7px;' +
        '}' +

        '.sf1-head .sf1-title .sf1-subtitle {' +
          'font-size: 7px;' +
        '}' +

        '.sf1-imglogo {' +
          'width: 14%;' +
          'float: left;' +
        '}' +

        '.sf1-headinput {' +
          'padding: 2px;' +
          'width: 83%;' +
          'margin-left: 157px;' +
          'margin-top: 41px;' +
        '}' +

        '.sf1-headinput .sf1-subheadinput {' +
          'font-size: 14px;' +
          'margin-bottom: 5px;' +
        '}' +

        '.sf1-headinput .sf1-subheadinput .sf1-2subheadinput {' +
          'font-size: 7px;' +
        '}' +

        '.textsf1 {' +
          'font-size: 11px;' +
          'border: 1px solid black;' +
        '}' +

        '.sf1-borders {' +
          'border-bottom: 1px solid black;' +
        '}' +

        '.border-right {' +
          'border-right: 1px solid black;' +
          // 'border-bottom: none;' +
        '}' +

        /*'.table-responsive-x {' +
          'overflow-x:auto;' +
        '}' +*/

        '.table-floats {' +
          'float: left;' +
          'margin-left: 300px;' +
          'margin-top: -151px;' +
          'width: 25%!important;' +
        '}' +

        '.table-floats1 {' +
          'float: right;' +
          'margin-right: 300px;' +
          'margin-top: -150px;' +
          'width: 13%;' +
        '}' +

        '.sub-signa {' +
          'float: right;' +
          'margin-right: 0px;' +
          'width : 270px;' +
          'margin-top: -150px;' +
          'font-size: 12px;' +
        '}' +

        '.sub-signa .marg1 {' +
          'margin-top: 20px;' +
        '}' +

        '.sub-signa .marg2 {' +
          'margin-top: -5px;' +
          'margin-left: 45px;' +
        '}' +

        '.sub-signa .marg3 {' +
          'margin-top: 24px;' +
          'width: 29%;' +
        '}' +

        '.sub-signa .marg4 {' +
          'float: right;' +
          'margin-top: -19px;' +
          'width: 29%;' +
          'margin-right: 31px;' +
        '}' +

        '.sub-signa .marg5 {' +
          'margin-top: -19px;' +
        '}' +

        '.sub-signa1 {' +
          'float: right;' +
          'margin-right: 0px;' +
          'margin-top: -40px;' +
          'font-size: 12px;' +
          'width : 270px;' +
        '}' +

        '.sub-signa1 .marg1 {' +
          'margin-top: 20px;' +
        '}' +

        '.sub-signa1 .marg2 {' +
          'margin-top: -5px;' +
          'margin-left: 45px;' +
        '}' +

        '.sub-signa1 .marg3 {' +
          'margin-top: 27px;' +
          'width: 29%;' +
        '}' +

        '.sub-signa1 .marg4 {' +
          'float: right;' +
          'margin-top: -19px;' +
          'width: 29%;' +
          'margin-right: 31px;' +
        '}' +

        '.sub-signa1 .marg5 {' +
          'margin-top: -19px;' +
        '}' +

        '@media print {'+
        '.table_print {'+
          'width: 50%;'+
          '}'+
        '}'+

        '</style>';
    htmlToPrint += divToPrint.outerHTML;//table
    newWin = window.open("");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
    }
  function sf1_filter_controller(type){
 
    if(type == "block_name"){
      var data_html = "show-block-data-sf1";
      $.ajax({
        type : 'POST',
        url  : 'pages/adviser/forms/forms_sf1.php',
        data : $('#sf1_form_view').serialize()+'&type=block_name',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    } else if (type == "sf1_form") {
      var data_html = "show-accountsudent-info";
      $.ajax({
        type : 'POST',
        url  : 'pages/adviser/forms/forms_sf1.php',
        data : $('#sf1_form_view').serialize()+'&type=sf1_form',
        beforeSend:function (){
          $('#action_loading').show();
        },
        success:function(data){
          $('#action_loading').hide();
          $('#'+data_html).html(data);
        }
      });
    }
  } 

</script>