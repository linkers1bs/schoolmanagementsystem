<?php
if (session_status() == PHP_SESSION_NONE) {
          
      session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  require '../../../controllers/db_controller/ViewAdviserDetails.php';
  
  $view_details_register = new ViewCashierDetails;
  $view_details = new ViewAdviserDetails;



  if (isset($_POST["type"])) {
		$type = $_POST["type"];
		$branchid =  trim($_POST["branchid"]);
		$syid =  trim($_POST["syid"]);
		$semid =  trim($_POST["semid"]);
    $studenttypeid = trim($_POST["studenttypeid"]);
    $sectionid = trim($_POST["sectionid"]);

  		if ($_POST["type"] == "view_details") {
  		 ?>
          <div class="spacetable">
             <div class="table-responsive">
                <table id="subject_table">
                <thead>
                  <tr style="background-color: #666" align="center" class="table-heads">
                    <th>#</th>
                    <th>LRN</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Remarks</th>
                  </tr>
                </thead>
                 <?php
                  $sql = $view_details->section_view_details($semid,$syid,$branchid,$studenttypeid,$sectionid);
                  if ($sql) {
                    $i = 1;
                    while($row = pg_fetch_assoc($sql)){
                  ?>
                    <tr>
                        <td><?=$i++?></td>
                        <td><?=$row['lrnid']?></td>
                        <td class="remarks"><?=ucwords($row['fullname'])?></td>
                        <td><?=$row['sex']?></td>
                        <td class="remarks"><?=$row['remarks']?></td>
                     </tr>
                    
                  <?php
                    }
                  }else {
                    echo "<td colspan=5 class='remarks'>No Data Found.</td>";
                  }
                ?>
              </table>
            </div>
          </div>
       <?php
  		} 
  }
?>