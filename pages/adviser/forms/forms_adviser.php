<?php 
	if (session_status() == PHP_SESSION_NONE) {
			        
	  session_start();

	}
	require '../../../controllers/db_controller/theDBConn.php';
	require '../../../controllers/db_controller/ViewCashierDetails.php';
	require '../../../controllers/db_controller/ViewAdviserDetails.php';

 	$view_details_register = new ViewCashierDetails;
	$view_details = new ViewAdviserDetails;

	$branchid =  trim($_POST["branchid"]);
	$syid =  trim($_POST["syid"]);
	$semid =  trim($_POST["semid"]);

	if (isset($_POST["type"])) {
		
		if ($_POST["type"] == "register_student") {
			?>
		        <span id="hidename">
		        Name:&nbsp;&nbsp;
		        <select class="wid-fix" style="width: 255px!important" name="regnum" id="student_view" onchange="adviser_student_details(this.id)" >
		        <?php
			         
			        $query = $view_details_register->get_perbilling($semid,$syid,$branchid,$id = 'all',$type_billing = "all");
			        if($query) {
			            echo "<option value='none'>Select Student..</option>";
			            while ($r = pg_fetch_assoc($query)){
			                                                    
			                echo '<option value="'.$r['regnum'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
			                                               
			            } 
			    
			        } else {
			                    
			                echo "<option>No record found..</option>";
			        }
			               
			         ?>
                </select>&nbsp;&nbsp;</span>Use ID:&nbsp;&nbsp;<input type="checkbox" name="useID" id="checkstud"/>
		        <span  id='studenthide' >Student ID:&nbsp;&nbsp;<input type="text" name="studentid" id="studentid"/>
		        <button type="button" id="buttstudent" class="button_local">View</button>
		        </span>&nbsp; <button  type="button" onclick="adviseprint()" class="button_local">Print</button>
			     <script type="text/javascript">
			     	  $('#studenthide').hide();
					  $('#hidename').show();
					        
					  var allButtonsOnPage = document.getElementById("checkstud");
					  var button = allButtonsOnPage;
					  button.addEventListener('click', function() {

					  if ($('#checkstud').is(':checked')) {
					    $('#studenthide').show();
					    $('#hidename').hide();
					    $('#show5-data').hide();
					  } else {
					    $('#studenthide').hide();
					    $('#hidename').show();
					  }
					  });
			     </script>
			<?php
		} elseif ($_POST["type"] == "student_details") {

			$regnum = $_POST["regnum"];
  			$sql = $view_details_register->get_info_student_account($regnum);
  			$curcode = $view_details->getVal('curcode',$sql);
  			$coursecode = $view_details->getVal('coursecode',$sql);
  			$course_tofalse = "false";
  			$studenttypeid =  $view_details->getVal('studenttypeid',$sql);
			?>
			<style type="text/css">
				  .create {
				  	width: 100%!important;
				  	/* padding:10px; */
				  }
			</style>
			<br>
			<input type="hidden" id="get_advise_studentid" value="<?php echo $view_details->getVal('studentid',$sql);?>">
			<span style="font-size: 12px"><b>Student Information</b></span>
			<table class="table table-bordered" style="width: 100%;">
	        <tr>
	          <td class="remarks nb">Student ID No.</td>
	          <td class="remarks "><?php echo $view_details->getVal('studentid',$sql);?></td>
	        </tr>
	        <tr>
	          <td class="remarks nb"><label class="txt2">Student Name</label></td>
	          <td class="remarks"><label class="txt2"><?php echo ucfirst($view_details->getVal('lastname',$sql)).", ".ucfirst($view_details->getVal('firstname',$sql))." ".ucfirst($view_details->getVal('middlename',$sql)); ?></label></td>
	        </tr>
	        <tr>
	          <td class="remarks nb">Courses</td>
	          <td class="remarks"><?php echo $view_details->getVal('coursename',$sql);?></td>

	        <tr>
	          <td class="remarks nb">Scholarship</td>
	          <td class="remarks"><?php echo $view_details->getVal('scholarshipname',$sql);?></td>
	        </tr>
	        <tr>
	          <td class="remarks nb">Score</td>
	          <td class="remarks"><?php //echo $view_details->getVal('scholarshipname',$sql);?></td>
	        </tr>
	      </table>
	      <div class="create" style="background-color: #F2F3F4;">
		<div align="center" class="headadvise">
		  <div><b><?php echo $view_details->getVal('description',$sql);?></b></div>
		  <div><b>Applied To Course: <?php echo $view_details->get_applied($curcode);?></b></div>
		</div>
		   <?php
		    $totallec = 0;
		    $totallab = 0;
		    $studentid = $view_details->getVal('studentid',$sql);
		    for ($i=1; $i <= 4; $i++) { 
		      $semtotallec = 0;
		      $semtotallab = 0;
		      ?>
		      <span class="subcurri2">Semester <?=$i?></span>

		      <table style="margin-top: 5px;width: 100%!important; margin-bottom: 18px;">
		        <tr>
			      <th rowspan="2">Grade</th>
			      <th rowspan="2" >Blocks</th>
			      <th rowspan="2" >Subject Codes</th>
			      <th rowspan="2" >Description</th>
			      <th colspan="2" >Unit</th>
			      <th rowspan="2" >Pre Requisite</th>
			      <th rowspan="2" >Co Requisite</th>
			      <th colspan="4" >Credit Subject</th>
			    </tr>
			    <tr>
			      <th>Lec</th>
			      <th>lab</th>
			      <th>Grade</th>
			      <th>Code</th>
			      <th>Desc</th>
			      <th>School</th>
			    </tr>
		        <?php
		          $details =$view_details->get_curriculum($curcode,$i);
		          if ($details) {
		           
		            while ($data = pg_fetch_assoc($details)) {
		              $semtotallec += $data["lec"];
		              $semtotallab += $data["lab"];

		              $subject_control = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$data["subjectcode"],$studentid);
					  if($subject_control){
					  	echo "<tr style='color:blue';>";
					  }else{
					  	echo "<tr>";
					  }
		             ?>

		                <!-- <tr> -->

					      <td class="tdinput">
					      	<?php 
					      	if($subject_control){
						  		while ($data_conrtroll = pg_fetch_assoc($subject_control)) {
					      			$average = ($data_conrtroll["first"] + $data_conrtroll["second"]);
					      			if ($average > 150) {
					      				echo number_format($average / 2,2);
					      			}
					      			$blockname = $data_conrtroll["blockname"];
					      		}
						  	}
					      	?>
					      </td><!-- GRADE TD  -->
					      <td>
					      	<?php 
					      	// $subject_control = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$data["subjectcode"],$studentid);
					      	if(!$subject_control){
					      		$offer = $view_details->get_block_offer($coursecode,$studenttypeid,$semid,$syid,$branchid,$data["subjectcode"]);
					      		 if ($offer) {
									   ?>
									   <div id="displaynone_print">
					      		 	<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#offer<?=$data["subjectcode"]?>"><i class="fa fa-eye"></i></button>
					      		 		 <div class="modal fade" id="offer<?=$data["subjectcode"]?>"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"data-backdrop="false">
										  <div class="modal-dialog" role="document" style="margin: 0px auto;margin-right: 42%;">
										    <div class="_modal_notif_container" style="margin-top: 10%;width: 800px">
										      <div class="modal-content">
										      <div class="title">
										      <h6>List of Schedule</h6>
										      </div>
										      <div class="_modal_content">
										            
										      <table style="width: 100%;">
										        <thead>
										          <tr style="background-color: #666" align="center" class="table-heads">
										          	<th></th>
										            <th>Room</th>
										            <th>Block</th>
										            <th >Subject</th>
										            <th>Day</th>
										            <th >Time</th>
										            <th>Start</th>
										            <th>Instructor</th>
										            <th style="width: 120px">Enrolle</th>
										          </tr>
										        </thead>
										        <?php
										        	$j = 0;
													 while($row = pg_fetch_assoc($offer)){
												        $j++;
												        ?>
												            <tr>
												            	<td><input type="checkbox" name="scheduleid" value="<?=$row["scheduleid"]?>"></td>
													            <td><?=$row['roomcode']?></td>
													            <td><?=$row['blockname']?></td>
													            <td><?=$row['subjectcode']?></td>
													            <td><?=$row['days']?></td>
													            <td><?=$row['starttime'].' - '.$row['endtime']?></td>
													            <td><?=$row['datestart']?></td>
													            <td>
													            	<?php
													            		if($row["username"] === "TBA"){
													            			echo strtoupper($row["username"]);
													            		} else{
													            			echo ucwords($row['fullname']);
													            		}
													            
													            	?>
													            		
													            </td>
													            <td><?='20/ '.$row['capacity']?></td>
												          </tr>
												        <?php
												     }
														
													?>
										      </table>

										      </div>
										      <div class="_modal_footer">
										        <button type="button" data-dismiss="modal" >Cancel</button>&nbsp;&nbsp;&nbsp;
										        <button type="button" data-dismiss="modal" onclick="show_block_offer()" >Save</button>
										      </div>
										      </div>
										    </div>
										  </div>
										</div>
									   </div>

					      		 	<?php
					      		 }
					      	}else {
					      		if($subject_control){
					      			echo $blockname;
					      		}
					      	}

					      	?>
					      </td>
					      <td class="remarks"><?=$data["subjectcode"];?></td>
					      <td class="remarks"><?=$data["description"]?></td>
					      <td><?=$data["lec"]?></td>
			              <td><?=$data["lab"]?></td>
			              <td><?=$data["prerequisite"]?></td>
			              <td><?=$data["corerequisite"]?></td>
					      <td></td>
					      <td></td>
					      <td></td>
					      <td></td>
					    </tr>

					   


		            <?php
		            }
		            
		          }else {
		            echo "<tr><td class='remarks' colspan=12>No record found...</td></tr>";
		          }
		        ?>

		    
		        <tr>
		          <td colspan="4" class="subcurri2" style="text-align:right">Total: </td>
		          <td><?=$semtotallec?></td>
		          <td><?=$semtotallab?></td>
		          <td colspan="6"></td>
		        </tr>
		      </table>
			  <div id="displaynone_print3">
				  <button type="button" class="button_local" data-toggle="modal" data-target="#viewadvise">View Advised</button>
				  <br><br>
		  	</div>
		      <?php
		      $totallec += $semtotallec;
		      $totallab += $semtotallab;
		    }

		    ?>
		    	<!--TOTAL OF THE LEC AND LAB   -->
			    <div style="width: 30%;  font-size: 13px">
			      <span class="subcurri2">Total Unit: <?=($totallab + $totallec)?></span>
			      <span class="subcurri2">Lec: <?=$totallec?></span>
			      <span class="subcurri2">Lab: <?=$totallab?></span>
			    </div>
			    <!-- END OF THE TOTAL LECT AND LAB  -->

			 <!--MODAL VIEW ADVISED  -->
			 <div id="displaynone_print5">
			<div class="modal fade" id="viewadvise"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
			    <div class="modal-dialog" role="document" style="margin: 0px auto;margin-right: 50%;">
			    <div class="_modal_notif_container" style="margin-top: 10%;width: 1000px">
			      <div class="modal-content">
			      <div class="title">
			      <h6>Student's Advised Subjects</h6>
			      </div>
			      <div class="_modal_content">
			            
			      <table  style="width: 100%;">
			        <thead>
			          <tr style="background-color: #666" align="center" class="table-heads">
			          	<th></th>
			            <th>Subject Code</th>
			            <th>Block</th>
			            <th>Descriptive Title</th>
			            <th>Lec</th>
			            <th >Lab</th>
			            <th>Classes Start</th>
			            <th>Classes End</th>
			            <th>Day</th>
			            <th>Time</th>
			            <th>Room</th>
			            <th>Controlled</th>
			          </tr>
			        </thead>
			      	<?php
			      		 $semtotallec_advise = 0;
      					 $semtotallab_advise = 0;
      					 $subjectcode = "false";
			      		$offer = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
			      		if ($offer) {
			      			while($row = pg_fetch_assoc($offer)){
			      				 $semtotallec_advise += $row["lec"];
             					 $semtotallab_advise += $row["lab"];
					        ?>
					            <tr>
					            	<td><input type="checkbox" checked disabled></td>
					            	<td><?=$row['subjectcode']?></td>
						            <td><?=$row['blockname']?></td>
						            <td><?=$row['description']?></td>
						            <td><?=$row['lec']?></td>
						            <td><?=$row['lab']?></td>
						            <td><?=$row['starttime']?></td>
						            <td><?=$row['endtime']?></td>
						            <td><?=$row['days']?></td>
						            <td><?=$row["datestart"]?></td>
						            <td><?=$row["roomcode"]?></td>
						            <td>Yes</td>
					          </tr>
					        <?php
					     	}
			      		}



			      		$offer = $view_details->get_block_offer($coursecode,$studenttypeid,$semid,$syid,$branchid);
			      		if ($offer) {

			      			while($row = pg_fetch_assoc($offer)){
					        ?>
					            <tr id='show_subje<?=$row["scheduleid"]?>' style="display: none;color:blue" class="advise_subject_new">
					            	<td><input type="checkbox" name="get_id_scdhe" id='scheduleid_advise<?=$row["scheduleid"]?>'  value="<?=$row["scheduleid"]?>"></td>
					            	<td><?=$row['subjectcode']?></td>
						            <td><?=$row['blockname']?></td>
						            <td><?=$row['description']?></td>
						            <td><?=$row['lec']?></td>
						            <td><?=$row['lab']?></td>
						            <td><?=$row['starttime']?></td>
						            <td><?=$row['endtime']?></td>
						            <td><?=$row['days']?></td>
						            <td><?=$row["datestart"]?></td>
						            <td><?=$row["roomcode"]?></td>
						            <td>Yes</td>
					          </tr>
					        <?php
					     	}
			      		}
			      		

			      	?>
			        <tr>
			          <td colspan="4" class="subcurri2 remarks">Total Unit: <?php echo $semtotallec_advise + $semtotallab_advise?></td>
			            <td><?=$semtotallec_advise?></td>
          				<td><?=$semtotallab_advise?></td>
			          <td colspan="6"></td>
			        </tr>
			      </table>

			      </div>
			      <div class="_modal_footer">
				    <div id="displaynone_print3" style="display: inline-block;">
			        <button type="button" data-dismiss="modal" >Close</button>&nbsp;&nbsp;&nbsp;
			    	</div>
			        <button type="button" data-dismiss="modal" onclick="controller_advise()" id="controller_save" style="display: none">Save</button>
			      </div>
			      </div>
			    </div>
			  </div>
			</div>	
			 </div>
			<!-- END OF MODAL VIEW ADVISE -->

		  	</div>

			<?php
		}

	}

?>