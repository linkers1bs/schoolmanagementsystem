<?php
if (session_status() == PHP_SESSION_NONE) {
          
      session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  require '../../../controllers/db_controller/ViewAdviserDetails.php';
  
  $view_details_register = new ViewCashierDetails;
  $view_details = new ViewAdviserDetails;



  if (isset($_POST["type"])) {
		$type = $_POST["type"];
		$branchid =  trim($_POST["branchid"]);
		$syid =  trim($_POST["syid"]);
		$semid =  trim($_POST["semid"]);

  		if ($_POST["type"] == "studentname") {
  			?>
  			<span id="print_corhidename">
            Name:&nbsp;&nbsp;
            <select class="wid-fix" style="width: 255px!important" name="regnum" id="student_view" onchange="change_shedule_name(this.id)" >
            <?php
               
              $query = $view_details_register->get_perbilling($semid,$syid,$branchid,$id = 'all',$type_billing = "all");
              if($query) {
                  echo "<option value='none'>Select Student..</option>";
                  while ($r = pg_fetch_assoc($query)){
                                                          
                      echo '<option value="'.$r['regnum'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
                                                     
                  } 
          
              } else {
                          
                      echo "<option>No record found..</option>";
              }
                     
               ?>
               </select>&nbsp;&nbsp;</span>Use ID:&nbsp;&nbsp;<input type="checkbox" name="useID" id="print_corcheckstud"/>
            <span  id='print_corstudenthide' >Student ID:&nbsp;&nbsp;<input type="text" name="studentid" id="studentid"/>
            <button type="button" id="buttstudent" class="button_local">View</button>
           <script type="text/javascript">
	            $('#print_corstudenthide').hide();
	            $('#print_corhidename').show();
	                  
	            var allButtonsOnPage = document.getElementById("print_corcheckstud");
	            var button = allButtonsOnPage;
	            button.addEventListener('click', function() {

	            if ($('#print_corcheckstud').is(':checked')) {
	              $('#print_corstudenthide').show();
	              $('#print_corhidename').hide();
	              $('#display_print_cor').hide();
	            } else {
	              $('#print_corstudenthide').hide();
	              $('#print_corhidename').show();
	            }
	            });
           </script>
  			<?php
  		} elseif ($_POST["type"] == "show_change_schedule") {
  			$regnum = $_POST['regnum']; //student id
		    $sql = $view_details_register->get_info_student_account($regnum);
		    $curcode = $view_details->getVal('curcode',$sql);
		    $coursecode = $view_details->getVal('coursecode',$sql);
		    $studenttypeid =  $view_details->getVal('studenttypeid',$sql);
		    $studentid = $view_details->getVal('studentid',$sql);
		    ?>
		    <br>
       		<table>
                <tr align="center" bgcolor="gray">
                  <th rowspan="2">Block</th>
                  <th rowspan="2">Subject Code</th>
                  <th rowspan="2">Descriptive Title</th>
                  <th rowspan="2">Instructor</th>
                  <th rowspan="2">Vacant Slot</th>
                  <th rowspan="2">Room</th>
                  <th rowspan="2">Date Start</th>
                  <th rowspan="2">Date End</th>
                  <th colspan="2">Unit</th>
                  <th rowspan="2">Day</th>
                  <th rowspan="2">Time</th>
                </tr>
                <tr bgcolor="gray">
                  <th>Lec</th>
                  <th>Lab</th>
                </tr>
                 <?php
                    $semtotallec_advise = 0;
                    $semtotallab_advise = 0;
                    $course_tofalse = "false";
                    $subjectcode = "false";
                    $offer = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
                    if ($offer) {
                        while($row = pg_fetch_assoc($offer)){
                           $semtotallec_advise += $row["lec"];
                             $semtotallab_advise += $row["lab"];
                        ?>
                            <tr>
                              <td><?=$row['blockname']?></td>
                              <td class="remarks"><?=$row['subjectcode']?></td>
                              <td class="remarks"><?=$row['description']?></td>
                              <td><?=$row['fullname']?></td>
                              <td><?=$row['capacity']?></td>
                              <td class="remarks"><?=$row["roomcode"]?></td>
                              <td><?=$row['datestart']?></td>
                              <td><?=$row['dateend']?></td>
                              <td><?=$row['lec']?></td>
                              <td><?=$row['lab']?></td>
                              <td><?=$row["days"]?></td>
                              <td><?=$row['starttime'].' - '.$row["endtime"]?></td>
                          </tr>

                          

                        <?php
                      }
                    }else {
                      ?>
                        <tr>
                          <td colspan="12">No record found...</td>
                        </tr>
                      <?php
                    }
                ?>
                <tr>
                  <td colspan="8" class="subcurri2 remarks">Credit</td>
                    <td><?=$semtotallec_advise?></td>
                    <td><?=$semtotallab_advise?></td>
                    <td></td>
                    <td></td> 
                </tr>
        	</table>
           <div style="text-align: right;padding-top: 10px">
              <button type="button" class="button_local" data-toggle="modal" data-target="#change_shedule_name_modify">Modify</button>
              <button type="button" class="button_local" data-toggle="modal" data-target="#change_shedule_name_delete">Delete</button>
           </div>
           <div class="modal fade" id="change_shedule_name_modify"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"data-backdrop="false">
            <div class="modal-dialog" role="document" style="margin: 0px auto;margin-right: 42%;">
              <div class="_modal_notif_container" style="margin-top: 10%;width: 800px">
                <div class="modal-content">
                <div class="title">
                <h6>Change Schedule</h6>
                </div>
                <div class="_modal_content">
                Subject Code: <select style="width: 270px!important" name="scheduleid_modify" id="scheduleid_modify" style="display: inline-block;" onchange="change_shedule_name_modify_view()">
                  <?php
                  $offer = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
                   if ($offer) {
                     echo '<option value="none" >Select subject..</option>';
                        while($r = pg_fetch_assoc($offer)){
                          echo '<option value="'.$r['gradeid'].'" >'.$r['subjectcode'].'</option>';
                        }
                    }else {
                        echo "<option>No record found...</option>";
                    }

                    ?>
                </select>
                <div id="show_change_schedule_modify">
                  
                </div>
                

                </div>
                <div class="_modal_footer">
                  <button type="button" data-dismiss="modal" >Cancel</button>&nbsp;&nbsp;&nbsp;
                  <button type="button" data-dismiss="modal" onclick="change_schedule_controller()" >Save</button>
                </div>
                </div>
              </div>
            </div>
          </div>

		    <?php
  		} elseif ($_POST["type"] == "show_change_schedule_modify") {
        $studenttypeid = $_POST["studenttypeid"];
        $scheduleid = $_POST["scheduleid_modify"];
        $subjectcode = "false" ;
        $details = $view_details->get_subject_change_schedule($studenttypeid,$semid,$syid,$branchid,$subjectcode, $scheduleid);
        $data = pg_fetch_assoc($details);
        $scheduleid = $data['scheduleid'];
        $subjectcode = $data["subjectcode"];
        $change_subject = $view_details->get_subject_change_schedule($studenttypeid,$semid,$syid,$branchid,$subjectcode);
        ?>
        <br>
        <table>
          <thead>
            <tr style="background-color: #666" align="center" class="table-heads">
              <th></th>
              <th>Block</th>
              <th>Day</th>
              <th>Time</th>
              <th>Date Start</th>
              <th>Date End</th>
              <th>Room</th>
              <th>Instructor</th>
              <th>Vacant Slot</th>
            </tr>
          </thead>
         
        
        <?php
        if ($details) { 
            while($row = pg_fetch_assoc($change_subject)){
              if ($scheduleid != $row["scheduleid"]) {
                ?> 
                <tr>
                    <td><input type="checkbox" name="checkbox_change_schedule" value="<?=$row['scheduleid']?>"></td>
                    <td class="remarks"><?=$row['blockname']?></td>
                    <td class="remarks"><?=$row['days']?></td>
                    <td><?=$row['starttime'].' - '.$row["endtime"]?></td>
                    <td><?=$row['datestart']?></td>
                    <td><?=$row['dateend']?></td>
                    <td class="remarks"><?=$row["roomcode"]?></td>
                    <td><?=$row['fullname']?></td>
                    <td><?=$row['capacity']?></td>
                </tr>
               <?php
              }
            }
            echo "</table>";
        }else {
            echo "<option>No record found...</option>";
        }

        # code...
      }
  	# code...
  }
?>