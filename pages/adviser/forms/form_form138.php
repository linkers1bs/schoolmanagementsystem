<?php


if (session_status() == PHP_SESSION_NONE) {
          
      session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewRegistrarDetails.php';
  $view_details = new ViewRegistrarDetails;

if (isset($_POST["type"])) {

  if ($_POST["type"] == "cardpage") {

?>
<span id="print_grades138hide">
  <span >Student Name:</span>&nbsp;&nbsp;
  <select class="wid-fix" name="studentid" style="display: inline-block;width: 255px!important" onchange="show_studname(this.id)" id="show_cardpage">
      <?php
          $query = $view_details->cardcoverpage_details($_POST["semid"], $_POST["syid"], $_POST["branchid"], $_POST["scholarshipid"]);
          if ($query) {
              echo "<option value='none'>Select Student..</option>";
              while ($r = pg_fetch_assoc($query)){
                echo '<option value="'.$r['studentid'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
      }
    }else {
      echo"<option>No record found....</option>";
    }
      ?>
  </select>
</span>
<button  type="button" onclick="printGrades138()" class="button_local">Print</button>

<script type="text/javascript">
  $('#print_grades138hide').show();
</script>


<?php
  } elseif ($_POST["type"] == "show_cardpage") {

      $studentid = $_POST["studentid"];

      $sql = "SELECT * FROM studentinfo 
              INNER JOIN register reg USING (studentid)
              INNER JOIN schoolyear USING (syid)
              LEFT JOIN studentcourse studentcur USING (studentid)
              LEFT JOIN curriculum USING (curcode)
              LEFT JOIN course USING (coursecode)
              WHERE studentid = '$studentid' ";

?>
<div class="covercard" id='covercard'>

<div class="contcover"> 
  <div class='reportss'>
    <table class="card" >
      <tr>
        <td colspan="11" class="coverreport">Report on Attendance</td>
      </tr>
      <tr>
        <td style="width: 5px;"></td>
        <td style="font-size: 9px;">JUN</td>
        <td style="font-size: 9px;" >JUL</td>
        <td style="font-size: 9px;" >AUG</td>
        <td style="font-size: 9px;" >SEP</td>
        <td style="font-size: 9px;" >OCT</td>
        <td style="font-size: 9px;" >NOV</td>
        <td style="font-size: 9px;" >DEC</td>
        <td style="font-size: 9px;" >JAN</td>
        <td style="font-size: 9px;" >FEB</td>
        <td style="font-size: 9px;" >MAR</td>
      </tr>
      <tr>
        <td>No of Week days</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>No of Days Present</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>No of Days Absent</td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>
  </div>

<div style="font-size: 10px;"> 
  <div style="margin-top: 70px; " >
    <table class='guardiansig'>
      <tr>
        <td colspan="2" class='noneb' style="text-align: center;"><b>PARENT/GUARDIAN SIGNATURES</b></td>
      </tr>
      <tr>
        <td class='noneb'>1ST QUARTER</td>
        <td class='noneb sssf' ></td>
      </tr>
      <tr>
        <td class='noneb'>2ST QUARTER</td>
        <td class='noneb sssf'></td>
      </tr>
      <tr>
        <td class='noneb'>3ST QUARTER</td>
        <td class='noneb sssf'></td>
      </tr>
      <tr>
        <td class='noneb'>4ST QUARTER </td>
        <td class='noneb sssf'></td>
      </tr>
    </table>
  </div>
</div>


</div>

<div class="whole"> <!-- whole -->

  <div align="left" style="float: left;">
    <img src="assets/image/logo.png" style="width: 103px; height: 34px;">
  </div>

  <div align="right" style="float: right; padding-right: 43px;">
    <img src="assets/image/logodeped.png" style="width: 61px; height: 61px;">
  </div>

  <div> <!-- Header -->
    <center>
      <div style="font-weight: 600;">

        <div>
          <span>Department of the Philippines</span>
        </div>
        <div>
          <span>Department of Education</span>
        </div>
        <div>
          <span>Region X</span>
        </div>
        <div style="margin-left: 103px;">
          <span>Division of Iligan CIty</span>
        </div>
        <div style="margin-left: 100px;">
          <span>Distinct I</span>
        </div>
        <div style="margin-top: 10px;">
          <span>Iligan Computer Institute</span>
        </div>
        <div>
          <span>School</span>
        </div>

      </div>
    </center>
  </div> <!-- header end -->

  <div style="padding-top: 15px; width: 63%;" align="left">
    <label>Name:</label>
      <b><u>
        <span>
        <?php echo ucfirst($view_details->getVal('lastname',$sql)).' '.ucfirst($view_details->getVal('firstname',$sql)).''.ucfirst($view_details->getVal('middlename',$sql));?>
        </span>
      </b></u>

    <label>Age:</label>
    <b><u>
      <span>
      <?php 

      function getAge($dob,$condate){

        $birthdate = new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $dob))))));
        $today= new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $condate))))));           
        $age = $birthdate->diff($today)->y;

        return $age;

        }

        if($view_details->getVal('dateofbirth',$sql) != '') {
            $dob= $view_details->getVal('dateofbirth',$sql); //date of Birth
            $condate=date('Y-m-d'); //Certain fix Date of Age 
            echo getAge($dob,$condate);
        }

      ?>
      </span>
    </u></b>

    <label>Sex:</label>
      <span><b><u> <?php echo $view_details->getVal('sex',$sql) ?> </b></u></span>
    <label>Grade:</label>
      <span><b><u> <?php echo $view_details->getVal('gradelevel',$sql);?> </b></u></span>
    <label>Section:</label>
      <span><b><u> <?php  echo $view_details->getVal('coursename',$sql);?> </b></u></span>
    <label>Track/Strand:</label>
      <span><b><u> <?php echo $view_details->getVal('track',$sql)."/". $view_details->getVal('strand',$sql);?> </b></u></span>
    <label>School Year:</label>
      <span><b><u> <?php echo $view_details->getVal('sy',$sql);?> </b></u></span>
    <label>LRN:</label>
      <span><b><u> <?php echo $view_details->getVal('lrnid',$sql);?> </b></u></span>

  </div>

  <div align="center" style="margin-left: -4px; margin-top: 10px;">

    <div style="font-weight: 600;">
      <span>Dear Parents</span>
    </div>

    <div style="margin-top: 15px;">
      <span>This report card shows the ability and progress your child has made in</span>
    </div>

    <div>
      <span>the different learning areas as well as his/her core values.</span>
    </div>

    <div style="margin-top: 15px;">
      <span>The school welcomes should you desire to know more about your</span>
    </div>

    <div>
      <span>Child's progress.</span>
    </div>

    <div style="padding-top: 17px;" align="center">
      <label class="label1">NABIL R. LAGUINDAB, LPT PRINCIPAL</label>
      <label class="label2">NABIL R. LAGUINDAB, LPT PRINCIPAL</label>
      <label class="label3">Name and Signature of Program Head</label>
      <label>Name and Signature of Program Head</label>
    </div>

    <center>

      <div class="certi">
        <div>Certificate of Transfer</div>
      </div>

      <div class="admitted">
        <label style="padding-right: 0px;">Admitted to Grade _________</label>
        <label style="padding-right: 0px; padding-left: 1px;">Section _________</label>
        <label style="padding-left: 1px;">Eligibility for Admission to Grade _______</label>
      </div>

      <div style="padding-top: 15px; font-weight: 600;">
        <div>APPROVED:</div>
      </div>

      <div style="padding-top: 17px;">
        <label class="label3" style="border-top: 1px solid black;">Name & Signature of Program Head</label>
        <label style="border-top: 1px solid black;">Name & Signature of Adviser</label>
      </div>

      <div style="padding-top: 15px; font-weight: 600;">
        <div>Cancellation of Transfer</div>
      </div>

      <div style="padding-top: 17px;">
        <label class="label3" style="border-top: 1px solid black;">Admitted :</label>
        <label style="border-top: 1px solid black;">Name & Signature of Adviser</label>
        <br>
        <label class="label3">Date : __________________</label>
      </div>

    </center>
  </div>
</div> <!-- end Whole -->



<?php
  }
}
?>