<?php 
	if (session_status() == PHP_SESSION_NONE) {
			        
	  session_start();

	}
	require '../../../controllers/db_controller/theDBConn.php';
	require '../../../controllers/db_controller/ViewAdviserDetails.php';
	$view_details = new ViewAdviserDetails;

	if (isset($_POST["type"])) {
		
		if ($_POST["type"] == "block_name") {
			?>
      <span>&nbsp;Block:&nbsp;&nbsp;&nbsp;</span>
			<select class="wid-fix" name="blockname" style="width: 150px!important"  style="display: inline-block;" onchange="sf1_filter_controller(this.id);" id="sf1_form">
  			<?php
  				$query = $view_details->advise_sf1r_get_block($_POST["semid"], $_POST["syid"], $_POST["studenttypeid"], $_POST["branchid"] );
  				if($query) {
  					echo '<option value="none">Select..</option>';
  					while ($r = pg_fetch_assoc($query)){
  						echo '<option value="'.$r['blockname'].'">'.$r['blockname'].'</option>';
  					}
  				} else {
  					echo '<option>No record found..</option>';
  				}

  			?>
			</select>
      <script type="text/javascript">
        $('#savemodfy').hide();
      </script>
      <span id='modifyhide'><button type="button" id="modify" class="button_local" onclick="sf1Modify()">Modify</button></span>
      <span id='showsave'><button type="button" id="savemodfy" class="button_local" onclick="saveModifysf1()">Save</button></span>
      <span><button  type="button" onclick="printsf1()" class="button_local">Print</button></span>
	<?php
		} elseif ($_POST["type"] == "sf1_form") {

      $semid = $_POST["semid"];
      $syid = $_POST["syid"];
      $branchid = $_POST["branchid"];
      $studenttypeid = $_POST["studenttypeid"];
      $blockname = $_POST["blockname"];

      $sql = "SELECT branch.name as branchname, CONCAT(lastname, ', ', firstname, ' ', middlename) as fullname, * FROM studentinfo 
      INNER JOIN register USING (studentid)
      INNER JOIN branch USING(branchid)
      INNER JOIN schoolyear USING (syid)
      WHERE studentid IN
      (SELECT studentid FROM schedules
      INNER JOIN studentgrades USING (scheduleid)
      WHERE blockname = '$blockname' AND schedules.semid = $semid AND schedules.syid = $syid AND schedules.branchid = $branchid
      ) AND register.semid = $semid AND register.syid = $syid AND register.branchid = $branchid ";
	?>
      <script type="text/javascript">
       $('.hidessssss').each(function(){
          $(this).hide();
       });

        function sf1Modify(){
          $('#savemodfy').show();
          $('#modify').hide();
          $('.hidessssss').each(function(){
            $(this).show();
          });
          $('.hide_details').each(function(){
            $(this).hide();
          })
        }

        function saveModifysf1(){
          $('#savemodfy').hide();
          $('#modify').show();
          var lrn = [];
          var studentid = [];
          var lastname = [];
          var firstname = [];
          var middlename = [];
          var sex = [];
          var dateofbirth = [];
          var placeofbirth = [];
          var mothertongue = [];
          var ethnic = [];
          var religion = [];
          var streets = [];
          var barangay = [];
          var city = [];
          var province = [];
          var fathername = [];
          var mothername = [];
          var guardian_name = [];
          var guardian_relationship = [];
          var mother_mobile = [];
          var remarks = [];
          $('.hidessssss').each(function(){
            $(this).hide();
              if (this.name == "lrnid") {
                lrn.push($(this).val());
              } else if (this.name == "lastname"){
                lastname.push($(this).val());
              } else if (this.name == "firstname"){
                firstname.push($(this).val());
              } else if (this.name == "middlename"){
                middlename.push($(this).val());
              } else if (this.name == "sex") {
                sex.push($(this).val());
              } else if (this.name == "studentid") {
                studentid.push($(this).val());
              } else if (this.name == "dateofbirth") {
                dateofbirth.push($(this).val());
              } else if (this.name == "placeofbirth") {
                placeofbirth.push($(this).val());
              } else if (this.name == "mothertongue") {
                mothertongue.push($(this).val());
              } else if (this.name == "ethnic") {
                ethnic.push($(this).val());
              } else if (this.name == "religion") {
                religion.push($(this).val());
              } else if (this.name == "streets") {
                streets.push($(this).val());
              } else if (this.name == "barangay") {
                barangay.push($(this).val());
              } else if (this.name == "city") {
                city.push($(this).val());
              } else if (this.name == "province") {
                province.push($(this).val());
              } else if (this.name == "fathername") {
                fathername.push($(this).val());
              } else if (this.name == "mothername") {
                mothername.push($(this).val());
              } else if (this.name == "guardian_name") {
                guardian_name.push($(this).val());
              } else if (this.name == "guardian_relationship") {
                guardian_relationship.push($(this).val());
              } else if (this.name == "mother_mobile") {
                mother_mobile.push($(this).val());
              } else if (this.name == "remarks") {
                remarks.push($(this).val());
              }
          });
          $('.hide_details').each(function(){
            $(this).show();
          })
          $.ajax({
            type : 'POST',
            url : 'controllers/function/AdviserController.php',
            data : 'lrn='+lrn+'&type=sf1_modify_details&studentid='+studentid+'&sex='+sex+'&firstname='+firstname+'&dateofbirth='+dateofbirth+'&placeofbirth='+placeofbirth+'&mothertongue='+mothertongue+'&ethnic='+ethnic+'&religion='+religion+'&streets='+streets+'&barangay='+barangay+'&city='+city+'&province='+province+'&fathername='+fathername+'&mothername='+mothername+'&guardian_name='+guardian_name+'&guardian_relationship='+guardian_relationship+'&mother_mobile='+mother_mobile+'&remarks='+remarks+'&lastname='+lastname+'&middlename='+middlename,
            beforeSend : function(){
              $('#action_loading').show();
            },
            success: function(data){
              $('#action_loading').hide();
              $('#testststststs').html(data);

            }
          });  
        }
      </script>

      <style type="text/css">
        @media print {
          
          .table-responsive-x {
            width: 100%!important;
            overflow: hidden!important;

          }
          .table_print {
            width: 100%!important;
          }
          .table_print th {
            font-size: 12px!important;
          }
        }
      </style>


        <div id="testststststs">

        </div>
        <div class="tablecon">
          <div class="spacetable">
            <div class="form-container">
              <div class="containerP sf1">
                <div class="table-responsive-x" id="table-responsive-x"> 
                  <div class="sf1-head">
                    <div class="sf1-title">
                      <span class="title-print"><b>School Form 1 (SF 1 ) School Register</b></span>
                        <div class="sf1-subtitle">
                          <span><i>(This replaces Form 1, Master List & STS Form 2-Family Background and Profile)</i></span>
                        </div>
                    </div>
                  </div>
                  <div class="sf1-imglogo">
                    <img src="assets/image/sf1-imglogo.gif">
                  </div>
                  <div class="sf1-headinput">
                    <div class="sf1-subheadinput">
                      <span style="padding: 10px">School ID: <b style="text-decoration: underline;">405751</b> </span>
                      <span style="margin-left: 14px; padding: 10px">Region: <b style="text-decoration: underline;">X</b></span>
                      <span style="margin-left: 16px; padding: 10px">Division: <b style="text-decoration: underline;"> ILIGAN CITY</b></span>
                      <span style="margin-left: 26px; padding: 10px">District: <b style="text-decoration: underline;"> LONE DISCTRICT</b></span>
                    </div>
                    <div class="sf1-2subheadinput">
                      <span style="padding: 10px">School Name: 
                        <b style="text-decoration: underline;"><?php echo  $view_details->getVal("branchname", $sql)?></b>
                      </span>
                      <span style="margin-left: 15px; padding: 10px">School Year: 
                        <b style="text-decoration: underline;"><?php echo  $view_details->getVal("sy", $sql)?></b>
                      </span>
                      <span style="margin-left: 19px; padding: 10px">Grade Level: 
                        <b style="text-decoration: underline;"><?php echo  $view_details->getVal("gradelevel", $sql)?></b>
                      </span>
                      <span style="margin-left: 27px; padding: 10px">Section:  </span>
                    </div>
                  </div>


                  <br><br><br>
                  <table style="width: 180%" class="table_print">
                  	
                    <tr>
                      <th rowspan="2" class="sf1-text"><b>#</b></th>
                      <th rowspan="2" class="sf1-text"><b>LRN</b></th>
                      <th rowspan="2" class="sf1-text"><p>NAME <br> (Lastname, First Name, <br> Middle Name)</p></th>
                      <th rowspan="2" class="sf1-text"><b>Sex <br> (M/F)</b></th>
                      <th rowspan="2" class="sf1-text"><b>BIRTH <br> DATE <br> (yyyy/mm/dd)</b></th>
                      <th rowspan="2" class="sf1-text"><b>AGE as <br> of 1st <br> Friday <br> June</b></th>
                      <th rowspan="2" class="sf1-text"><b>BIRTH PLACE <br> (Province)</b></th>
                      <th rowspan="2" class="sf1-text"><b>MOTHER <br> TOUNGE</b></th>
                      <th rowspan="2" class="sf1-text"><b>IP <br> (Ethnic Group)</b></th>
                      <th rowspan="2" class="sf1-text"><b>RELIGION</b></th>
                      <th colspan="4" class="sf1-text"><b>ADDRESS</b></th>
                      <th colspan="2" class="sf1-text"><b>PARENTS</b></th>
                      <th colspan="2" class="sf1-text"><b>GUARDIAN(if not Parent)</b></th>
                      <th rowspan="2" class="sf1-text"><b>Contact Number <br> of Parent <br> or Guardian</b></th>
                      <th colspan="1" class="sf1-text"><b>REMARKS</b></th>
                    </tr>
                    <tr>
                      <th><b>House #/ <br> Street/ <br> Sitio/ <br> Purok</b></th>
                      <th><b>Barangay</b></th>
                      <th><b>Municipality/ <br> City</b></th>
                      <th><b>Province</b></th>
                      <th><b>Father's Name <br> (Last Name, <br> First Name, <br> Middle Name)</b></th>
                      <th><b>Mother's Maiden Name <br> (Last Name, <br> First Name, <br> Middle Name)</b></th>
                      <th><b>Name</b></th>
                      <th><b>Relationship</b></th>
                      <th><b>(Please <br> refer to the <br> legend on <br> last page)</b></th>
                    </tr>
                    <?php

                    	

        					if($view_details->notEmpty($sql)){
        						$openqry = $view_details->openqry($sql);
                    $i = 1;
        						while ($r = pg_fetch_assoc($openqry)) {
                    ?>
                    <tr>
                    	<td class="sf1-textss"><?php echo $i++ ?><input type="hidden" class="hidessssss" name="studentid" value="<?=$r["studentid"]?>"></td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['lrnid']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="lrnid" value="<?=$r["lrnid"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo ucwords($r['fullname']); ?>
                        </span>
                        <input type="text" class="hidessssss" name="lastname" value="<?=$r["lastname"]?>">
                        <input type="text" class="hidessssss" name="firstname" value="<?=$r["firstname"]?>">
                        <input type="text" class="hidessssss" name="middlename" value="<?=$r["middlename"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['sex']; ?>
                        </span>
                        <select class="hidessssss" name="sex" value="<?=$r["sex"]?>">
                          <option value="Male" <?php if($r['sex'] == 'Male'){ echo 'selected';} ?>>Male</option>
                          <option value="Female" <?php if($r['sex'] == 'Female'){ echo 'selected';} ?>>Female</option>
                        </select>
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['dateofbirth']; ?>
                        </span>
                        <input type="date" class="hidessssss" name="dateofbirth" value="<?=$r["dateofbirth"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['age']; ?>
                        </span>
                          
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['placeofbirth']; ?>
                        </span>
                          <input type="text" class="hidessssss" name="placeofbirth" value="<?=$r["placeofbirth"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['mothertongue']; ?>
                        </span>
                          <input type="text" class="hidessssss" name="mothertongue" value="<?=$r["mothertongue"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['ethnic']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="ethnic" value="<?=$r["ethnic"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['religion']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="religion" value="<?=$r["religion"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['streets']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="streets" value="<?=$r["streets"]?>">
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['barangay']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="barangay" value="<?=$r["barangay"]?>"> 
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['city']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="city" value="<?=$r["city"]?>"> 
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['province']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="province" value="<?=$r["province"]?>">  
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['fathername']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="fathername" value="<?=$r["fathername"]?>">   
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['mothername']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="mothername" value="<?=$r["mothername"]?>">   
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['guardian_name']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="guardian_name" value="<?=$r["guardian_name"]?>"> 
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['guardian_relationship']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="guardian_relationship" value="<?=$r["guardian_relationship"]?>">  
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['mother_mobile']; ?>
                        </span>
                        <input type="text" class="hidessssss" name="mother_mobile" value="<?=$r["mother_mobile"]?>"> 
                      </td>
                    	<td class="sf1-textss">
                        <span class="hide_details">
                          <?php echo $r['remarks']; ?>
                        </span>
                        <select class="hidessssss" name="remarks" value="<?=$r["remarks"]?>">
                          <option value=""> SELECT </option>
                          <option value="T/O" <?php if($r['remarks'] == 'T/O'){ echo 'selected';} ?>>T/O</option>
                          <option value="T/I" <?php if($r['remarks'] == 'T/I'){ echo 'selected';} ?>>T/I</option>
                          <option value="DRP" <?php if($r['remarks'] == 'DRP'){ echo 'selected';} ?>>DRP</option>
                          <option value="LE" <?php if($r['remarks'] == 'LE'){ echo 'selected';} ?>>LE</option>
                          <option value="CCT" <?php if($r['remarks'] == 'CCT'){ echo 'selected';} ?>>CCT</option>
                          <option value="B/A" <?php if($r['remarks'] == 'B/A'){ echo 'selected';} ?>>B/A</option>
                          <option value="LWD" <?php if($r['remarks'] == 'LWD'){ echo 'selected';} ?>>LWD</option>
                          <option value="ACL" <?php if($r['remarks'] == 'ACL'){ echo 'selected';} ?>>ACL</option>
                        </select>
                      </td>
                    </tr>
                    <?php
                  		}
        					}else {
        						echo "<td class='remarks' colspan=20>No record found..</td>";
        					}
                  	?>
                  </table>
                  <br>
                  		

        					
                  <div class="lists">
                    <span class="table-tdsize" style="margin-left: 139px"><b>List and Code of Indicators under REMARKS column</b></span>
                  </div>
                  <table style="width: 41%;" class="table_first">
                    <tr>
                      <td class="table-tdsize textsf1"><b>Indicator</b></td>
                      <td class="table-tdsize textsf1"><b>Code</b></td>
                      <td class="table-tdsize textsf1"><b>Required Information</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Transferred Out</b></td>
                      <td class="table-tdsize border-right textsf1"><b>T/O</b></td>
                      <td class="table-tdsize border textsf1"><b>Name of Public (P) Private (PR) School & Effectivity Date</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Transferred IN</b></td>
                      <td class="table-tdsize border-right textsf1"><b>T/I</b></td>
                      <td class="table-tdsize border textsf1"><b>Name of Public (P) Private (PR) School & Effectivity Date</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Dropped</b></td>
                      <td class="table-tdsize border-right textsf1"><b>DRP</b></td>
                      <td class="table-tdsize border textsf1"><b>Reason  and Effectivity Date</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Late Enrollment</b></td>
                      <td class="table-tdsize border-right textsf1"><b>LE</b></td>
                      <td class="table-tdsize border textsf1"><b>Reason (Enrollment beyond 1st Friday of June)</b></td>
                    </tr>
                  </table>

                  <table class="table-floats">
                    <tr>
                      <td class="table-tdsize textsf1"><b>Indicator</b></td>
                      <td class="table-tdsize textsf1"><b>Code</b></td>
                      <td class="table-tdsize textsf1"><b>Required Information</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>CCT Recipient</b></td>
                      <td class="table-tdsize border-right textsf1"><b>CCT</b></td>
                      <td class="table-tdsize border textsf1"><b>CCT Control/reference number & Effectivity Date</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Balik-Aral</b></td>
                      <td class="table-tdsize border-right textsf1"><b>B/A</b></td>
                      <td class="table-tdsize border textsf1"><b>Name of school last attended & Year</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Learner With Dissability</b></td>
                      <td class="table-tdsize border-right textsf1"><b>LWD</b></td>
                      <td class="table-tdsize border textsf1"><b>Specify</b></td>
                    </tr>
                    <tr>
                      <td class="table-tdsize border-right textsf1"><b>Accelarated</b></td>
                      <td class="table-tdsize border-right textsf1"><b>ACL</b></td>
                      <td class="table-tdsize border textsf1"><b>Specify Level & Effectivity Data</b></td>
                    </tr>
                  </table>

                  <table class="table-floats1">
                    <tr>
                      <td class="table-tdsize textsf1"><b>REGISTERED</b></td>
                      <td class="table-tdsize textsf1">BoSY</td>
                      <td class="table-tdsize textsf1">EoSY</td>
                    </tr>
                    <tr>
                      <td class="table-tdsize textsf1"><b>MALE</b></td>
                      <td class="table-tdsize textsf1">&nbsp;</td>
                      <td class="table-tdsize textsf1">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="table-tdsize textsf1"><b>FEMALE</b></td>
                      <td class="table-tdsize textsf1">&nbsp;</td>
                      <td class="table-tdsize textsf1">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="table-tdsize textsf1"><b>TOTAL</b></td>
                      <td class="table-tdsize textsf1">&nbsp;</td>
                      <td class="table-tdsize textsf1">&nbsp;</td>
                    </tr>
                  </table>

                  <div class="sub-signa">
                    <span>Prepared by:</span>
                    <div class="marg1">____________________________________________________</div>
                    <div class="marg2">(Signature of Adviser over Printed Name)</div>
                    <div class="marg3">BoSY Date:</div>
                    <div class="marg4">EoSY Date:</div>
                    <div class="marg5">____________________________________________________</div>
                  </div>

                  <div class="sub-signa1">
                    <span>Certified Correct:</span>
                    <div class="marg1">____________________________________________________</div>
                    <div class="marg2">(Signature of School Head over Printed Name)</div>
                    <div class="marg3">BoSY Date:</div>
                    <div class="marg4">EoSY Date:</div>
                    <div class="marg5">____________________________________________________</div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- END OF THE TABLECON -->


<?php
		}
	}
?>