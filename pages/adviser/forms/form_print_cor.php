
<?php


if (session_status() == PHP_SESSION_NONE) {
          
      session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  require '../../../controllers/db_controller/ViewAdviserDetails.php';
  
  $view_details_register = new ViewCashierDetails;
  $view_details = new ViewAdviserDetails;
if (isset($_POST["type"])) {


  $type = $_POST["type"];
  $branchid =  trim($_POST["branchid"]);
  $syid =  trim($_POST["syid"]);
  $semid =  trim($_POST["semid"]);

  if ($type == "studentname") {

  ?>
            <span id="print_corhidename">
            Name:&nbsp;&nbsp;
            <select class="wid-fix" style="width: 255px!important" name="regnum" id="student_view" onchange="show_cor_student(this.id)" >
            <?php
               
              $query = $view_details_register->get_perbilling($semid,$syid,$branchid,$id = 'all',$type_billing = "all");
              if($query) {
                  echo "<option value='none'>Select Student..</option>";
                  while ($r = pg_fetch_assoc($query)){
                                                          
                      echo '<option value="'.$r['regnum'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
                                                     
                  } 
          
              } else {
                          
                      echo "<option>No record found..</option>";
              }
                     
               ?>
               </select>&nbsp;&nbsp;
             </span>
             Use ID:&nbsp;&nbsp;<input type="checkbox" name="useID" id="print_corcheckstud"/>
            <span  id='print_corstudenthide' >Student ID:&nbsp;&nbsp;<input type="text" name="studentid" id="studentid"/>
            <button type="button" id="buttstudent" class="button_local">View</button>
            </span>&nbsp; 
            <button  type="button" onclick="printsss()" class="button_local">Print</button>
            <!-- <a download=”file.pdf href=”#” class=”btn-print” data-name=”#{show-data}”>Download PDF File </a> -->

          <script type="text/javascript">
            $('#print_corstudenthide').hide();
            $('#print_corhidename').show();
                  
            var allButtonsOnPage = document.getElementById("print_corcheckstud");
            var button = allButtonsOnPage;
            button.addEventListener('click', function() {

            if ($('#print_corcheckstud').is(':checked')) {
              $('#print_corstudenthide').show();
              $('#print_corhidename').hide();
              $('#display_print_cor').hide();
            } else {
              $('#print_corstudenthide').hide();
              $('#print_corhidename').show();
            }
            });
           </script>

  <?php

  } elseif ($type == "studentdata") {
    // $regnum = $_POST['regnum']; //student id
    // $sql = $view_details_register->get_info_student_account($regnum);
    // $curcode = $view_details->getVal('curcode',$sql);
    // $coursecode = $view_details->getVal('coursecode',$sql);
    // $studenttypeid =  $view_details->getVal('studenttypeid',$sql);
    // $studentid = $view_details->getVal('studentid',$sql);

     $regnum = $_POST['regnum']; //student id
      $sql = $view_details_register->get_info_student_account($regnum);
      $curcode = $view_details->getVal('curcode',$sql);
      $coursecode = $view_details->getVal('coursecode',$sql);
      $studenttypeid =  $view_details->getVal('studenttypeid',$sql);
      $studentid = $view_details->getVal('studentid',$sql);
    ?>

      <div class="tablecon" id="text-val">
        <div id="printableArea">
          <div style="padding-top: 24px;" class="create" >
            <table style="width: 80%;" align="center" class="print-cent">
                <div>
                  <tr>
                    <td style="text-align: right;" class="text-b"><span>Name</span>:</td>
                    <td style="text-align: left;"><?php echo ucfirst($view_details->getVal('lastname',$sql)).", ".ucfirst($view_details->getVal('firstname',$sql))." ".ucfirst($view_details->getVal('middlename',$sql)[0]).'.'; ?></td>
                    <td style="text-align: right;"><span class="text-b">Date</span>:</td>
                    <td style="text-align: left;" ><?php  echo $today = date("F j, Y");?></td>  
                  </tr>
                  <tr>
                    <td style="text-align: right;"><span class="text-b">ID No.</span>:</td>
                    <td style="text-align: left;" ><?php echo $view_details->getVal('studentid',$sql);?></td>
                    <td style="text-align: right;"><span class="text-b">Sem</span>:</td>
                    <td style="text-align: left;" ><?php echo $view_details->getVal('semid',$sql);?></td>
                  </tr>
                   <tr>
                    <td style="text-align: right;"><span class="text-b">Course</span>:</td>
                    <td style="text-align: left;" ><?php echo  $view_details->getVal('coursename',$sql);?></td>
                    <td style="text-align: right;"><span class="text-b">S.Y</span>:</td>
                    <td style="text-align: left;" ><?php echo $view_details->getVal('syid',$sql);?></td>
                  </tr>
                </div>
            </table>
            <br>

            <table class="table2">
                <tr align="center" bgcolor="gray">
                  <th rowspan="2" ><p>No.</p></th>
                  <th rowspan="2" class="wid3"><p>Subject Code</p></th>
                  <th rowspan="2" ><p>Block</p></th>
                  <th rowspan="2" class="wid"><p>Descriptive Title</p></th>
                  <th rowspan="2" class="wid1"><p>Start Date</p></th>
                  <th rowspan="2" ><p>Day</p></th>
                  <th rowspan="2" class="wid2"><p>Time</p></th>
                  <th rowspan="2" ><p>Room</p></th>
                  <th colspan="2" ><p>Unit</p></th>
                </tr>
                <tr bgcolor="gray">
                  <th><p>Lec</p></th>
                  <th><p>Lab</p></th>
                </tr>
                 <?php
                    $semtotallec_advise = 0;
                    $semtotallab_advise = 0;
                    $course_tofalse = "false";
                    $subjectcode = "false";
                    $offer = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
                    $i = 1;
                    if ($offer) {
                        while($row = pg_fetch_assoc($offer)){
                           $semtotallec_advise += $row["lec"];
                             $semtotallab_advise += $row["lab"];
                        ?>
                            <tr>
                              <td class="print-txt"><?=$i++?></td>
                              <td class="print-txt"><?=$row['subjectcode']?></td>
                              <td class="print-txt"><?=$row['blockname']?></td>
                              <td class="print-txt"><?=$row['description']?></td>
                              <td class="print-txt"><?=$row["datestart"]?></td>
                              <td class="print-txt"><?=$row['days']?></td>
                              <td class="print-txt"><?=$row['starttime'].' - '.$row["endtime"]?></td>
                              <td class="print-txt"><?=$row["roomcode"]?></td>
                              <td class="print-txt"><?=$row['lec']?></td>
                              <td class="print-txt"><?=$row['lab']?></td>
                          </tr>
                        <?php
                      }
                    }else {
                      ?>
                        <tr>
                          <td colspan="10">No record found...</td>
                        </tr>
                      <?php
                    }
                ?>
                <tr>
                  <td colspan="8" class="subcurri2" style="text-align: right;">Credit</td>
                    <td><?=$semtotallec_advise?></td>
                    <td><?=$semtotallab_advise?></td>
                </tr>
            </table>
            <br>

            <div><b>Legend:</b></div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;M-monday; T-tuesday; W-wednesday; H-thursday; F-friday; S-saturday; U-sunday</div><br>

            <div><b>Room Location</b></div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;ICI - Annex 1: &nbsp;&nbsp;Activity Center, HRMLEC1, HRMLEC2, HRMLEC3</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;ICI - Annex 2: &nbsp;&nbsp;Activity Center, HRMLEC1, HRMLEC2, HRMLEC3</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;ICI - Annex 3: &nbsp;&nbsp;Activity Center, HRMLEC1, HRMLEC2, HRMLEC3</div>
            <div>&nbsp;&nbsp;&nbsp;&nbsp;ICI - Annex 4: &nbsp;&nbsp;Activity Center, HRMLEC1, HRMLEC2, HRMLEC3</div>
          </div>

          <br><br><br>

          <div class="tablecon">
            <div><b>Remarks:</b></div><br>
            <div>Class Start: Monday, June 3, 2020 <span style="display: inline-block; padding-left: 54%">Dolly Jean Annasco Tan</span></div><br>
            <div>Important: Registration fee is NON-REFUNDABLE <span style="display: inline-block; padding-left: 48%"><b>Registrar</b></span></div><br>
          </div>
          </div>
        </div>
      </div>

<?php
  }
}
?>