<?php
if (session_status() == PHP_SESSION_NONE) {
          
      session_start();

  }
  require '../../../controllers/db_controller/theDBConn.php';
  require '../../../controllers/db_controller/ViewCashierDetails.php';
  require '../../../controllers/db_controller/ViewAdviserDetails.php';
  
  $view_details_register = new ViewCashierDetails;
  $view_details = new ViewAdviserDetails;



  if (isset($_POST["type"])) {
		$type = $_POST["type"];
		$branchid =  trim($_POST["branchid"]);
		$syid =  trim($_POST["syid"]);
		$semid =  trim($_POST["semid"]);
    $studenttypeid = trim($_POST["studenttypeid"]);
    $gradelvl = trim($_POST["gradelvl"]);
    $coursecode = trim($_POST["coursecode"]);

    if($type == "view_name"){
      ?>
        <span id="print_form138hide">
          Name:&nbsp;&nbsp;
          <select class="wid-fix" style="width: 255px!important" name="regnum" id="regnum" onchange="form_138_controller(this.id);">
          <?php
           
          $query = $view_details->view_form135_details($semid,$syid,$branchid,$studenttypeid,$gradelvl,$coursecode, $type);
          if($query) {
              echo "<option value='none'>Select Student..</option>";
              while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['studentid'].'">'.ucfirst($r['lastname']).', '.ucfirst($r['firstname']).' '.ucfirst($r['middlename']).'</option>';
              } 
      
          } else {
                      
                  echo "<option>No record found..</option>";
          }
                 
           ?>
          </select>
        </span>
        <button  type="button" onclick="printFrom138()" class="button_local">Print</button>

        <script type="text/javascript">
          $('#print_form138hide').show();
        </script>

      <?php
    } elseif ($type == "show_details_grades") {
      $studentid = $_POST["regnum"];
      $subjectcode = "false";
      $course_tofalse = "false";
      $detial = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
      $gpa = 0;
      $count_sub = 0;

     ?>
      <div align="center" class="tablecon"> 
        <div  style="width: 47%; float: left;">
        <table>
          <tr>
            <th colspan="4" class="f138-txtsize">REPORT ON LEARNING PROGRESS AND SCHIVEMENT</th>
          </tr>
          <tr>
            <td colspan="4" class="f138-txtsize center"><b>FIRST SEMESTER</b></td>
          </tr>
          <tr>
            <td rowspan="2" style="width: 61%;" class="f138-txtsize center"><b>SUBJECTS</b></td>
            <td colspan="2" class="f138-txtsize center"><b>QUARTER</b></td>
            <td rowspan="2" class="f138-txtsize center"><b>FINAL GRADE</b></td>
          </tr>
          <tr>
            <td class="f138-txtsize center"><b>1</b></td>
            <td class="f138-txtsize center"><b>2</b></td>
          </tr>
           <tr>
            <td style="text-align: left;" colspan="4" class="f138-txtsize2">CORE SUBJECTS</td>
          </tr>
           <?php 
            if($detial) {
              while ($r = pg_fetch_assoc($detial)){
                $average_persubject = 0;
               if($r["subjecttype"] == "CORE"){
                 $count_sub++;
                  ?>
                  <tr>
                    <td class="remarks"><?=$r["description"]?></td>
                    <td ><?=$r["first"]?></td>
                    <td><?=$r["second"]?></td>
                    <td>
                    <?php
                      $average_persubject = ($r["first"] + $r["second"]);
                      echo $average_persubject > 100 ? $average_persubject / 2 : "";
                      $gpa += $average_persubject > 100 ? $average_persubject / 2 : 0;
                    ?>
                    </td>
                  </tr>
                <?php
               } 
             }
      
            } else {
                echo "<td colspan=4 class='center2'>No controlle subject..</td>";
            }
          ?>
          <tr>
            <td style="text-align: left;" colspan="4" class="f138-txtsize2">APPLIED AND SPECIALIZED SUBJECTS</td>
          </tr>
          <?php 
            $detial = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
            if($detial) {
              while ($row = pg_fetch_assoc($detial)){
              $average_persubject = 0;
               if($row["subjecttype"] != "CORE"){
                $count_sub++;
                  ?>
                  <tr>
                    <td class="remarks"><?=$row["description"]?></td>
                    <td><?=$row["first"]?></td>
                    <td><?=$row["second"]?></td>
                    <td>
                    <?php
                      $average_persubject = ($row["first"] + $row["second"]);
                      echo $average_persubject > 100 ? $average_persubject / 2 : "";
                      $gpa += $average_persubject > 100 ? $average_persubject / 2 : 0;
                    ?>
                    </td>
                  </tr>
                <?php
               } 
             }
      
            } else {
                echo "<td colspan=4 class='center2'>No controlle subject..</td>";
            }
          ?>
          <tr>
            <td style="text-align: right;" colspan="3" class="f138-txtsize2">General Average for the Semester</td>
            <td>
              <?php 
                if($gpa != 0){
                  $get_gpa = $gpa / $count_sub ;
                  echo $get_gpa > 75 ? $get_gpa : "";
                }
                
              ?>
            </td>
          </tr>
        </table>
        
        <br>

        <table>
          <tr>
            <th colspan="6" class="f138-txtsize center">REPORT ON LEARNER' SOBSERVED VALUES</th>
          </tr>
          <tr class="f138-txtsize center">
            <td rowspan="2"><b>CORE VALES</b></td>
            <td rowspan="2"><b>BEHAVIOR STATEMENTS</b></td>
            <td colspan="4"><b>QUARTER</b></td>
          </tr>
          <tr>
            <td><b>1</b></td>
            <td><b>2</b></td>
            <td><b>3</b></td>
            <td><b>4</b></td>
          </tr>
          <tr>
            <td rowspan="2" class="f138-txtsize2 center">MAKA-DIYOS</td>
            <td style="text-align: left;" class="f138-txtsize2">Expresses one's spiritual beliefs while respecting the spiritual beliefs of others</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style="text-align: left;" class="f138-txtsize2">Shows adherance to ethical principles by uploading truth</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td rowspan="2" class="f138-txtsize2 centers">MAKA-TAO</td>
            <td style="text-align: left;" class="f138-txtsize2">Is sensitive to individual, social and cultural differences</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
           <tr>
            <td style="text-align: left;" class="f138-txtsize2">Demonstrates contributions toward solidarity</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
           <tr>
            <td class="f138-txtsize2 center">MAKA-KALIKASAN</td>
            <td style="text-align: left;" class="f138-txtsize2">Cares for the environment and utilizes resources wisely, judiciously and economically</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td rowspan="2" class="f138-txtsize2 center">MAKA-BANSA</td>
            <td style="text-align: left;" class="f138-txtsize2">Demonstrates pride in being a Filipino excercises the rights and responsibilities of a Filipino citizen</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
           <tr>
            <td style="text-align: left;" class="f138-txtsize2">Demonstrates appropriate behavior in carrying out activities int the school,community and country</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
        <div style="float: right; width: 47%">
          <table>
          <tr>
            <th colspan="4" class="f138-txtsize">REPORT ON LEARNING PROGRESS AND SCHIVEMENT</th>
          </tr>
          <tr>
            <td colspan="4" class="f138-txtsize center"><b>FIRST SEMESTER</b></td>
          </tr>
          <tr>
            <td rowspan="2" style="width: 61%;" class="f138-txtsize center"><b>SUBJECTS</b></td>
            <td colspan="2" class="f138-txtsize center"><b>QUARTER</b></td>
            <td rowspan="2" class="f138-txtsize center"><b>FINAL GRADE</b></td>
          </tr>
          <tr>
            <td class="f138-txtsize center"><b>3</b></td>
            <td class="f138-txtsize center"><b>4</b></td>
          </tr>
           <tr>
            <td style="text-align: left;" colspan="4" class="f138-txtsize2">CORE SUBJECTS</td>
          </tr>
           <?php 
            if($detial) {
              while ($r = pg_fetch_assoc($detial)){
                $average_persubject = 0;
               if($r["subjecttype"] == "CORE"){
                 $count_sub++;
                  ?>
                  <tr>
                    <td class="remarks"><?=$r["description"]?></td>
                    <td ><?=$r["third"]?></td>
                    <td><?=$r["forth"]?></td>
                    <td>
                    <?php
                      $average_persubject = ($r["third"] + $r["forth"]);
                      echo $average_persubject > 100 ? $average_persubject / 2 : "";
                      $gpa += $average_persubject > 100 ? $average_persubject / 2 : 0;
                    ?>
                    </td>
                  </tr>
                <?php
               }
             }
      
            } else {
                echo "<td colspan=4 class='center2'>No controlle subject..</td>";
            }
          ?>
          <tr>
            <td style="text-align: left;" colspan="4" class="f138-txtsize2">APPLIED AND SPECIALIZED SUBJECTS</td>
          </tr>
          <?php 
            $detial = $view_details->get_my_controlle_subject($course_tofalse,$studenttypeid,$semid,$syid,$branchid,$subjectcode,$studentid);
            if($detial) {
              while ($row = pg_fetch_assoc($detial)){
              $average_persubject = 0;
               if($row["subjecttype"] != "CORE"){
                $count_sub++;
                  ?>
                  <tr>
                    <td class="remarks"><?=$row["description"]?></td>
                    <td><?=$row["third"]?></td>
                    <td><?=$row["forth"]?></td>
                    <td>
                    <?php
                      $average_persubject = ($row["third"] + $row["forth"]);
                      echo $average_persubject > 100 ? $average_persubject / 2 : "";
                      $gpa += $average_persubject > 100 ? $average_persubject / 2 : 0;
                    ?>
                    </td>
                  </tr>
                <?php
               } 
             }
      
            } else {
                echo "<td colspan=4 class='center2'>No controlle subject..</td>";
            }
          ?>
          <tr>
            <td style="text-align: right;" colspan="3" class="f138-txtsize2">General Average for the Semester</td>
            <td>
              <?php 
                if($gpa != 0){
                  $get_gpa = $gpa / $count_sub ;
                  echo $get_gpa > 75 ? $get_gpa : "";
                }
                
              ?>
            </td>
          </tr>
        </table>
        <br>
        <table class="border">
          <tr>
            <td colspan="4" class="border align-left f138-txtsize"><b>OBSERVED VALUES</b></td>
          </tr>
          <tr>
            <td class="border f138-txtsize2">AO</td>
            <td class="border f138-txtsize2">Always Observed</td>
            <td class="border f138-txtsize2">SO</td>
            <td class="border f138-txtsize2">Sometimes Observed</td>
          </tr>
          <tr>
            <td class="border f138-txtsize2">RO</td>
            <td class="border f138-txtsize2">Rarely Observed</td>
            <td class="border f138-txtsize2">NO</td>
            <td class="border f138-txtsize2">Not Observed</td>
          </tr>
        </table>
        <table class="border">
          <tr>
            <td colspan="3" class="border align-left f138-txtsize"><b>LONG PROGRESS AND ACHIEVEMENT</b></td>
          </tr>
          <tr>
            <td class="border f138-txtsize2 align-left">Descriptors</td>
            <td class="border f138-txtsize2 align-left">Grading Style</td>
            <td class="border f138-txtsize2 align-left">Remarks</td>
          </tr>
          <tr>
            <td class="border f138-txtsize2 align-left">Outstanding</td>
            <td class="border f138-txtsize2 align-left">90-100</td>
            <td class="border f138-txtsize2 align-left">Passed</td>
          </tr>
          <tr>
            <td class="border f138-txtsize2 align-left">Very Satisfactory</td>
            <td class="border f138-txtsize2 align-left">85-89</td>
            <td class="border f138-txtsize2 align-left">Passed</td>
          </tr>
          <tr>
            <td class="border f138-txtsize2 align-left">Satisfactory</td>
            <td class="border f138-txtsize2 align-left">80-84</td>
            <td class="border f138-txtsize2 align-left">Passed</td>
          </tr>
          <tr>
            <td class="border f138-txtsize2 align-left">Fairly Satisfactory</td>
            <td class="border f138-txtsize2 align-left">75-79</td>
            <td class="border f138-txtsize2 align-left">Passed</td>
          </tr>
          <tr>
            <td class="border f138-txtsize2 align-left">Did Not Meet Expectations</td>
            <td class="border f138-txtsize2 align-left">Below 75</td>
            <td class="border f138-txtsize2 align-left">Failed</td>
          </tr>
        </table>
        </div>
      </div>
     <?php      
    }
  }

?>