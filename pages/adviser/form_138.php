 <div class="tab_add_data_table7">
   <?php  
      require 'secondpage_138.php';
   ?>
 </div>

<div class="table_info_show7">
  <div class="tablecon cardcover_form_show">
      <form name="cardcover_form_show" id="cardcover_form_show">
        <div class="enroll">
          <b class="line_r">Required Option</b>
        </div>
        
        <div class="select-head">
          <div class="hrsb">
               <div class="container_select">
                  <span>Branch:&nbsp;&nbsp;</span>
                  <select style="width: 180px!important" name="branchid" id="branchid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails('branch');
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                      }
                    ?>
                  </select>
                </div>
                <div class="container_select">
                    <span>School Year:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important" name="syid" id="syid" style="display: inline-block;">
                      <?php
                        $query = $view_option_details->optionDetails("schoolyear");
                        while ($r = pg_fetch_assoc($query)){
                          echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                        }
                      ?>
                    </select>
                </div>
                 <div class="container_select">
                    <span>Semester:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important"  name="semid" id="semid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("semester");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
                      }
                    ?>
                    </select>
                  </div>
                  <div class="container_select">
                    <span>Scholarship:&nbsp;&nbsp;</span>
                    <select style="width: 100px!important"  name="scholarshipid" id="scholarshipid" style="display: inline-block;">
                    <?php
                      $query = $view_option_details->optionDetails("scholarship");
                      while ($r = pg_fetch_assoc($query)){
                        echo '<option value="'.$r['scholarshipid'].'" >'.$r['name'].'</option>';
                      }
                    ?>
                    </select>
                  </div>
                   <div class="container_select">
                    <span>
                      <button  type="button" id="cardpage" class='button_local' onclick="show_studname(this.id)">SELECT</button>
                    </span>
                   </div>
                   <div class="container_select">
                     <div id="show_students">
                       <!-- DISPLAY STUDENT CATEGORY -->
                     </div>
                   </div>
          </div>
        </div>
      </form>
      <br>

      <div id="grades138_print">
        <div id="show_card_students" style="display:block;width:100%;">
                  <!-- DISPLAY CARD -->
        </div> 
      </div>
      
    </div>
  </div>

<script type="text/javascript">

function printGrades138() {
    var divToPrint = document.getElementById('grades138_print');
    var htmlToPrint = '' +
        '<style type="text/css">' +

          // 'table, td{' +
          //     'border: 1px solid black;' +
          //     'border-collapse: collapse;' +
          //     'padding: 6px;' +
          // '}' +

          '.card {' +
              'position: relative;' +
              'display: -ms-flexbox;' +
              '-ms-flex-direction: column;' +
              'flex-direction: column;' +
              'min-width: 0;' +
              'word-wrap: break-word;' +
              'background-color: #fff;' +
              'background-clip: border-box;' +
              'border: 1px solid black!important;' +
              'border-collapse: collapse;' +
          '}' +

          '.card td {' +
            'border: 1px solid black!important;' +
            'padding: 10px;' +
          '}' +

          '.covercard {' +
              'display: flex;' + 
          '}' +

          '.contcover {' +
              'width: 432px;' + 
              'height: 640px;' +
              'margin: 0px auto;' +
          '}' +

          '.coverreport {' +
              'font-size: 12px;' + 
              'font-weight: 600;' +
          '}' +

          '.whole {' +
              'width: 500px;' +
              'height: 545px;' + 
               'font-size: 10px;' +
          '}' +
          'table .card {' +
              'height: 138px;' +
              'display: inline-block;' +
              'border:none;' +
          '}' +

          'table.card td {' +
              'background-color: white;' +
          '}' +

          'table.name td {' +
              'background-color: white;' +
          '}' +

          'table.cardsig td {' +
              'background-color: white;' +
              'border-top: 0px;' +
              'border-right: 0px;' +
              'border-left: 0px;' +
          '}' +

          'table.t3 td {' +
              'background-color: white;' +
              'border: 0px;' +
              'text-align: left;' +
              'border-bottom: 1px solid gray;' +
          '}' +

          '.card td {' +
              'font-size: 10px;' +
          '}' +

          'table.name {' +
              'height: 10px;' +
          '}' +

          'table.name td {' +
              'text-align: left;' +
          '}' +

          '.label1 {' +
              'border-bottom: 1px solid black; ' +
              'margin-right: 20px; ' +
              'margin-bottom: 0px; ' +
              'font-weight: bold;' +
          '}' +

          '.label2 {' +
              'border-bottom: 1px solid black; ' +
              'margin-bottom: 0px; ' +
              'font-weight: bold;' +
          '}' +

          '.label3 {' +
              'margin-right: 20px; margin-top: 0px;' +
          '}' +

          '.certi {' +
              'padding-top: 15px; font-weight: 600;' +
          '}' +

          '.admitted {' +
              'padding-top: 15px;' +
         ' }' +

          '.guardiansig {' +
              'border:none;' +
              'margin-left: -105px;' +
              'margin-top: -35px;' +
          '}' +
          '.guardiansig tr, td {' +
              'background-color: white;' +
          '}' +
          '.noneb {' +
              'border:none;' +
              'text-align: right;' +
          '}' +
          '.sssf {' +
              'border-bottom: 1px solid black;' +
              'width:200px;' +
          '}' +

        '</style>';
    htmlToPrint += divToPrint.outerHTML;//table
    newWin = window.open("");
    // newWin.document.write("<img src='./assets/image/logo.png'>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
}

  function show_studname(type){
    // alert( $('#cardcover_form_show').serialize() );
    var form = "cardcover_form_show";
    if (type == "cardpage") {
      var id = "show_students";
      $.ajax({
        type : 'POST',
        url  : 'pages/adviser/forms/form_form138.php',
        data : $('#'+form).serialize()+'&type=cardpage',
        beforeSend : function (){
          $('#action_loading').show();
        },
        success : function (data){
          $('#action_loading').hide();
          $('#'+id).html(data);
        }
      });
    } else if (type == "show_cardpage"){
      var id = "show_card_students";
      $.ajax({
        type : 'POST',
        url  : 'pages/adviser/forms/form_form138.php',
        data : $('#'+form).serialize()+'&type=show_cardpage',
        beforeSend : function (){
          $('#action_loading').show();
        },
        success : function (data){
          $('#action_loading').hide();
          $('#'+id).html(data);
          $('#show_card_students').html(data);
        }
      });
    }

  }

</script>
 