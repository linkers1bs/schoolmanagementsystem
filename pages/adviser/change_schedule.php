<div class="tablecon change_shedule">

  <form name="change_shedule" id="change_shedule">
    <div class="enroll">
      <b class="line_r">Required Option</b>
    </div>
    <div class="select-head">
      <div class="hrsb">
        <div>
          <div class="container_select">
            <span>Branch:&nbsp;&nbsp;</span>
            <select style="width: 270px!important" name="branchid" id="branchid" style="display: inline-block;">
              <?php
                $query = $view_option_details->optionDetails('branch');
                while ($r = pg_fetch_assoc($query)){
                  echo '<option value="'.$r['branchid'].'" >'.$r['name'].'</option>';
                }
              ?>
            </select>
          </div>
          <div class="container_select">
             <span>School Year:&nbsp;&nbsp;</span>
              <select style="width: 140px!important" name="syid" id="syid" style="display: inline-block;" >
                <?php
                  $query = $view_option_details->optionDetails("schoolyear");
                  while ($r = pg_fetch_assoc($query)){
                    echo '<option value="'.$r['syid'].'">'.$r['sy'].'</option>';
                  }
                ?>
              </select>
          </div>
         <div class="container_select">
          <span>Semester:&nbsp;&nbsp;</span>
          <select style="width: 140px!important"  name="semid" id="semid" style="display: inline-block;">
          <?php
            $query = $view_option_details->optionDetails("semester");
            while ($r = pg_fetch_assoc($query)){
              echo '<option value="'.$r['semid'].'" >'.$r['sem'].'</option>';
            }
          ?>
          </select>
         </div>
          <div class="container_select">
            <span>Type:&nbsp;&nbsp;</span>
            <select style="width: 140px!important"  name="studenttypeid" id="studenttypeid" style="display: inline-block;">
            <?php
              $query = $view_option_details->optionDetails("studenttype");
              while ($r = pg_fetch_assoc($query)){
                 $select = '';
                if($r["studenttypeid"] == "2"){
                  $select = 'selected';
                }
                echo '<option value="'.$r['studenttypeid'].'" '.$select.'>'.$r['name'].'</option>';
              }
            ?>
            </select>
         </div>
          <div class="container_select" style="display: inline-block;">
            <span><button  type="button" id="select" class='button_local' onclick="change_shedule_name(this.id)">SELECT</button></span>
          </div>
          <div class="container_select" style="display: inline-block;">
            <div id="display_change_shedule_name" style="display: inline-block;"> 
              <!-- DISPLAYT NAME OF REGISTER -->
            </div>
          </div>
        </div>
      </div>
     <!--  This module change or delete selected schedules. In case you deleted some schedules, please to back to advising module for retrieval. -->
    </div>

    <div id="display_change_shedule" >
                  <!-- DISPLAY PRINT COR -->
    </div>  


  </form>
</div>

