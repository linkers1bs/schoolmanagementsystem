<?php
require 'controllers/db_controller/theDBConn.php';
require 'controllers/db_controller/verify_session.php';
require 'controllers/db_controller/ViewOptionDetails.php'; 
$verrify_session = new VerifySession();
$db = new DB();
$verrify_session->verrify_session($_SESSION["theUID"]);
require 'controllers/function/errorrmsg.php';
require 'controllers/function/getPage.php';
$view_option_details = new ViewOptionDetails();

if(isset($_GET["page"])){
	$type_page = $_GET["page"];

	if($type_page == "technical"){
		include('controllers/db_controller/ViewTechnicalDetails.php');
	}elseif ($type_page == "management") {
		include('controllers/db_controller/ViewManagementDetails.php');
	} elseif ($type_page == "myaccount") {
		include('controllers/db_controller/ViewMyaccountDetails.php');
	}elseif ($type_page == "registrar") {
		include('controllers/db_controller/ViewRegistrarDetails.php');
	}elseif ($type_page == "cashier") {
		include('controllers/db_controller/ViewCashierDetails.php');
	}

}

?>