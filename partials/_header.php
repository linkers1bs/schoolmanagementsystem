<style type="text/css">
	.branch_head {
		position: absolute;
    	right: 76px;
    	top: 8px;
	}
	.branch_head label {
		color: white;
	}
</style>
<div class="_header-container">
	
	<div class="logo_container">
		<img src="./assets/image/logo.png">
	</div>
	<div class="_header_menu">
		<div class="_h_title">
			<h6>SCHOOL MANAGEMENT SYSTEM</h6>

		</div>
		<div class="branch_head">
			<label>Branch:</label>
			<select name="branch_sesion" id="branch_sesion" onchange="sessionBranch(this.value)">
				 <?php
                  $query = $view_option_details->optionDetails('branch');
                  while ($r = pg_fetch_assoc($query)){
                  	$var = "";
                  	if ((int)$r["branchid"] == (int)$_SESSION["branch_id"]) {
                  		$var = 'selected';
                  	}
                    echo '<option value="'.$r['branchid'].'" '.$var.' >'.$r['name'].'</option>';
                  }
                ?>
			</select>
		</div>
		<div class="sub_menu">
			
			<div class="dropdown">	
				<div class="_header_notification" data-toggle="dropdown" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
					<img src="./assets/image/notif.png">
				</div>
			 	<div class="dropdown-menu notificationbox" aria-labelledby="dropdownMenuButton">
		 			<div class="notif_box_content" >
		 				<div class="notif_box_header">
		 					<div class="notif_title">
		 						<h6>Notification</h6>
		 						<h5>4</h5>
		 					</div>
		 					<div class="settings_area">
		 						<a href="#">Mark All As Read</a>
		 						<a href="#">Settings</a>
		 					</div>
		 					<div class="_custom_arrow"></div>
		 				</div>
					    <div class="notification_scroll">

				    	<?php for ($x = 0; $x <= 6; $x++): ?>
					    	<div class="notification_message" data-toggle="modal" data-target="#modal_notif_<?=$x?>">
					    		<div class="_nm_title">
					    			<h6>New E-mail!</h6>
					    		</div>
					    		<div class="_nm_message">
					    			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					    			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					    			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					    			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					    			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					    			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					    		</div>
					    		<div class="footer">
	                                <time class="timeago" datetime="2020-05-12 12:00:00"></time>
	                            </div>
					    	</div>
				    	<?php endfor;?>

					    </div>
		 			</div>	
			  	</div>
			  	
			</div>
			<div class="dropdown">
			<?php
				$name = $_SESSION["theUID"];
				$sql = "  SELECT * FROM usergroup
							LEFT JOIN grouptype USING(grouptypeid)
							LEFT JOIN userinfo USING (username)
				WHERE userinfo.username IN (SELECT username FROM userinfo WHERE username = '$name')";
				$row = pg_fetch_assoc($db->openqry($sql));
			?>
				<div class="_header_profile" data-toggle="dropdown" id="dropdownUserprofile" aria-haspopup="true" aria-expanded="false">
					<img src="./assets/image/user.png">
				</div>
				<div class="dropdown-menu user_profile_box" aria-labelledby="dropdownMenuButton">
		 			<div class="notif_box_content" >
		 				<div class="_profile_dropdown_header">
		 					<h6>Welcome! <?php echo ucwords($db->getVal('position',$sql)); ?></h6>
		 				</div>
		 				<div class="_personal_details">
		 					<h6>Personal</h6>
		 					<div class="_second_row_user">
		 						<img src='<?php if($row["userprofile"] == '' || $row["userprofile"] == null){ echo "./assets/image/emptyimage.png";}else {echo $row["userprofile"];} ?>' width="50">
		 						<div class="_sm_detail">
		 							<h6><?php echo $row["email"]; ?></h6>
		 							<h5>
		 								<i class="fa fa-check"></i>	
		 								<span>Sync is on</span>
		 							</h5>
		 						</div>
		 					</div>
		 					<a href="">Manage Profile settings</a>
		 				</div>
		 				<div class="_action_dropdown_settings">
		 					<div class="_edit_profile">
		 						<img src="./assets/image/icons/edit_profile.png" width="50">
		 						<div class="_edit_p">
		 							<h6><a href="index.php?page=myaccount" style='color:black'>Edit Profile</a></h6>
		 							<p>To Sign in to microsoft edge with an additional e-mail account, add a profile.</p>
		 						</div>
		 					</div>
		 					<div class="_change_password">
		 						<img src="./assets/image/icons/lock.png" width="50">	
		 						<h6><a href="index.php?page=myaccount" style='color:black'>Change Password</a></h6>
		 					</div>
		 					<div class="_logout">
		 						<img src="./assets/image/icons/logout.png" width="50">	
		 						<h6><a href="partials/logout.php" style='color:black'>Logout</a></h6>
		 					</div>
		 				</div>
		 			</div>
					<div class="_custom_arrow _custom_arrow2"></div>	
			  	</div>
		  	</div>
		</div>
	</div>


</div>
 
