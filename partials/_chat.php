<div class="_float_chat">
    
    <div class="_float_chat_container">
        <img src="assets/image/chat-icon.png">
    </div>

</div>

<div class="_chat_container">
	
	<div class="_chat_header">
		<h6>Chats</h6>

		<button class="fa fa-times _close_chat_list"></button>
	</div>
	
	<div class="_chat_list">

		<div class="_chat_list_scroll">

			<?php for ($x = 0; $x <= 10; $x++): ?>

			<button class="_chat_box_user" value="<?=$x?>" id="_show_chat_box_<?=$x?>">
				<div class="_user_profile">
					<img src="./assets/image/user.png">
					<i class="fa fa-circle"></i>
				</div>
				<div class="_username">
					<h6>Janet Fowler <?=$x?></h6>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
				<div class="_timeago">
				 	<time class="timeago" datetime="2020-05-13 12:00:00"></time>
				</div>
			</button>

			<?php endfor; ?>

		</div>

	</div>

	<div class="_chat_search">
		<input type="search" name="search" placeholder="   Type Name here"">
		<i class="fa fa-search"></i>
	</div>

</div>


<div class="_show_chat_box_selected">
	
	<?php for ($x = 0; $x <= 10; $x++): ?>
	<div class="_chat_box_show" id="_chat_box_user_id_<?=$x?>">
		
		<div class="_cbs_header">
			<div class="_first_div">
				<div class="_user_icon">
					<img src="./assets/image/test.jpg">
				</div>
				<a href="?page=messages"><h6>Janet Fowler <?=$x?></h6></a>
			</div>
			<div class="_chat_box_action">
				<button class="fa fa-video-camera"></button>
				<button class="fa fa-phone"></button>
				<button class="fa fa-user"></button>
				<button class="fa fa-times _close_chat_window" value="<?=$x?>"></button>
			</div>
		</div>
		<div class="_cbs_message_content">

			<div class="_left_user_messages">
				<div class="_l_u_m_icon">
					<img src="./assets/image/test.jpg">
				</div>
				<div class="_l_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
				</div>
			</div>

			<div class="_left_user_messages">
				<div class="_l_u_m_icon">
					<img src="./assets/image/test.jpg">
				</div>
				<div class="_l_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
				</div>
			</div>

			<div class="_right_user_messages">
				<div class="_r_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
				</div>
			</div>

			<div class="_right_user_messages">
				<div class="_r_u_message">
					<p>The quick brown fox jump over the lazy dog. The quick brown fox jump over the lazy dog. </p>
					<form >
		<input type="text">
		<input type="submit">
	</form>
				</div>
			</div>

		</div>
		<div class="_input_message_area">
			<div class="_input_ma dropdown">
				<button class="fa fa-smile-o"></button>
				<textarea class="chat_text_box"  placeholder="Type your message">
					
				</textarea>
				<button class="_mic fa fa-microphone"></button>
				<button class="_mic fa fa-ellipsis-h " id="menu_icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
				<div class="dropdown-menu menu_icons" aria-labelledby="menu_icons">
				    <button><i class="fa fa-camera"></i> <h6>Take Photo</h6></button>
			  	</div>
			</div>
		</div>

	</div>
	<?php endfor; ?>
</div>