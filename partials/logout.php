<?php
	require '../controllers/db_controller/theDBConn.php';
	$db = new DB;
	function rand_a ( $length = 50 ) {
	    $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	    $shuffled = substr( str_shuffle($str), 0, $length);
	    return $shuffled;
	}

	$shuffled_logout = rand_a(57);
	$db->destroySession();

	
	echo " Logging out ... Please wait ...";
	header('location:../login.php');

?>