<div class="_sidebar_section">
	
	<div class="_show_sidebar" onclick="openNav()">
		<i class="fa fa-angle-right"></i>
	</div>

	<div id="side_bar" class="_sidebar_container" >
		<div class="_sidebar_header">
			<div class="logo">
				<img src="./assets/image/logo.png">
			</div>
			<h6>SYSTEM MANAGEMENT</h6>
			<div class="_close_sidebar" onclick="closeNav()">
				<i class="fa fa-angle-left"></i>
			</div>
		</div>
		<div class="_sidebar_body">

		<?php  
		
			if(isset($_GET["page"])) {
			
				if( $_GET["page"] == 'myaccount') {
					 include 'sidebar/myaccountside.php';
				}elseif ($_GET["page"] == 'technical') {
					include 'sidebar/technicalside.php';
				}elseif ($_GET["page"] == 'cashier') {
					include 'sidebar/cashierside.php';
				}elseif ($_GET["page"] == 'management') {
					include 'sidebar/managementside.php';
				}elseif ($_GET["page"] == 'adviser') {
					include 'sidebar/adviserside.php';
				}elseif ($_GET["page"] == 'report-menubar') {
					include 'sidebar/reportside.php';
				}elseif ($_GET["page"] == 'registrar') {
					include 'sidebar/registrarside.php';
				}elseif ($_GET["page"] == 'tcenter-menubar') {
					include 'sidebar/testcenterside.php';
				}elseif ($_GET["page"] == 'guidance') {
					include 'sidebar/guidanceside.php';
				}elseif ($_GET["page"] == 'instructor') {
					include 'sidebar/instructorside.php';
				}else {
					echo("<script>location.href = 'index.php';</script>");
				}


			} else {

		
			?>

			<div class="_menu_box_container _menu_my_acc " >
			<!-- <a href="?/" class="show_report_1"> -->
				<a href="?page=myaccount">
				<img src="./assets/image/accountweb.png">
					<div class="_box_body_content">
						<div class="_box_title">
							<h6>MY ACCOUNT</h6>
						</div>
						<div class="_box_content">
							<!-- <a href="#" class="show_report_1 text_W"> -->
							<p>My Profile</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Change Password</p>
							<!-- </a> -->
						</div>
					</div>
				</a>
			</div>



			<div class="_menu_box_container _menu_my_acc ">
				<a href="?page=tasklist">
				<img src="./assets/image/tasklist.png">
					<div class="_box_body_content">
						<div class="_box_title">
							<h6>MY TASK LIST</h6>
						</div>
						<div class="_box_content">
							<!-- <a href="#" class="show_report_1 text_W"> -->
							<p>Subject & Rooms</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Room & Instructors</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_3 text_W"> -->
							<p>Room & Instructors</p>
							<!-- </a> -->
						</div>
					</div>
				</a>
			</div>

			<div class="_menu_box_container _menu_my_acc ">
				<a href="?page=adviser">
				<img src="./assets/image/student-type.png">
					<div class="_box_body_content">
						<div class="_box_title">
							<h6>ADVISER</h6>
						</div>
						<div class="_box_content">
							<!-- <a href="#" class="show_report_1 text_W"> -->
							<p>Advise Sem1 Student</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_2 text_W"> -->
							<p>Advise Student</p>
							<!-- </a> -->
							<!-- <a href="#" class="show_report_3 text_W"> -->
							<p>Print COR</p>
							<!-- </a> -->
						</div>
					</div>
				</a>
			</div>

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=report-menubar">
				<img src="./assets/image/report.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>REPORTS</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>Enrollment Sysytem Report</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Classroom Schedule</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=registrar">
				<img src="./assets/image/registrar.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>REGISTRAR</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>School Year</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Scholarship</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_3 text_W"> -->
						<p>Student Type</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=tcenter-menubar">
				<img src="./assets/image/testcenter.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>TEST CENTER</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>Result Entry</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Result Summary</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>

			

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=guidance">
				<img src="./assets/image/guidance.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>GUIDANCE</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>Guidance Info</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=management">
				<img src="./assets/image/management.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>MANAGEMENT</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>Semester</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Subjects</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_3 text_W"> -->
						<p>Rooms</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>	

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=instructor">
				<img src="./assets/image/instructor.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>INSTRUCTOR</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>Class List</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Attendance</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_3 text_W"> -->
						<p>Grade Management</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>

			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=cashier">
				<img src="./assets/image/cashier.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>CASHIER</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>Registration</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Student Account</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_3 text_W"> -->
						<p>Create Billing</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>


			<div class="_menu_box_container _menu_my_acc">
				<a href="?page=technical">
				<img src="./assets/image/technical.png">
				<div class="_box_body_content">
					<div class="_box_title">
						<h6>TECHNICAL</h6>
					</div>
					<div class="_box_content">
						<!-- <a href="#" class="show_report_1 text_W"> -->
						<p>User's</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_2 text_W"> -->
						<p>Branch Management</p>
						<!-- </a> -->
						<!-- <a href="#" class="show_report_3 text_W"> -->
						<p>Group Management</p>
						<!-- </a> -->
					</div>
				</div>
				</a>
			</div>
			<?php 
			}
			?>

			<!-- DO NOT ERASE OR REPLACE THIS. BY SIR RANDY -->
				<div class="_menu_box_container _menu_my_acc">
					&nbsp;
				</div>
			<!-- DO NOT ERASE OR REPLACE ABOVE UNTIL HERE. BY SIR RANDY -->

		</div>
	</div>

</div>

<div class="background">
	
</div>