<?php

if (session_status() == PHP_SESSION_NONE) {
	session_start();
  }
 	
	if (isset($_SESSION["theUID"])) {		
		header('location:index.php');
	}
	
 ?>
<!DOCTYPE html>
<html>

<head>
    <title>ICI Enrollment System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	   <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <script src="assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="assets/js/popper.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
   
   <style type="text/css">
      @import "bourbon";
      body{
        background-repeat: no-repeat;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100vh;  
        /*animation: animate 16s ease-in-out infinite;*/
        background-size: cover; 
        /*background-image: url('assets/image/background.jpg');*/
      }
      .wrapper {  
        margin-top: 9%;
        float: right;
        margin-right: 3%;
      }
      /*@keyframes animate{
        0%,100% {
            background-image: url('assets/image/BG.png');
          }
          25% {
            background-image: url('assets/image/sanaallba.png');
          }
          50% {
            background-image: url('assets/image/login.jpg');
          }
          75% {
            background-image: url('assets/image/login.jpg');
        }
      }*/
      .form-signin {
        max-width: 380px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: transparent;
        border: 0px solid rgba(0,0,0,0.1);  
      }
      .form-signin-heading {
        margin-bottom: 20px;
        color: white;
        font-family: Century Gothic;
      }
      .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 7px;
        width: 100%;
        font-family: Century Gothic;  

      }
      input[type="text"] {
        margin-bottom: 10px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
          text-align: center;
        width: 262px;
        height: 35px;
        color: white;
        border-width: 1px;
        border-color: white;
        background-color: transparent;
        border:1px solid #f0eeee;
        font-family: Century Gothic;
        opacity: 0.8;
      }
      input::placeholder { color: white; }
      input[type="password"] {
        margin-bottom: 17px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        text-align: center;
        width: 262px;
        height: 35px;
        background-color: transparent;
        border:1px solid #f0eeee;
        color: white;
        font-family: Century Gothic;
        opacity: 0.8;
        }
      /*input[type="submit"] {
        margin-bottom: -1px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        width: 262px;
        height: 38px;
        color: white;
        background-color: #699de5 ;
        border:none;
        opacity: 0.8;
        font-family: Century Gothic;
      }*/
      #alert {
        margin-top: 0px;
        margin-right: 10px; 
        width: 30%;
      }
      /*@media only screen and (min-width: 1365px) {
        .wrapper {  
          margin-top: 9%;
          margin-bottom: 80px;
          margin-left:  75%;
        }
      }*/
    </style>
  </head>
  <body>
    <header>
       <div class="_header-container">
          <div class="logo_container">
            <img src="./assets/image/logo.png">
            <!-- <img src="assets/image/linker-logo1.png"> -->
          </div>
          <div class="_header_menu">
            <div class="_h_title">
              <h6>SCHOOL MANAGEMENT SYSTEM</h6>

            </div>
           
          </div>
        </div>
    </header>

    <div class="wrapper">
    	<div class="form">
	      <form class="form-signin alert" method="POST" action="" autocomplete="off">       
	        <h2 class="form-signin-heading">LOGIN</h2>
	        <div class="topbar">
		        <input type="text" class="form-control" id="username" name="username" placeholder="Username" autofocus required />
		        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required />
		     </div>   
		     <button class="form-control" name="login" id="submit" >Submit</button> 
	      </form>
      </div>
    </div>
    <footer>
      <div class="_footer_container">
  
        <h6>Copyright @ 2020</h6>
        <div class="_footer_img_cont">
          <img src="assets/image/linkers-logo2.png">
        </div>
        <h6>All Rights Reserved.</h6>

      </div>
    </footer>
   


  </body>

</html>


<script type="text/javascript">
	
var form = $('.form');
var btn = $('#submit');
var topbar = $('.topbar');
var input = $('#password');
var username = $('#username');
var h = input.height();
var tre = "";
$('.spanColor').height(h+23);

input.on('focus',function(){
  topbar.removeClass('error success');
  input.text('');
   setTimeout(function(){
	      btn.text('Login');
	    },250);
});
username.on('focus',function(){
  topbar.removeClass('error success');
  input.text('');
   setTimeout(function(){
	      btn.text('Login');
	    },250);
});
btn.on('click',function(){
    var pass = $('#password').val();
    var username = $('#username').val();
    if (pass === '' && username === '') {
    	 topbar.addClass('error');
	      setTimeout(function(){
          btn.text('Please fill up all textbox!');
          },300);
    }else if (username === '') {
    	topbar.addClass('error');
	      setTimeout(function(){
          btn.text('Please enter the username!');
          },300);
    }else if (pass === ''){
    		topbar.addClass('error');
	      setTimeout(function(){
          btn.text('Please enter the pass!');
          },300);
    }
    else {
    	$.post("controllers/function/logincontroller.php",{ username: username, pass: pass},
        function(data) {
        if(data ==='suc'){
		    setTimeout(function(){
		      btn.text('Success!');
	    },250);
	    topbar.addClass('success');	 
	    window.location.assign("login.php?username="+username);
	  	
	  	} else if (data === 'inactive') {
	  	  topbar.addClass('error');
	      setTimeout(function(){
          btn.text('Username had been disabled!.. Please contact the Administrator');
          },300);
	  	} else if (data === 'error') {
	      topbar.addClass('error');
	      setTimeout(function(){
          btn.text('Invalid Username & Password!');
          },300);
	    } else {
	      topbar.addClass('error');
	      setTimeout(function(){
          btn.text('Connection Failed!...');
          },300);
	    }
     });
 
    }

    topbar.addClass('disabled');
  		
});

$('.form').keypress(function(e){
   if(e.keyCode==13)
   submit.click();
	
});
input.keypress(function(){
  topbar.removeClass('success error');
 
});

</script>

<?php
	if (isset($_POST["username"])) {
		$username = $_POST["username"];
		$_SESSION["theUID"] = $username;
		require 'controllers/function/logincontroller.php';
		$login_controller = new loginController;
		echo $login_controller->get_usernameTologin($username);
					 
	}

?>
